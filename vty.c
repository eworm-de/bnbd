/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include <pcre.h>

#include "io.h"
#include "thread.h"
#include "session.h"
#include "mirror.h"
#include "volume.h"
#include "vty.h"

static int vty_module_initialized = 0;
static pthread_mutex_t vty_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct vty_list *available_vtyl = NULL;
static struct vty_list *inuse_vtyl = NULL;

struct vty_list *active_vtyl = NULL;

static void vty_dealloc(struct vty *);
static void vty_thread_shutdown(struct vty *);
static void vty_list_dealloc(struct vty_list *);
static void *vty_thread(void *);
static void vty_thread_loop(struct vty *);
static int keyval_get(const char *, const char *, size_t, const char **);
static int str_isnumber(const char *);
static int vol_id_isvalid(const char *);
static int mir_target_isvalid(const char *);
static int cmd_dispatch(const char *, size_t, char **, size_t *, size_t *);
static int cmd_help(void *, const char *, int *, char **, size_t *, size_t *);
static int cmd_quit(void *, const char *, int *, char **, size_t *, size_t *);
static int cmd_volume_create(void *, const char *, int *, char **, size_t *,
    size_t *);
static int cmd_volume_list(void *, const char *, int *, char **, size_t *,
    size_t *);
static int cmd_volume_up(void *, const char *, int *, char **, size_t *,
    size_t *);
static int cmd_volume_down(void *, const char *, int *, char **, size_t *,
    size_t *);
static int cmd_volume_delete(void *, const char *, int *, char **, size_t *,
    size_t *);
static int cmd_mirror_create(void *, const char *, int *, char **, size_t *,
    size_t *);
static int cmd_mirror_list(void *, const char *, int *, char **, size_t *,
    size_t *);
static int cmd_mirror_up(void *, const char *, int *, char **, size_t *,
    size_t *);
static int cmd_mirror_down(void *, const char *, int *, char **, size_t *,
    size_t *);
static int cmd_mirror_sync(void *, const char *, int *, char **, size_t *,
    size_t *);
static int cmd_mirror_delete(void *, const char *, int *, char **, size_t *,
    size_t *);
static int cmd_test(void *, const char *, int *, char **, size_t *, size_t *);

static const char *keyval_pattern = "([[:alpha:]]+)=([[:graph:]]+)";
static pcre *keyval_re = NULL;
static int keyval_options = PCRE_CASELESS;

struct command {
	const char *name;
	const char *pattern;
	int options;
	pcre *re;
	int ovecsize;
	int (*handle)(void *, const char *, int *, char **, size_t *, size_t *);
};

static struct command cmd_ary[] = {
	{
		.name			= "HELP",
		.pattern		=
		    "^[[:blank:]]*"
		    "HELP"
		    "[[:blank:]]*$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 0,
		.handle			= cmd_help
	},
	{
		.name			= "QUIT",
		.pattern		=
		    "^[[:blank:]]*"
		    "QUIT"
		    "[[:blank:]]*$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 0,
		.handle			= cmd_quit
	},
	{
		.name			= "VOLUME_CREATE",
		.pattern		=
		    "^[[:blank:]]*"
		    "VOLUME"
		    "[[:blank:]]+"
		    "CREATE"
		    "([[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_volume_create
	},
	{
		.name			= "VOLUME_LIST",
		.pattern		=
		    "^[[:blank:]]*"
		    "VOLUME"
		    "[[:blank:]]+"
		    "LIST"
		    "([[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_volume_list
	},
	{
		.name			= "VOLUME_UP",
		.pattern		=
		    "^[[:blank:]]*"
		    "VOLUME"
		    "[[:blank:]]+"
		    "UP"
		    "([[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_volume_up
	},
	{
		.name			= "VOLUME_DOWN",
		.pattern		=
		    "^[[:blank:]]*"
		    "VOLUME"
		    "[[:blank:]]+"
		    "DOWN"
		    "([[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_volume_down
	},
	{
		.name			= "VOLUME_DELETE",
		.pattern		=
		    "^[[:blank:]]*"
		    "VOLUME"
		    "[[:blank:]]+"
		    "DELETE"
		    "([[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_volume_delete
	},
	{
		.name			= "MIRROR_CREATE",
		.pattern		=
		    "^[[:blank:]]*"
		    "MIRROR"
		    "[[:blank:]]+"
		    "CREATE"
		    "([[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_mirror_create
	},
	{
		.name			= "MIRROR_LIST",
		.pattern		=
		    "^[[:blank:]]*"
		    "MIRROR"
		    "[[:blank:]]+"
		    "LIST"
		    "([[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_mirror_list
	},
	{
		.name			= "MIRROR_UP",
		.pattern		=
		    "^[[:blank:]]*"
		    "MIRROR"
		    "[[:blank:]]+"
		    "UP"
		    "([[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_mirror_up
	},
	{
		.name			= "MIRROR_DOWN",
		.pattern		=
		    "^[[:blank:]]*"
		    "MIRROR"
		    "[[:blank:]]+"
		    "DOWN"
		    "([[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_mirror_down
	},
	{
		.name			= "MIRROR_SYNC",
		.pattern		=
		    "^[[:blank:]]*"
		    "MIRROR"
		    "[[:blank:]]+"
		    "SYNC"
		    "([[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_mirror_sync
	},
	{
		.name			= "MIRROR_DELETE",
		.pattern		=
		    "^[[:blank:]]*"
		    "MIRROR"
		    "[[:blank:]]+"
		    "DELETE"
		    "([[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_mirror_delete
	},
	{
		.name			= "TEST",
		.pattern		=
		    "^[[:blank:]]*"
		    "TEST"
		    "([[:blank:]]+"
		    "[[:print:]]*)$",
		.options		= PCRE_CASELESS,
		.re			= NULL,
		.ovecsize		= 2,
		.handle			= cmd_test
	}
};

int
vty_module_setup(void)
{
	struct command *cmd;
	const char *errptr;
	int erroffset, i;

	assert(!vty_module_initialized);
	assert(available_vtyl == NULL);
	if (vty_list_alloc(&available_vtyl, VTYL_IDX_ALLOC) < 0) {
		assert(available_vtyl == NULL);
		goto out0;
	}
	assert(available_vtyl != NULL);
	assert(inuse_vtyl == NULL);
	if (vty_list_alloc(&inuse_vtyl, VTYL_IDX_ALLOC) < 0) {
		assert(inuse_vtyl == NULL);
		goto out1;
	}
	assert(inuse_vtyl != NULL);
	assert(active_vtyl == NULL);
	if (vty_list_alloc(&active_vtyl, VTYL_IDX_STD) < 0) {
		assert(active_vtyl == NULL);
		goto out2;
	}
	assert(active_vtyl != NULL);
	keyval_re = pcre_compile(keyval_pattern, keyval_options, &errptr,
	    &erroffset, NULL);
	if (keyval_re == NULL) {
		ERR("pcre_compile(%s): %s", keyval_pattern, errptr);
		goto out3;
	}
	for (i = 0; i < NELEM(cmd_ary); i++) {
		cmd = &cmd_ary[i];
		assert(cmd->re == NULL);
		cmd->re = pcre_compile(cmd->pattern, cmd->options, &errptr,
		    &erroffset, NULL);
		if (cmd->re == NULL) {
			ERR("pcre_compile(%s): %s", cmd->pattern, errptr);
			goto out4;
		}
	}
	vty_module_initialized = 1;
	return (0);
out4:
	for (i = 0; i < NELEM(cmd_ary); i++) {
		cmd = &cmd_ary[i];
		if (cmd->re == NULL)
			break;
		pcre_free(cmd->re);
		cmd->re = NULL;
	}
	assert(keyval_re != NULL);
	pcre_free(keyval_re);
	keyval_re = NULL;
out3:
	assert(active_vtyl != NULL);
	vty_list_free(active_vtyl);
	active_vtyl = NULL;
out2:
	assert(inuse_vtyl != NULL);
	vty_list_free(inuse_vtyl);
	inuse_vtyl = NULL;
out1:
	assert(available_vtyl != NULL);
	vty_list_dealloc(available_vtyl);
	vty_list_free(available_vtyl);
	available_vtyl = NULL;
out0:
	return (-1);
}

void
vty_module_hello(void)
{

	assert(vty_module_initialized);
	DEBUG("sizeof(vty)=%zu", sizeof(struct vty));
	DEBUG("sizeof(vty_list)=%zu", sizeof(struct vty_list));
}

void
vty_module_teardown(void)
{
	struct command *cmd;
	int i;

	assert(vty_module_initialized);
	for (i = 0; i < NELEM(cmd_ary); i++) {
		cmd = &cmd_ary[i];
		assert(cmd->re != NULL);
		pcre_free(cmd->re);
		cmd->re = NULL;
	}
	assert(keyval_re != NULL);
	pcre_free(keyval_re);
	keyval_re = NULL;
	assert(active_vtyl != NULL);
	vty_list_free(active_vtyl);
	active_vtyl = NULL;
	assert(inuse_vtyl != NULL);
	vty_list_free(inuse_vtyl);
	inuse_vtyl = NULL;
	assert(available_vtyl != NULL);
	vty_list_dealloc(available_vtyl);
	vty_list_free(available_vtyl);
	available_vtyl = NULL;
	vty_module_initialized = 0;
}

void
vty_module_stats(void)
{
	int available_count, available_peak, inuse_count, inuse_peak;

	assert(vty_module_initialized);
	PTHREAD_MUTEX_LOCK(&vty_mutex);
	available_count = available_vtyl->count;
	available_peak = available_vtyl->peak;
	available_vtyl->peak = available_vtyl->count;
	inuse_count = inuse_vtyl->count;
	inuse_peak = inuse_vtyl->peak;
	inuse_vtyl->peak = inuse_vtyl->count;
	PTHREAD_MUTEX_UNLOCK(&vty_mutex);
	NOTICE("inuse=%d/%d available=%d/%d limit=%d",
	    inuse_count, inuse_peak, available_count, available_peak,
	    cfg_max_vty_clients);
}

int
vty_alloc(struct vty **vtypp)
{
	struct vty *vtyp;

	assert(vty_module_initialized);
	PTHREAD_MUTEX_LOCK(&vty_mutex);
	vtyp = vty_list_first(available_vtyl);
	if (vtyp == NULL) {
		assert(available_vtyl->count == 0);
		if (cfg_max_vty_clients > 0 &&
		    inuse_vtyl->count >= cfg_max_vty_clients) {
			PTHREAD_MUTEX_UNLOCK(&vty_mutex);
			NOTICE("max_vty_clients=%d limit hit!",
			    cfg_max_vty_clients);
			return (-1);
		}
		PTHREAD_MUTEX_UNLOCK(&vty_mutex);
		vtyp = malloc(sizeof(*vtyp));
		if (vtyp == NULL) {
			ERR("malloc(%zu): %s", sizeof(*vtyp), strerror(errno));
			return (-2);
		}
		vtyp->thr = NULL;
		if (thread_alloc(&vtyp->thr, vtyp) < 0) {
			assert(vtyp->thr == NULL);
			free(vtyp);
			return (-3);
		}
		assert(vtyp->thr != NULL);
		vty_init(vtyp);
		PTHREAD_MUTEX_LOCK(&vty_mutex);
	} else {
		vty_list_remove(available_vtyl, vtyp);
	}
	assert(!vtyp->flags);
	vtyp->flags |= VTY_FL_INUSE;
	vty_list_insert(inuse_vtyl, vtyp);
	PTHREAD_MUTEX_UNLOCK(&vty_mutex);
	assert(vtyp->so < 0);
	*vtypp = vtyp;
	return (0);
}

void
vty_init(struct vty *vtyp)
{

	vtyp->flags = 0;
	vtyp->so = -1;
	memset(vtyp->inet_addr, 0x00, sizeof(vtyp->inet_addr));
	vtyp->inet_port = -1;
}

void
vty_uninit(struct vty *vtyp)
{

	thread_uninit(vtyp->thr);
	assert(vtyp->flags == VTY_FL_INUSE);
	vtyp->so = -1;
	memset(vtyp->inet_addr, 0x00, sizeof(vtyp->inet_addr));
	vtyp->inet_port = -1;
}

void
vty_free(struct vty *vtyp)
{

	assert(vty_module_initialized);
	vty_uninit(vtyp);
	PTHREAD_MUTEX_LOCK(&vty_mutex);
	vty_list_remove(inuse_vtyl, vtyp);
	vtyp->flags &= ~VTY_FL_INUSE;
	assert(!vtyp->flags);
	vty_list_insert(available_vtyl, vtyp);
	PTHREAD_MUTEX_UNLOCK(&vty_mutex);
}

static
void
vty_dealloc(struct vty *vtyp)
{

	assert(!vtyp->flags);
	assert(vtyp->so < 0);
	thread_free(vtyp->thr);
	free(vtyp);
}

int
vty_thread_create(struct vty *vtyp)
{

	return (thread_create(vtyp->thr, "vty", vty_thread));
}

void
vty_thread_launch(struct vty *vtyp)
{

	thread_launch(vtyp->thr);
}

static
void
vty_thread_shutdown(struct vty *vtyp)
{

	thread_lock(vtyp->thr);
	thread_shutdown_begin(vtyp->thr);
	thread_unlock(vtyp->thr);
	thread_kill(vtyp->thr, SIGUSR1);
}

static
void *
vty_thread(void *data)
{
	struct vty *vtyp;
	int detach_self;

	vtyp = (struct vty *)data;
	assert(vtyp != NULL);
	assert(vtyp->thr != NULL);

	thread_prologue(vtyp->thr);

	vty_thread_loop(vtyp);

	vty_list_lock(active_vtyl);
	vty_list_remove(active_vtyl, vtyp);
	detach_self = thread_shutdown_commit(vtyp->thr);
	vty_list_unlock(active_vtyl);
	if (detach_self) {
		thread_detach(vtyp->thr);
		vty_free(vtyp);
	}

	thread_epilogue(NULL);
	return (NULL);
}

static const char *msg_cmd_unknown = "-ERR Command unknown\n";
static const char *msg_cmd_failed = "-ERR Command failed\n";

static
void
vty_thread_loop(struct vty *vtyp)
{
	char buf[1024], *eol, *inbuf, *outbuf;
	size_t buf_idx, buf_len, inbuf_len, outbuf_idx, outbuf_len;
	size_t eaten_idx, len;
	ssize_t n;
	int error, so;

	so = vtyp->so;

	outbuf_len = 4096;
	outbuf = malloc(outbuf_len);
	if (outbuf == NULL) {
		ERR("malloc(%zu): %s", outbuf_len, strerror(errno));
		goto out;
	}

	if (fd_blocking(so, 1) < 0)
		goto out;

	if (so_timeo(so, 0, cfg_vty_timeout) < 0)
		goto out;

	if (so_timeo(so, 1, cfg_vty_timeout) < 0)
		goto out;

	if (so_linger(so, 1, cfg_vty_timeout) < 0)
		goto out;

	n = buf_tx(so, "+OK\n", (size_t)4, 0);
	if (n < (ssize_t)0)
		goto out;
	assert(n == (ssize_t)4);

	buf_idx = (size_t)0;
	buf_len = sizeof(buf);
	for (;;) {
		thread_lock(vtyp->thr);
		if (THREAD_SHUTDOWN_PENDING(vtyp->thr)) {
			thread_unlock(vtyp->thr);
			DEBUG("Shutting down...");
			break;
		}
		thread_unlock(vtyp->thr);
		assert(buf_idx < buf_len);
		if (buf_idx == buf_len - 1)
			break;
		n = read(so, buf + buf_idx, buf_len - buf_idx - 1);
		if (n < (ssize_t)0) {
			if (errno == EINTR) {
				WARNING("read(): %s", strerror(errno));
				continue;
			}
			if (errno == EAGAIN || errno == EWOULDBLOCK) {
				INFO("VTY client %s:%d timed out",
				    vtyp->inet_addr, vtyp->inet_port);
				break;	/* TIMEOUT */
			}
			ERR("read(): %s", strerror(errno));
			break;
		}
		if (n == (ssize_t)0) {
			/* EOF */
			INFO("EOF from %s:%d",
			    vtyp->inet_addr, vtyp->inet_port);
			break;
		}
		assert(n > (ssize_t)0);
		buf_idx += n;
		assert(buf_idx < buf_len);
		buf[buf_idx] = '\0';

		eaten_idx = (size_t)0;
		for (;;) {
			inbuf = buf + eaten_idx;
			eol = strchr(inbuf, '\n');
			if (eol == NULL)
				break;
			inbuf_len = eol - inbuf;
			eol[0] = '\0';
			eaten_idx += inbuf_len + 1;
			if (eol > inbuf && eol[-1] == '\r') {
				inbuf_len--;
				eol[-1] = '\0';
			}
			outbuf_idx = (size_t)0;
			error = cmd_dispatch(inbuf, inbuf_len, &outbuf,
			    &outbuf_idx, &outbuf_len);
			assert(outbuf != NULL);
			assert(outbuf_idx <= outbuf_len);
			assert(outbuf_len > (size_t)0);
			if (error < -1) {
				/* command unknown */
				assert(outbuf_idx == (size_t)0);
				len = strlen(msg_cmd_unknown);
				n = buf_tx(so, msg_cmd_unknown, len, 0);
				if (n < (ssize_t)0)
					goto out;
				assert(n == (ssize_t)len);
				continue;
			}
			if (error == -1) {
				/* command failed */
				assert(outbuf_idx == (size_t)0);
				len = strlen(msg_cmd_failed);
				(void)buf_tx(so, msg_cmd_failed, len, 0);
				goto out;
			}
			if (error == 0) {
				assert(outbuf_idx > (size_t)0);
				(void)buf_tx(so, outbuf, outbuf_idx, 0);
				goto out;
			}
			n = buf_tx(so, outbuf, outbuf_idx, 0);
			if (n < (ssize_t)0)
				goto out;
			assert(n == (ssize_t)outbuf_idx);
		}

		/* REWIND BUF */
		if (eaten_idx > (size_t)0) {
			len = buf_idx - eaten_idx;
			if (len > (size_t)0)
				memmove(buf, buf + eaten_idx, len + 1);
			buf_idx -= eaten_idx;
		}
	}
out:
	if (outbuf != NULL)
		free(outbuf);
	fd_close(so);
	NOTICE("Closed VTY connection from %s:%d",
	    vtyp->inet_addr, vtyp->inet_port);
}

static
int
keyval_get(const char *key, const char *inbuf, size_t inbuf_len,
    const char **val)
{
	int ovector[3 * 3];
	const char *ptr;
	size_t inbuf_idx;
	int error;

	inbuf_idx = (size_t)0;
	for (;;) {
		error = pcre_exec(keyval_re, NULL, inbuf + inbuf_idx,
		    (int)(inbuf_len - inbuf_idx), 0, 0, ovector,
		    NELEM(ovector));
		if (error < 0)
			return (-1);
		assert(error == NELEM(ovector) / 3);
		ptr = NULL;
		error = pcre_get_substring(inbuf + inbuf_idx, ovector,
		    NELEM(ovector) / 3, 1, &ptr);
		if (error < 0) {
			assert(ptr == NULL);
			return (-2);
		}
		assert(ptr != NULL);
		if (strcmp(key, ptr)) {
			pcre_free_substring(ptr);
			inbuf_idx += (ovector[1] - ovector[0]);
			continue;
		}
		pcre_free_substring(ptr);
		ptr = NULL;
		error = pcre_get_substring(inbuf + inbuf_idx, ovector,
		    NELEM(ovector) / 3, 2, &ptr);
		if (error < 0) {
			assert(ptr == NULL);
			return (-3);
		}
		assert(ptr != NULL);
		break;
	}
	*val = ptr;
	return (0);
}

static
int
str_isnumber(const char *ptr)
{

	assert(*ptr != '\0');
	while (*ptr != '\0') {
		if (isdigit((int)*ptr)) {
			ptr++;
			continue;
		}
		return (0);
	}
	return (1);
}

static
int
vol_id_isvalid(const char *ptr)
{

	assert(*ptr != '\0');
	while (*ptr != '\0') {
		if (isalnum((int)*ptr) || *ptr == '_' || *ptr == '-') {
			ptr++;
			continue;
		}
		return (0);
	}
	return (1);
}

static
int
mir_target_isvalid(const char *ptr)
{

	assert(*ptr != '\0');
	while (*ptr != '\0') {
		if (isalnum((int)*ptr) || *ptr == '_' || *ptr == '-' ||
		    *ptr == '/' || *ptr == '.') {
			ptr++;
			continue;
		}
		return (0);
	}
	return (1);
}

static
int
cmd_dispatch(const char *inbuf, size_t inbuf_len, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct command *cmd;
	size_t ovector_len;
	int error, i, *ovector;

	for (i = 0; i < NELEM(cmd_ary); i++) {
		cmd = &cmd_ary[i];
		assert(cmd->ovecsize >= 0);
		if (cmd->ovecsize > 0) {
			ovector_len = sizeof(int) * 3 * cmd->ovecsize;
			ovector = malloc(ovector_len);
			if (ovector == NULL) {
				ERR("malloc(%zu): %s",
				    ovector_len, strerror(errno));
				break;
			}
		} else {
			ovector = NULL;
		}
		error = pcre_exec(cmd->re, NULL, inbuf, (int)inbuf_len, 0, 0,
		    ovector, cmd->ovecsize * 3);
		if (error < 0) {
			if (ovector != NULL)
				free(ovector);
			continue;
		}
		assert(error == cmd->ovecsize);
		error = cmd->handle(cmd, inbuf, ovector, outbuf, outbuf_idx,
		    outbuf_len);
		assert(error > -2);
		if (ovector != NULL)
			free(ovector);
		return (error);
	}
	return (-2);
}

static const char *msg_help =
    "+OK Help follows\n"
    "help                                                        "
    ":this screen\n"
    "quit                                                        "
    ":quit vty\n"
    "volume create  id=<ID>  size=<MB>  [soft=1]                 "
    ":create volume\n"
    "volume list   [id=<ID>]                                     "
    ":list volume(s)\n"
    "volume up      id=<ID>                                      "
    ":activate volume\n"
    "volume down    id=<ID>                                      "
    ":shut down volume\n"
    "volume delete  id=<ID>                                      "
    ":delete volume\n"
    "mirror create  target=<PATH>  volume=<ID>                   "
    ":create mirror\n"
    "mirror list   [target=<PATH>]                               "
    ":list mirror(s)\n"
    "mirror up      target=<PATH>                                "
    ":activate mirror\n"
    "mirror down    target=<PATH>                                "
    ":shut down mirror\n"
    "mirror sync    target=<PATH>  [rs=<KB>]  [ql=<N>]  [full=1] "
    ":synchronise mirror\n"
    "mirror delete  target=<PATH>                                "
    ":delete mirror\n";

static
int
cmd_help(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	int len;

	(void)arg;
	(void)inbuf;
	(void)ovector;

	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	len = snprintf(*outbuf, *outbuf_len, "%s", msg_help);
	assert(len > 0 && (size_t)len < *outbuf_len);
	*outbuf_idx = (size_t)len;
	return (1);
}

static
int
cmd_quit(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	int len;

	(void)arg;
	(void)inbuf;
	(void)ovector;

	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	len = snprintf(*outbuf, *outbuf_len, "+OK Bye\n");
	assert(len > 0 && (size_t)len < *outbuf_len);
	*outbuf_idx = (size_t)len;
	return (0);
}

static
int
cmd_volume_create(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct command *cmd;
	struct volume *volp;
	const char *args[4], *id, *opts, *ptr;
	char *buf;
	uint64_t vol_size;
	size_t buf_len, opts_len;
	int args_idx, error, i, len, retval, soft;

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	id = NULL;
	if (keyval_get("id", opts, opts_len, &id) < 0) {
		assert(id == NULL);
		len = snprintf(buf, buf_len, "-ERR Missing volume ID\n");
		goto done;
	}
	assert(id != NULL);
	assert(id[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = id;

	if (!vol_id_isvalid(id)) {
		len = snprintf(buf, buf_len, "-ERR Invalid volume ID\n");
		goto done;
	}

	if (strlen(id) >= sizeof(volp->id)) {
		len = snprintf(buf, buf_len, "-ERR Volume ID too long\n");
		goto done;
	}

	ptr = NULL;
	if (keyval_get("size", opts, opts_len, &ptr) < 0) {
		assert(ptr == NULL);
		len = snprintf(buf, buf_len, "-ERR Missing volume SIZE\n");
		goto done;
	}
	assert(ptr != NULL);
	assert(ptr[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = ptr;

	if (!str_isnumber(ptr)) {
		len = snprintf(buf, buf_len, "-ERR Invalid volume SIZE\n");
		goto done;
	}
	vol_size = (uint64_t)atoll(ptr);

	if (vol_size < (uint64_t)64) {
		len = snprintf(buf, buf_len,
		    "-ERR Volume SIZE less than 64MB\n");
		goto done;
	}

	if (vol_size > (uint64_t)(4 * 1024 * 1024)) {
		len = snprintf(buf, buf_len,
		    "-ERR Volume SIZE over 4TB\n");
		goto done;
	}

	soft = 0;
	ptr = NULL;
	if (keyval_get("soft", opts, opts_len, &ptr) >= 0) {
		assert(ptr != NULL);
		args[args_idx++] = ptr;
		if (!str_isnumber(ptr)) {
			len = snprintf(buf, buf_len,
			    "-ERR Invalid SOFT format\n");
			goto done;
		}
		soft = atoi(ptr);
		if (soft < 0 || soft > 1) {
			len = snprintf(buf, buf_len,
			    "-ERR Invalid SOFT value\n");
			goto done;
		}
	}

	volume_list_lock(master_voll);
	volp = volume_list_lookup_by_id(master_voll, id);
	volume_list_unlock(master_voll);
	if (volp != NULL) {
		len = snprintf(buf, buf_len, "-ERR Already exists\n");
		goto done;
	}

	error = volume_create(id, vol_size << 20, soft);
	if (error < 0) {
		len = snprintf(buf, buf_len, "-ERR Failure #1\n");
		goto done;
	}

	error = volume_list_reload(master_voll);
	if (error < 0) {
		len = snprintf(buf, buf_len, "-ERR Failure #2\n");
		goto done;
	}

	volume_list_lock(master_voll);
	volp = volume_list_lookup_by_id(master_voll, id);
	volume_list_unlock(master_voll);
	if (volp == NULL) {
		len = snprintf(buf, buf_len, "-ERR Failure #3\n");
		goto done;
	}

	len = snprintf(buf, buf_len, "+OK Success\n");
done:
	assert(len > 0 && (size_t)len < buf_len);
	*outbuf_idx = (size_t)len;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}

static
int
cmd_volume_list(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct stat st;
	struct command *cmd;
	struct volume *nvolp, *volp;
	const char *args[2], *fmt, *id, *opts;
	char *buf, *nbuf;
	size_t buf_idx, buf_len, nbuf_len, opts_len;
	int args_idx, i, len, retval;
#ifndef NDEBUG
	int len2;
#endif

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	fmt = "id=%s size=%llu usage=%llu block=%u soft=%d status=%s\n";
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	id = NULL;
	if (keyval_get("id", opts, opts_len, &id) < 0) {
		assert(id == NULL);
		goto list_all;
	}
	assert(id != NULL);
	assert(id[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = id;

	if (!vol_id_isvalid(id)) {
		len = snprintf(buf, buf_len, "-ERR Invalid volume ID\n");
		assert(len > 0 && (size_t)len < buf_len);
		buf_idx = (size_t)len;
		goto done;
	}

	volume_list_lock(master_voll);
	volp = volume_list_lookup_by_id(master_voll, id);
	if (volp == NULL) {
		volume_list_unlock(master_voll);
		len = snprintf(buf, buf_len, "-ERR No such volume\n");
		assert(len > 0 && (size_t)len < buf_len);
		buf_idx = (size_t)len;
		goto done;
	}
	memset(&st, 0x00, sizeof(st));
	if (fstat(volp->fd, &st) < 0) {
		ERR("fstat(%d): %s", volp->fd, strerror(errno));
		volume_list_unlock(master_voll);
		len = snprintf(buf, buf_len, "-ERR Failure\n");
		assert(len > 0 && (size_t)len < buf_len);
		buf_idx = (size_t)len;
		goto done;
	}
	len = snprintf(buf, buf_len, "+OK Success\n");
	assert(len > 0 && (size_t)len < buf_len);
	buf_idx = (size_t)len;
	len = snprintf(buf + buf_idx, buf_len - buf_idx,
	    fmt,
	    volp->id,
	    (unsigned long long)volp->size,
	    (unsigned long long)(st.st_blocks << 9),
	    (unsigned int)volp->blk_size,
	    volp->bm != NULL,
	    volume_strstate(volp));
	assert(len > 0);
	buf_idx += len;
	assert(buf_idx < buf_len);
	volume_list_unlock(master_voll);
	goto done;

list_all:
	len = snprintf(buf, buf_len, "+OK Volume list follows\n");
	assert(len > 0 && (size_t)len < buf_len);
	buf_idx = (size_t)len;

	volume_list_lock(master_voll);
	nvolp = volume_list_first(master_voll);
	for (;;) {
		volp = nvolp;
		if (volp == NULL)
			break;
		nvolp = volume_list_next(master_voll, volp);
		memset(&st, 0x00, sizeof(st));
		if (fstat(volp->fd, &st) < 0) {
			ERR("fstat(%d): %s", volp->fd, strerror(errno));
			break;
		}
		len = snprintf(NULL, (size_t)0, fmt,
		    volp->id,
		    (unsigned long long)volp->size,
		    (unsigned long long)(st.st_blocks << 9),
		    (unsigned int)volp->blk_size,
		    volp->bm != NULL,
		    volume_strstate(volp));
		assert(len > 0);
		if (buf_idx + len >= buf_len) {
			nbuf_len = buf_len + 4096;
			nbuf = malloc(nbuf_len);
			if (nbuf == NULL) {
				ERR("malloc(%zu): %s",
				    nbuf_len, strerror(errno));
				len = snprintf(buf, buf_len,
				    "-ERR Out of memory\n");
				assert(len > 0 && (size_t)len < buf_len);
				buf_idx = (size_t)len;
				break;
			}
			memcpy(nbuf, buf, buf_idx);
			free(buf);
			buf = nbuf;
			buf_len = nbuf_len;
			*outbuf = buf;
			*outbuf_len = buf_len;
		}
#ifndef NDEBUG
		len2 =
#endif
		    snprintf(buf + buf_idx, buf_len - buf_idx, fmt,
			volp->id,
			(unsigned long long)volp->size,
			(unsigned long long)(st.st_blocks << 9),
			(unsigned int)volp->blk_size,
			volp->bm != NULL,
			volume_strstate(volp));
		assert(len == len2);
		buf_idx += len;
		assert(buf_idx < buf_len);
	}
	volume_list_unlock(master_voll);

done:
	*outbuf_idx = buf_idx;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}

static
int
cmd_volume_up(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct command *cmd;
	struct volume *volp;
	const char *args[2], *id, *opts;
	char *buf;
	size_t buf_len, opts_len;
	int args_idx, error, i, len, retval;

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	id = NULL;
	if (keyval_get("id", opts, opts_len, &id) < 0) {
		assert(id == NULL);
		len = snprintf(buf, buf_len, "-ERR Missing volume ID\n");
		goto done;
	}
	assert(id != NULL);
	assert(id[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = id;

	if (!vol_id_isvalid(id)) {
		len = snprintf(buf, buf_len, "-ERR Invalid volume ID\n");
		goto done;
	}

	volume_list_lock(master_voll);
	volp = volume_list_lookup_by_id(master_voll, id);
	if (volp == NULL) {
		volume_list_unlock(master_voll);
		len = snprintf(buf, buf_len, "-ERR No such volume\n");
		goto done;
	}
	error = volume_up(volp);
	volume_list_unlock(master_voll);
	if (error < 0) {
		len = snprintf(buf, buf_len, "-ERR Failure\n");
		goto done;
	}
	if (error == 0) {
		len = snprintf(buf, buf_len, "+OK Already up\n");
		goto done;
	}

	len = snprintf(buf, buf_len, "+OK Success\n");
done:
	assert(len > 0 && (size_t)len < buf_len);
	*outbuf_idx = (size_t)len;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}

static
int
cmd_volume_down(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct command *cmd;
	struct volume *volp;
	const char *args[2], *id, *opts;
	char *buf;
	size_t buf_len, opts_len;
	int args_idx, error, i, len, retval;

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	id = NULL;
	if (keyval_get("id", opts, opts_len, &id) < 0) {
		assert(id == NULL);
		len = snprintf(buf, buf_len, "-ERR Missing volume ID\n");
		goto done;
	}
	assert(id != NULL);
	assert(id[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = id;

	if (!vol_id_isvalid(id)) {
		len = snprintf(buf, buf_len, "-ERR Invalid volume ID\n");
		goto done;
	}

	volume_list_lock(master_voll);
	volp = volume_list_lookup_by_id(master_voll, id);
	if (volp == NULL) {
		volume_list_unlock(master_voll);
		len = snprintf(buf, buf_len, "-ERR No such volume\n");
		goto done;
	}
	error = volume_down_begin(volp);
	volume_list_unlock(master_voll);
	if (error < 0) {
		len = snprintf(buf, buf_len, "-ERR Volume shutdown pending\n");
		goto done;
	}
	if (error == 0) {
		len = snprintf(buf, buf_len, "+OK Volume already shut down\n");
		goto done;
	}
	session_list_shutdown(volp->established_sesl);
	volume_list_lock(master_voll);
	volume_down_commit(volp);
	volume_list_unlock(master_voll);

	len = snprintf(buf, buf_len, "+OK Success\n");
done:
	assert(len > 0 && (size_t)len < buf_len);
	*outbuf_idx = (size_t)len;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}

static
int
cmd_volume_delete(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct command *cmd;
	struct volume *volp;
	const char *args[2], *id, *opts;
	char *buf;
	size_t buf_len, opts_len;
	int args_idx, i, len, retval;

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	id = NULL;
	if (keyval_get("id", opts, opts_len, &id) < 0) {
		assert(id == NULL);
		len = snprintf(buf, buf_len, "-ERR Missing volume ID\n");
		goto done;
	}
	assert(id != NULL);
	assert(id[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = id;

	if (!vol_id_isvalid(id)) {
		len = snprintf(buf, buf_len, "-ERR Invalid volume ID\n");
		goto done;
	}

	volume_list_lock(master_voll);
	volp = volume_list_lookup_by_id(master_voll, id);
	if (volp == NULL) {
		volume_list_unlock(master_voll);
		len = snprintf(buf, buf_len, "-ERR No such volume\n");
		goto done;
	}
	if (volp->state != VOL_ST_DOWN) {
		volume_list_unlock(master_voll);
		len = snprintf(buf, buf_len,
		    "-ERR Volume must be shut down first\n");
		goto done;
	}
	if (volume_delete_begin(volp) < 0) {
		volume_list_unlock(master_voll);
		len = snprintf(buf, buf_len, "-ERR Failure #1\n");
		goto done;
	}
	volume_list_remove(master_voll, volp);
	volume_list_unlock(master_voll);

	volume_shutdown(volp);
	if (volume_delete_commit(volp) < 0)
		len = snprintf(buf, buf_len, "-ERR Failure #2\n");
	else
		len = snprintf(buf, buf_len, "+OK Success\n");
	volume_free(volp);
done:
	assert(len > 0 && (size_t)len < buf_len);
	*outbuf_idx = (size_t)len;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}

static
int
cmd_mirror_create(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct command *cmd;
	struct volume *nvolp, *volp;
	struct mirror *mirp;
	const char *args[3], *id, *opts, *target;
	char *buf;
	size_t buf_len, opts_len;
	int args_idx, error, i, len, retval;

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	target = NULL;
	if (keyval_get("target", opts, opts_len, &target) < 0) {
		assert(target == NULL);
		len = snprintf(buf, buf_len, "-ERR Missing mirror TARGET\n");
		goto done;
	}
	assert(target != NULL);
	assert(target[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = target;

	if (!mir_target_isvalid(target)) {
		len = snprintf(buf, buf_len, "-ERR Invalid mirror TARGET\n");
		goto done;
	}

	if (target[0] != '/') {
		len = snprintf(buf, buf_len,
		    "-ERR Mirror TARGET must be an absolute path\n");
		goto done;
	}

	if (strlen(target) >= sizeof(mirp->target)) {
		len = snprintf(buf, buf_len, "-ERR Mirror TARGET too long\n");
		goto done;
	}

	id = NULL;
	if (keyval_get("volume", opts, opts_len, &id) < 0) {
		assert(id == NULL);
		len = snprintf(buf, buf_len, "-ERR Missing volume ID\n");
		goto done;
	}
	assert(id != NULL);
	assert(id[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = id;

	if (!vol_id_isvalid(id)) {
		len = snprintf(buf, buf_len, "-ERR Invalid volume ID\n");
		goto done;
	}

	mirp = NULL;
	volume_list_lock(master_voll);
	nvolp = volume_list_first(master_voll);
	for (;;) {
		volp = nvolp;
		if (volp == NULL)
			break;
		nvolp = volume_list_next(master_voll, volp);
		if (volp->state == VOL_ST_SHUTDOWN)
			continue;
		mirror_list_lock(volp->slave_mirl);
		mirp = mirror_list_lookup_by_target(volp->slave_mirl, target);
		mirror_list_unlock(volp->slave_mirl);
		if (mirp != NULL)
			break;
	}
	if (mirp != NULL) {
		volume_list_unlock(master_voll);
		len = snprintf(buf, buf_len, "-ERR Already exists\n");
		goto done;
	}
	assert(mirp == NULL);
	volp = volume_list_lookup_by_id(master_voll, id);
	if (volp == NULL) {
		volume_list_unlock(master_voll);
		len = snprintf(buf, buf_len, "-ERR No such volume\n");
		goto done;
	}
	if (volp->state == VOL_ST_SHUTDOWN) {
		volume_list_unlock(master_voll);
		len = snprintf(buf, buf_len, "-ERR Volume shutting down\n");
		goto done;
	}
	error = mirror_create(volp, target);
	volume_list_unlock(master_voll);
	if (error < 0) {
		len = snprintf(buf, buf_len, "-ERR Failure: %d\n", error);
		goto done;
	}

	len = snprintf(buf, buf_len, "+OK Success\n");
done:
	assert(len > 0 && (size_t)len < buf_len);
	*outbuf_idx = (size_t)len;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}

static
int
cmd_mirror_list(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct stat st;
	struct command *cmd;
	struct volume *nvolp, *volp;
	struct mirror *mirp, *nmirp;
	const char *args[2], *fmt, *opts, *status, *target;
	char *buf, *nbuf, sync_status[32];
	size_t buf_idx, buf_len, nbuf_len, opts_len;
	int args_idx, i, len, retval;
#ifndef NDEBUG
	int len2;
#endif

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	fmt =
	    "volume=%s target=%s size=%llu usage=%llu block=%u status=%s%s%s\n";
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	target = NULL;
	if (keyval_get("target", opts, opts_len, &target) < 0) {
		assert(target == NULL);
		goto list_all;
	}
	assert(target != NULL);
	assert(target[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = target;

	if (!mir_target_isvalid(target)) {
		len = snprintf(buf, buf_len, "-ERR Invalid mirror TARGET\n");
		assert(len > 0 && (size_t)len < buf_len);
		buf_idx = (size_t)len;
		goto done;
	}

	if (target[0] != '/') {
		len = snprintf(buf, buf_len,
		    "-ERR Mirror TARGET must be an absolute path\n");
		assert(len > 0 && (size_t)len < buf_len);
		buf_idx = (size_t)len;
		goto done;
	}

	mirp = NULL;
	volume_list_lock(master_voll);
	nvolp = volume_list_first(master_voll);
	for (;;) {
		volp = nvolp;
		if (volp == NULL)
			break;
		nvolp = volume_list_next(master_voll, volp);
		if (volp->state == VOL_ST_SHUTDOWN)
			continue;
		mirror_list_lock(volp->slave_mirl);
		mirp = mirror_list_lookup_by_target(volp->slave_mirl, target);
		if (mirp != NULL)
			break;
		mirror_list_unlock(volp->slave_mirl);
	}
	volume_list_unlock(master_voll);

	if (mirp == NULL) {
		len = snprintf(buf, buf_len, "-ERR No such mirror\n");
		assert(len > 0 && (size_t)len < buf_len);
		buf_idx = (size_t)len;
		goto done;
	}

	mirror_lock(mirp);
	memset(&st, 0x00, sizeof(st));
	if (fstat(mirp->fd, &st) < 0) {
		ERR("fstat(%d): %s", mirp->fd, strerror(errno));
		mirror_unlock(mirp);
		mirror_list_unlock(volp->slave_mirl);
		len = snprintf(buf, buf_len, "-ERR Failure\n");
		assert(len > 0 && (size_t)len < buf_len);
		buf_idx = (size_t)len;
		goto done;
	}
	len = snprintf(buf, buf_len, "+OK Success\n");
	assert(len > 0 && (size_t)len < buf_len);
	buf_idx = (size_t)len;
	if (mirp->flags & MIR_FL_COMPLETE)
		status = ",complete";
	else
		status = ",degraded";
	if (mirp->flags & MIR_FL_SYNCHRONISING) {
#ifndef NDEBUG
		len =
#endif
		    snprintf(sync_status, sizeof(sync_status),
			",%s,%d%%",
			"synchronising", mirp->sync_progress);
		assert(len > 0 && len < (int)sizeof(sync_status));
	} else {
		sync_status[0] = '\0';
	}
	len = snprintf(buf + buf_idx, buf_len - buf_idx, fmt,
	    volp->id,
	    mirp->target,
	    (unsigned long long)mirp->size,
	    (unsigned long long)(st.st_blocks << 9),
	    (unsigned int)mirp->blk_size,
	    mirror_strstate(mirp),
	    status,
	    sync_status);
	assert(len > 0);
	buf_idx += len;
	assert(buf_idx < buf_len);
	mirror_unlock(mirp);
	mirror_list_unlock(volp->slave_mirl);
	goto done;

list_all:
	len = snprintf(buf, buf_len, "+OK Mirror list follows\n");
	assert(len > 0 && (size_t)len < buf_len);
	buf_idx = (size_t)len;

	volume_list_lock(master_voll);
	nvolp = volume_list_first(master_voll);
	for (;;) {
		volp = nvolp;
		if (volp == NULL)
			break;
		nvolp = volume_list_next(master_voll, volp);
		if (volp->state == VOL_ST_SHUTDOWN)
			continue;
		mirror_list_lock(volp->slave_mirl);
		nmirp = mirror_list_first(volp->slave_mirl);
		for (;;) {
			mirp = nmirp;
			if (mirp == NULL)
				break;
			nmirp = mirror_list_next(volp->slave_mirl, mirp);
			mirror_lock(mirp);
			memset(&st, 0x00, sizeof(st));
			if (fstat(mirp->fd, &st) < 0) {
				ERR("fstat(%d): %s", mirp->fd, strerror(errno));
				mirror_unlock(mirp);
				break;
			}
			if (mirp->flags & MIR_FL_COMPLETE)
				status = ",complete";
			else
				status = ",degraded";
			if (mirp->flags & MIR_FL_SYNCHRONISING) {
#ifndef NDEBUG
				len =
#endif
				    snprintf(sync_status, sizeof(sync_status),
					",%s,%d%%",
					"synchronising", mirp->sync_progress);
				assert(len > 0 &&
				    len < (int)sizeof(sync_status));
			} else {
				sync_status[0] = '\0';
			}
			len = snprintf(NULL, (size_t)0, fmt,
			    volp->id,
			    mirp->target,
			    (unsigned long long)mirp->size,
			    (unsigned long long)(st.st_blocks << 9),
			    (unsigned int)mirp->blk_size,
			    mirror_strstate(mirp),
			    status,
			    sync_status);
			assert(len > 0);
			if (buf_idx + len >= buf_len) {
				nbuf_len = buf_len + 4096;
				nbuf = malloc(nbuf_len);
				if (nbuf == NULL) {
					ERR("malloc(%zu): %s",
					    nbuf_len, strerror(errno));
					mirror_unlock(mirp);
					len = snprintf(buf, buf_len,
					    "-ERR Out of memory\n");
					assert(len > 0 && (size_t)len < buf_len);
					buf_idx = (size_t)len;
					break;
				}
				memcpy(nbuf, buf, buf_idx);
				free(buf);
				buf = nbuf;
				buf_len = nbuf_len;
				*outbuf = buf;
				*outbuf_len = buf_len;
			}
#ifndef NDEBUG
			len2 =
#endif
			    snprintf(buf + buf_idx, buf_len - buf_idx, fmt,
				volp->id,
				mirp->target,
				(unsigned long long)mirp->size,
				(unsigned long long)(st.st_blocks << 9),
				(unsigned int)mirp->blk_size,
				mirror_strstate(mirp));
			assert(len == len2);
			buf_idx += len;
			assert(buf_idx < buf_len);
			mirror_unlock(mirp);
		}
		mirror_list_unlock(volp->slave_mirl);
	}
	volume_list_unlock(master_voll);

done:
	*outbuf_idx = buf_idx;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}

static
int
cmd_mirror_up(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct command *cmd;
	struct volume *nvolp, *volp;
	struct mirror *mirp;
	const char *args[2], *opts, *target;
	char *buf;
	size_t buf_len, opts_len;
	int args_idx, error, i, len, retval;

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	target = NULL;
	if (keyval_get("target", opts, opts_len, &target) < 0) {
		assert(target == NULL);
		len = snprintf(buf, buf_len, "-ERR Missing mirror TARGET\n");
		goto done;
	}
	assert(target != NULL);
	assert(target[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = target;

	if (!mir_target_isvalid(target)) {
		len = snprintf(buf, buf_len, "-ERR Invalid mirror TARGET\n");
		goto done;
	}

	if (target[0] != '/') {
		len = snprintf(buf, buf_len,
		    "-ERR Mirror TARGET must be an absolute path\n");
		goto done;
	}

	mirp = NULL;
	volume_list_lock(master_voll);
	nvolp = volume_list_first(master_voll);
	for (;;) {
		volp = nvolp;
		if (volp == NULL)
			break;
		nvolp = volume_list_next(master_voll, volp);
		if (volp->state == VOL_ST_SHUTDOWN)
			continue;
		mirror_list_lock(volp->slave_mirl);
		mirp = mirror_list_lookup_by_target(volp->slave_mirl, target);
		if (mirp != NULL)
			break;
		mirror_list_unlock(volp->slave_mirl);
	}
	volume_list_unlock(master_voll);

	if (mirp == NULL) {
		len = snprintf(buf, buf_len, "-ERR No such mirror\n");
		goto done;
	}

	mirror_lock(mirp);
	error = mirror_up(mirp);
	mirror_unlock(mirp);
	mirror_list_unlock(volp->slave_mirl);
	if (error < 0) {
		len = snprintf(buf, buf_len, "-ERR Failure\n");
		goto done;
	}
	if (error == 0) {
		len = snprintf(buf, buf_len, "+OK Already up\n");
		goto done;
	}

	len = snprintf(buf, buf_len, "+OK Success\n");
done:
	assert(len > 0 && (size_t)len < buf_len);
	*outbuf_idx = (size_t)len;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}

static
int
cmd_mirror_down(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct command *cmd;
	struct volume *nvolp, *volp;
	struct mirror *mirp;
	const char *args[2], *opts, *target;
	char *buf;
	size_t buf_len, opts_len;
	int args_idx, error, i, len, retval;

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	target = NULL;
	if (keyval_get("target", opts, opts_len, &target) < 0) {
		assert(target == NULL);
		len = snprintf(buf, buf_len, "-ERR Missing mirror TARGET\n");
		goto done;
	}
	assert(target != NULL);
	assert(target[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = target;

	if (!mir_target_isvalid(target)) {
		len = snprintf(buf, buf_len, "-ERR Invalid mirror TARGET\n");
		goto done;
	}

	if (target[0] != '/') {
		len = snprintf(buf, buf_len,
		    "-ERR Mirror TARGET must be an absolute path\n");
		goto done;
	}

	mirp = NULL;
	volume_list_lock(master_voll);
	nvolp = volume_list_first(master_voll);
	for (;;) {
		volp = nvolp;
		if (volp == NULL)
			break;
		nvolp = volume_list_next(master_voll, volp);
		if (volp->state == VOL_ST_SHUTDOWN)
			continue;
		mirror_list_lock(volp->slave_mirl);
		mirp = mirror_list_lookup_by_target(volp->slave_mirl, target);
		if (mirp != NULL)
			break;
		mirror_list_unlock(volp->slave_mirl);
	}
	volume_list_unlock(master_voll);

	if (mirp == NULL) {
		len = snprintf(buf, buf_len, "-ERR No such mirror\n");
		goto done;
	}

	mirror_lock(mirp);
	error = mirror_shutdown_begin(mirp);
	mirror_unlock(mirp);
	mirror_list_unlock(volp->slave_mirl);
	if (error == 0) {
		len = snprintf(buf, buf_len, "+OK Mirror was shut down\n");
		goto done;
	}
	assert(error < 0);
	len = snprintf(buf, buf_len, "+OK Mirror shutdown pending\n");
done:
	assert(len > 0 && (size_t)len < buf_len);
	*outbuf_idx = (size_t)len;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}

static
int
cmd_mirror_sync(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct command *cmd;
	struct volume *nvolp, *volp;
	struct mirror *mirp;
	const char *args[5], *opts, *ptr, *target;
	char *buf;
	size_t buf_len, opts_len;
	int args_idx, full, i, len, queue_length, request_size, retval;

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	target = NULL;
	if (keyval_get("target", opts, opts_len, &target) < 0) {
		assert(target == NULL);
		len = snprintf(buf, buf_len, "-ERR Missing mirror TARGET\n");
		goto done;
	}
	assert(target != NULL);
	assert(target[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = target;

	if (!mir_target_isvalid(target)) {
		len = snprintf(buf, buf_len, "-ERR Invalid mirror TARGET\n");
		goto done;
	}

	if (target[0] != '/') {
		len = snprintf(buf, buf_len,
		    "-ERR Mirror TARGET must be an absolute path\n");
		goto done;
	}

	request_size = -1;
	ptr = NULL;
	if (keyval_get("rs", opts, opts_len, &ptr) >= 0) {
		assert(ptr != NULL);
		args[args_idx++] = ptr;
		if (!str_isnumber(ptr)) {
			len = snprintf(buf, buf_len,
			    "-ERR Invalid REQUEST SIZE format\n");
			goto done;
		}
		request_size = atoi(ptr);
		if (request_size < 4 || request_size > 32768 ||
		    !powerof2(request_size)) {
			len = snprintf(buf, buf_len,
			    "-ERR Invalid REQUEST SIZE value\n");
			goto done;
		}
	}

	queue_length = -1;
	ptr = NULL;
	if (keyval_get("ql", opts, opts_len, &ptr) >= 0) {
		assert(ptr != NULL);
		args[args_idx++] = ptr;
		if (!str_isnumber(ptr)) {
			len = snprintf(buf, buf_len,
			    "-ERR Invalid QUEUE LENGTH format\n");
			goto done;
		}
		queue_length = atoi(ptr);
		if (queue_length < 1 || queue_length > 128) {
			len = snprintf(buf, buf_len,
			    "-ERR Invalid QUEUE LENGTH value\n");
			goto done;
		}
	}

	full = 0;
	ptr = NULL;
	if (keyval_get("full", opts, opts_len, &ptr) >= 0) {
		assert(ptr != NULL);
		args[args_idx++] = ptr;
		if (!str_isnumber(ptr)) {
			len = snprintf(buf, buf_len,
			    "-ERR Invalid FULL format\n");
			goto done;
		}
		full = atoi(ptr);
		if (full < 0 || full > 1) {
			len = snprintf(buf, buf_len,
			    "-ERR Invalid FULL value\n");
			goto done;
		}
	}

	mirp = NULL;
	volume_list_lock(master_voll);
	nvolp = volume_list_first(master_voll);
	for (;;) {
		volp = nvolp;
		if (volp == NULL)
			break;
		nvolp = volume_list_next(master_voll, volp);
		if (volp->state == VOL_ST_SHUTDOWN)
			continue;
		mirror_list_lock(volp->slave_mirl);
		mirp = mirror_list_lookup_by_target(volp->slave_mirl, target);
		if (mirp != NULL)
			break;
		mirror_list_unlock(volp->slave_mirl);
	}
	volume_list_unlock(master_voll);

	if (mirp == NULL) {
		len = snprintf(buf, buf_len, "-ERR No such mirror\n");
		goto done;
	}

	mirror_lock(mirp);
	if (mirp->state != MIR_ST_UP) {
		mirror_unlock(mirp);
		mirror_list_unlock(volp->slave_mirl);
		len = snprintf(buf, buf_len,
		    "-ERR Mirror must be activated first\n");
		goto done;
	}
	if (mirp->flags & MIR_FL_SYNCHRONISING) {
		mirror_unlock(mirp);
		mirror_list_unlock(volp->slave_mirl);
		len = snprintf(buf, buf_len,
		    "+OK Mirror synchronisation already running\n");
		goto done;
	}
	if (mirp->volp->bm != NULL)
		mirp->sync_full = full;
	else
		mirp->sync_full = 1;
	if (request_size < 0)
		request_size = cfg_mirror_request_kb;
	if (cfg_max_mirror_requests_kb > 0 &&
	    request_size > cfg_max_mirror_requests_kb) {
		mirror_unlock(mirp);
		mirror_list_unlock(volp->slave_mirl);
		len = snprintf(buf, buf_len,
		    "-ERR mirror_request_kb=%d > max_mirror_requests_kb=%d\n",
		    cfg_mirror_request_kb, cfg_max_mirror_requests_kb);
		goto done;
	}
	mirp->sync_request_size = (uint32_t)request_size << 10;
	if (queue_length < 0)
		queue_length = cfg_mirror_queue_length;
	mirp->sync_queue_length = queue_length;
	if (mirror_thread_create(mirp) < 0) {
		mirror_unlock(mirp);
		mirror_list_unlock(volp->slave_mirl);
		len = snprintf(buf, buf_len,
		    "-ERR Failed to start synchronisation thread\n");
		goto done;
	}
	mirp->flags |= MIR_FL_SYNCHRONISING;
	mirp->sync_progress = 0;
	mirror_thread_launch(mirp);
	mirror_unlock(mirp);
	mirror_list_unlock(volp->slave_mirl);

	len = snprintf(buf, buf_len, "+OK Mirror synchronisation initiated\n");
done:
	assert(len > 0 && (size_t)len < buf_len);
	*outbuf_idx = (size_t)len;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}

static
int
cmd_mirror_delete(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct command *cmd;
	struct volume *nvolp, *volp;
	struct mirror *mirp;
	const char *args[2], *opts, *target;
	char *buf;
	size_t buf_len, opts_len;
	int args_idx, i, len, retval;

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	target = NULL;
	if (keyval_get("target", opts, opts_len, &target) < 0) {
		assert(target == NULL);
		len = snprintf(buf, buf_len, "-ERR Missing mirror TARGET\n");
		goto done;
	}
	assert(target != NULL);
	assert(target[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = target;

	if (!mir_target_isvalid(target)) {
		len = snprintf(buf, buf_len, "-ERR Invalid mirror TARGET\n");
		goto done;
	}

	if (target[0] != '/') {
		len = snprintf(buf, buf_len,
		    "-ERR Mirror TARGET must be an absolute path\n");
		goto done;
	}

	mirp = NULL;
	volume_list_lock(master_voll);
	nvolp = volume_list_first(master_voll);
	for (;;) {
		volp = nvolp;
		if (volp == NULL)
			break;
		nvolp = volume_list_next(master_voll, volp);
		if (volp->state == VOL_ST_SHUTDOWN)
			continue;
		mirror_list_lock(volp->slave_mirl);
		mirp = mirror_list_lookup_by_target(volp->slave_mirl, target);
		if (mirp != NULL)
			break;
		mirror_list_unlock(volp->slave_mirl);
	}
	volume_list_unlock(master_voll);

	if (mirp == NULL) {
		len = snprintf(buf, buf_len, "-ERR No such mirror\n");
		goto done;
	}

	mirror_lock(mirp);
	if (mirp->state != MIR_ST_DOWN) {
		mirror_unlock(mirp);
		mirror_list_unlock(volp->slave_mirl);
		len = snprintf(buf, buf_len,
		    "-ERR Mirror must be shut down first\n");
		goto done;
	}
	assert(mirp->state == MIR_ST_DOWN);
	mirror_unlock(mirp);
	mirror_list_remove(volp->slave_mirl, mirp);
	mirror_list_unlock(volp->slave_mirl);
	mirror_delete(mirp);

	len = snprintf(buf, buf_len, "+OK Success\n");
done:
	assert(len > 0 && (size_t)len < buf_len);
	*outbuf_idx = (size_t)len;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}

static
int
cmd_test(void *arg, const char *inbuf, int *ovector, char **outbuf,
    size_t *outbuf_idx, size_t *outbuf_len)
{
	struct command *cmd;
	const char *args[2], *opts, *ping;
	char *buf;
	size_t buf_len, opts_len;
	int args_idx, len, i, retval;

	cmd = (struct command *)arg;
	assert(cmd != NULL);
	assert(*outbuf != NULL);
	assert(*outbuf_idx == (size_t)0);
	assert(*outbuf_len > (size_t)0);
	buf = *outbuf;
	buf_len = *outbuf_len;
	for (i = 0; i < NELEM(args); i++)
		args[i] = NULL;
	args_idx = 0;
	retval = -1;

	assert(cmd->ovecsize > 1);
	opts = NULL;
	if (pcre_get_substring(inbuf, ovector, cmd->ovecsize, 1, &opts) < 0) {
		assert(opts == NULL);
		goto fail;
	}
	assert(opts != NULL);
	assert(args_idx < NELEM(args));
	args[args_idx++] = opts;
	opts_len = strlen(opts);

	ping = NULL;
	if (keyval_get("ping", opts, opts_len, &ping) < 0) {
		assert(ping == NULL);
		len = snprintf(buf, buf_len, "-ERR No ping\n");
		goto done;
	}
	assert(ping != NULL);
	assert(ping[0] != '\0');
	assert(args_idx < NELEM(args));
	args[args_idx++] = ping;

	if (strcmp(ping, "pong")) {
		len = snprintf(buf, buf_len, "-ERR No pong\n");
		goto done;
	}

	len = snprintf(buf, buf_len, "+OK Ping-Pong\n");
done:
	assert(len > 0 && (size_t)len < buf_len);
	*outbuf_idx = (size_t)len;
	retval = 1;
fail:
	for (i = 0; i < NELEM(args); i++)
		if (args[i] != NULL)
			pcre_free_substring(args[i]);
	return (retval);
}
	
int
vty_list_alloc(struct vty_list **vtylpp, int index)
{
	struct vty_list *vtylp;
	int error;

	vtylp = malloc(sizeof(*vtylp));
	if (vtylp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*vtylp), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&vtylp->mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(vtylp);
		return (-3);
	}
	vty_list_init(vtylp, index);
	*vtylpp = vtylp;
	return (0);
}

void
vty_list_init(struct vty_list *vtylp, int index)
{
#ifndef NDEBUG
	struct vty *vtyp;
#endif

	assert(index >= 0 && index < NELEM(vtyp->all_vtyl_ary));
	TAILQ_INIT(&vtylp->head);
	vtylp->index = index;
	vtylp->count = 0;
	vtylp->peak = 0;
}

void
vty_list_uninit(struct vty_list *vtylp)
{

	assert(TAILQ_EMPTY(&vtylp->head));
	assert(vtylp->count == 0);
	vtylp->peak = 0;
}

void
vty_list_free(struct vty_list *vtylp)
{

	vty_list_uninit(vtylp);
	PTHREAD_MUTEX_DESTROY(&vtylp->mutex);
	free(vtylp);
}

static
void
vty_list_dealloc(struct vty_list *vtylp)
{
	struct vty *nvtyp, *vtyp;

	nvtyp = vty_list_first(vtylp);
	for (;;) {
		vtyp = nvtyp;
		if (vtyp == NULL)
			break;
		nvtyp = vty_list_next(vtylp, vtyp);
		vty_list_remove(vtylp, vtyp);
		vty_dealloc(vtyp);
	}
}

void
vty_list_lock(struct vty_list *vtylp)
{

	PTHREAD_MUTEX_LOCK(&vtylp->mutex);
}

void
vty_list_unlock(struct vty_list *vtylp)
{

	PTHREAD_MUTEX_UNLOCK(&vtylp->mutex);
}

int
vty_list_empty(struct vty_list *vtylp)
{

	return (TAILQ_EMPTY(&vtylp->head));
}

struct vty *
vty_list_first(struct vty_list *vtylp)
{

	return (TAILQ_FIRST(&vtylp->head));
}

struct vty *
vty_list_next(struct vty_list *vtylp, struct vty *vtyp)
{

	return (TAILQ_NEXT(vtyp, all_vtyl_ary[vtylp->index]));
}

void
vty_list_insert(struct vty_list *vtylp, struct vty *vtyp)
{

	TAILQ_INSERT_HEAD(&vtylp->head, vtyp, all_vtyl_ary[vtylp->index]);
	vtylp->count++;
	assert(vtylp->count > 0);
	if (vtylp->count > vtylp->peak)
		vtylp->peak = vtylp->count;
}

void
vty_list_append(struct vty_list *vtylp, struct vty *vtyp)
{

	TAILQ_INSERT_TAIL(&vtylp->head, vtyp, all_vtyl_ary[vtylp->index]);
	vtylp->count++;
	assert(vtylp->count > 0);
	if (vtylp->count > vtylp->peak)
		vtylp->peak = vtylp->count;
}

void
vty_list_remove(struct vty_list *vtylp, struct vty *vtyp)
{

	TAILQ_REMOVE(&vtylp->head, vtyp, all_vtyl_ary[vtylp->index]);
	vtylp->count--;
	assert(vtylp->count >= 0);
}

void
vty_list_shutdown(struct vty_list *vtylp)
{
	struct vty *vtyp;

	for (;;) {
		vty_list_lock(vtylp);
		vtyp = vty_list_first(vtylp);
		if (vtyp == NULL) {
			vty_list_unlock(vtylp);
			break;
		}
		vty_thread_shutdown(vtyp);
		vty_list_unlock(vtylp);
		thread_join(vtyp->thr);
		vty_free(vtyp);
	}
}
