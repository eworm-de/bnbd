/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"

#include <sys/ioctl.h>
#ifdef __linux__
#include <linux/falloc.h>
#include <linux/fs.h>
#endif

#include "io.h"

int
fd_open(const char *path, int flags, int mode)
{
	int fd;

try_open:
	fd = open(path, flags, (mode_t)mode);
	if (fd < 0) {
		if (errno == EINTR) {
			WARNING("open(%s): %s", path, strerror(errno));
			goto try_open;
		}
		ERR("open(%s): %s", path, strerror(errno));
	}
	return (fd);
}

int
fd_openat(int dirfd, const char *path, int flags, int mode)
{
	int fd;

try_openat:
	fd = openat(dirfd, path, flags, (mode_t)mode);
	if (fd < 0) {
		if (errno == EINTR) {
			WARNING("openat(%s): %s", path, strerror(errno));
			goto try_openat;
		}
		ERR("openat(%s): %s", path, strerror(errno));
	}
	return (fd);
}

int
fd_close(int fd)
{

	assert(fd >= 0);
try_close:
	if (close(fd) < 0) {
		if (errno == EINTR) {
			WARNING("close(%d): %s", fd, strerror(errno));
			goto try_close;
		}
		ERR("close(%d): %s", fd, strerror(errno));
		return (-1);
	}
	return (0);
}

int
fd_truncate(int fd, uint64_t length)
{

try_ftruncate:
	if (ftruncate(fd, (off_t)length) < 0) {
		if (errno == EINTR) {
			WARNING("ftruncate(%d, %llu): %s",
			    fd, (unsigned long long)length, strerror(errno));
			goto try_ftruncate;
		}
		ERR("ftruncate(%d, %llu): %s",
		    fd, (unsigned long long)length, strerror(errno));
		return (-1);
	}
	return (0);
}

int
fd_delete(int fd, uint64_t offset, uint64_t length)
{
#ifdef __linux__
	int error;

try_fallocate:
	if (fallocate(fd, FALLOC_FL_PUNCH_HOLE | FALLOC_FL_KEEP_SIZE,
	    (off_t)offset, (off_t)length) < 0) {
		if (errno == EINTR) {
			WARNING(
			    "fallocate(FALLOC_FL_PUNCH_HOLE, %llu, %llu): %s",
			    (unsigned long long)offset,
			    (unsigned long long)length,
			    strerror(errno));
			goto try_fallocate;
		}
		error = -errno;
		ERR("fallocate(FALLOC_FL_PUNCH_HOLE, %llu, %llu): %s",
		    (unsigned long long)offset,
		    (unsigned long long)length,
		    strerror(errno));
		return (error);
	}
	return (0);
#else
	(void)fd;
	(void)offset;
	(void)length;
	/* XXX TODO FIXME */
	WARNING("fallocate(FALLOC_FL_PUNCH_HOLE): Function not implemented");
	return (-1);
#endif
}

int
fd_blocking(int fd, int blocking)
{
	int flags;

	flags = fcntl(fd, F_GETFL);
	if (flags < 0) {
		ERR("fcntl(F_GETFL): %s", strerror(errno));
		return (-1);
	}
	if (blocking > 0)
		flags &= ~O_NONBLOCK;
	else
		flags |= O_NONBLOCK;
	if (fcntl(fd, F_SETFL, (long)flags) < 0) {
		ERR("fcntl(F_SETFL): %s", strerror(errno));
		return (-2);
	}
	return (0);
}

int
fd_blkdevmediasize(int fd, uint64_t *sizep)
{
#ifdef __linux__
	uint64_t size;

	if (ioctl(fd, BLKGETSIZE64, &size) < 0) {
		ERR("ioctl(BLKGETSIZE64): %s", strerror(errno));
		return (-1);
	}
	*sizep = size;
	return (0);
#else
	(void)fd;
	(void)sizep;
	/* XXX TODO FIXME */
	WARNING("ioctl(BLKGETSIZE64): Function not implemented");
	return (-1);
#endif
}

int
fd_blkdevsectorsize(int fd, uint32_t *sizep)
{
#ifdef __linux__
	uint32_t size;

	if (ioctl(fd, BLKSSZGET, &size) < 0) {
		ERR("ioctl(BLKSSZGET): %s", strerror(errno));
		return (-1);
	}
	*sizep = size;
	return (0);
#else
	(void)fd;
	(void)sizep;
	/* XXX TODO FIXME */
	WARNING("ioctl(BLKSSZGET): Function not implemented");
	return (-1);
#endif
}

int
fd_blkdevdelete(int fd, uint64_t offset, uint64_t length)
{
#ifdef __linux__
	uint64_t range[2];
	int error;

	range[0] = offset;
	range[1] = length;
	if (ioctl(fd, BLKDISCARD, &range) < 0) {
		error = -errno;
		ERR("ioctl(BLKDISCARD, %llu, %llu): %s",
		    (unsigned long long)offset,
		    (unsigned long long)length,
		    strerror(errno));
		return (error);
	}
	return (0);
#else
	(void)fd;
	(void)offset;
	(void)length;
	/* XXX TODO FIXME */
	WARNING("ioctl(BLKDISCARD): Function not implemented");
	return (-1);
#endif
}

int
so_buf(int so, int tx, int kb)
{

	kb <<= 10;
	if (setsockopt(so, SOL_SOCKET, tx > 0 ? SO_SNDBUF : SO_RCVBUF,
	    &kb, (socklen_t)sizeof(kb)) < 0) {
		ERR("setsockopt(SO_%sBUF, %d): %s",
		    tx > 0 ? "SND" : "RCV", kb, strerror(errno));
		return (-1);
	}
	return (0);
}

int
so_lowat(int so, int tx, int bytes)
{

	if (setsockopt(so, SOL_SOCKET, tx > 0 ? SO_SNDLOWAT : SO_RCVLOWAT,
	    &bytes, (socklen_t)sizeof(bytes)) < 0) {
		ERR("setsockopt(SO_%sLOWAT, %d): %s",
		    tx > 0 ? "SND" : "RCV", bytes, strerror(errno));
		return (-1);
	}
	return (0);
}

int
so_timeo(int so, int tx, int timeout)
{
	struct timeval tv;

	tv.tv_sec = timeout;
	tv.tv_usec = 0;
	if (setsockopt(so, SOL_SOCKET, tx > 0 ? SO_SNDTIMEO : SO_RCVTIMEO,
	    &tv, (socklen_t)sizeof(tv)) < 0) {
		ERR("setsockopt(SO_%sTIMEO, %d): %s",
		    tx > 0 ? "SND" : "RCV", timeout, strerror(errno));
		return (-1);
	}
	return (0);
}

int
so_linger(int so, int onoff, int linger)
{
	struct linger opt;

	opt.l_onoff = onoff;
	opt.l_linger = linger;
	if (setsockopt(so, SOL_SOCKET, SO_LINGER, &opt,
	    (socklen_t)sizeof(opt)) < 0) {
		ERR("setsockopt(SO_LINGER, %d, %d): %s",
		    onoff, linger, strerror(errno));
		return (-1);
	}
	return (0);
}

int
tcp_user_timeout(int so, int timeout)
{
#ifdef __linux__

	timeout *= 1000;
	assert(timeout >= 0);
#ifndef TCP_USER_TIMEOUT
#define TCP_USER_TIMEOUT	18
#endif
	if (setsockopt(so, IPPROTO_TCP, TCP_USER_TIMEOUT, &timeout,
	    (socklen_t)sizeof(timeout)) < 0) {
		ERR("setsockopt(TCP_USER_TIMEOUT): %s", strerror(errno));
		return (-1);
	}
	return (0);
#else
	(void)so;
	(void)timeout;
	/* XXX TODO FIXME */
	WARNING("setsockopt(TCP_USER_TIMEOUT): Function not implemented");
	return (-1);
#endif
}

int
tcp_keepalive(int so, int idle, int interval, int count)
{
	int opt;

	opt = 1;
	if (setsockopt(so, SOL_SOCKET, SO_KEEPALIVE, &opt,
	    (socklen_t)sizeof(opt)) < 0) {
		ERR("setsockopt(SO_KEEPALIVE): %s", strerror(errno));
		return (-1);
	}

	if (setsockopt(so, IPPROTO_TCP, TCP_KEEPIDLE, &idle,
	    (socklen_t)sizeof(idle)) < 0) {
		ERR("setsockopt(TCP_KEEPIDLE): %s", strerror(errno));
		return (-2);
	}

	if (setsockopt(so, IPPROTO_TCP, TCP_KEEPINTVL, &interval,
	    (socklen_t)sizeof(interval)) < 0) {
		ERR("setsockopt(TCP_KEEPINTVL): %s", strerror(errno));
		return (-3);
	}

	if (setsockopt(so, IPPROTO_TCP, TCP_KEEPCNT, &count,
	    (socklen_t)sizeof(count)) < 0) {
		ERR("setsockopt(TCP_KEEPCNT): %s", strerror(errno));
		return (-4);
	}
	return (0);
}

int
tcp_listen(const char *addr, int port, int backlog)
{
	struct sockaddr_in sa;
	int so, opt;

	so = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (so < 0) {
		ERR("socket(): %s", strerror(errno));
		goto fail;
	}

	opt = 1;
	if (setsockopt(so, SOL_SOCKET, SO_REUSEADDR, &opt,
	    (socklen_t)sizeof(opt)) < 0) {
		ERR("setsockopt(SO_REUSEADDR): %s", strerror(errno));
		goto fail;
	}

	memset(&sa, 0x00, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons((uint16_t)port);
	if (!inet_aton(addr, &sa.sin_addr)) {
		ERR("inet_aton(%s): failed", addr);
		goto fail;
	}

	if (bind(so, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
		ERR("bind(%s:%d): %s", addr, port, strerror(errno));
		goto fail;
	}

	if (listen(so, backlog > 0 ? backlog : SOMAXCONN) < 0) {
		ERR("listen(): %s", strerror(errno));
		goto fail;
	}
	return (so);
fail:
	if (so >= 0)
		fd_close(so);
	return (-1);
}

int
tcp_accept(int so, char *inet_addr, int *inet_port)
{
	struct sockaddr_in sa;
	socklen_t sl;
	const char *ptr;
	int nsock, opt;

try_accept:
	memset(&sa, 0x00, sizeof(sa));
	sl = sizeof(sa);
	nsock = accept(so, (struct sockaddr *)&sa, &sl);
	if (nsock < 0) {
		if (errno == EINTR) {
			WARNING("accept(): %s", strerror(errno));
			goto try_accept;
		}
		ERR("accept(): %s", strerror(errno));
		goto fail;
	}

	ptr = inet_ntop(AF_INET, &sa.sin_addr, inet_addr, INET_ADDRSTRLEN);
	if (ptr == NULL) {
		ERR("inet_ntop(): %s", strerror(errno));
		goto fail;
	}
	*inet_port = (int)ntohs(sa.sin_port);

	opt = 1;
	if (setsockopt(nsock, IPPROTO_TCP, TCP_NODELAY, &opt,
	    (socklen_t)sizeof(opt)) < 0) {
		ERR("setsockopt(TCP_NODELAY): %s", strerror(errno));
		goto fail;
	}
	return (nsock);
fail:
	if (nsock >= 0)
		fd_close(nsock);
	return (-1);
}

ssize_t
buf_rx(int fd, char *buf, size_t len, int nointr)
{
	size_t idx;
	ssize_t n;

	idx = (size_t)0;
	for (;;) {
		n = read(fd, buf + idx, len - idx);
		if (n < (ssize_t)0) {
			if (errno == EINTR && nointr > 0)
				continue;
			ERR("read(%zu): %s", len - idx, strerror(errno));
			return ((ssize_t)-1);
		}
		if (n == (ssize_t)0) {
			ERR("read(%zu): EOF", len - idx);
			return ((ssize_t)0);
		}
		assert(n > (ssize_t)0);
		idx += n;
		assert(idx <= len);
		if (len == idx)
			break;
	}
	return ((ssize_t)idx);
}

ssize_t
buf_tx(int fd, const char *buf, size_t len, int nointr)
{
	size_t idx;
	ssize_t n;

	assert(len > (size_t)0);
	idx = (size_t)0;
	for (;;) {
		n = write(fd, buf + idx, len - idx);
		if (n < (ssize_t)0) {
			if (errno == EINTR && nointr > 0)
				continue;
			ERR("write(%zu): %s", len - idx, strerror(errno));
			return ((ssize_t)-1);
		}
		assert(n > (ssize_t)0);
		idx += n;
		assert(idx <= len);
		if (idx == len)
			break;
	}
	return ((ssize_t)idx);
}

ssize_t
iov_tx(int fd, struct iovec *iov, int iovcnt, int nointr)
{
	size_t idx, len, sum;
	ssize_t n;
	int i;
	char *ptr;

	assert(iovcnt > 0);
	len = (size_t)0;
	for (i = 0; i < iovcnt; i++)
		len += iov[i].iov_len;
	idx = (size_t)0;
	for (;;) {
		n = writev(fd, iov, iovcnt);
		if (n < (ssize_t)0) {
			if (errno == EINTR && nointr > 0)
				continue;
			ERR("writev(%zu/%d): %s",
			    len - idx, iovcnt, strerror(errno));
			return ((ssize_t)-1);
		}
		assert(n > (ssize_t)0);
		idx += n;
		assert(idx <= len);
		if (idx == len)
			break;
		NOTICE("PARTIAL writev(): %zd/%zu", n, len - (idx - n));
		sum = (size_t)0;
		for (i = 0; i < iovcnt; i++) {
			sum += iov[i].iov_len;
			if (sum > (size_t)n)
				break;
		}
		assert(i < iovcnt);
		ptr = (char *)iov[i].iov_base;
		ptr += (n - (sum - iov[i].iov_len));
		iov[i].iov_base = (void *)ptr;
		iov[i].iov_len = sum - n;
		iov += i;
		iovcnt -= i;
	}
	return ((ssize_t)idx);
}
