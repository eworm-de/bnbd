/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

struct thread {
	TAILQ_ENTRY(thread)	all_thrl_ary[2];
#define THR_FL_INUSE		0x00000001
#define THR_FL_SHUTDOWN		0x00000002
	int			flags;
#define THR_FL2_NOTIFIED	0x10000000
	int			flags2;
	pthread_mutex_t		mutex;
	pthread_cond_t		cond;
	pthread_t		td;
	pthread_t		*tdp;
	void			*data;
	const char		*name;
	size_t			alloc_len;
};
TAILQ_HEAD(thread_head, thread);

struct thread_list {
	struct thread_head	head;
#define THRL_IDX_ALLOC		0
#define THRL_IDX_STD		1
	int			index;
	int			count;
	int			peak;
};

int thread_module_setup(void);
void thread_module_hello(void);
void thread_module_teardown(void);
void thread_module_stats(void);
int thread_alloc(struct thread **, void *);
void thread_init(struct thread *);
void thread_uninit(struct thread *);
void thread_free(struct thread *);
void thread_lock(struct thread *);
void thread_unlock(struct thread *);
int thread_create(struct thread *, const char *, void *(*)(void *));
void thread_launch(struct thread *);
void thread_prologue(struct thread *);
void thread_epilogue(void *);
void thread_detach(struct thread *);
void thread_join(struct thread *);
void thread_kill(struct thread *, int);
void thread_shutdown_begin(struct thread *);
int thread_shutdown_commit(struct thread *);
#define THREAD_SHUTDOWN_PENDING(thrp)	(thrp->flags & THR_FL_SHUTDOWN)
void thread_wait(struct thread *);
void thread_wait_mutex(struct thread *, pthread_mutex_t *);
void thread_wakeup(struct thread *);
void thread_head_init(struct thread_head *);
void thread_head_uninit(struct thread_head *);
int thread_head_empty(struct thread_head *);
struct thread *thread_head_first(struct thread_head *);
struct thread *thread_head_next(struct thread *, int);
void thread_head_insert(struct thread_head *, struct thread *, int);
void thread_head_append(struct thread_head *, struct thread *, int);
void thread_head_remove(struct thread_head *, struct thread *, int);
int thread_list_alloc(struct thread_list **, int);
void thread_list_init(struct thread_list *, int);
void thread_list_uninit(struct thread_list *);
void thread_list_free(struct thread_list *);
int thread_list_empty(struct thread_list *);
struct thread *thread_list_first(struct thread_list *);
struct thread *thread_list_next(struct thread_list *, struct thread *);
void thread_list_insert(struct thread_list *, struct thread *);
void thread_list_append(struct thread_list *, struct thread *);
void thread_list_remove(struct thread_list *, struct thread *);
