/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "thread.h"
#include "request.h"
#include "reader.h"
#include "writer.h"
#include "worker.h"
#include "skiver.h"
#include "session.h"
#include "blkmap.h"
#include "mirror.h"
#include "volume.h"
#include "dispatch_glue.h"

static int dispatch_glue_initialized = 0;
static int64_t max_request_burst_bytes = (int64_t)0;

int
dispatch_glue_setup(void)
{

	assert(!dispatch_glue_initialized);
	if (cfg_max_request_burst_kb > 0)
		max_request_burst_bytes =
		    (int64_t)cfg_max_request_burst_kb << 10;
	dispatch_glue_initialized = 1;
	return (0);
}

void
dispatch_glue_teardown(void)
{

	assert(dispatch_glue_initialized);
	dispatch_glue_initialized = 0;
}

void
dispatch_read(struct reader *rdrp, struct request *reqp)
{

	reqp->type = REQ_TY_READ;
	reqp->id = rdrp->cur_req_id++;
	assert(rdrp->cur_req_id > (int64_t)0);
	request_list_append(rdrp->aux_reql, reqp);
	rdrp->aux_reql->submit_bytes += reqp->nbd.request.len;
	assert(rdrp->aux_reql->submit_bytes > (int64_t)0);

	if ((cfg_max_request_burst > 0 &&
	    rdrp->aux_reql->count >= cfg_max_request_burst) ||
	    (max_request_burst_bytes > (int64_t)0 &&
	    rdrp->aux_reql->submit_bytes > max_request_burst_bytes))
		dispatch_pending(rdrp);
}

void
dispatch_write(struct reader *rdrp, struct request *master_reqp)
{
	struct request *reqp;
	struct blkmap *bmp;
	struct mirror *mirp, *nmirp;
	struct mirror_list *slave_mirlp;
	int refs;

	token_setup(master_reqp->tok, master_reqp->nbd.request.from,
	    master_reqp->nbd.request.len, master_reqp->blk_size);
	if (token_acquire(rdrp->thr, master_reqp->volp->range_tokt,
	    master_reqp->tok, 0) < 0) {
		dispatch_pending(rdrp);
		token_acquire(rdrp->thr, master_reqp->volp->range_tokt,
		    master_reqp->tok, 1);	/* tok.refs == 1 */
	}

	bmp = master_reqp->volp->bm;
	if (bmp != NULL) {
		blkmap_lock(bmp);
		(void)blkmap_exec(bmp,
		    master_reqp->nbd.request.from,
		    master_reqp->nbd.request.len,
		    BM_OP_SET);
		blkmap_unlock(bmp);
	}

	slave_mirlp = master_reqp->volp->slave_mirl;
	mirror_list_lock(slave_mirlp);
	if (mirror_list_empty(slave_mirlp)) {
		mirror_list_unlock(slave_mirlp);
		master_reqp->type = REQ_TY_WRITE;
		master_reqp->id = rdrp->cur_req_id++;
		assert(rdrp->cur_req_id > (int64_t)0);
		request_list_append(rdrp->aux_reql, master_reqp);
		rdrp->aux_reql->submit_bytes += master_reqp->nbd.request.len;
		assert(rdrp->aux_reql->submit_bytes > (int64_t)0);
		goto done;
	}

	master_reqp->master_reqp = master_reqp;
	token_acquire(NULL, master_reqp->volp->range_tokt,
	    master_reqp->tok, 1);		/* tok.refs == 2 */
	master_reqp->type = REQ_TY_MASTER_WRITE;
	master_reqp->id = rdrp->cur_req_id++;
	assert(rdrp->cur_req_id > (int64_t)0);
	request_list_append(rdrp->aux_reql, master_reqp);
	rdrp->aux_reql->submit_bytes += master_reqp->nbd.request.len;
	assert(rdrp->aux_reql->submit_bytes > (int64_t)0);

	nmirp = mirror_list_first(slave_mirlp);
	for (;;) {
		mirp = nmirp;
		if (mirp == NULL)
			break;
		nmirp = mirror_list_next(slave_mirlp, mirp);
		reqp = NULL;
		if (request_alloc(rdrp->thr, &reqp, 0) < 0) {
			assert(reqp == NULL);
			dispatch_pending(rdrp);
			if (request_alloc(rdrp->thr, &reqp, 1) < 0) {
				assert(reqp == NULL);
				ERR("OUT OF MEMORY (req-slave)");
				mirror_lock(mirp);
				mirror_shutdown_begin(mirp);
				mirror_unlock(mirp);
				continue;
			}
		}
		assert(reqp != NULL);
		mirror_lock(mirp);
		if (mirp->state != MIR_ST_UP) {
			mirror_unlock(mirp);
			request_free(reqp);
			continue;
		}
		memcpy(&reqp->nbd.request, &master_reqp->nbd.request,
		    sizeof(reqp->nbd.request));
		memcpy(&reqp->buf, &master_reqp->buf, sizeof(reqp->buf));
		reqp->blk_size = master_reqp->blk_size;
		reqp->fd = mirp->fd;
		reqp->master_reqp = master_reqp;
		reqp->outgoing_reqlp = master_reqp->outgoing_reqlp;
		reqp->mirp = mirp;
		token_acquire(NULL, master_reqp->volp->range_tokt,
		    master_reqp->tok, 1);	/* tok.refs >= 3 */
		reqp->type = REQ_TY_SLAVE_WRITE;
		assert(reqp->id < (int64_t)0);
		request_list_append(rdrp->aux_reql, reqp);
		mirp->inflight++;
		mirror_unlock(mirp);
	}
	mirror_list_unlock(slave_mirlp);

	refs = token_release(master_reqp->volp->range_tokt, master_reqp->tok);
						/* tok.refs >= 0 */
	if (refs)
		goto done;
	if (master_reqp->buf.buf_ptr != NULL)
		request_buffer_free(master_reqp);
	request_reply(master_reqp);
	return;
done:
	if ((cfg_max_request_burst > 0 &&
	    rdrp->aux_reql->count >= cfg_max_request_burst) ||
	    (max_request_burst_bytes > (int64_t)0 &&
	    rdrp->aux_reql->submit_bytes > max_request_burst_bytes))
		dispatch_pending(rdrp);
}

void
dispatch_flush(struct reader *rdrp, struct request *reqp)
{
	struct session *sesp;
	struct request_list *outgoing_reqlp;
	int wakeup;

	sesp = rdrp->sesp;
	outgoing_reqlp = reqp->outgoing_reqlp;
	dispatch_pending(rdrp);
	reqp->type = REQ_TY_FLUSH;
	reqp->id = rdrp->cur_req_id++;
	assert(rdrp->cur_req_id > (int64_t)0);
	reqp->outgoing_reqlp = sesp->flush_reql;
	request_list_lock(outgoing_reqlp);
	reqp->nbd.request.from = (uint64_t)sesp->wrr->cur_req_id;
	request_list_append(sesp->flush_reql, reqp);
	DEBUG("Flush-%lld: %lld/%d",
	    (long long)reqp->id,
	    (long long)(reqp->id - sesp->wrr->cur_req_id),
	    sesp->flush_reql->count);
	if (outgoing_reqlp->flags & REQL_FL_NOTIFIED) {
		wakeup = 0;
	} else {
		outgoing_reqlp->flags |= REQL_FL_NOTIFIED;
		wakeup = 1;
	}
	request_list_unlock(outgoing_reqlp);
	if (wakeup)
		request_list_wakeup(outgoing_reqlp);
}

void
dispatch_trim(struct reader *rdrp, struct request *master_reqp)
{
	struct request *reqp;
	struct blkmap *bmp;
	struct mirror *mirp, *nmirp;
	struct mirror_list *slave_mirlp;
	int refs;

	/* Dispatch all pending requests */
	dispatch_pending(rdrp);

	token_setup(master_reqp->tok, master_reqp->nbd.request.from,
	    master_reqp->nbd.request.len, master_reqp->blk_size);
	token_acquire(rdrp->thr, master_reqp->volp->range_tokt,
	    master_reqp->tok, 1);		/* tok.refs == 1 */

	bmp = master_reqp->volp->bm;
	if (bmp != NULL) {
		blkmap_lock(bmp);
		(void)blkmap_exec(bmp,
		    master_reqp->nbd.request.from,
		    master_reqp->nbd.request.len,
		    BM_OP_CLEAR);
		blkmap_unlock(bmp);
	}

	slave_mirlp = master_reqp->volp->slave_mirl;
	mirror_list_lock(slave_mirlp);
	if (mirror_list_empty(slave_mirlp)) {
		mirror_list_unlock(slave_mirlp);
		master_reqp->type = REQ_TY_TRIM;
		master_reqp->id = rdrp->cur_req_id++;
		assert(rdrp->cur_req_id > (int64_t)0);
		request_list_append(rdrp->aux_reql, master_reqp);
		rdrp->aux_reql->submit_bytes += master_reqp->nbd.request.len;
		assert(rdrp->aux_reql->submit_bytes > (int64_t)0);
		dispatch_pending(rdrp);
		return;
	}

	master_reqp->master_reqp = master_reqp;
	token_acquire(NULL, master_reqp->volp->range_tokt,
	    master_reqp->tok, 1);		/* tok.refs == 2 */
	master_reqp->type = REQ_TY_MASTER_TRIM;
	master_reqp->id = rdrp->cur_req_id++;
	assert(rdrp->cur_req_id > (int64_t)0);
	request_list_append(rdrp->aux_reql, master_reqp);
	rdrp->aux_reql->submit_bytes += master_reqp->nbd.request.len;
	assert(rdrp->aux_reql->submit_bytes > (int64_t)0);

	nmirp = mirror_list_first(slave_mirlp);
	for (;;) {
		mirp = nmirp;
		if (mirp == NULL)
			break;
		nmirp = mirror_list_next(slave_mirlp, mirp);
		reqp = NULL;
		if (request_alloc(rdrp->thr, &reqp, 0) < 0) {
			assert(reqp == NULL);
			dispatch_pending(rdrp);
			if (request_alloc(rdrp->thr, &reqp, 1) < 0) {
				assert(reqp == NULL);
				ERR("OUT OF MEMORY (req-slave)");
				mirror_lock(mirp);
				mirror_shutdown_begin(mirp);
				mirror_unlock(mirp);
				continue;
			}
		}
		assert(reqp != NULL);
		mirror_lock(mirp);
		if (mirp->state != MIR_ST_UP) {
			mirror_unlock(mirp);
			request_free(reqp);
			continue;
		}
		memcpy(&reqp->nbd.request, &master_reqp->nbd.request,
		    sizeof(reqp->nbd.request));
		reqp->blk_size = master_reqp->blk_size;
		reqp->fd = mirp->fd;
		reqp->master_reqp = master_reqp;
		reqp->outgoing_reqlp = master_reqp->outgoing_reqlp;
		reqp->mirp = mirp;
		token_acquire(NULL, master_reqp->volp->range_tokt,
		    master_reqp->tok, 1);	/* tok.refs >= 3 */
		reqp->type = REQ_TY_SLAVE_TRIM;
		assert(reqp->id < (int64_t)0);
		request_list_append(rdrp->aux_reql, reqp);
		mirp->inflight++;
		mirror_unlock(mirp);
	}
	mirror_list_unlock(slave_mirlp);

	refs = token_release(master_reqp->volp->range_tokt, master_reqp->tok);
						/* tok.refs >= 0 */
	if (refs) {
		dispatch_pending(rdrp);
		return;
	}
	request_reply(master_reqp);
}

void
dispatch_pending(struct reader *rdrp)
{
	struct request *first;
	struct blkmap *bmp;
	int error;

	first = request_list_first(rdrp->aux_reql);
	if (first == NULL)
		return;
	bmp = rdrp->sesp->volp->bm;
	if (bmp != NULL) {
		blkmap_lock(bmp);
		error = blkmap_sync(bmp);
		blkmap_unlock(bmp);
		if (error < 0) {
			request_list_abort(rdrp->aux_reql,
			    REQ_TY_ALL & ~REQ_TY_READ, -EROFS);
			request_list_dump(rdrp->aux_reql,
			    REQ_TY_ALL & ~REQ_TY_READ);
		}
	}
	if (first->type & REQ_TY_AIO)
		worker_reql_submit(rdrp->aux_reql);
	else if (first->type & REQ_TY_BIO)
		skiver_reql_submit(rdrp->aux_reql);
	else
		abort();
}
