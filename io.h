/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

int fd_open(const char *, int, int);
int fd_openat(int, const char *, int, int);
int fd_close(int);
int fd_truncate(int, uint64_t);
int fd_delete(int, uint64_t, uint64_t);
int fd_blocking(int, int);
int fd_blkdevmediasize(int, uint64_t *);
int fd_blkdevsectorsize(int, uint32_t *);
int fd_blkdevdelete(int, uint64_t, uint64_t);
int so_buf(int, int, int);
int so_lowat(int, int, int);
int so_timeo(int, int, int);
int so_linger(int, int, int);
int tcp_user_timeout(int, int);
int tcp_keepalive(int, int, int, int);
int tcp_listen(const char *, int, int);
int tcp_accept(int, char *, int *);
ssize_t buf_rx(int, char *, size_t, int);
ssize_t buf_tx(int, const char *, size_t, int);
ssize_t iov_tx(int, struct iovec *, int, int);
