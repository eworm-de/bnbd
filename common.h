/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include <sys/param.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/uio.h>
#include "sys/queue.h"
#include "sys/tree.h"

#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include <assert.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <regex.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>

#ifdef __linux__
#include <endian.h>
#else
#include <sys/endian.h>
#endif
#define ntohll(x) be64toh(x)
#define htonll(x) htobe64(x)

#ifndef __unused
#define __unused	__attribute__((__unused__))
#endif

#define NELEM(ary)	((int)(sizeof(ary) / sizeof(ary[0])))
#define MY_MAXPATH	256

#define SYSLOG(lvl, pfx, fmt, ...)	do {				\
	pthread_t self;							\
									\
	self = pthread_self();						\
	syslog(lvl,							\
	    pfx ": %llX: %s(): " fmt "%s",				\
	    (long long)self, __func__,  __VA_ARGS__);			\
} while (0)

#define DEBUG(...)	SYSLOG(LOG_DEBUG, "debug", __VA_ARGS__, "")
#define INFO(...)	SYSLOG(LOG_INFO, "info", __VA_ARGS__, "")
#define NOTICE(...)	SYSLOG(LOG_NOTICE, "notice", __VA_ARGS__, "")
#define WARNING(...)	SYSLOG(LOG_WARNING, "warn", __VA_ARGS__, "")
#define ERR(...)	SYSLOG(LOG_ERR, "error", __VA_ARGS__, "")

#define RMDIR(path)			do {				\
	if (rmdir(path) < 0)						\
		ERR("rmdir(%s): %s", path, strerror(errno));		\
} while (0)

#define UNLINK(path)			do {				\
	if (unlink(path) < 0)						\
		ERR("unlink(%s): %s", path, strerror(errno));		\
} while (0)

#ifndef NDEBUG
#define SIGACTION(sig, act, oldact)	do {				\
	int error;							\
									\
	error = sigaction(sig, act, oldact);				\
	assert(!error);							\
} while (0)
#else
#define SIGACTION(sig, act, oldact)	sigaction(sig, act, oldact)
#endif

#ifndef NDEBUG
#define PTHREAD_DETACH(thread)		do {				\
	int error;							\
									\
	error = pthread_detach(thread);					\
	assert(!error);							\
} while (0)
#else
#define PTHREAD_DETACH(thread)		pthread_detach(thread)
#endif

#ifndef NDEBUG
#define PTHREAD_JOIN(thread, retval)	do {				\
	int error;							\
									\
	error = pthread_join(thread, retval);				\
	assert(!error);							\
} while (0)
#else
#define PTHREAD_JOIN(thread, retval)	pthread_join(thread, retval)
#endif

#ifndef NDEBUG
#define PTHREAD_KILL(thread, sig)	do {				\
	int error;							\
									\
	error = pthread_kill(thread, sig);				\
	assert(error == 0 || error == ESRCH);				\
} while (0)
#else
#define PTHREAD_KILL(thread, sig)	pthread_kill(thread, sig)
#endif

#ifndef NDEBUG
#define PTHREAD_SIGMASK(how, set, oset)	do {				\
	int error;							\
									\
	error = pthread_sigmask(how, set, oset);			\
	assert(!error);							\
} while (0)
#else
#define PTHREAD_SIGMASK(how, set, oset)	pthread_sigmask(how, set, oset)
#endif

#ifndef NDEBUG
#define PTHREAD_MUTEX_LOCK(mutex)	do {				\
	int error;							\
									\
	error = pthread_mutex_lock(mutex);				\
	assert(!error);							\
} while (0)
#else
#define PTHREAD_MUTEX_LOCK(mutex)	pthread_mutex_lock(mutex)
#endif

#ifndef NDEBUG
#define PTHREAD_MUTEX_UNLOCK(mutex)	do {				\
	int error;							\
									\
	error = pthread_mutex_unlock(mutex);				\
	assert(!error);							\
} while (0)
#else
#define PTHREAD_MUTEX_UNLOCK(mutex)	pthread_mutex_unlock(mutex)
#endif

#ifndef NDEBUG
#define PTHREAD_MUTEX_DESTROY(mutex)	do {				\
	int error;							\
									\
	error = pthread_mutex_destroy(mutex);				\
	assert(!error);							\
} while (0)
#else
#define PTHREAD_MUTEX_DESTROY(mutex)	pthread_mutex_destroy(mutex)
#endif

#ifndef NDEBUG
#define PTHREAD_COND_WAIT(cond, mutex)	do {				\
	int error;							\
									\
	error = pthread_cond_wait(cond, mutex);				\
	assert(!error);							\
} while (0)
#else
#define PTHREAD_COND_WAIT(cond,mutex)	pthread_cond_wait(cond, mutex)
#endif

#ifndef NDEBUG
#define PTHREAD_COND_SIGNAL(cond)	do {				\
	int error;							\
									\
	error = pthread_cond_signal(cond);				\
	assert(!error);							\
} while (0)
#else
#define PTHREAD_COND_SIGNAL(cond)	pthread_cond_signal(cond)
#endif

#ifndef NDEBUG
#define PTHREAD_COND_DESTROY(cond)	do {				\
	int error;							\
									\
	error = pthread_cond_destroy(cond);				\
	assert(!error);							\
} while (0)
#else
#define PTHREAD_COND_DESTROY(cond)	pthread_cond_destroy(cond)
#endif
