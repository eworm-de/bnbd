#!/bin/sh

PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin
export PATH
LANG=C
export LANG

usage()
{
	local _me _sp

	_me=`basename $0`
	_sp=`echo $_me | sed 's/./ /g'`
	echo "Usage: $_me [-b block-size] [-H host] [-P port] [-t timeout]"
	echo "       $_sp -N volume-name [--] device"
	echo "       $_me -d [--] device"
	exit 1
}

Disconnect=0
Block=4096
Host='127.0.0.1'
Port='10809'
Timeout=60
Volume=''
[ -z "$Q_Add_Random" ]		&& Q_Add_Random=0
[ -z "$Q_Max_Sectors_KB" ]	&& Q_Max_Sectors_KB=128
[ -z "$Q_NoMerges" ]		&& Q_NoMerges=0
[ -z "$Q_NR_Requests" ]		&& Q_NR_Requests=128
[ -z "$Q_Read_Ahead_KB" ]	&& Q_Read_Ahead_KB=128
[ -z "$Q_Scheduler" ]		&& Q_Scheduler='noop'

args=`getopt b:dH:N:P:t: $*`
if [ $? -ne 0 ]
then
	usage
fi
set -- $args
while true
do
	case "$1"
	in
		-b)
			Block="$2"; shift
			shift
			;;
		-d)
			Disconnect=1
			shift
			;;
		-H)
			Host="$2"; shift
			shift
			;;
		-N)
			Volume="$2"; shift
			shift
			;;
		-P)
			Port="$2"; shift
			shift
			;;
		-t)
			Timeout="$2"; shift
			shift
			;;
		--)
			shift
			break
			;;
	esac
done

i=0
for Device in $@
do
	i=`expr $i + 1`
done
if [ $i -ne 1 ]
then
	usage
fi

NBD=${1#/dev/}
Device="/dev/$NBD"

pid=`nbd-client -c $Device`
retval=$?
if [ $retval -ne 0 -a $retval -ne 1 ]
then
	echo "FAILURE: nbd-client -c $Device returned $retval"
	exit 1
fi
inuse=`expr 1 - $retval`

if [ $Disconnect -ne 0 ]
then
	echo "Shutting down $Device"
	if [ $inuse -ne 1 ]
	then
		echo "$Device already shut down"
		exit 0
	fi
	nbd-client -d $Device
	retval=$?
	if [ $retval -ne 0 ]
	then
		"FAILURE: nbd-client -d $Device returned $retval"
		exit 1
	fi
	exit 0
fi

if [ -z "$Volume" ]
then
	usage
fi

echo "Setting up $Device"
if [ $inuse -eq 1 ]
then
	echo "$Device already set up"
	exit 1
fi

echo "Volume=$Volume"
echo "Block=$Block"
echo "Host=$Host"
echo "Port=$Port"
echo "Timeout=$Timeout"
echo "Q_Add_Random=$Q_Add_Random"
echo "$Q_Add_Random" > /sys/block/$NBD/queue/add_random || exit 1
echo "Q_Max_Sectors_KB=$Q_Max_Sectors_KB"
echo "$Q_Max_Sectors_KB" > /sys/block/$NBD/queue/max_sectors_kb || exit 1
echo "Q_NoMerges=$Q_NoMerges"
echo "$Q_NoMerges" > /sys/block/$NBD/queue/nomerges || exit 1
echo "Q_NR_Requests=$Q_NR_Requests"
echo "$Q_NR_Requests" > /sys/block/$NBD/queue/nr_requests || exit 1
echo "Q_Read_Ahead_KB=$Q_Read_Ahead_KB"
echo "$Q_Read_Ahead_KB" > /sys/block/$NBD/queue/read_ahead_kb || exit 1
echo "Q_Scheduler=$Q_Scheduler"
echo "$Q_Scheduler" > /sys/block/$NBD/queue/scheduler || exit 1

nbd-client $Host $Port $Device -b $Block -t $Timeout -N $Volume
retval=$?
if [ $retval -ne 0 ]
then
	echo "FAILURE: nbd-client returned $retval"
	exit 1
fi
exit 0
