/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#define SYSLOG_NAMES
#include "common.h"
#include "config.h"

#include "syslog.h"

static int config_module_initialized = 0;

/* comment */
static const char *patt_comment = "^(#|;)|^[[:blank:]]*$";
static regex_t preg_comment;

/* key = value pair */
static const char *patt_keyval =
	"^[[:blank:]]*([[:alnum:]_.]+)[[:blank:]]*="
	"[[:blank:]]*([[:graph:]]+)[[:blank:]]*$";
static regex_t preg_keyval;

/* key = "quoted value" */
static const char *patt_quoted =
	"^[[:blank:]]*([[:alnum:]_.]+)[[:blank:]]*="
	"[[:blank:]]*\"([[:print:]]*)\"[[:blank:]]*$";
static regex_t preg_quoted;

/* command line options */
const char *opt_config = DEF_CONFIG;
int opt_debug = DEF_DEBUG;

/* configuration file options */
char *cfg_syslog_ident = NULL;
int cfg_syslog_facility = -1;
int cfg_syslog_priority = -1;
char *cfg_pid_file = NULL;
char *cfg_work_directory = NULL;
char *cfg_ctrl_directory = NULL;
char *cfg_data_directory = NULL;
int cfg_sync = -1;
int cfg_flush = -1;
int cfg_trim = -1;
char *cfg_vty_listen_ip = NULL;
int cfg_vty_listen_port = -1;
int cfg_vty_listen_backlog = -1;
int cfg_max_vty_clients = -1;
int cfg_vty_timeout = -1;
char *cfg_nbd_listen_ip = NULL;
int cfg_nbd_listen_port = -1;
int cfg_nbd_listen_backlog = -1;
int cfg_max_nbd_clients = -1;
int cfg_nbd_negotiate_timeout = -1;
int cfg_nbd_send_timeout = -1;
int cfg_nbd_keepalive_idle = -1;
int cfg_nbd_keepalive_interval = -1;
int cfg_nbd_keepalive_count = -1;
int cfg_nbd_receive_sockbuf_kb = -1;
int cfg_nbd_send_sockbuf_kb = -1;
int cfg_aio_workers = -1;
int cfg_aio_queue_length = -1;
int cfg_bio_workers = -1;
int cfg_request_buffer_kb = -1;
int cfg_max_requests = -1;
int cfg_max_read_requests_kb = -1;
int cfg_max_write_requests_kb = -1;
int cfg_max_mirror_requests_kb = -1;
int cfg_mirror_request_kb = -1;
int cfg_mirror_queue_length = -1;
int cfg_max_request_burst = -1;
int cfg_max_request_burst_kb = -1;
int cfg_max_reply_burst = -1;
int cfg_max_reply_burst_kb = -1;
int cfg_stats_interval = -1;

void
config_module_setup(void)
{
	char errbuf[1024];
	const char *patt;
	regex_t *preg;
	int errcode;

	assert(!config_module_initialized);
	errcode = regcomp(&preg_comment, patt_comment,
	    REG_EXTENDED | REG_NOSUB);
	if (errcode != 0) {
		patt = patt_comment;
		preg = &preg_comment;
		goto fail;
	}
	errcode = regcomp(&preg_keyval, patt_keyval, REG_EXTENDED);
	if (errcode != 0) {
		patt = patt_keyval;
		preg = &preg_keyval;
		goto fail;
	}
	errcode = regcomp(&preg_quoted, patt_quoted, REG_EXTENDED);
	if (errcode != 0) {
		patt = patt_quoted;
		preg = &preg_quoted;
		goto fail;
	}

	/* initialize configuration file options */
	cfg_syslog_ident = strdup(DEF_SYSLOG_IDENT);
	assert(cfg_syslog_ident != NULL);
	cfg_syslog_facility = DEF_SYSLOG_FACILITY;
	cfg_syslog_priority = DEF_SYSLOG_PRIORITY;
	cfg_pid_file = strdup(DEF_PID_FILE);
	assert(cfg_pid_file != NULL);
	cfg_work_directory = strdup(DEF_WORK_DIRECTORY);
	assert(cfg_work_directory != NULL);
	cfg_ctrl_directory = strdup(DEF_CTRL_DIRECTORY);
	assert(cfg_ctrl_directory != NULL);
	cfg_data_directory = strdup(DEF_DATA_DIRECTORY);
	assert(cfg_data_directory != NULL);
	cfg_sync = DEF_SYNC;
	cfg_flush = DEF_FLUSH;
	cfg_trim = DEF_TRIM;
	cfg_vty_listen_ip = strdup(DEF_VTY_LISTEN_IP);
	assert(cfg_vty_listen_ip != NULL);
	cfg_vty_listen_port = DEF_VTY_LISTEN_PORT;
	cfg_vty_listen_backlog = DEF_VTY_LISTEN_BACKLOG;
	cfg_max_vty_clients = DEF_MAX_VTY_CLIENTS;
	cfg_vty_timeout = DEF_VTY_TIMEOUT;
	cfg_nbd_listen_ip = strdup(DEF_NBD_LISTEN_IP);
	assert(cfg_nbd_listen_ip != NULL);
	cfg_nbd_listen_port = DEF_NBD_LISTEN_PORT;
	cfg_nbd_listen_backlog = DEF_NBD_LISTEN_BACKLOG;
	cfg_max_nbd_clients = DEF_MAX_NBD_CLIENTS;
	cfg_nbd_negotiate_timeout = DEF_NBD_NEGOTIATE_TIMEOUT;
	cfg_nbd_send_timeout = DEF_NBD_SEND_TIMEOUT;
	cfg_nbd_keepalive_idle = DEF_NBD_KEEPALIVE_IDLE;
	cfg_nbd_keepalive_interval = DEF_NBD_KEEPALIVE_INTERVAL;
	cfg_nbd_keepalive_count = DEF_NBD_KEEPALIVE_COUNT;
	cfg_nbd_receive_sockbuf_kb = DEF_NBD_RECEIVE_SOCKBUF_KB;
	cfg_nbd_send_sockbuf_kb = DEF_NBD_SEND_SOCKBUF_KB;
	cfg_aio_workers = DEF_AIO_WORKERS;
	cfg_aio_queue_length = DEF_AIO_QUEUE_LENGTH;
	cfg_bio_workers = DEF_BIO_WORKERS;
	cfg_request_buffer_kb = DEF_REQUEST_BUFFER_KB;
	cfg_max_requests = DEF_MAX_REQUESTS;
	cfg_max_read_requests_kb = DEF_MAX_READ_REQUESTS_KB;
	cfg_max_write_requests_kb = DEF_MAX_WRITE_REQUESTS_KB;
	cfg_max_mirror_requests_kb = DEF_MAX_MIRROR_REQUESTS_KB;
	cfg_mirror_request_kb = DEF_MIRROR_REQUEST_KB;
	cfg_mirror_queue_length = DEF_MIRROR_QUEUE_LENGTH;
	cfg_max_request_burst = DEF_MAX_REQUEST_BURST;
	cfg_max_request_burst_kb = DEF_MAX_REQUEST_BURST_KB;
	cfg_max_reply_burst = DEF_MAX_REPLY_BURST;
	cfg_max_reply_burst_kb = DEF_MAX_REPLY_BURST_KB;
	cfg_stats_interval = DEF_STATS_INTERVAL;

	config_module_initialized = 1;
	return;
fail:
	regerror(errcode, preg, errbuf, sizeof(errbuf));
	fprintf(stderr, "regcomp(%s): %s\n", patt, errbuf);
	exit(EXIT_FAILURE);
}

void
config_module_teardown(void)
{

	assert(config_module_initialized);
	free(cfg_nbd_listen_ip);
	free(cfg_vty_listen_ip);
	free(cfg_data_directory);
	free(cfg_ctrl_directory);
	free(cfg_work_directory);
	free(cfg_pid_file);
	free(cfg_syslog_ident);
	regfree(&preg_quoted);
	regfree(&preg_keyval);
	regfree(&preg_comment);
	config_module_initialized = 0;
}

void
config_load(void)
{
	char ln[1024];
	char *key, *ptr, *val;
	FILE *fp;
	const CODE *facility, *priority;
	int found, intval;
	regmatch_t pmatch[3];

	assert(config_module_initialized);
	if ((fp = fopen(opt_config, "r")) == NULL) {
		fprintf(stderr,
		    "error: %s(): fopen(%s): %s\n",
		    __func__, opt_config, strerror(errno));
		exit(EXIT_FAILURE);
	}

	for (;;) {
		if (fgets(ln, sizeof(ln), fp) == NULL) {
			if (ferror(fp)) {
				fclose(fp);
				fprintf(stderr,
				    "error: %s(): Error reading config\n",
				    __func__);
				break;
			}
			if (feof(fp)) {
				fclose(fp);
				return;
			}
		}
		ptr = strchr(ln, '\n');
		if (ptr == NULL) {
			fclose(fp);
			fprintf(stderr,
			    "error: %s(): Config line too long\n",
			    __func__);
			break;
		}
		*ptr = '\0';

		/* comment */
		if (regexec(&preg_comment, ln, 0, NULL, 0) == 0)
			continue;

		/* key = value */
		if (regexec(&preg_quoted, ln, 3, pmatch, 0) != 0 &&
		    regexec(&preg_keyval, ln, 3, pmatch, 0) != 0) {
			fprintf(stderr,
			    "error: %s(): Broken config line `%s'\n",
			    __func__, ln);
			break;
		}

		key = ln + pmatch[1].rm_so;
		*(ln + pmatch[1].rm_eo) = '\0';
		val = ln + pmatch[2].rm_so;
		*(ln + pmatch[2].rm_eo) = '\0';

		if (strcmp(key, "syslog_ident") == 0) {
			free(cfg_syslog_ident);
			cfg_syslog_ident = strdup(val);
			assert(cfg_syslog_ident != NULL);
			syslog_module_setup();
			continue;
		}

		if (strcmp(key, "syslog_facility") == 0) {
			facility = facilitynames;
			found = 0;
			while (facility->c_name != NULL) {
				if (strcasecmp(facility->c_name, val) == 0) {
					cfg_syslog_facility = facility->c_val;
					found = 1;
					break;
				}
				++facility;
			}
			if (found) {
				syslog_module_setup();
				continue;
			}
			goto bad_val;
		}

		if (strcmp(key, "syslog_priority") == 0) {
			priority = prioritynames;
			found = 0;
			while (priority->c_name != NULL) {
				if (strcasecmp(priority->c_name, val) == 0) {
					cfg_syslog_priority = priority->c_val;
					found = 1;
					break;
				}
				++priority;
			}
			if (found) {
				syslog_module_setup();
				continue;
			}
			goto bad_val;
		}

		if (strcmp(key, "pid_file") == 0) {
			free(cfg_pid_file);
			cfg_pid_file = strdup(val);
			assert(cfg_pid_file != NULL);
			continue;
		}

		if (strcmp(key, "work_directory") == 0) {
			free(cfg_work_directory);
			cfg_work_directory = strdup(val);
			assert(cfg_work_directory != NULL);
			continue;
		}

		if (strcmp(key, "ctrl_directory") == 0) {
			free(cfg_ctrl_directory);
			cfg_ctrl_directory = strdup(val);
			assert(cfg_ctrl_directory != NULL);
			continue;
		}

		if (strcmp(key, "data_directory") == 0) {
			free(cfg_data_directory);
			cfg_data_directory = strdup(val);
			assert(cfg_data_directory != NULL);
			continue;
		}

		if (strcmp(key, "sync") == 0) {
			cfg_sync = atoi(val);
			continue;
		}

		if (strcmp(key, "flush") == 0) {
			cfg_flush = atoi(val);
			continue;
		}

		if (strcmp(key, "trim") == 0) {
			cfg_trim = atoi(val);
			continue;
		}

		if (strcmp(key, "vty_listen_ip") == 0) {
			free(cfg_vty_listen_ip);
			cfg_vty_listen_ip = strdup(val);
			assert(cfg_vty_listen_ip != NULL);
			continue;
		}

		if (strcmp(key, "vty_listen_port") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 65535)
				goto bad_intval;
			cfg_vty_listen_port = intval;
			continue;
		}

		if (strcmp(key, "vty_listen_backlog") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 65535)
				goto bad_intval;
			cfg_vty_listen_backlog = intval;
			continue;
		}

		if (strcmp(key, "max_vty_clients") == 0) {
			intval = atoi(val);
			if (intval < 1)
				goto bad_intval;
			cfg_max_vty_clients = intval;
			continue;
		}

		if (strcmp(key, "vty_timeout") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 86400)
				goto bad_intval;
			cfg_vty_timeout = intval;
			continue;
		}

		if (strcmp(key, "nbd_listen_ip") == 0) {
			free(cfg_nbd_listen_ip);
			cfg_nbd_listen_ip = strdup(val);
			assert(cfg_nbd_listen_ip != NULL);
			continue;
		}

		if (strcmp(key, "nbd_listen_port") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 65535)
				goto bad_intval;
			cfg_nbd_listen_port = intval;
			continue;
		}

		if (strcmp(key, "nbd_listen_backlog") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 65535)
				goto bad_intval;
			cfg_nbd_listen_backlog = intval;
			continue;
		}

		if (strcmp(key, "max_nbd_clients") == 0) {
			intval = atoi(val);
			if (intval < 1)
				goto bad_intval;
			cfg_max_nbd_clients = intval;
			continue;
		}

		if (strcmp(key, "nbd_negotiate_timeout") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 86400)
				goto bad_intval;
			cfg_nbd_negotiate_timeout = intval;
			continue;
		}

		if (strcmp(key, "nbd_send_timeout") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 86400)
				goto bad_intval;
			cfg_nbd_send_timeout = intval;
			continue;
		}

		if (strcmp(key, "nbd_keepalive_idle") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 86400)
				goto bad_intval;
			cfg_nbd_keepalive_idle = intval;
			continue;
		}

		if (strcmp(key, "nbd_keepalive_interval") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 86400)
				goto bad_intval;
			cfg_nbd_keepalive_interval = intval;
			continue;
		}

		if (strcmp(key, "nbd_keepalive_count") == 0) {
			intval = atoi(val);
			if (intval < 1)
				goto bad_intval;
			cfg_nbd_keepalive_count = intval;
			continue;
		}

		if (strcmp(key, "nbd_receive_sockbuf_kb") == 0) {
			intval = atoi(val);
			if (intval > (INT_MAX >> 10))
				goto bad_intval;
			cfg_nbd_receive_sockbuf_kb = intval;
			continue;
		}

		if (strcmp(key, "nbd_send_sockbuf_kb") == 0) {
			intval = atoi(val);
			if (intval > (INT_MAX >> 10))
				goto bad_intval;
			cfg_nbd_send_sockbuf_kb = intval;
			continue;
		}

		if (strcmp(key, "aio_workers") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 128)
				goto bad_intval;
			cfg_aio_workers = intval;
			continue;
		}

		if (strcmp(key, "aio_queue_length") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 65536)
				goto bad_intval;
			cfg_aio_queue_length = intval;
			continue;
		}

		if (strcmp(key, "bio_workers") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 128)
				goto bad_intval;
			cfg_bio_workers = intval;
			continue;
		}

		if (strcmp(key, "request_buffer_kb") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > (INT_MAX >> 10))
				goto bad_intval;
			cfg_request_buffer_kb = intval;
			continue;
		}

		if (strcmp(key, "max_requests") == 0) {
			intval = atoi(val);
			if (intval < 0)
				goto bad_intval;
			cfg_max_requests = intval;
			continue;
		}

		if (strcmp(key, "max_read_requests_kb") == 0) {
			intval = atoi(val);
			if (intval > (INT_MAX >> 10))
				goto bad_intval;
			cfg_max_read_requests_kb = intval;
			continue;
		}

		if (strcmp(key, "max_write_requests_kb") == 0) {
			intval = atoi(val);
			if (intval > (INT_MAX >> 10))
				goto bad_intval;
			cfg_max_write_requests_kb = intval;
			continue;
		}

		if (strcmp(key, "max_mirror_requests_kb") == 0) {
			intval = atoi(val);
			if (intval > (INT_MAX >> 10))
				goto bad_intval;
			cfg_max_mirror_requests_kb = intval;
			continue;
		}

		if (strcmp(key, "mirror_request_kb") == 0) {
			intval = atoi(val);
			if (intval < 4 || intval > 32768 ||
			    intval & 3)
				goto bad_intval;
			cfg_mirror_request_kb = intval;
			continue;
		}

		if (strcmp(key, "mirror_queue_length") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > 128)
				goto bad_intval;
			cfg_mirror_queue_length = intval;
			continue;
		}

		if (strcmp(key, "max_request_burst") == 0) {
			intval = atoi(val);
			if (intval < 0)
				goto bad_intval;
			cfg_max_request_burst = intval;
			continue;
		}

		if (strcmp(key, "max_request_burst_kb") == 0) {
			intval = atoi(val);
			if (intval > (INT_MAX >> 10))
				goto bad_intval;
			cfg_max_request_burst_kb = intval;
			continue;
		}

		if (strcmp(key, "max_reply_burst") == 0) {
			intval = atoi(val);
			if (intval < 1 || intval > IOV_MAX)
				goto bad_intval;
			cfg_max_reply_burst = intval;
			continue;
		}

		if (strcmp(key, "max_reply_burst_kb") == 0) {
			intval = atoi(val);
			if (intval > (INT_MAX >> 10))
				goto bad_intval;
			cfg_max_reply_burst_kb = intval;
			continue;
		}

		if (strcmp(key, "stats_interval") == 0) {
			intval = atoi(val);
			if (intval < 0 || intval > 86400)
				goto bad_intval;
			cfg_stats_interval = intval;
			continue;
		}

		fprintf(stderr,
		    "error: %s(): Unknown config option `%s'\n",
		    __func__, key);
		break;
bad_val:
		fprintf(stderr,
		    "error: %s(): Incorrect %s value: `%s'\n",
		    __func__, key, val);
		break;
bad_intval:
		fprintf(stderr,
		    "error: %s(): Incorrect %s value: %d\n",
		    __func__, key, intval);
		break;
	}
	exit(EXIT_FAILURE);
}

void
config_dump(void)
{
	const CODE *facility, *priority;

	assert(config_module_initialized);
	facility = facilitynames;
	while (facility->c_name != NULL) {
		if (facility->c_val == cfg_syslog_facility)
			break;
		++facility;
	}
	assert(facility->c_name != NULL);

	priority = prioritynames;
	while (priority->c_name != NULL) {
		if (priority->c_val == cfg_syslog_priority)
			break;
		++priority;
	}
	assert(priority->c_name != NULL);

	syslog(LOG_INFO,
	    "syslog_ident=%s "
	    "syslog_facility=%s syslog_priority=%s "
	    "pid_file=%s work_directory=%s "
	    "ctrl_directory=%s data_directory=%s "
	    "sync=%d flush=%d trim=%d "
	    "vty_listen_ip=%s vty_listen_port=%d "
	    "vty_listen_backlog=%d max_vty_clients=%d "
	    "vty_timeout=%d "
	    "nbd_listen_ip=%s nbd_listen_port=%d "
	    "nbd_listen_backlog=%d max_nbd_clients=%d "
	    "nbd_negotiate_timeout=%d nbd_send_timeout=%d "
	    "nbd_keepalive_idle=%d nbd_keepalive_interval=%d "
	    "nbd_keepalive_count=%d "
	    "nbd_receive_sockbuf_kb=%d nbd_send_sockbuf_kb=%d "
	    "aio_workers=%d aio_queue_length=%d "
	    "bio_workers=%d "
	    "request_buffer_kb=%d max_requests=%d "
	    "max_read_requests_kb=%d max_write_requests_kb=%d "
	    "max_mirror_requests_kb=%d "
	    "mirror_request_kb=%d mirror_queue_length=%d "
	    "max_request_burst=%d max_request_burst_kb=%d "
	    "max_reply_burst=%d max_reply_burst_kb=%d "
	    "stats_interval=%d",
	    cfg_syslog_ident,
	    facility->c_name, priority->c_name,
	    cfg_pid_file, cfg_work_directory,
	    cfg_ctrl_directory, cfg_data_directory,
	    cfg_sync, cfg_flush, cfg_trim,
	    cfg_vty_listen_ip, cfg_vty_listen_port,
	    cfg_vty_listen_backlog, cfg_max_vty_clients,
	    cfg_vty_timeout,
	    cfg_nbd_listen_ip, cfg_nbd_listen_port,
	    cfg_nbd_listen_backlog, cfg_max_nbd_clients,
	    cfg_nbd_negotiate_timeout, cfg_nbd_send_timeout,
	    cfg_nbd_keepalive_idle, cfg_nbd_keepalive_interval,
	    cfg_nbd_keepalive_count,
	    cfg_nbd_receive_sockbuf_kb, cfg_nbd_send_sockbuf_kb,
	    cfg_aio_workers, cfg_aio_queue_length,
	    cfg_bio_workers,
	    cfg_request_buffer_kb, cfg_max_requests,
	    cfg_max_read_requests_kb, cfg_max_write_requests_kb,
	    cfg_max_mirror_requests_kb,
	    cfg_mirror_request_kb, cfg_mirror_queue_length,
	    cfg_max_request_burst, cfg_max_request_burst_kb,
	    cfg_max_reply_burst, cfg_max_reply_burst_kb,
	    cfg_stats_interval
	);
}
