/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

struct writer {
	TAILQ_ENTRY(writer)	all_wrrl_ary[2];
	struct thread		*thr;
#define WRR_FL_INUSE		0x00000001
#define WRR_FL_ABORT		0x00000002
	int			flags;
	int			pad;
	struct request_list	*aux_reql;
	struct iovec		*aux_iov_ary;
	int			aux_iov_ary_len;
	int			pad2;
	int64_t			cur_req_id;
	struct session		*sesp;
};
TAILQ_HEAD(writer_head, writer);

struct writer_list {
	pthread_mutex_t		mutex;
	struct writer_head	head;
#define WRRL_IDX_ALLOC		0
#define WRRL_IDX_STD		1
	int			index;
	int			count;
	int			peak;
};

extern struct writer_list *connected_wrrl;
extern struct writer_list *blocked_wrrl;

int writer_module_setup(void);
void writer_module_hello(void);
void writer_module_teardown(void);
void writer_module_stats(void);
int writer_alloc(struct writer **);
void writer_init(struct writer *);
void writer_uninit(struct writer *);
void writer_free(struct writer *);
int writer_thread_create(struct writer *);
void writer_thread_launch(struct writer *);
void writer_thread_join(struct writer *);
void writer_thread_shutdown(struct writer *);
int writer_list_alloc(struct writer_list **, int);
void writer_list_init(struct writer_list *, int);
void writer_list_uninit(struct writer_list *);
void writer_list_free(struct writer_list *);
void writer_list_lock(struct writer_list *);
void writer_list_unlock(struct writer_list *);
int writer_list_empty(struct writer_list *);
struct writer *writer_list_first(struct writer_list *);
struct writer *writer_list_next(struct writer_list *, struct writer *);
void writer_list_insert(struct writer_list *, struct writer *);
void writer_list_append(struct writer_list *, struct writer *);
void writer_list_remove(struct writer_list *, struct writer *);
