/*-
 * Copyright (c) 2013-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "io.h"
#include "thread.h"
#include "request.h"
#include "skiver.h"
#include "mirror.h"
#include "volume.h"

static int skiver_module_initialized = 0;
static pthread_mutex_t skiver_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct skiver_list *available_skvl = NULL;
static struct skiver_list *inuse_skvl = NULL;
static struct request_list *incoming_reql = NULL;
static struct skiver_list *waiting_skvl = NULL;
static struct skiver_list *running_skvl = NULL;

static void skiver_dealloc(struct skiver *);
static void skiver_list_dealloc(struct skiver_list *);
static void skiver_thread_shutdown(struct skiver *);
static void *skiver_thread(void *);
static void skiver_thread_loop(struct skiver *);
static void skiver_list_shutdown(struct skiver_list *);

int
skiver_module_setup(void)
{
	struct skiver *skvp;
	int count, i;

	assert(!skiver_module_initialized);
	assert(incoming_reql == NULL);
	if (request_list_alloc(&incoming_reql, REQL_IDX_STD) < 0) {
		assert(incoming_reql == NULL);
		goto out0;
	}
	assert(incoming_reql != NULL);
	assert(available_skvl == NULL);
	if (skiver_list_alloc(&available_skvl, SKVL_IDX_ALLOC) < 0) {
		assert(available_skvl == NULL);
		goto out1;
	}
	assert(available_skvl != NULL);
	assert(inuse_skvl == NULL);
	if (skiver_list_alloc(&inuse_skvl, SKVL_IDX_ALLOC) < 0) {
		assert(inuse_skvl == NULL);
		goto out2;
	}
	assert(inuse_skvl != NULL);
	assert(waiting_skvl == NULL);
	if (skiver_list_alloc(&waiting_skvl, SKVL_IDX_STD) < 0) {
		assert(waiting_skvl == NULL);
		goto out3;
	}
	assert(waiting_skvl != NULL);
	assert(running_skvl == NULL);
	if (skiver_list_alloc(&running_skvl, SKVL_IDX_STD) < 0) {
		assert(running_skvl == NULL);
		goto out4;
	}
	assert(running_skvl != NULL);
	skiver_module_initialized = 1;

	for (i = 0; i < cfg_bio_workers; i++) {
		skvp = NULL;
		if (skiver_alloc(&skvp) < 0) {
			assert(skvp == NULL);
			break;
		}
		assert(skvp != NULL);
		if (thread_create(skvp->thr, "skiver", skiver_thread) < 0) {
			skiver_free(skvp);
			break;
		}
		thread_launch(skvp->thr);
	}
	for (;;) {
		request_list_lock(incoming_reql);
		count = waiting_skvl->count;
		request_list_unlock(incoming_reql);
		assert(count <= i);
		NOTICE("%d of %d BIO workers UP", count, i);
		if (count == i)
			break;
		usleep(100000);
	}
	if (count < cfg_bio_workers) {
		ERR("ONLY %d of %d BIO workers UP, ABORTING",
		    count, cfg_bio_workers);
		skiver_module_teardown();
		return (-1);
	}
	return (0);
out4:
	assert(waiting_skvl != NULL);
	skiver_list_free(waiting_skvl);
	waiting_skvl = NULL;
out3:
	assert(inuse_skvl != NULL);
	skiver_list_free(inuse_skvl);
	inuse_skvl = NULL;
out2:
	assert(available_skvl != NULL);
	skiver_list_dealloc(available_skvl);
	skiver_list_free(available_skvl);
	available_skvl = NULL;
out1:
	assert(incoming_reql != NULL);
	request_list_free(incoming_reql);
	incoming_reql = NULL;
out0:
	return (-1);
}

void
skiver_module_hello(void)
{

	DEBUG("sizeof(skiver)=%zu", sizeof(struct skiver));
	DEBUG("sizeof(skiver_list)=%zu", sizeof(struct skiver_list));
}

void
skiver_module_teardown(void)
{
	int count, total;

	assert(skiver_module_initialized);
	PTHREAD_MUTEX_LOCK(&skiver_mutex);
	total = available_skvl->count + inuse_skvl->count;
	PTHREAD_MUTEX_UNLOCK(&skiver_mutex);
	assert(total >= 0);
	for (;;) {
		request_list_lock(incoming_reql);
		count = waiting_skvl->count;
		request_list_unlock(incoming_reql);
		assert(count >= 0 && count <= total);
		if (count < total) {
			NOTICE("%d of %d BIO workers still RUNNING",
			    total - count, total);
			usleep((useconds_t)200000);
			continue;
		}
		break;
	}
	assert(running_skvl != NULL);
	skiver_list_free(running_skvl);
	running_skvl = NULL;
	assert(waiting_skvl != NULL);
	skiver_list_shutdown(waiting_skvl);
	skiver_list_free(waiting_skvl);
	waiting_skvl = NULL;
	assert(inuse_skvl != NULL);
	skiver_list_free(inuse_skvl);
	inuse_skvl = NULL;
	assert(available_skvl != NULL);
	skiver_list_dealloc(available_skvl);
	skiver_list_free(available_skvl);
	available_skvl = NULL;
	assert(incoming_reql != NULL);
	request_list_free(incoming_reql);
	incoming_reql = NULL;
}

void
skiver_module_stats(void)
{
	int available_count, available_peak, inuse_count, inuse_peak,
	    incoming_count, incoming_peak, running_count, running_peak,
	    waiting_count, waiting_peak;

	assert(skiver_module_initialized);
	PTHREAD_MUTEX_LOCK(&skiver_mutex);
	available_count = available_skvl->count;
	available_peak = available_skvl->peak;
	available_skvl->peak = available_skvl->count;
	inuse_count = inuse_skvl->count;
	inuse_peak = inuse_skvl->peak;
	inuse_skvl->peak = inuse_skvl->count;
	PTHREAD_MUTEX_UNLOCK(&skiver_mutex);
	NOTICE("inuse=%d/%d available=%d/%d",
	    inuse_count, inuse_peak, available_count, available_peak);

	request_list_lock(incoming_reql);
	incoming_count = incoming_reql->count;
	incoming_peak = incoming_reql->peak;
	incoming_reql->peak = incoming_reql->count;
	running_count = running_skvl->count;
	running_peak = running_skvl->peak;
	running_skvl->peak = running_skvl->count;
	waiting_count = waiting_skvl->count;
	waiting_peak = waiting_skvl->peak;
	waiting_skvl->peak = waiting_skvl->count;
	request_list_unlock(incoming_reql);
	NOTICE("incoming_requests=%d/%d running_skivers=%d/%d "
	    "waiting_skivers=%d/%d",
	    incoming_count, incoming_peak, running_count, running_peak,
	    waiting_count, waiting_peak);
}

int
skiver_alloc(struct skiver **skvpp)
{
	struct skiver *skvp;

	assert(skiver_module_initialized);
	PTHREAD_MUTEX_LOCK(&skiver_mutex);
	skvp = skiver_list_first(available_skvl);
	if (skvp == NULL) {
		assert(available_skvl->count == 0);
		PTHREAD_MUTEX_UNLOCK(&skiver_mutex);
		skvp = malloc(sizeof(*skvp));
		if (skvp == NULL) {
			ERR("malloc(%zu): %s", sizeof(*skvp), strerror(errno));
			return (-2);
		}
		skvp->thr = NULL;
		if (thread_alloc(&skvp->thr, skvp) < 0) {
			assert(skvp->thr == NULL);
			free(skvp);
			return (-3);
		}
		assert(skvp->thr != NULL);
		skiver_init(skvp);
		PTHREAD_MUTEX_LOCK(&skiver_mutex);
	} else {
		skiver_list_remove(available_skvl, skvp);
	}
	assert(!skvp->flags);
	skvp->flags |= SKV_FL_INUSE;
	skiver_list_insert(inuse_skvl, skvp);
	PTHREAD_MUTEX_UNLOCK(&skiver_mutex);
	*skvpp = skvp;
	return (0);
}

void
skiver_init(struct skiver *skvp)
{

	skvp->flags = 0;
}

void
skiver_uninit(struct skiver *skvp)
{

	thread_uninit(skvp->thr);
	assert(skvp->flags == SKV_FL_INUSE);
}

void
skiver_free(struct skiver *skvp)
{

	assert(skiver_module_initialized);
	skiver_uninit(skvp);
	PTHREAD_MUTEX_LOCK(&skiver_mutex);
	skiver_list_remove(inuse_skvl, skvp);
	skvp->flags &= ~SKV_FL_INUSE;
	assert(!skvp->flags);
	skiver_list_insert(available_skvl, skvp);
	PTHREAD_MUTEX_UNLOCK(&skiver_mutex);
}

static
void
skiver_dealloc(struct skiver *skvp)
{

	assert(!skvp->flags);
	thread_free(skvp->thr);
	free(skvp);
}

void
skiver_thread_wakeup(struct skiver *skvp)
{

	thread_wakeup(skvp->thr);
}

static
void
skiver_thread_shutdown(struct skiver *skvp)
{

	request_list_lock(incoming_reql);
	thread_shutdown_begin(skvp->thr);
	skiver_thread_wakeup(skvp);
	request_list_unlock(incoming_reql);
}

void
skiver_reql_submit(struct request_list *reqlp)
{
	struct request *nreqp, *reqp;
	struct skiver *skvp;
	int peak;

	assert(reqlp->count >= 0);
	if (reqlp->count == 0)
		return;
	peak = 0;
	if (reqlp->count > reqlp->submit_peak) {
		reqlp->submit_peak = reqlp->count;
		peak = 1;
	}
	if (reqlp->submit_bytes > reqlp->submit_bytes_peak) {
		reqlp->submit_bytes_peak = reqlp->submit_bytes;
		peak = 1;
	}
	if (peak)
		DEBUG("request_burst_peak=%d/%lld",
		    reqlp->count, (long long)reqlp->submit_bytes);
	reqlp->submit_bytes = (int64_t)0;
	nreqp = request_list_first(reqlp);
	for (;;) {
		reqp = nreqp;
		if (reqp == NULL)
			break;
		nreqp = request_list_next(reqlp, reqp);
		request_list_remove(reqlp, reqp);
		request_list_lock(incoming_reql);
		skvp = skiver_list_first(waiting_skvl);
		if (skvp == NULL) {
			request_list_append(incoming_reql, reqp);
			request_list_concat(incoming_reql, reqlp);
			request_list_unlock(incoming_reql);
			break;
		}
		request_list_append(incoming_reql, reqp);
		skiver_list_remove(waiting_skvl, skvp);
		skiver_list_append(running_skvl, skvp);
		request_list_unlock(incoming_reql);
		skiver_thread_wakeup(skvp);
	}
	assert(request_list_empty(reqlp));
}

static
void *
skiver_thread(void *data)
{
	struct skiver *skvp;

	skvp = (struct skiver *)data;
	assert(skvp != NULL);
	assert(skvp->thr != NULL);

	thread_prologue(skvp->thr);

	skiver_thread_loop(skvp);

	thread_epilogue(NULL);
	return (NULL);
}

static
void
skiver_thread_loop(struct skiver *skvp)
{
	struct request *reqp;
	int error;

	/* off of any list */
	request_list_lock(incoming_reql);
	skiver_list_insert(running_skvl, skvp);
	request_list_unlock(incoming_reql);

	for (;;) {
		/* on the running_skvl with the incoming_reql unlocked */
		request_list_lock(incoming_reql);
retry:
		reqp = request_list_first(incoming_reql);
		if (reqp == NULL) {
			skiver_list_remove(running_skvl, skvp);
			skiver_list_insert(waiting_skvl, skvp);
			thread_wait_mutex(skvp->thr, &incoming_reql->mutex);
			if (THREAD_SHUTDOWN_PENDING(skvp->thr)) {
				request_list_unlock(incoming_reql);
				thread_shutdown_commit(skvp->thr);
				break;
			}
			goto retry;
		}
		assert(reqp != NULL);
		request_list_remove(incoming_reql, reqp);
		request_list_unlock(incoming_reql);

		switch (reqp->type) {
		case REQ_TY_TRIM:
			assert(reqp->nbd.request.type == NBD_CMD_TRIM);
			assert(reqp->volp != NULL);
			assert(reqp->outgoing_reqlp != NULL);
			assert(reqp->mirp == NULL);
			assert(reqp->master_reqp == NULL);
			break;
		case REQ_TY_MASTER_TRIM:
			assert(reqp->nbd.request.type == NBD_CMD_TRIM);
			assert(reqp->volp != NULL);
			assert(reqp->outgoing_reqlp != NULL);
			assert(reqp->mirp == NULL);
			assert(reqp->master_reqp == reqp);
			break;
		case REQ_TY_SLAVE_TRIM:
			assert(reqp->nbd.request.type == NBD_CMD_TRIM);
			assert(reqp->volp == NULL);
			assert(reqp->outgoing_reqlp != NULL);
			assert(reqp->mirp != NULL);
			assert(reqp->master_reqp != NULL);
			assert(reqp->master_reqp != reqp);
			break;
		default:
			abort();
		}

		if (reqp->flags & REQ_FL_ABORT) {
			request_done(reqp, 0xABABABAB);
			continue;
		}

		if (reqp->mirp != NULL && reqp->mirp->flags2 & MIR_FL2_ISBLK)
			error = fd_blkdevdelete(reqp->fd,
			    reqp->nbd.request.from,
			    (uint64_t)reqp->nbd.request.len);
		else
			error = fd_delete(reqp->fd,
			    reqp->nbd.request.from,
			    (uint64_t)reqp->nbd.request.len);
		if (error < 0) {
			request_done(reqp, error);
			continue;
		}

		if (cfg_sync == 1)
			error = 0;
		else
#ifdef __linux__
			error = fdatasync(reqp->fd);
#else
			error = fsync(reqp->fd);
#endif
		if (error < 0) {
			error = -errno;
#ifdef __linux__
			WARNING("fdatasync(): %s", strerror(errno));
#else
			WARNING("fsync(): %s", strerror(errno));
#endif
		}
		request_done(reqp, error);
	}
}

int
skiver_list_alloc(struct skiver_list **skvlpp, int index)
{
	struct skiver_list *skvlp;
	int error;

	skvlp = malloc(sizeof(*skvlp));
	if (skvlp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*skvlp), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&skvlp->mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(skvlp);
		return (-3);
	}
	skiver_list_init(skvlp, index);
	*skvlpp = skvlp;
	return (0);
}

void
skiver_list_init(struct skiver_list *skvlp, int index)
{
#ifndef NDEBUG
	struct skiver *skvp;
#endif

	assert(index >= 0 && index < NELEM(skvp->all_skvl_ary));
	TAILQ_INIT(&skvlp->head);
	skvlp->index = index;
	skvlp->count = 0;
	skvlp->peak = 0;
}

void
skiver_list_uninit(struct skiver_list *skvlp)
{

	assert(TAILQ_EMPTY(&skvlp->head));
	assert(skvlp->count == 0);
	skvlp->peak = 0;
}

void
skiver_list_free(struct skiver_list *skvlp)
{

	skiver_list_uninit(skvlp);
	PTHREAD_MUTEX_DESTROY(&skvlp->mutex);
	free(skvlp);
}

static
void
skiver_list_dealloc(struct skiver_list *skvlp)
{
	struct skiver *nskvp, *skvp;

	nskvp = skiver_list_first(skvlp);
	for (;;) {
		skvp = nskvp;
		if (skvp == NULL)
			break;
		nskvp = skiver_list_next(skvlp, skvp);
		skiver_list_remove(skvlp, skvp);
		skiver_dealloc(skvp);
	}
}

int
skiver_list_empty(struct skiver_list *skvlp)
{

	return (TAILQ_EMPTY(&skvlp->head));
}

struct skiver *
skiver_list_first(struct skiver_list *skvlp)
{

	return (TAILQ_FIRST(&skvlp->head));
}

struct skiver *
skiver_list_next(struct skiver_list *skvlp, struct skiver *skvp)
{

	return (TAILQ_NEXT(skvp, all_skvl_ary[skvlp->index]));
}

void
skiver_list_insert(struct skiver_list *skvlp, struct skiver *skvp)
{

	TAILQ_INSERT_HEAD(&skvlp->head, skvp, all_skvl_ary[skvlp->index]);
	skvlp->count++;
	assert(skvlp->count > 0);
	if (skvlp->count > skvlp->peak)
		skvlp->peak = skvlp->count;
}

void
skiver_list_append(struct skiver_list *skvlp, struct skiver *skvp)
{

	TAILQ_INSERT_TAIL(&skvlp->head, skvp, all_skvl_ary[skvlp->index]);
	skvlp->count++;
	assert(skvlp->count > 0);
	if (skvlp->count > skvlp->peak)
		skvlp->peak = skvlp->count;
}

void
skiver_list_remove(struct skiver_list *skvlp, struct skiver *skvp)
{

	TAILQ_REMOVE(&skvlp->head, skvp, all_skvl_ary[skvlp->index]);
	skvlp->count--;
	assert(skvlp->count >= 0);
}

static
void
skiver_list_shutdown(struct skiver_list *skvlp)
{
	struct skiver *skvp;

	for (;;) {
		skvp = skiver_list_first(skvlp);
		if (skvp == NULL)
			break;
		skiver_list_remove(skvlp, skvp);
		skiver_thread_shutdown(skvp);
		thread_join(skvp->thr);
		skiver_free(skvp);
	}
}
