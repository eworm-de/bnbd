/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

struct token {
	RB_ENTRY(token)		tok_rb_entries[1];
	uint64_t		tok_from;
	uint64_t		tok_to;
	int			tok_refs;
#define TOK_FL_INUSE		0x00000001
	int			tok_flags;
	void			*tok_tsp;
	TAILQ_ENTRY(token)	tok_tailq_entries[1];
	struct thread_head	tok_wait_thrh;
};
RB_HEAD(token_rb_head, token);

struct token_tree {
	struct token_rb_head	tt_tokens;
	pthread_mutex_t		tt_mutex;
};

int token_module_setup(void);
void token_module_hello(void);
void token_module_teardown(void);
void token_module_stats(void);
int token_alloc(struct token **);
void token_init(struct token *);
void token_uninit(struct token *);
void token_free(struct token *);
void token_setup(struct token *, uint64_t, uint32_t, uint32_t);
int token_acquire(struct thread *, struct token_tree *, struct token *, int);
int token_release(struct token_tree *, struct token *);
int token_tree_alloc(struct token_tree **);
void token_tree_init(struct token_tree *);
void token_tree_uninit(struct token_tree *);
void token_tree_free(struct token_tree *);
