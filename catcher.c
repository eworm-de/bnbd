/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "io.h"
#include "thread.h"
#include "request.h"
#include "worker.h"
#include "catcher.h"

static int catcher_module_initialized = 0;
static struct catcher *catcher0 = NULL;

uint64_t collect_count = 0;

static int catcher_alloc(struct catcher **);
static void catcher_free(struct catcher *);
static void catcher_thread_shutdown(struct catcher *);
static void *catcher_thread(void *);
static void catcher_thread_loop(struct catcher *);

int
catcher_module_setup(void)
{

	assert(!catcher_module_initialized);
	assert(catcher0 == NULL);
	if (catcher_alloc(&catcher0) < 0) {
		assert(catcher0 == NULL);
		return (-1);
	}
	assert(catcher0 != NULL);
	if (thread_create(catcher0->thr, "catcher0", catcher_thread) < 0) {
		catcher_free(catcher0);
		catcher0 = NULL;
		return (-2);
	}
	thread_launch(catcher0->thr);
	catcher_module_initialized = 1;
	return (0);
}

void
catcher_module_hello(void)
{

	assert(catcher_module_initialized);
	DEBUG("sizeof(catcher)=%zu", sizeof(struct catcher));
}

void
catcher_module_teardown(void)
{

	assert(catcher_module_initialized);
	assert(catcher0 != NULL);
	catcher_thread_shutdown(catcher0);
	catcher_free(catcher0);
	catcher0 = NULL;
	catcher_module_initialized = 0;
}

void
catcher_module_stats(void)
{
	uint64_t current_collect_count;
	int backoff_count, backoff_peak;

	assert(catcher_module_initialized);
	worker_list_lock(backoff_wrkl);
	current_collect_count = collect_count;
	backoff_count = backoff_wrkl->count;
	backoff_peak = backoff_wrkl->peak;
	backoff_wrkl->peak = backoff_wrkl->count;
	worker_list_unlock(backoff_wrkl);
	NOTICE("backoff_workers=%d/%d collect_count=%llu",
	    backoff_count, backoff_peak,
	    (unsigned long long)current_collect_count);
}

static
int
catcher_alloc(struct catcher **catpp)
{
	struct catcher *catp;
#ifdef USE_LINUX_AIO
	size_t len;
	int error;
#endif

	catp = malloc(sizeof(*catp));
	if (catp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*catp), strerror(errno));
		goto out0;
	}
	catp->thr = NULL;
	if (thread_alloc(&catp->thr, catp) < 0) {
		assert(catp->thr == NULL);
		goto out1;
	}
	assert(catp->thr != NULL);
	if (pipe(catp->pype) < 0) {
		ERR("pipe(): %s", strerror(errno));
		goto out2;
	}
#ifdef USE_LINUX_AIO
	len = cfg_aio_queue_length * sizeof(struct io_event);
	catp->event_ary = malloc(len);
	if (catp->event_ary == NULL) {
		ERR("malloc(%zu): %s", len, strerror(errno));
		goto out3;
	}
	memset(&catp->ioctx, 0x00, sizeof(catp->ioctx));
	error = io_setup(cfg_aio_queue_length, &catp->ioctx);
	if (error < 0) {
		ERR("io_setup(%d): %s", cfg_aio_queue_length, strerror(-error));
		goto out4;
	}
	catp->event_fd = eventfd(0, 0);
	if (catp->event_fd < 0) {
		ERR("eventfd(): %s", strerror(errno));
		goto out5;
	}
#endif
	*catpp = catp;
	return (0);
#ifdef USE_LINUX_AIO
out5:
	error = io_destroy(catp->ioctx);
	if (error < 0)
		WARNING("io_destroy(): %s", strerror(-error));
out4:
	free(catp->event_ary);
out3:
	fd_close(catp->pype[1]);
	fd_close(catp->pype[0]);
#endif
out2:
	thread_free(catp->thr);
out1:
	free(catp);
out0:
	return (-1);
}

static
void
catcher_free(struct catcher *catp)
{
#ifdef USE_LINUX_AIO
	int error;
#endif

#ifdef USE_LINUX_AIO
	fd_close(catp->event_fd);
	error = io_destroy(catp->ioctx);
	if (error < 0)
		WARNING("io_destroy(): %s", strerror(-error));
	free(catp->event_ary);
#endif
	fd_close(catp->pype[1]);
	fd_close(catp->pype[0]);
	thread_free(catp->thr);
	free(catp);
}

static
void
catcher_thread_shutdown(struct catcher *catp)
{
	ssize_t w;
	char go[1] = { 'X' };

	thread_shutdown_begin(catp->thr);
try_write:
	w = write(catp->pype[1], go, sizeof(go));
	if (w < (ssize_t)0 && errno == EINTR)
		goto try_write;
	assert(w == (ssize_t)sizeof(go));
	thread_join(catp->thr);
	thread_shutdown_commit(catp->thr);
}

static
void *
catcher_thread(void *data)
{
	struct catcher *catp;

	catp = (struct catcher *)data;
	assert(catp != NULL);
	assert(catp->thr != NULL);

	thread_prologue(catp->thr);

	catcher_thread_loop(catp);

	thread_epilogue(NULL);
	return (NULL);
}

#ifdef USE_LINUX_AIO
static
void
catcher_thread_loop(struct catcher *catp)
{
	struct io_event *eventp, *event_aryp;
	struct request *reqp;
	struct worker *wrkp;
	fd_set rfds;
	ssize_t r;
	int64_t event_count;
	int collect_peak, event_fd, i, n, nfds, cntrl_fd;

	event_aryp = catp->event_ary;
	cntrl_fd = catp->pype[0];
	event_fd = catp->event_fd;

	collect_peak = 0;
	nfds = MAX(cntrl_fd, event_fd) + 1;
	for (;;) {
		FD_ZERO(&rfds);
		FD_SET(cntrl_fd, &rfds);
		FD_SET(event_fd, &rfds);
		n = select(nfds, &rfds, NULL, NULL, NULL);
		if (n < 0) {
			if (errno == EINTR) {
				WARNING("select(): %s", strerror(errno));
				continue;
			}
			ERR("select(): %s", strerror(errno));
			continue;
		}

		event_count = 0;
		if (FD_ISSET(event_fd, &rfds)) {
try_read_event_fd:
			r = read(event_fd, &event_count, sizeof(event_count));
			if (r < (ssize_t)0 && errno == EINTR)
				goto try_read_event_fd;
			assert(r == sizeof(event_count));
try_io_getevents:
			n = io_getevents(catp->ioctx, (long)0,
			    (long)cfg_aio_queue_length, event_aryp, NULL);
			if (n < 0 && n == -EINTR)
				goto try_io_getevents;
			assert(n >= 0 && n <= cfg_aio_queue_length);
			if (n > 0) {
				worker_list_lock(backoff_wrkl);
				collect_count++;
				wrkp = worker_list_first(backoff_wrkl);
				if (wrkp != NULL) {
					worker_list_remove(backoff_wrkl, wrkp);
					worker_thread_wakeup(wrkp);
				}
				worker_list_unlock(backoff_wrkl);
			}
			if (n > collect_peak) {
				collect_peak = n;
				DEBUG("aio_collect_peak=%d", collect_peak);
			}
			for (i = 0; i < n; i++) {
				eventp = &event_aryp[i];
				reqp = eventp->data;
				assert(eventp->obj == &reqp->iocb);
				if ((long)eventp->res2 < (long)0) {
					ERR("AIO ERROR 2: %ld: %s",
					    (long)eventp->res2,
					    strerror((int)-eventp->res2));
					request_done(reqp, (int)eventp->res2);
					continue;
				}
				if ((long)eventp->res < (long)0) {
					/*
					 * Linux AIO API inconsistency?
					 * In some cases such as ENOSPC
					 * the eventp->res2 == 0 and an error
					 * code is returned in eventp->res
					 * instead of the number of bytes
					 * transferred.
					 */
					ERR("AIO ERROR 1: %ld: %s",
					    (long)eventp->res,
					    strerror((int)-eventp->res));
					request_done(reqp, (int)eventp->res);
					continue;
				}
				assert(eventp->res <= reqp->iocb.u.c.nbytes);
				if (eventp->res < reqp->iocb.u.c.nbytes) {
					ERR("PARTIAL AIO: %lu/%lu",
					    eventp->res, reqp->iocb.u.c.nbytes);
					request_done(reqp, -EAGAIN);
					continue;
				}
				request_done(reqp, 0);
			}
		}

		if (FD_ISSET(cntrl_fd, &rfds)) {
			char go[1];
try_read_cntrl_fd:
			r = read(cntrl_fd, go, sizeof(go));
			if (r < (ssize_t)0 && errno == EINTR)
				goto try_read_cntrl_fd;
			assert(r == (ssize_t)sizeof(go));
			assert(go[0] == 'X');
			break;
		}
	}
}

io_context_t
catcher_ioctx(void)
{

	assert(catcher_module_initialized);
	return (catcher0->ioctx);
}

int
catcher_event_fd(void)
{

	assert(catcher_module_initialized);
	return (catcher0->event_fd);
}
#else
static
void
catcher_thread_loop(struct catcher *catp)
{

	(void)catp;
	abort();
}
#endif
