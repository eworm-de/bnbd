/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "io.h"
#include "thread.h"
#include "request.h"
#include "reader.h"
#include "session.h"
#include "dispatch_glue.h"
#include "volume.h"

static int reader_module_initialized = 0;
static pthread_mutex_t reader_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct reader_list *available_rdrl = NULL;
static struct reader_list *inuse_rdrl = NULL;

struct reader_list *connected_rdrl = NULL;

static void reader_dealloc(struct reader *);
static void reader_list_dealloc(struct reader_list *);
static void *reader_thread(void *);
static void reader_thread_loop(struct reader *);
static int nbd_negotiate(struct session *);

int
reader_module_setup(void)
{

	assert(!reader_module_initialized);
	assert(available_rdrl == NULL);
	if (reader_list_alloc(&available_rdrl, RDRL_IDX_ALLOC) < 0) {
		assert(available_rdrl == NULL);
		goto out0;
	}
	assert(available_rdrl != NULL);
	assert(inuse_rdrl == NULL);
	if (reader_list_alloc(&inuse_rdrl, RDRL_IDX_ALLOC) < 0) {
		assert(inuse_rdrl == NULL);
		goto out1;
	}
	assert(inuse_rdrl != NULL);
	assert(connected_rdrl == NULL);
	if (reader_list_alloc(&connected_rdrl, RDRL_IDX_STD) < 0) {
		assert(connected_rdrl == NULL);
		goto out2;
	}
	assert(connected_rdrl != NULL);
	reader_module_initialized = 1;
	return (0);
out2:
	assert(inuse_rdrl != NULL);
	reader_list_free(inuse_rdrl);
	inuse_rdrl = NULL;
out1:
	assert(available_rdrl != NULL);
	reader_list_dealloc(available_rdrl);
	reader_list_free(available_rdrl);
	available_rdrl = NULL;
out0:
	return (-1);
}

void
reader_module_hello(void)
{

	assert(reader_module_initialized);
	DEBUG("sizeof(reader)=%zu", sizeof(struct reader));
	DEBUG("sizeof(reader_list)=%zu", sizeof(struct reader_list));
}

void
reader_module_teardown(void)
{

	assert(reader_module_initialized);
	assert(connected_rdrl != NULL);
	reader_list_free(connected_rdrl);
	connected_rdrl = NULL;
	assert(inuse_rdrl != NULL);
	reader_list_free(inuse_rdrl);
	inuse_rdrl = NULL;
	assert(available_rdrl != NULL);
	reader_list_dealloc(available_rdrl);
	reader_list_free(available_rdrl);
	available_rdrl = NULL;
	reader_module_initialized = 0;
}

void
reader_module_stats(void)
{
	int available_count, available_peak, inuse_count, inuse_peak;

	assert(reader_module_initialized);
	PTHREAD_MUTEX_LOCK(&reader_mutex);
	available_count = available_rdrl->count;
	available_peak = available_rdrl->peak;
	available_rdrl->peak = available_rdrl->count;
	inuse_count = inuse_rdrl->count;
	inuse_peak = inuse_rdrl->peak;
	inuse_rdrl->peak = inuse_rdrl->count;
	PTHREAD_MUTEX_UNLOCK(&reader_mutex);
	NOTICE("inuse=%d/%d available=%d/%d",
	    inuse_count, inuse_peak, available_count, available_peak);
}

int
reader_alloc(struct reader **rdrpp)
{
	struct reader *rdrp;

	assert(reader_module_initialized);
	PTHREAD_MUTEX_LOCK(&reader_mutex);
	rdrp = reader_list_first(available_rdrl);
	if (rdrp == NULL) {
		assert(available_rdrl->count == 0);
		PTHREAD_MUTEX_UNLOCK(&reader_mutex);
		rdrp = malloc(sizeof(*rdrp));
		if (rdrp == NULL) {
			ERR("malloc(%zu): %s", sizeof(*rdrp), strerror(errno));
			return (-2);
		}
		rdrp->thr = NULL;
		if (thread_alloc(&rdrp->thr, rdrp) < 0) {
			assert(rdrp->thr == NULL);
			free(rdrp);
			return (-3);
		}
		assert(rdrp->thr != NULL);
		rdrp->aux_reql = NULL;
		if (request_list_alloc(&rdrp->aux_reql, REQL_IDX_STD) < 0) {
			assert(rdrp->aux_reql == NULL);
			thread_free(rdrp->thr);
			free(rdrp);
			return (-4);
		}
		assert(rdrp->aux_reql != NULL);
		rdrp->inbuf_len = (size_t)cfg_request_buffer_kb << 10;
		rdrp->inbuf = malloc(rdrp->inbuf_len);
		if (rdrp->inbuf == NULL) {
			ERR("malloc(%zu): %s",
			    rdrp->inbuf_len, strerror(errno));
			request_list_free(rdrp->aux_reql);
			thread_free(rdrp->thr);
			free(rdrp);
			return (-5);
		}
		reader_init(rdrp);
		PTHREAD_MUTEX_LOCK(&reader_mutex);
	} else {
		reader_list_remove(available_rdrl, rdrp);
	}
	assert(!rdrp->flags);
	rdrp->flags |= RDR_FL_INUSE;
	reader_list_insert(inuse_rdrl, rdrp);
	PTHREAD_MUTEX_UNLOCK(&reader_mutex);
	assert(rdrp->inbuf_idx == (size_t)0);
	assert(rdrp->cur_req_id == (int64_t)0);
	assert(rdrp->sesp == NULL);
	*rdrpp = rdrp;
	return (0);
}

void
reader_init(struct reader *rdrp)
{

	rdrp->flags = 0;
	rdrp->inbuf_idx = (size_t)0;
	rdrp->cur_req_id = (int64_t)0;
	rdrp->sesp = NULL;
}

void
reader_uninit(struct reader *rdrp)
{

	thread_uninit(rdrp->thr);
	assert(rdrp->flags == RDR_FL_INUSE);
	request_list_uninit(rdrp->aux_reql);
	rdrp->inbuf_idx = (size_t)0;
	rdrp->cur_req_id = (int64_t)0;
	rdrp->sesp = NULL;
}

void
reader_free(struct reader *rdrp)
{

	assert(reader_module_initialized);
	reader_uninit(rdrp);
	PTHREAD_MUTEX_LOCK(&reader_mutex);
	reader_list_remove(inuse_rdrl, rdrp);
	rdrp->flags &= ~RDR_FL_INUSE;
	assert(!rdrp->flags);
	reader_list_insert(available_rdrl, rdrp);
	PTHREAD_MUTEX_UNLOCK(&reader_mutex);
}

static
void
reader_dealloc(struct reader *rdrp)
{

	assert(!rdrp->flags);
	free(rdrp->inbuf);
	request_list_free(rdrp->aux_reql);
	thread_free(rdrp->thr);
	free(rdrp);
}

int
reader_thread_create(struct reader *rdrp)
{

	return (thread_create(rdrp->thr, "reader", reader_thread));
}

void
reader_thread_launch(struct reader *rdrp)
{

	thread_launch(rdrp->thr);
}

void
reader_thread_join(struct reader *rdrp)
{

	thread_join(rdrp->thr);
	thread_lock(rdrp->thr);
	thread_shutdown_commit(rdrp->thr);
	thread_unlock(rdrp->thr);
}

void
reader_thread_abort(struct reader *rdrp)
{

	thread_shutdown_begin(rdrp->thr);
	reader_thread_launch(rdrp);
	reader_thread_join(rdrp);
}

void
reader_thread_shutdown(struct reader *rdrp)
{

	thread_lock(rdrp->thr);
	thread_shutdown_begin(rdrp->thr);
	thread_unlock(rdrp->thr);
	thread_kill(rdrp->thr, SIGUSR1);
}

static
void *
reader_thread(void *data)
{
	struct reader *rdrp;

	rdrp = (struct reader *)data;
	assert(rdrp != NULL);
	assert(rdrp->thr != NULL);
	assert(rdrp->aux_reql != NULL);
	assert(request_list_empty(rdrp->aux_reql));
	assert(rdrp->cur_req_id == (int64_t)0);

	thread_prologue(rdrp->thr);

	if (THREAD_SHUTDOWN_PENDING(rdrp->thr))
		goto out;

	assert(rdrp->sesp != NULL);
	assert(rdrp->sesp->outgoing_reql != NULL);
	assert(rdrp->sesp->volp == NULL);
	if (nbd_negotiate(rdrp->sesp) < 0)
		goto out;

	assert(rdrp->sesp->volp != NULL);
	reader_thread_loop(rdrp);
out:
	session_shutdown(rdrp->sesp);
	thread_epilogue(NULL);
	return (NULL);
}

static
void
reader_thread_loop(struct reader *rdrp)
{
	struct session *sesp;
	struct volume *volp;
	struct request_list *outgoing_reqlp;
	struct request *reqp;
	struct nbd_request *nbd_reqp;
	char *inbuf;
	size_t eaten_idx, inbuf_idx, inbuf_len, len;
	ssize_t n;
	int so;

	assert(rdrp->inbuf != NULL);
	assert(rdrp->inbuf_idx == (size_t)0);
	assert(rdrp->inbuf_len >= sizeof(reqp->nbd.request));
	sesp = rdrp->sesp;
	so = sesp->so;
	volp = sesp->volp;
	outgoing_reqlp = sesp->outgoing_reql;

	reqp = NULL;

	if (so_timeo(so, 0, 0) < 0)
		goto out;

	if (so_timeo(so, 1, 0) < 0)
		goto out;

	if (so_linger(so, 1, cfg_nbd_send_timeout) < 0)
		goto out;

	if (tcp_user_timeout(so, cfg_nbd_send_timeout) < 0)
		goto out;

	if (tcp_keepalive(so, cfg_nbd_keepalive_idle,
	    cfg_nbd_keepalive_interval, cfg_nbd_keepalive_count) < 0)
		goto out;

	for (;;) {
		thread_lock(rdrp->thr);
		if (THREAD_SHUTDOWN_PENDING(rdrp->thr)) {
			thread_unlock(rdrp->thr);
			DEBUG("Shutting down...");
			break;
		}
		thread_unlock(rdrp->thr);
		if (reqp != NULL) {
			inbuf = reqp->buf.buf_ptr;
			inbuf_len = (size_t)reqp->buf.buf_len;
			inbuf_idx = (size_t)reqp->buf.buf_idx;
		} else {
			inbuf = rdrp->inbuf;
			inbuf_len = rdrp->inbuf_len;
			inbuf_idx = rdrp->inbuf_idx;
		}
		n = recv(so, inbuf + inbuf_idx, inbuf_len - inbuf_idx,
		    MSG_DONTWAIT);
		if (n < (ssize_t)0 &&
		    (errno == EAGAIN || errno == EWOULDBLOCK)) {
			dispatch_pending(rdrp);
			n = recv(so, inbuf + inbuf_idx, inbuf_len - inbuf_idx,
			    0);
		}
		if (n < (ssize_t)0) {
			if (errno == EINTR) {
				WARNING("recv(): %s", strerror(errno));
				continue;
			}
			ERR("recv(): %s", strerror(errno));
			break;
		}
		if (n == (ssize_t)0) {
			/* EOF */
			NOTICE("EOF from %s:%d",
			    sesp->inet_addr, sesp->inet_port);
			break;
		}
		assert(n > (ssize_t)0);
		inbuf_idx += n;
		if (reqp != NULL)
			reqp->buf.buf_idx = inbuf_idx;
		else
			rdrp->inbuf_idx = inbuf_idx;

		eaten_idx = (size_t)0;
		for (;;) {
			if (rdrp->inbuf_idx - eaten_idx <
			    sizeof(reqp->nbd.request))
				break;
			if (reqp != NULL)
				goto no_request_alloc;
			nbd_reqp = (struct nbd_request *)
			    (rdrp->inbuf + eaten_idx);
			nbd_reqp->magic = ntohl(nbd_reqp->magic);
			nbd_reqp->type = ntohl(nbd_reqp->type);
			nbd_reqp->from = ntohll(nbd_reqp->from);
			nbd_reqp->len = ntohl(nbd_reqp->len);

			if (nbd_reqp->magic != NBD_REQUEST_MAGIC) {
				ERR("INVALID REQUEST MAGIC=0x%X",
				    (int)nbd_reqp->magic);
				goto out;
			}

			switch (nbd_reqp->type) {
			case NBD_CMD_READ:
			case NBD_CMD_WRITE:
			case NBD_CMD_TRIM:
				if (nbd_reqp->from & (uint64_t)0x1ff) {
					ERR("OFFSET=%llu NOT 512 BYTES ALIGNED",
					    (unsigned long long)nbd_reqp->from);
					goto out;
				}
				if (nbd_reqp->len & (uint32_t)0x1ff) {
					ERR("LENGTH=%u NOT 512 BYTES ALIGNED",
					    (unsigned int)nbd_reqp->len);
					goto out;
				}
				if (nbd_reqp->from >= volp->size) {
					ERR("OFFSET=%llu >= %llu BYTES",
					    (unsigned long long)nbd_reqp->from,
					    (unsigned long long)volp->size);
					goto out;
				}
				if (nbd_reqp->len == (uint32_t)0) {
					ERR("LENGTH == 0 BYTES");
					goto out;
				}
				if (nbd_reqp->from + nbd_reqp->len >
				    volp->size) {
					ERR("OFFSET+LENGTH=%llu > %llu BYTES",
					    (unsigned long long)nbd_reqp->from +
					    nbd_reqp->len,
					    (unsigned long long)volp->size);
					goto out;
				}
				break;
			case NBD_CMD_DISC:
				NOTICE("Disconnect from %s:%d",
				    sesp->inet_addr, sesp->inet_port);
				goto out;
			case NBD_CMD_FLUSH:
				break;
			default:
				ERR("REQUEST TYPE %d NOT IMPLEMENTED",
				    (int)nbd_reqp->type);
				goto out;
			}

			if (request_alloc(rdrp->thr, &reqp, 0) < 0) {
				assert(reqp == NULL);
				dispatch_pending(rdrp);
				if (request_alloc(rdrp->thr, &reqp, 1) < 0) {
					assert(reqp == NULL);
					ERR("OUT OF MEMORY (req)");
					goto out;
				}
			}
			assert(reqp != NULL);
			memcpy(&reqp->nbd.request, nbd_reqp, sizeof(*nbd_reqp));
			reqp->nbd.reply.magic = htonl(NBD_REPLY_MAGIC);
			reqp->nbd.reply.handle = reqp->nbd.request.handle;
			reqp->blk_size = volp->blk_size;
			reqp->fd = volp->fd;
			reqp->volp = volp;
			reqp->outgoing_reqlp = outgoing_reqlp;
no_request_alloc:
			eaten_idx += sizeof(*nbd_reqp);
			if (reqp->nbd.request.type == NBD_CMD_FLUSH) {
				dispatch_flush(rdrp, reqp);
				reqp = NULL;
				continue;
			}
			if (reqp->nbd.request.type == NBD_CMD_TRIM) {
				dispatch_trim(rdrp, reqp);
				reqp = NULL;
				continue;
			}
			if (reqp->nbd.request.type == NBD_CMD_READ) {
				if (request_rdbuf_alloc(rdrp->thr, reqp,
				    0) < 0) {
					dispatch_pending(rdrp);
					if (request_rdbuf_alloc(rdrp->thr, reqp,
					    1) < 0) {
						ERR("OUT OF MEMORY (rdbuf)");
						goto out;
					}
				}
				assert(reqp->buf.buf_ptr != NULL);
				assert(reqp->buf.buf_len ==
				    reqp->nbd.request.len);
				dispatch_read(rdrp, reqp);
				reqp = NULL;
				continue;
			}
			assert(reqp->nbd.request.type == NBD_CMD_WRITE);
			if (reqp->buf.buf_ptr != NULL)
				goto nbd_cmd_write_wrbuf;
			if (request_wrbuf_alloc(rdrp->thr, reqp, 0) < 0) {
				dispatch_pending(rdrp);
				if (request_wrbuf_alloc(rdrp->thr, reqp,
				    1) < 0) {
					ERR("OUT OF MEMORY (wrbuf)");
					goto out;
				}
			}
			assert(reqp->buf.buf_ptr != NULL);
			assert(reqp->buf.buf_len == reqp->nbd.request.len);
			if (eaten_idx + reqp->buf.buf_len > rdrp->inbuf_idx) {
				/* PARTIAL WRITE */
				reqp->buf.buf_idx =
				    rdrp->inbuf_idx - eaten_idx;
				memcpy(reqp->buf.buf_ptr,
				    rdrp->inbuf + eaten_idx,
				    reqp->buf.buf_idx);
				eaten_idx -= sizeof(reqp->nbd.request);
				rdrp->inbuf_idx -= reqp->buf.buf_idx;
				break;
			} else {
				/* COMPLETE WRITE */
				reqp->buf.buf_idx = reqp->buf.buf_len;
				memcpy(reqp->buf.buf_ptr,
				    rdrp->inbuf + eaten_idx,
				    (size_t)reqp->buf.buf_len);
				eaten_idx += reqp->buf.buf_len;
				dispatch_write(rdrp, reqp);
				reqp = NULL;
				continue;
			}
nbd_cmd_write_wrbuf:
			assert(eaten_idx == sizeof(reqp->nbd.request));
			if (reqp->buf.buf_idx < reqp->buf.buf_len) {
				/* PARTIAL WRITE */
				eaten_idx -= sizeof(reqp->nbd.request);
			} else {
				assert(reqp->buf.buf_idx == reqp->buf.buf_len);
				/* COMPLETE WRITE */
				dispatch_write(rdrp, reqp);
				reqp = NULL;
			}
			break;
		}
		/* REWIND INBUF */
		assert(eaten_idx <= rdrp->inbuf_idx);
		if (eaten_idx > (size_t)0) {
			len = rdrp->inbuf_idx - eaten_idx;
			if (len > (size_t)0)
				memmove(rdrp->inbuf, rdrp->inbuf + eaten_idx,
				    len);
			rdrp->inbuf_idx -= eaten_idx;
		}
	}
out:
	if (reqp != NULL)
		request_free(reqp);
	dispatch_pending(rdrp);
}

static
int
nbd_negotiate(struct session *sesp)
{
	static uint64_t opts_magic = 0x49484156454F5054ULL;
	char id[MY_MAXPATH], zeros[124];
	uint64_t magic, size;
	uint32_t cflags, flags, idlen, opt;
	uint16_t smallflags;
	ssize_t n;
	int so;
	struct volume *volp;

	flags = NBD_FLAG_HAS_FLAGS;
	smallflags = NBD_FLAG_FIXED_NEWSTYLE;
	so = sesp->so;
	assert(so >= 0);
	volp = NULL;

	if (fd_blocking(so, 1) < 0)
		return (-1);

	if (cfg_nbd_receive_sockbuf_kb > 0 &&
	    so_buf(so, 0, cfg_nbd_receive_sockbuf_kb) < 0) {
		return (-2);
	}

	if (cfg_nbd_send_sockbuf_kb > 0 &&
	    so_buf(so, 1, cfg_nbd_send_sockbuf_kb) < 0) {
		return (-3);
	}

	if (so_timeo(so, 0, cfg_nbd_negotiate_timeout) < 0)
		return (-4);

	if (so_timeo(so, 1, cfg_nbd_negotiate_timeout) < 0)
		return (-5);

	if (tcp_user_timeout(so, cfg_nbd_negotiate_timeout) < 0)
		return (-6);

	/* Send INIT_PASSWD */
	n = buf_tx(so, "NBDMAGIC", (size_t)8, 0);
	if (n < (ssize_t)0) {
		ERR("write(INIT_PASSWD): %zd", n);
		return (-7);
	}
	assert(n == (ssize_t)8);

	/* Send 64-bit opts_magic */
	magic = htonll(opts_magic);
	n = buf_tx(so, (const char *)&magic, sizeof(magic), 0);
	if (n < (ssize_t)0) {
		ERR("write(0x%llX): %zd", (long long)opts_magic, n);
		return (-8);
	}
	assert(n == (ssize_t)sizeof(magic));

	/* Send small flags */
	smallflags = htons(smallflags);
	n = buf_tx(so, (const char *)&smallflags, sizeof(smallflags), 0);
	if (n < (ssize_t)0) {
		ERR("write(smallflags): %zd", n);
		return (-9);
	}
	assert(n == (ssize_t)sizeof(smallflags));

	/* Read cflags */
	n = buf_rx(so, (char *)&cflags, sizeof(cflags), 0);
	if (n <= (ssize_t)0) {
		ERR("read(cflags): %zd", n);
		return (-10);
	}
	assert(n == (ssize_t)sizeof(cflags));

	/* Read 64-bit opts_magic */
	n = buf_rx(so, (char *)&magic, sizeof(magic), 0);
	if (n <= (ssize_t)0) {
		ERR("read(magic): %zd", n);
		return (-11);
	}
	assert(n == (ssize_t)sizeof(magic));
	magic = ntohll(magic);
	if (magic != opts_magic) {
		ERR("magic 0x%llX != 0x%llX",
		    (long long)magic, (long long)opts_magic);
		return (-12);
	}

	/* Read opt */
	n = buf_rx(so, (char *)&opt, sizeof(opt), 0);
	if (n <= (ssize_t)0) {
		ERR("read(opt): %zd", n);
		return (-13);
	}
	assert(n == (ssize_t)sizeof(opt));
	opt = ntohl(opt);
	if (opt != NBD_OPT_EXPORT_NAME) {
		ERR("NBD OPTION %u NOT SUPPORTED", (unsigned int)opt);
		return (-14);
	}

	n = buf_rx(so, (char *)&idlen, sizeof(idlen), 0);
	if (n <= (ssize_t)0) {
		ERR("read(idlen): %zd", n);
		return (-15);
	}
	assert(n == (ssize_t)sizeof(idlen));
	idlen = ntohl(idlen);
	if (idlen == 0 || idlen >= sizeof(id)) {
		ERR("idlen=%lu out of range", (unsigned long)idlen);
		return (-16);
	}
	n = buf_rx(so, id, (size_t)idlen, 0);
	if (n <= (ssize_t)0) {
		ERR("read(id): %zd", n);
		return (-17);
	}
	assert(n == (ssize_t)idlen);
	id[idlen] = '\0';
	volume_list_lock(master_voll);
	volp = volume_list_lookup_by_id(master_voll, id);
	if (volp == NULL) {
		volume_list_unlock(master_voll);
		NOTICE("VOLUME %s NOT FOUND", id);
		return (-18);
	}
	if (volp->state != VOL_ST_UP) {
		volume_list_unlock(master_voll);
		NOTICE("VOLUME %s NOT UP", id);
		return (-19);
	}
	session_list_lock(negotiating_sesl);
	sesp->volp = volp;
	session_list_remove(negotiating_sesl, sesp);
	session_list_lock(volp->established_sesl);
	session_list_append(volp->established_sesl, sesp);
	session_list_unlock(volp->established_sesl);
	session_list_unlock(negotiating_sesl);
	volume_list_unlock(master_voll);
	NOTICE("NBD client %s:%d attached to volume %s",
	    sesp->inet_addr, sesp->inet_port, volp->id);

	/* Send 64-bit size of exported device */
	size = htonll(volp->size);
	n = buf_tx(so, (const char *)&size, sizeof(size), 0);
	if (n != (ssize_t)sizeof(size)) {
		ERR("write(size): %zd", n);
		return (-20);
	}

	if (cfg_flush > 0)
		flags |= NBD_FLAG_SEND_FLUSH;
	if (cfg_trim > 0)
		flags |= NBD_FLAG_SEND_TRIM;
#if 0
	flags |= NBD_FLAG_READ_ONLY;
#endif
	smallflags = (uint16_t)(flags & ~((uint16_t)0));
	smallflags = htons(smallflags);
	n = buf_tx(so, (const char *)&smallflags, sizeof(smallflags), 0);
	if (n < (ssize_t)0) {
		ERR("write(smallflags): %zd", n);
		return (-21);
	}
	assert(n == (ssize_t)sizeof(smallflags));

	/* Send 124 bytes of zeros (reserved for future use) */
	memset(zeros, 0x00, sizeof(zeros));
	n = buf_tx(so, (const char *)&zeros, sizeof(zeros), 0);
	if (n < (ssize_t)0) {
		ERR("write(zeros): %zd", n);
		return (-22);
	}
	assert(n == (ssize_t)(sizeof(zeros)));
	return (0);
}

int
reader_list_alloc(struct reader_list **rdrlpp, int index)
{
	struct reader_list *rdrlp;
	int error;

	rdrlp = malloc(sizeof(*rdrlp));
	if (rdrlp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*rdrlp), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&rdrlp->mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(rdrlp);
		return (-3);
	}
	reader_list_init(rdrlp, index);
	*rdrlpp = rdrlp;
	return (0);
}

void
reader_list_init(struct reader_list *rdrlp, int index)
{
#ifndef NDEBUG
	struct reader *rdrp;
#endif

	assert(index >= 0 && index < NELEM(rdrp->all_rdrl_ary));
	TAILQ_INIT(&rdrlp->head);
	rdrlp->index = index;
	rdrlp->count = 0;
	rdrlp->peak = 0;
}

void
reader_list_uninit(struct reader_list *rdrlp)
{

	assert(TAILQ_EMPTY(&rdrlp->head));
	assert(rdrlp->count == 0);
	rdrlp->peak = 0;
}

void
reader_list_free(struct reader_list *rdrlp)
{

	reader_list_uninit(rdrlp);
	PTHREAD_MUTEX_DESTROY(&rdrlp->mutex);
	free(rdrlp);
}

static
void
reader_list_dealloc(struct reader_list *rdrlp)
{
	struct reader *nrdrp, *rdrp;

	nrdrp = reader_list_first(rdrlp);
	for (;;) {
		rdrp = nrdrp;
		if (rdrp == NULL)
			break;
		nrdrp = reader_list_next(rdrlp, rdrp);
		reader_list_remove(rdrlp, rdrp);
		reader_dealloc(rdrp);
	}
}

void
reader_list_lock(struct reader_list *rdrlp)
{

	PTHREAD_MUTEX_LOCK(&rdrlp->mutex);
}

void
reader_list_unlock(struct reader_list *rdrlp)
{

	PTHREAD_MUTEX_UNLOCK(&rdrlp->mutex);
}

int
reader_list_empty(struct reader_list *rdrlp)
{

	return (TAILQ_EMPTY(&rdrlp->head));
}

struct reader *
reader_list_first(struct reader_list *rdrlp)
{

	return (TAILQ_FIRST(&rdrlp->head));
}

struct reader *
reader_list_next(struct reader_list *rdrlp, struct reader *rdrp)
{

	return (TAILQ_NEXT(rdrp, all_rdrl_ary[rdrlp->index]));
}

void
reader_list_insert(struct reader_list *rdrlp, struct reader *rdrp)
{

	TAILQ_INSERT_HEAD(&rdrlp->head, rdrp, all_rdrl_ary[rdrlp->index]);
	rdrlp->count++;
	assert(rdrlp->count > 0);
	if (rdrlp->count > rdrlp->peak)
		rdrlp->peak = rdrlp->count;
}

void
reader_list_append(struct reader_list *rdrlp, struct reader *rdrp)
{

	TAILQ_INSERT_TAIL(&rdrlp->head, rdrp, all_rdrl_ary[rdrlp->index]);
	rdrlp->count++;
	assert(rdrlp->count > 0);
	if (rdrlp->count > rdrlp->peak)
		rdrlp->peak = rdrlp->count;
}

void
reader_list_remove(struct reader_list *rdrlp, struct reader *rdrp)
{

	TAILQ_REMOVE(&rdrlp->head, rdrp, all_rdrl_ary[rdrlp->index]);
	rdrlp->count--;
	assert(rdrlp->count >= 0);
}
