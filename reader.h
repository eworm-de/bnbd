/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

struct reader {
	TAILQ_ENTRY(reader)	all_rdrl_ary[2];
	struct thread		*thr;
#define RDR_FL_INUSE		0x00000001
	int			flags;
	int			pad;
	struct request_list	*aux_reql;
	char			*inbuf;
	size_t			inbuf_idx;
	size_t			inbuf_len;
	int64_t			cur_req_id;
	struct session		*sesp;
};
TAILQ_HEAD(reader_head, reader);

struct reader_list {
	struct reader_head	head;
	pthread_mutex_t		mutex;
#define RDRL_IDX_ALLOC		0
#define RDRL_IDX_STD		1
	int			index;
	int			count;
	int			peak;
};

extern struct reader_list *connected_rdrl;

int reader_module_setup(void);
void reader_module_hello(void);
void reader_module_teardown(void);
void reader_module_stats(void);
int reader_alloc(struct reader **);
void reader_init(struct reader *);
void reader_uninit(struct reader *);
void reader_free(struct reader *);
int reader_thread_create(struct reader *);
void reader_thread_launch(struct reader *);
void reader_thread_join(struct reader *);
void reader_thread_abort(struct reader *);
void reader_thread_shutdown(struct reader *);
int reader_list_alloc(struct reader_list **, int);
void reader_list_init(struct reader_list *, int);
void reader_list_uninit(struct reader_list *);
void reader_list_free(struct reader_list *);
void reader_list_lock(struct reader_list *);
void reader_list_unlock(struct reader_list *);
int reader_list_empty(struct reader_list *);
struct reader *reader_list_first(struct reader_list *);
struct reader *reader_list_next(struct reader_list *, struct reader *);
void reader_list_insert(struct reader_list *, struct reader *);
void reader_list_append(struct reader_list *, struct reader *);
void reader_list_remove(struct reader_list *, struct reader *);
