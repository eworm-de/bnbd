/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "thread.h"
#include "request.h"
#include "reader.h"
#include "writer.h"
#include "session.h"

static int session_module_initialized = 0;
static pthread_mutex_t session_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct session_list *available_sesl = NULL;
static struct session_list *inuse_sesl = NULL;

struct session_list *negotiating_sesl = NULL;

static void session_dealloc(struct session *);
static void session_list_dealloc(struct session_list *);

int
session_module_setup(void)
{

	assert(!session_module_initialized);
	assert(available_sesl == NULL);
	if (session_list_alloc(&available_sesl, SESL_IDX_ALLOC) < 0) {
		assert(available_sesl == NULL);
		goto out0;
	}
	assert(available_sesl != NULL);
	assert(inuse_sesl == NULL);
	if (session_list_alloc(&inuse_sesl, SESL_IDX_ALLOC) < 0) {
		assert(inuse_sesl == NULL);
		goto out1;
	}
	assert(inuse_sesl != NULL);
	assert(negotiating_sesl == NULL);
	if (session_list_alloc(&negotiating_sesl, SESL_IDX_STD) < 0) {
		assert(negotiating_sesl == NULL);
		goto out2;
	}
	assert(negotiating_sesl != NULL);
	session_module_initialized = 1;
	return (0);
out2:
	assert(inuse_sesl != NULL);
	session_list_free(inuse_sesl);
	inuse_sesl = NULL;
out1:
	assert(available_sesl != NULL);
	session_list_dealloc(available_sesl);
	session_list_free(available_sesl);
	available_sesl = NULL;
out0:
	return (-1);
}

void
session_module_hello(void)
{

	assert(session_module_initialized);
	DEBUG("sizeof(session)=%zu", sizeof(struct session));
	DEBUG("sizeof(session_list)=%zu", sizeof(struct session_list));
}

void
session_module_teardown(void)
{

	assert(session_module_initialized);
	assert(negotiating_sesl != NULL);
	session_list_free(negotiating_sesl);
	negotiating_sesl = NULL;
	assert(inuse_sesl != NULL);
	session_list_free(inuse_sesl);
	inuse_sesl = NULL;
	assert(available_sesl != NULL);
	session_list_dealloc(available_sesl);
	session_list_free(available_sesl);
	available_sesl = NULL;
	session_module_initialized = 0;
}

void
session_module_stats(void)
{
	int available_count, available_peak, inuse_count, inuse_peak;

	assert(session_module_initialized);
	PTHREAD_MUTEX_LOCK(&session_mutex);
	available_count = available_sesl->count;
	available_peak = available_sesl->peak;
	available_sesl->peak = available_sesl->count;
	inuse_count = inuse_sesl->count;
	inuse_peak = inuse_sesl->peak;
	inuse_sesl->peak = inuse_sesl->count;
	PTHREAD_MUTEX_UNLOCK(&session_mutex);
	NOTICE("inuse=%d/%d available=%d/%d limit=%d",
	    inuse_count, inuse_peak, available_count, available_peak,
	    cfg_max_nbd_clients);
}

int
session_alloc(struct session **sespp)
{
	struct session *sesp;

	assert(session_module_initialized);
	PTHREAD_MUTEX_LOCK(&session_mutex);
	sesp = session_list_first(available_sesl);
	if (sesp == NULL) {
		assert(available_sesl->count == 0);
		if (cfg_max_nbd_clients > 0 &&
		    inuse_sesl->count >= cfg_max_nbd_clients) {
			PTHREAD_MUTEX_UNLOCK(&session_mutex);
			NOTICE("max_nbd_clients=%d limit hit!",
			    cfg_max_nbd_clients);
			return (-1);
		}
		PTHREAD_MUTEX_UNLOCK(&session_mutex);
		sesp = malloc(sizeof(*sesp));
		if (sesp == NULL) {
			ERR("malloc(%zu): %s", sizeof(*sesp), strerror(errno));
			return (-2);
		}
		sesp->rdr = NULL;
		if (reader_alloc(&sesp->rdr) < 0) {
			assert(sesp->rdr == NULL);
			free(sesp);
			return (-3);
		}
		assert(sesp->rdr != NULL);
		sesp->wrr = NULL;
		if (writer_alloc(&sesp->wrr) < 0) {
			assert(sesp->wrr == NULL);
			reader_free(sesp->rdr);
			free(sesp);
			return (-4);
		}
		assert(sesp->wrr != NULL);
		sesp->outgoing_reql = NULL;
		if (request_list_alloc(&sesp->outgoing_reql, REQL_IDX_STD) <
		    0) {
			assert(sesp->outgoing_reql == NULL);
			writer_free(sesp->wrr);
			reader_free(sesp->rdr);
			free(sesp);
			return (-5);
		}
		assert(sesp->outgoing_reql != NULL);
		sesp->flush_reql = NULL;
		if (request_list_alloc(&sesp->flush_reql, REQL_IDX_STD) < 0) {
			assert(sesp->flush_reql == NULL);
			request_list_free(sesp->outgoing_reql);
			writer_free(sesp->wrr);
			reader_free(sesp->rdr);
			free(sesp);
			return (-6);
		}
		assert(sesp->flush_reql != NULL);
		session_init(sesp);
		PTHREAD_MUTEX_LOCK(&session_mutex);
	} else {
		session_list_remove(available_sesl, sesp);
	}
	assert(!sesp->flags);
	sesp->flags |= SES_FL_INUSE;
	session_list_insert(inuse_sesl, sesp);
	PTHREAD_MUTEX_UNLOCK(&session_mutex);
	assert(sesp->so < 0);
	assert(sesp->volp == NULL);
	assert(sesp->last_req_id < (int64_t)0);
	*sespp = sesp;
	return (0);
}

void
session_init(struct session *sesp)
{

	sesp->flags = 0;
	sesp->so = -1;
	memset(sesp->inet_addr, 0x00, sizeof(sesp->inet_addr));
	sesp->inet_port = -1;
	sesp->volp = NULL;
	sesp->last_req_id = (int64_t)-1;
}

void
session_uninit(struct session *sesp)
{

	assert(sesp->flags == SES_FL_INUSE);
	sesp->so = -1;
	memset(sesp->inet_addr, 0x00, sizeof(sesp->inet_addr));
	sesp->inet_port = -1;
	reader_uninit(sesp->rdr);
	writer_uninit(sesp->wrr);
	sesp->volp = NULL;
	sesp->last_req_id = (int64_t)-1;
	request_list_uninit(sesp->outgoing_reql);
	request_list_uninit(sesp->flush_reql);
}

void
session_free(struct session *sesp)
{

	assert(session_module_initialized);
	session_uninit(sesp);
	PTHREAD_MUTEX_LOCK(&session_mutex);
	session_list_remove(inuse_sesl, sesp);
	sesp->flags &= ~SES_FL_INUSE;
	assert(!sesp->flags);
	session_list_insert(available_sesl, sesp);
	PTHREAD_MUTEX_UNLOCK(&session_mutex);
}

static
void
session_dealloc(struct session *sesp)
{

	assert(!sesp->flags);
	request_list_free(sesp->flush_reql);
	request_list_free(sesp->outgoing_reql);
	writer_free(sesp->wrr);
	reader_free(sesp->rdr);
	free(sesp);
}

void
session_shutdown(struct session *sesp)
{
	int wakeup;

	request_list_lock(sesp->outgoing_reql);
	sesp->outgoing_reql->flags |= REQL_FL_SHUTDOWN;
	sesp->last_req_id = sesp->rdr->cur_req_id;
	if (sesp->outgoing_reql->flags & REQL_FL_NOTIFIED) {
		wakeup = 0;
	} else {
		sesp->outgoing_reql->flags |= REQL_FL_NOTIFIED;
		wakeup = 1;
	}
	request_list_unlock(sesp->outgoing_reql);
	if (wakeup)
		request_list_wakeup(sesp->outgoing_reql);
}

int
session_list_alloc(struct session_list **seslpp, int index)
{
	struct session_list *seslp;
	int error;

	seslp = malloc(sizeof(*seslp));
	if (seslp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*seslp), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&seslp->mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(seslp);
		return (-3);
	}
	session_list_init(seslp, index);
	*seslpp = seslp;
	return (0);
}

void
session_list_init(struct session_list *seslp, int index)
{
#ifndef NDEBUG
	struct session *sesp;
#endif

	assert(index >= 0 && index < NELEM(sesp->all_sesl_ary));
	TAILQ_INIT(&seslp->head);
	seslp->index = index;
	seslp->count = 0;
	seslp->peak = 0;
}

void
session_list_uninit(struct session_list *seslp)
{

	assert(TAILQ_EMPTY(&seslp->head));
	assert(seslp->count == 0);
	seslp->peak = 0;
}

void
session_list_free(struct session_list *seslp)
{

	session_list_uninit(seslp);
	PTHREAD_MUTEX_DESTROY(&seslp->mutex);
	free(seslp);
}

static
void
session_list_dealloc(struct session_list *seslp)
{
	struct session *nsesp, *sesp;

	nsesp = session_list_first(seslp);
	for (;;) {
		sesp = nsesp;
		if (sesp == NULL)
			break;
		nsesp = session_list_next(seslp, sesp);
		session_list_remove(seslp, sesp);
		session_dealloc(sesp);
	}
}

void
session_list_lock(struct session_list *seslp)
{

	PTHREAD_MUTEX_LOCK(&seslp->mutex);
}

void
session_list_unlock(struct session_list *seslp)
{

	PTHREAD_MUTEX_UNLOCK(&seslp->mutex);
}

int
session_list_empty(struct session_list *seslp)
{

	return (TAILQ_EMPTY(&seslp->head));
}

struct session *
session_list_first(struct session_list *seslp)
{

	return (TAILQ_FIRST(&seslp->head));
}

struct session *
session_list_next(struct session_list *seslp, struct session *sesp)
{

	return (TAILQ_NEXT(sesp, all_sesl_ary[seslp->index]));
}

void
session_list_insert(struct session_list *seslp, struct session *sesp)
{

	TAILQ_INSERT_HEAD(&seslp->head, sesp, all_sesl_ary[seslp->index]);
	seslp->count++;
	assert(seslp->count > 0);
	if (seslp->count > seslp->peak)
		seslp->peak = seslp->count;
}

void
session_list_append(struct session_list *seslp, struct session *sesp)
{

	TAILQ_INSERT_TAIL(&seslp->head, sesp, all_sesl_ary[seslp->index]);
	seslp->count++;
	assert(seslp->count > 0);
	if (seslp->count > seslp->peak)
		seslp->peak = seslp->count;
}

void
session_list_remove(struct session_list *seslp, struct session *sesp)
{

	TAILQ_REMOVE(&seslp->head, sesp, all_sesl_ary[seslp->index]);
	seslp->count--;
	assert(seslp->count >= 0);
}

void
session_list_shutdown(struct session_list *seslp)
{
	struct session *sesp;

	for (;;) {
		session_list_lock(seslp);
		sesp = session_list_first(seslp);
		if (sesp == NULL) {
			session_list_unlock(seslp);
			break;
		}
		writer_thread_shutdown(sesp->wrr);
		reader_thread_shutdown(sesp->rdr);
		session_list_unlock(seslp);
		writer_thread_join(sesp->wrr);
		session_free(sesp);
	}
}
