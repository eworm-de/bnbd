/*-
 * Copyright (c) 2013-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

struct skiver {
	TAILQ_ENTRY(skiver)	all_skvl_ary[2];
	struct thread		*thr;
#define SKV_FL_INUSE		0x00000001
	int			flags;
};
TAILQ_HEAD(skiver_head, skiver);

struct skiver_list {
	struct skiver_head	head;
	pthread_mutex_t		mutex;
#define SKVL_IDX_ALLOC		0
#define SKVL_IDX_STD		1
	int			index;
	int			count;
	int			peak;
};

int skiver_module_setup(void);
void skiver_module_hello(void);
void skiver_module_teardown(void);
void skiver_module_stats(void);
int skiver_alloc(struct skiver **);
void skiver_init(struct skiver *);
void skiver_uninit(struct skiver *);
void skiver_free(struct skiver *);
void skiver_thread_wakeup(struct skiver *);
void skiver_reql_submit(struct request_list *);
int skiver_list_alloc(struct skiver_list **, int);
void skiver_list_init(struct skiver_list *, int);
void skiver_list_uninit(struct skiver_list *);
void skiver_list_free(struct skiver_list *);
int skiver_list_empty(struct skiver_list *);
struct skiver *skiver_list_first(struct skiver_list *);
struct skiver *skiver_list_next(struct skiver_list *, struct skiver *);
void skiver_list_insert(struct skiver_list *, struct skiver *);
void skiver_list_append(struct skiver_list *, struct skiver *);
void skiver_list_remove(struct skiver_list *, struct skiver *);
