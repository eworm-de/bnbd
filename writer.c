/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "io.h"
#include "thread.h"
#include "request.h"
#include "reader.h"
#include "writer.h"
#include "session.h"
#include "volume.h"

static int writer_module_initialized = 0;
static pthread_mutex_t writer_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct writer_list *available_wrrl = NULL;
static struct writer_list *inuse_wrrl = NULL;
static int64_t max_reply_burst_bytes = (int64_t)0;

struct writer_list *connected_wrrl = NULL;

static void writer_dealloc(struct writer *);
static void writer_list_dealloc(struct writer_list *);
static void *writer_thread(void *);
static void writer_thread_loop(struct writer *);
static struct request *flush_reql_next(struct request_list *);
static struct request *flush_reql_bump(struct request_list *, struct request *);

int
writer_module_setup(void)
{

	assert(!writer_module_initialized);
	assert(available_wrrl == NULL);
	if (writer_list_alloc(&available_wrrl, WRRL_IDX_ALLOC) < 0) {
		assert(available_wrrl == NULL);
		goto out0;
	}
	assert(available_wrrl != NULL);
	assert(inuse_wrrl == NULL);
	if (writer_list_alloc(&inuse_wrrl, WRRL_IDX_ALLOC) < 0) {
		assert(inuse_wrrl == NULL);
		goto out1;
	}
	assert(inuse_wrrl != NULL);
	assert(connected_wrrl == NULL);
	if (writer_list_alloc(&connected_wrrl, WRRL_IDX_STD) < 0) {
		assert(connected_wrrl == NULL);
		goto out2;
	}
	assert(connected_wrrl != NULL);
	if (cfg_max_reply_burst_kb > 0)
		max_reply_burst_bytes = (int64_t)cfg_max_reply_burst_kb << 10;
	writer_module_initialized = 1;
	return (0);
out2:
	assert(inuse_wrrl != NULL);
	writer_list_free(inuse_wrrl);
	inuse_wrrl = NULL;
out1:
	assert(available_wrrl != NULL);
	writer_list_dealloc(available_wrrl);
	writer_list_free(available_wrrl);
	available_wrrl = NULL;
out0:
	return (-1);
}

void
writer_module_hello(void)
{

	assert(writer_module_initialized);
	DEBUG("sizeof(writer)=%zu", sizeof(struct writer));
	DEBUG("sizeof(writer_list)=%zu", sizeof(struct writer_list));
}

void
writer_module_teardown(void)
{

	assert(writer_module_initialized);
	assert(connected_wrrl != NULL);
	writer_list_free(connected_wrrl);
	connected_wrrl = NULL;
	assert(inuse_wrrl != NULL);
	writer_list_free(inuse_wrrl);
	inuse_wrrl = NULL;
	assert(available_wrrl != NULL);
	writer_list_dealloc(available_wrrl);
	writer_list_free(available_wrrl);
	available_wrrl = NULL;
	writer_module_initialized = 0;
}

void
writer_module_stats(void)
{
	int available_count, available_peak, inuse_count, inuse_peak;

	assert(writer_module_initialized);
	PTHREAD_MUTEX_LOCK(&writer_mutex);
	available_count = available_wrrl->count;
	available_peak = available_wrrl->peak;
	available_wrrl->peak = available_wrrl->count;
	inuse_count = inuse_wrrl->count;
	inuse_peak = inuse_wrrl->peak;
	inuse_wrrl->peak = inuse_wrrl->count;
	PTHREAD_MUTEX_UNLOCK(&writer_mutex);
	NOTICE("inuse=%d/%d available=%d/%d",
	    inuse_count, inuse_peak, available_count, available_peak);
}

int
writer_alloc(struct writer **wrrpp)
{
	struct writer *wrrp;
	size_t len;

	assert(writer_module_initialized);
	PTHREAD_MUTEX_LOCK(&writer_mutex);
	wrrp = writer_list_first(available_wrrl);
	if (wrrp == NULL) {
		assert(available_wrrl->count == 0);
		PTHREAD_MUTEX_UNLOCK(&writer_mutex);
		wrrp = malloc(sizeof(*wrrp));
		if (wrrp == NULL) {
			ERR("malloc(%zu): %s", sizeof(*wrrp), strerror(errno));
			return (-2);
		}
		wrrp->thr = NULL;
		if (thread_alloc(&wrrp->thr, wrrp) < 0) {
			assert(wrrp->thr == NULL);
			free(wrrp);
			return (-3);
		}
		assert(wrrp->thr != NULL);
		wrrp->aux_reql = NULL;
		if (request_list_alloc(&wrrp->aux_reql, REQL_IDX_STD) < 0) {
			assert(wrrp->aux_reql == NULL);
			thread_free(wrrp->thr);
			free(wrrp);
			return (-4);
		}
		assert(wrrp->aux_reql != NULL);
		assert(cfg_max_reply_burst > 0);
		if (cfg_max_reply_burst * 2 < IOV_MAX)
			wrrp->aux_iov_ary_len = cfg_max_reply_burst * 2;
		else
			wrrp->aux_iov_ary_len = IOV_MAX;
		len = (size_t)wrrp->aux_iov_ary_len * sizeof(struct iovec);
		wrrp->aux_iov_ary = malloc(len);
		if (wrrp->aux_iov_ary == NULL) {
			ERR("malloc(%zu): %s", len, strerror(errno));
			request_list_free(wrrp->aux_reql);
			thread_free(wrrp->thr);
			free(wrrp);
			return (-5);
		}
		writer_init(wrrp);
		PTHREAD_MUTEX_LOCK(&writer_mutex);
	} else {
		writer_list_remove(available_wrrl, wrrp);
	}
	assert(!wrrp->flags);
	wrrp->flags |= WRR_FL_INUSE;
	writer_list_insert(inuse_wrrl, wrrp);
	PTHREAD_MUTEX_UNLOCK(&writer_mutex);
	assert(wrrp->cur_req_id == (int64_t)0);
	assert(wrrp->sesp == NULL);
	*wrrpp = wrrp;
	return (0);
}

void
writer_init(struct writer *wrrp)
{

	wrrp->flags = 0;
	wrrp->cur_req_id = (int64_t)0;
	wrrp->sesp = NULL;
}

void
writer_uninit(struct writer *wrrp)
{

	thread_uninit(wrrp->thr);
	assert(wrrp->flags == WRR_FL_INUSE);
	request_list_uninit(wrrp->aux_reql);
	wrrp->cur_req_id = (int64_t)0;
	wrrp->sesp = NULL;
}

void
writer_free(struct writer *wrrp)
{

	assert(writer_module_initialized);
	writer_uninit(wrrp);
	PTHREAD_MUTEX_LOCK(&writer_mutex);
	writer_list_remove(inuse_wrrl, wrrp);
	wrrp->flags &= ~WRR_FL_INUSE;
	assert(!wrrp->flags);
	writer_list_insert(available_wrrl, wrrp);
	PTHREAD_MUTEX_UNLOCK(&writer_mutex);
}

static
void
writer_dealloc(struct writer *wrrp)
{

	assert(!wrrp->flags);
	free(wrrp->aux_iov_ary);
	request_list_free(wrrp->aux_reql);
	thread_free(wrrp->thr);
	free(wrrp);
}

int
writer_thread_create(struct writer *wrrp)
{

	return (thread_create(wrrp->thr, "writer", writer_thread));
}

void
writer_thread_launch(struct writer *wrrp)
{

	thread_launch(wrrp->thr);
}

void
writer_thread_join(struct writer *wrrp)
{

	thread_join(wrrp->thr);
}

void
writer_thread_shutdown(struct writer *wrrp)
{

	thread_shutdown_begin(wrrp->thr);
}

static
void *
writer_thread(void *data)
{
	struct writer *wrrp;
	struct session *sesp;
	struct session_list *seslp;
	int detach_self;

	wrrp = (struct writer *)data;
	assert(wrrp != NULL);
	assert(wrrp->thr != NULL);
	assert(wrrp->aux_iov_ary != NULL);
	assert(wrrp->aux_iov_ary_len > 0);

	thread_prologue(wrrp->thr);

	if (wrrp->flags & WRR_FL_ABORT) {
		wrrp->flags &= ~WRR_FL_ABORT;
		goto out;
	}

	sesp = wrrp->sesp;
	assert(sesp != NULL);
	writer_thread_loop(wrrp);

	seslp = negotiating_sesl;
	session_list_lock(seslp);
	if (sesp->volp != NULL) {
		session_list_unlock(seslp);
		seslp = sesp->volp->established_sesl;
		session_list_lock(seslp);
	}
	session_list_remove(seslp, sesp);
	detach_self = thread_shutdown_commit(wrrp->thr);
	session_list_unlock(seslp);
	reader_thread_join(sesp->rdr);
	if (detach_self) {
		thread_detach(wrrp->thr);
		session_free(sesp);
	}
out:
	thread_epilogue(NULL);
	return (NULL);
}

static
void
writer_thread_loop(struct writer *wrrp)
{
	struct session *sesp;
	struct iovec *aux_iov_aryp;
	struct request_list *aux_reqlp, *flush_reqlp, *outgoing_reqlp;
	struct request *nreqp, *reqp;
	int64_t reply_bytes, reply_bytes_peak;
	ssize_t n;
	int aux_iov_ary_len, do_flush, do_sync, error, i, iov_count, peak,
	    reply_peak, so;

	aux_reqlp = wrrp->aux_reql;
	assert(aux_reqlp != NULL);
	assert(request_list_empty(aux_reqlp));
	aux_iov_aryp = wrrp->aux_iov_ary;
	aux_iov_ary_len = wrrp->aux_iov_ary_len;
	assert(wrrp->cur_req_id == (int64_t)0);
	sesp = wrrp->sesp;
	so = sesp->so;
	outgoing_reqlp = sesp->outgoing_reql;
	assert(outgoing_reqlp != NULL);
	flush_reqlp = sesp->flush_reql;
	assert(flush_reqlp != NULL);

	reply_peak = 0;
	reply_bytes_peak = (int64_t)0;
	request_list_lock(outgoing_reqlp);
	for (;;) {
		if (outgoing_reqlp->flags & REQL_FL_SHUTDOWN) {
			assert(sesp->last_req_id >= (int64_t)0);
			if (wrrp->cur_req_id == sesp->last_req_id) {
				outgoing_reqlp->flags &= ~REQL_FL_SHUTDOWN;
				request_list_unlock(outgoing_reqlp);
				break;
			}
		}
		if (request_list_empty(outgoing_reqlp) &&
		    flush_reql_next(flush_reqlp) == NULL) {
			outgoing_reqlp->flags &= ~REQL_FL_NOTIFIED;
			request_list_wait(outgoing_reqlp);
			continue;
		}
		assert(outgoing_reqlp->flags & REQL_FL_NOTIFIED);
		assert(!request_list_empty(outgoing_reqlp) ||
		    !request_list_empty(flush_reqlp));
		assert(request_list_empty(aux_reqlp));

		do_flush = 0;
		do_sync = 0;
		iov_count = 0;
		reply_bytes = (int64_t)0;
		for (i = 0; i < cfg_max_reply_burst; i++) {
			reqp = flush_reql_next(flush_reqlp);
			if (reqp != NULL) {
				if (i > 0)
					break;
			} else {
				reqp = request_list_first(outgoing_reqlp);
				if (reqp == NULL)
					break;
			}
			assert(reqp != NULL);
			switch (reqp->type) {
			case REQ_TY_READ:
				assert(reqp->nbd.request.type == NBD_CMD_READ);
				assert(reqp->master_reqp == NULL);
				assert(reqp->nbd.reply.error !=
				    (uint32_t)0xABABABAB);
				assert(reqp->outgoing_reqlp == outgoing_reqlp);
				break;
			case REQ_TY_WRITE:
			case REQ_TY_TRIM:
				assert(reqp->nbd.request.type == NBD_CMD_WRITE ||
				    reqp->nbd.request.type == NBD_CMD_TRIM);
				assert(reqp->master_reqp == NULL);
				assert(reqp->nbd.reply.error !=
				    (uint32_t)0xABABABAB);
				assert(reqp->outgoing_reqlp == outgoing_reqlp);
				break;
			case REQ_TY_MASTER_WRITE:
			case REQ_TY_MASTER_TRIM:
				assert(reqp->nbd.request.type == NBD_CMD_WRITE ||
				    reqp->nbd.request.type == NBD_CMD_TRIM);
				assert(reqp->nbd.reply.error !=
				    (uint32_t)0xABABABAB);
				assert(reqp->master_reqp == reqp);
				assert(reqp->outgoing_reqlp == outgoing_reqlp);
				break;
			case REQ_TY_FLUSH:
				assert(reqp->nbd.request.type == NBD_CMD_FLUSH);
				assert(reqp->nbd.reply.error ==
				    (uint32_t)0xABABABAB);
				assert(reqp->master_reqp == NULL);
				assert(reqp->outgoing_reqlp == flush_reqlp);
				break;
			default:
				abort();
			}
			assert(reqp->id >= (int64_t)0);
			assert(reqp->nbd.reply.magic == htonl(NBD_REPLY_MAGIC));
			assert(reqp->nbd.reply.handle ==
			    reqp->nbd.request.handle);
			assert(reqp->volp == sesp->volp);
			assert(reqp->mirp == NULL);
			if (reqp->nbd.request.type == NBD_CMD_READ &&
			    reqp->nbd.reply.error == (uint32_t)0 &&
			    reqp->buf.buf_ptr != NULL &&
			    iov_count + 2 <= aux_iov_ary_len) {
				reply_bytes += reqp->buf.buf_len;
				assert(reply_bytes > (int64_t)0);
				if (i > 0 &&
				    max_reply_burst_bytes > (int64_t)0 &&
				    reply_bytes > max_reply_burst_bytes) {
					reply_bytes -= reqp->buf.buf_len;
					assert(reply_bytes >= (int64_t)0);
					break;
				}
				iov_count += 2;
			} else if (iov_count + 1 <= aux_iov_ary_len) {
				iov_count += 1;
			} else {
				break;
			}
			if (reqp->nbd.request.type == NBD_CMD_WRITE &&
			    do_sync == 0 && cfg_sync == 2)
				do_sync = 1;
			request_list_remove(reqp->outgoing_reqlp, reqp);
			flush_reql_bump(flush_reqlp, reqp);
			request_list_append(aux_reqlp, reqp);
			wrrp->cur_req_id++;
			assert(wrrp->cur_req_id > (int64_t)0);
			if (reqp->nbd.request.type == NBD_CMD_FLUSH) {
				do_flush = 1;
				break;
			}
		}
		assert(iov_count >= 0 && iov_count <= aux_iov_ary_len);
		request_list_unlock(outgoing_reqlp);

		if (do_flush || do_sync) {
			if (cfg_sync == 1)
				error = 0;
			else
#ifdef __linux__
				error = fdatasync(sesp->volp->fd);
#else
				error = fsync(sesp->volp->fd);
#endif
			if (error < 0) {
				error = -errno;
#ifdef __linux__
				WARNING("fdatasync(): %s", strerror(errno));
#else
				WARNING("fsync(): %s", strerror(errno));
#endif
			}
			if (do_flush) {
				assert(i == 0);
				reqp->nbd.reply.error = htonl((uint32_t)error);
				DEBUG("Flush-%lld: %d",
				    (long long)reqp->id, error);
			}
		}

		iov_count = 0;
		nreqp = request_list_first(aux_reqlp);
		for (;;) {
			reqp = nreqp;
			if (reqp == NULL)
				break;
			nreqp = request_list_next(aux_reqlp, reqp);
			aux_iov_aryp[iov_count].iov_base = &reqp->nbd.reply;
			aux_iov_aryp[iov_count].iov_len =
			    sizeof(reqp->nbd.reply);
			iov_count++;
			if (reqp->nbd.request.type == NBD_CMD_READ &&
			    reqp->nbd.reply.error == (uint32_t)0 &&
			    reqp->buf.buf_ptr != NULL) {
				aux_iov_aryp[iov_count].iov_base =
				    reqp->buf.buf_ptr;
				aux_iov_aryp[iov_count].iov_len =
				    (size_t)reqp->buf.buf_len;
				iov_count++;
			}
		}
		assert(iov_count >= 0 && iov_count <= aux_iov_ary_len);
		assert(i <= iov_count);
		if (iov_count > 0) {
			n = iov_tx(so, aux_iov_aryp, iov_count, 0);
			peak = 0;
			if (i > reply_peak) {
				reply_peak = i;
				peak = 1;
			}
			if (reply_bytes > reply_bytes_peak) {
				reply_bytes_peak = reply_bytes;
				peak = 1;
			}
			if (peak)
				DEBUG("reply_burst_peak=%d/%d/%lld/%zd",
				    reply_peak, iov_count,
				    (long long)reply_bytes, n);
		}
		nreqp = request_list_first(aux_reqlp);
		for (;;) {
			reqp = nreqp;
			if (reqp == NULL)
				break;
			nreqp = request_list_next(aux_reqlp, reqp);
			request_list_remove(aux_reqlp, reqp);
			request_free(reqp);
		}
		request_list_lock(outgoing_reqlp);
	}
	fd_close(so);
	NOTICE("Closed NBD connection from %s:%d",
	    sesp->inet_addr, sesp->inet_port);
}

static
struct request *
flush_reql_next(struct request_list *flush_reqlp)
{
	struct request *nreqp, *reqp;

	nreqp = request_list_first(flush_reqlp);
	for (;;) {
		reqp = nreqp;
		if (reqp == NULL)
			break;
		assert(reqp->type == REQ_TY_FLUSH);
		if ((int64_t)reqp->nbd.request.from == reqp->id)
			break;
		nreqp = request_list_next(flush_reqlp, reqp);
	}
	return (reqp);
}

static
struct request *
flush_reql_bump(struct request_list *flush_reqlp, struct request *cur_reqp)
{
	struct request *nreqp, *reqp;

	nreqp = request_list_first(flush_reqlp);
	for (;;) {
		reqp = nreqp;
		if (reqp == NULL)
			break;
		assert(reqp->type == REQ_TY_FLUSH);
		if (cur_reqp->id < reqp->id) {
			reqp->nbd.request.from++;
			assert((int64_t)reqp->nbd.request.from <= reqp->id);
			break;
		}
		nreqp = request_list_next(flush_reqlp, reqp);
	}
	return (reqp);
}

int
writer_list_alloc(struct writer_list **wrrlpp, int index)
{
	struct writer_list *wrrlp;
	int error;

	wrrlp = malloc(sizeof(*wrrlp));
	if (wrrlp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*wrrlp), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&wrrlp->mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(wrrlp);
		return (-3);
	}
	writer_list_init(wrrlp, index);
	*wrrlpp = wrrlp;
	return (0);
}

void
writer_list_init(struct writer_list *wrrlp, int index)
{
#ifndef NDEBUG
	struct writer *wrrp;
#endif

	assert(index >= 0 && index < NELEM(wrrp->all_wrrl_ary));
	TAILQ_INIT(&wrrlp->head);
	wrrlp->index = index;
	wrrlp->count = 0;
	wrrlp->peak = 0;
}

void
writer_list_uninit(struct writer_list *wrrlp)
{

	assert(TAILQ_EMPTY(&wrrlp->head));
	assert(wrrlp->count == 0);
	wrrlp->peak = 0;
}

void
writer_list_free(struct writer_list *wrrlp)
{

	writer_list_uninit(wrrlp);
	PTHREAD_MUTEX_DESTROY(&wrrlp->mutex);
	free(wrrlp);
}

static
void
writer_list_dealloc(struct writer_list *wrrlp)
{
	struct writer *nwrrp, *wrrp;

	nwrrp = writer_list_first(wrrlp);
	for (;;) {
		wrrp = nwrrp;
		if (wrrp == NULL)
			break;
		nwrrp = writer_list_next(wrrlp, wrrp);
		writer_list_remove(wrrlp, wrrp);
		writer_dealloc(wrrp);
	}
}

void
writer_list_lock(struct writer_list *wrrlp)
{

	PTHREAD_MUTEX_LOCK(&wrrlp->mutex);
}

void
writer_list_unlock(struct writer_list *wrrlp)
{

	PTHREAD_MUTEX_UNLOCK(&wrrlp->mutex);
}

int
writer_list_empty(struct writer_list *wrrlp)
{

	return (TAILQ_EMPTY(&wrrlp->head));
}

struct writer *
writer_list_first(struct writer_list *wrrlp)
{

	return (TAILQ_FIRST(&wrrlp->head));
}

struct writer *
writer_list_next(struct writer_list *wrrlp, struct writer *wrrp)
{

	return (TAILQ_NEXT(wrrp, all_wrrl_ary[wrrlp->index]));
}

void
writer_list_insert(struct writer_list *wrrlp, struct writer *wrrp)
{

	TAILQ_INSERT_HEAD(&wrrlp->head, wrrp, all_wrrl_ary[wrrlp->index]);
	wrrlp->count++;
	assert(wrrlp->count > 0);
	if (wrrlp->count > wrrlp->peak)
		wrrlp->peak = wrrlp->count;
}

void
writer_list_append(struct writer_list *wrrlp, struct writer *wrrp)
{

	TAILQ_INSERT_TAIL(&wrrlp->head, wrrp, all_wrrl_ary[wrrlp->index]);
	wrrlp->count++;
	assert(wrrlp->count > 0);
	if (wrrlp->count > wrrlp->peak)
		wrrlp->peak = wrrlp->count;
}

void
writer_list_remove(struct writer_list *wrrlp, struct writer *wrrp)
{

	TAILQ_REMOVE(&wrrlp->head, wrrp, all_wrrl_ary[wrrlp->index]);
	wrrlp->count--;
	assert(wrrlp->count >= 0);
}
