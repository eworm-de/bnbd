/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "thread.h"

static int thread_module_initialized = 0;
static pthread_mutex_t thread_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct thread_list *available_thrl = NULL;
static struct thread_list *inuse_thrl = NULL;

static void thread_dealloc(struct thread *);
static void thread_list_dealloc(struct thread_list *);

int
thread_module_setup(void)
{

	assert(!thread_module_initialized);
	assert(available_thrl == NULL);
	if (thread_list_alloc(&available_thrl, THRL_IDX_ALLOC) < 0) {
		assert(available_thrl == NULL);
		goto out0;
	}
	assert(available_thrl != NULL);
	assert(inuse_thrl == NULL);
	if (thread_list_alloc(&inuse_thrl, THRL_IDX_ALLOC) < 0) {
		assert(inuse_thrl == NULL);
		goto out1;
	}
	assert(inuse_thrl != NULL);
	thread_module_initialized = 1;
	return (0);
out1:
	assert(available_thrl != NULL);
	thread_list_dealloc(available_thrl);
	thread_list_free(available_thrl);
	available_thrl = NULL;
out0:
	return (-1);
}

void
thread_module_hello(void)
{

	assert(thread_module_initialized);
	DEBUG("sizeof(thread)=%zu", sizeof(struct thread));
	DEBUG("sizeof(thread_head)=%zu", sizeof(struct thread_head));
	DEBUG("sizeof(thread_list)=%zu", sizeof(struct thread_list));
}

void
thread_module_teardown(void)
{

	assert(thread_module_initialized);
	assert(inuse_thrl != NULL);
	thread_list_free(inuse_thrl);
	inuse_thrl = NULL;
	assert(available_thrl != NULL);
	thread_list_dealloc(available_thrl);
	thread_list_free(available_thrl);
	available_thrl = NULL;
	thread_module_initialized = 0;
}

void
thread_module_stats(void)
{
	int available_count, available_peak, inuse_count, inuse_peak;

	assert(thread_module_initialized);
	PTHREAD_MUTEX_LOCK(&thread_mutex);
	available_count = available_thrl->count;
	available_peak = available_thrl->peak;
	available_thrl->peak = available_thrl->count;
	inuse_count = inuse_thrl->count;
	inuse_peak = inuse_thrl->peak;
	inuse_thrl->peak = inuse_thrl->count;
	PTHREAD_MUTEX_UNLOCK(&thread_mutex);
	NOTICE("inuse=%d/%d available=%d/%d",
	    inuse_count, inuse_peak, available_count, available_peak);
}

int
thread_alloc(struct thread **thrpp, void *data)
{
	struct thread *thrp;
	int error;

	assert(thread_module_initialized);
	assert(data != NULL);
	PTHREAD_MUTEX_LOCK(&thread_mutex);
	thrp = thread_list_first(available_thrl);
	if (thrp == NULL) {
		assert(available_thrl->count == 0);
		PTHREAD_MUTEX_UNLOCK(&thread_mutex);
		thrp = malloc(sizeof(*thrp));
		if (thrp == NULL) {
			ERR("malloc(%zu): %s", sizeof(*thrp), strerror(errno));
			return (-2);
		}
		error = pthread_mutex_init(&thrp->mutex, NULL);
		if (error) {
			ERR("pthread_mutex_init(): %d", error);
			free(thrp);
			return (-3);
		}
		error = pthread_cond_init(&thrp->cond, NULL);
		if (error) {
			ERR("pthread_cond_init(): %d", error);
			PTHREAD_MUTEX_DESTROY(&thrp->mutex);
			free(thrp);
			return (-4);
		}
		thread_init(thrp);
		PTHREAD_MUTEX_LOCK(&thread_mutex);
	} else {
		thread_list_remove(available_thrl, thrp);
	}
	assert(!thrp->flags);
	thrp->flags |= THR_FL_INUSE;
	thread_list_insert(inuse_thrl, thrp);
	PTHREAD_MUTEX_UNLOCK(&thread_mutex);
	assert(thrp->data == NULL);
	thrp->data = data;
	*thrpp = thrp;
	return (0);
}

void
thread_init(struct thread *thrp)
{

	thrp->flags = 0;
	thrp->flags2 = 0;
	thrp->tdp = NULL;
	thrp->data = NULL;
	thrp->name = NULL;
}

void
thread_uninit(struct thread *thrp)
{

#ifndef NDEBUG
	assert(thrp->flags == THR_FL_INUSE);
	assert(!thrp->flags2);
	assert(thrp->tdp == NULL);
	assert(thrp->data != NULL);
	assert(thrp->name == NULL);
#else
	(void)thrp;
#endif
}

void
thread_free(struct thread *thrp)
{

	assert(thread_module_initialized);
	thread_uninit(thrp);
	thrp->data = NULL;
	PTHREAD_MUTEX_LOCK(&thread_mutex);
	thread_list_remove(inuse_thrl, thrp);
	thrp->flags &= ~THR_FL_INUSE;
	assert(!thrp->flags);
	thread_list_insert(available_thrl, thrp);
	PTHREAD_MUTEX_UNLOCK(&thread_mutex);
}

static
void
thread_dealloc(struct thread *thrp)
{

	assert(!thrp->flags);
	assert(thrp->tdp == NULL);
	assert(thrp->data == NULL);
	assert(thrp->name == NULL);
	PTHREAD_COND_DESTROY(&thrp->cond);
	PTHREAD_MUTEX_DESTROY(&thrp->mutex);
	free(thrp);
}

void
thread_lock(struct thread *thrp)
{

	PTHREAD_MUTEX_LOCK(&thrp->mutex);
}

void
thread_unlock(struct thread *thrp)
{

	PTHREAD_MUTEX_UNLOCK(&thrp->mutex);
}

int
thread_create(struct thread *thrp, const char *name, void *(*main)(void *))
{
	int error;

	assert(thrp->tdp == NULL);
	assert(thrp->name == NULL);
	thrp->name = name;
	thread_lock(thrp);
	error = pthread_create(&thrp->td, NULL, main, thrp->data);
	if (error) {
		thread_unlock(thrp);
		thrp->name = NULL;
		return (-2);
	}
	thrp->tdp = &thrp->td;
	thread_wait(thrp);
	DEBUG("Created %s %llX", thrp->name, (long long)thrp->td);
	return (0);
}

void
thread_launch(struct thread *thrp)
{

	DEBUG("Unlock %s %llX", thrp->name, (long long)thrp->td);
	thread_unlock(thrp);
	DEBUG("Wake up %s %llX", thrp->name, (long long)thrp->td);
	thread_wakeup(thrp);
}

void
thread_prologue(struct thread *thrp)
{

	DEBUG("Hello");
	assert(pthread_equal(thrp->td, pthread_self()));

	DEBUG("Thread %s %llX started!", thrp->name, (long long)thrp->td);
	thread_lock(thrp);
	DEBUG("Thread %s %llX locked, wake up parent",
	    thrp->name, (long long)thrp->td);
	thread_wakeup(thrp);
	DEBUG("Thread %s %llX unlocked, waiting...",
	    thrp->name, (long long)thrp->td);
	thread_wait(thrp);
	DEBUG("Thread %s %llX running!", thrp->name, (long long)thrp->td);
	thread_unlock(thrp);
}

void
thread_epilogue(void *retval)
{

	DEBUG("Bye");
	pthread_exit(retval);
}

void
thread_detach(struct thread *thrp)
{

	DEBUG("Detach %s %llX", thrp->name, (long long)thrp->td);
	PTHREAD_DETACH(thrp->td);
	DEBUG("Detached %s %llX", thrp->name, (long long)thrp->td);
	thrp->tdp = NULL;
	thrp->name = NULL;
}

void
thread_join(struct thread *thrp)
{

	DEBUG("Join %s %llX", thrp->name, (long long)thrp->td);
	PTHREAD_JOIN(thrp->td, NULL);
	DEBUG("Joined %s %llX", thrp->name, (long long)thrp->td);
	thrp->tdp = NULL;
	thrp->name = NULL;
}

void
thread_kill(struct thread *thrp, int sig)
{

	DEBUG("Send SIG#%d to %s %llX", sig, thrp->name, (long long)thrp->td);
	PTHREAD_KILL(thrp->td, sig);
	DEBUG("Sent SIG#%d to %s %llX", sig, thrp->name, (long long)thrp->td);
}

void
thread_shutdown_begin(struct thread *thrp)
{

	DEBUG("Shut down %s %llX", thrp->name, (long long)thrp->td);
	assert(!(thrp->flags & THR_FL_SHUTDOWN));
	thrp->flags |= THR_FL_SHUTDOWN;
}

int
thread_shutdown_commit(struct thread *thrp)
{

	if (thrp->flags & THR_FL_SHUTDOWN) {
		thrp->flags &= ~THR_FL_SHUTDOWN;
		return (0);
	}
	return (1);
}

void
thread_wait(struct thread *thrp)
{

	PTHREAD_COND_WAIT(&thrp->cond, &thrp->mutex);
}

void
thread_wait_mutex(struct thread *thrp, pthread_mutex_t *mtxp)
{

	PTHREAD_COND_WAIT(&thrp->cond, mtxp);
}

void
thread_wakeup(struct thread *thrp)
{

	PTHREAD_COND_SIGNAL(&thrp->cond);
}

int
thread_list_alloc(struct thread_list **thrlpp, int index)
{
	struct thread_list *thrlp;

	thrlp = malloc(sizeof(*thrlp));
	if (thrlp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*thrlp), strerror(errno));
		return (-2);
	}
	thread_list_init(thrlp, index);
	*thrlpp = thrlp;
	return (0);
}

void
thread_head_init(struct thread_head *thrhp)
{

	TAILQ_INIT(thrhp);
}

void
thread_head_uninit(struct thread_head *thrhp)
{

	assert(TAILQ_EMPTY(thrhp));
}

int
thread_head_empty(struct thread_head *thrhp)
{

	return (TAILQ_EMPTY(thrhp));
}

struct thread *
thread_head_first(struct thread_head *thrhp)
{

	return (TAILQ_FIRST(thrhp));
}

struct thread *
thread_head_next(struct thread *thrp, int index)
{

	return (TAILQ_NEXT(thrp, all_thrl_ary[index]));
}

void
thread_head_insert(struct thread_head *thrhp, struct thread *thrp, int index)
{

	TAILQ_INSERT_HEAD(thrhp, thrp, all_thrl_ary[index]);
}

void
thread_head_append(struct thread_head *thrhp, struct thread *thrp, int index)
{

	TAILQ_INSERT_TAIL(thrhp, thrp, all_thrl_ary[index]);
}

void
thread_head_remove(struct thread_head *thrhp, struct thread *thrp, int index)
{

	TAILQ_REMOVE(thrhp, thrp, all_thrl_ary[index]);
}

void
thread_list_init(struct thread_list *thrlp, int index)
{
#ifndef NDEBUG
	struct thread *thrp;
#endif

	assert(index >= 0 && index < NELEM(thrp->all_thrl_ary));
	TAILQ_INIT(&thrlp->head);
	thrlp->index = index;
	thrlp->count = 0;
	thrlp->peak = 0;
}

void
thread_list_uninit(struct thread_list *thrlp)
{

	assert(TAILQ_EMPTY(&thrlp->head));
	assert(thrlp->count == 0);
	thrlp->peak = 0;
}

void
thread_list_free(struct thread_list *thrlp)
{

	thread_list_uninit(thrlp);
	free(thrlp);
}

static
void
thread_list_dealloc(struct thread_list *thrlp)
{
	struct thread *nthrp, *thrp;

	nthrp = thread_list_first(thrlp);
	for (;;) {
		thrp = nthrp;
		if (thrp == NULL)
			break;
		nthrp = thread_list_next(thrlp, thrp);
		thread_list_remove(thrlp, thrp);
		thread_dealloc(thrp);
	}
}

int
thread_list_empty(struct thread_list *thrlp)
{

	return (TAILQ_EMPTY(&thrlp->head));
}

struct thread *
thread_list_first(struct thread_list *thrlp)
{

	return (TAILQ_FIRST(&thrlp->head));
}

struct thread *
thread_list_next(struct thread_list *thrlp, struct thread *thrp)
{

	return (TAILQ_NEXT(thrp, all_thrl_ary[thrlp->index]));
}

void
thread_list_insert(struct thread_list *thrlp, struct thread *thrp)
{

	TAILQ_INSERT_HEAD(&thrlp->head, thrp, all_thrl_ary[thrlp->index]);
	thrlp->count++;
	assert(thrlp->count > 0);
	if (thrlp->count > thrlp->peak)
		thrlp->peak = thrlp->count;
}

void
thread_list_append(struct thread_list *thrlp, struct thread *thrp)
{

	TAILQ_INSERT_TAIL(&thrlp->head, thrp, all_thrl_ary[thrlp->index]);
	thrlp->count++;
	assert(thrlp->count > 0);
	if (thrlp->count > thrlp->peak)
		thrlp->peak = thrlp->count;
}

void
thread_list_remove(struct thread_list *thrlp, struct thread *thrp)
{

	TAILQ_REMOVE(&thrlp->head, thrp, all_thrl_ary[thrlp->index]);
	thrlp->count--;
	assert(thrlp->count >= 0);
}
