/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

struct vty {
	TAILQ_ENTRY(vty)	all_vtyl_ary[2];
	struct thread		*thr;
#define VTY_FL_INUSE		0x00000001
	int			flags;
	int			so;
	char			inet_addr[INET_ADDRSTRLEN];
	int			inet_port;
};
TAILQ_HEAD(vty_head, vty);

struct vty_list {
	pthread_mutex_t		mutex;
	struct vty_head		head;
#define VTYL_IDX_ALLOC		0
#define VTYL_IDX_STD		1
	int			index;
	int			count;
	int			peak;
};

extern struct vty_list *active_vtyl;

int vty_module_setup(void);
void vty_module_hello(void);
void vty_module_teardown(void);
void vty_module_stats(void);
int vty_alloc(struct vty **);
void vty_init(struct vty *);
void vty_uninit(struct vty *);
void vty_free(struct vty *);
int vty_thread_create(struct vty *);
void vty_thread_launch(struct vty *);
int vty_list_alloc(struct vty_list **, int);
void vty_list_init(struct vty_list *, int);
void vty_list_uninit(struct vty_list *);
void vty_list_free(struct vty_list *);
void vty_list_lock(struct vty_list *);
void vty_list_unlock(struct vty_list *);
int vty_list_empty(struct vty_list *);
struct vty *vty_list_first(struct vty_list *);
struct vty *vty_list_next(struct vty_list *, struct vty *);
void vty_list_insert(struct vty_list *, struct vty *);
void vty_list_append(struct vty_list *, struct vty *);
void vty_list_remove(struct vty_list *, struct vty *);
void vty_list_shutdown(struct vty_list *);
