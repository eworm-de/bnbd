/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "thread.h"
#include "request.h"
#include "worker.h"
#include "volume.h"
#include "catcher.h"

static int worker_module_initialized = 0;
static pthread_mutex_t worker_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct worker_list *available_wrkl = NULL;
static struct worker_list *inuse_wrkl = NULL;
static struct request_list *incoming_reql = NULL;
static struct worker_list *waiting_wrkl = NULL;
static struct worker_list *running_wrkl = NULL;

struct worker_list *backoff_wrkl = NULL;

static void worker_dealloc(struct worker *);
static void worker_list_dealloc(struct worker_list *);
static void worker_thread_shutdown(struct worker *);
static void *worker_thread(void *);
static void worker_thread_loop(struct worker *);
static void worker_list_shutdown(struct worker_list *);

int
worker_module_setup(void)
{
	struct worker *wrkp;
	int count, i;

	assert(!worker_module_initialized);
	assert(incoming_reql == NULL);
	if (request_list_alloc(&incoming_reql, REQL_IDX_STD) < 0) {
		assert(incoming_reql == NULL);
		goto out0;
	}
	assert(incoming_reql != NULL);
	assert(available_wrkl == NULL);
	if (worker_list_alloc(&available_wrkl, WRKL_IDX_ALLOC) < 0) {
		assert(available_wrkl == NULL);
		goto out1;
	}
	assert(available_wrkl != NULL);
	assert(inuse_wrkl == NULL);
	if (worker_list_alloc(&inuse_wrkl, WRKL_IDX_ALLOC) < 0) {
		assert(inuse_wrkl == NULL);
		goto out2;
	}
	assert(inuse_wrkl != NULL);
	assert(waiting_wrkl == NULL);
	if (worker_list_alloc(&waiting_wrkl, WRKL_IDX_STD) < 0) {
		assert(waiting_wrkl == NULL);
		goto out3;
	}
	assert(waiting_wrkl != NULL);
	assert(running_wrkl == NULL);
	if (worker_list_alloc(&running_wrkl, WRKL_IDX_STD) < 0) {
		assert(running_wrkl == NULL);
		goto out4;
	}
	assert(running_wrkl != NULL);
	assert(backoff_wrkl == NULL);
	if (worker_list_alloc(&backoff_wrkl, WRKL_IDX_AUX) < 0) {
		assert(backoff_wrkl == NULL);
		goto out5;
	}
	assert(backoff_wrkl != NULL);
	worker_module_initialized = 1;

	for (i = 0; i < cfg_aio_workers; i++) {
		wrkp = NULL;
		if (worker_alloc(&wrkp) < 0) {
			assert(wrkp == NULL);
			break;
		}
		assert(wrkp != NULL);
		if (thread_create(wrkp->thr, "worker", worker_thread) < 0) {
			worker_free(wrkp);
			break;
		}
		thread_launch(wrkp->thr);
	}
	for (;;) {
		request_list_lock(incoming_reql);
		count = waiting_wrkl->count;
		request_list_unlock(incoming_reql);
		assert(count <= i);
		NOTICE("%d of %d AIO workers UP", count, i);
		if (count == i)
			break;
		usleep((useconds_t)200000);
	}
	if (count < cfg_aio_workers) {
		ERR("ONLY %d of %d AIO workers UP, ABORTING",
		    count, cfg_aio_workers);
		worker_module_teardown();
		return (-1);
	}
	return (0);
out5:
	assert(running_wrkl != NULL);
	worker_list_free(running_wrkl);
	running_wrkl = NULL;
out4:
	assert(waiting_wrkl != NULL);
	worker_list_free(waiting_wrkl);
	waiting_wrkl = NULL;
out3:
	assert(inuse_wrkl != NULL);
	worker_list_free(inuse_wrkl);
	inuse_wrkl = NULL;
out2:
	assert(available_wrkl != NULL);
	worker_list_dealloc(available_wrkl);
	worker_list_free(available_wrkl);
	available_wrkl = NULL;
out1:
	assert(incoming_reql != NULL);
	request_list_free(incoming_reql);
	incoming_reql = NULL;
out0:
	return (-1);
}

void
worker_module_hello(void)
{

	assert(worker_module_initialized);
	DEBUG("sizeof(worker)=%zu", sizeof(struct worker));
	DEBUG("sizeof(worker_list)=%zu", sizeof(struct worker_list));
}

void
worker_module_teardown(void)
{
	int count, total;

	assert(worker_module_initialized);
	PTHREAD_MUTEX_LOCK(&worker_mutex);
	total = available_wrkl->count + inuse_wrkl->count;
	PTHREAD_MUTEX_UNLOCK(&worker_mutex);
	assert(total >= 0);
	for (;;) {
		request_list_lock(incoming_reql);
		count = waiting_wrkl->count;
		request_list_unlock(incoming_reql);
		assert(count >= 0 && count <= total);
		if (count < total) {
			NOTICE("%d of %d AIO workers still RUNNING",
			    total - count, total);
			usleep((useconds_t)200000);
			continue;
		}
		break;
	}
	assert(backoff_wrkl != NULL);
	worker_list_free(backoff_wrkl);
	backoff_wrkl = NULL;
	assert(running_wrkl != NULL);
	worker_list_free(running_wrkl);
	running_wrkl = NULL;
	assert(waiting_wrkl != NULL);
	worker_list_shutdown(waiting_wrkl);
	worker_list_free(waiting_wrkl);
	waiting_wrkl = NULL;
	assert(inuse_wrkl != NULL);
	worker_list_free(inuse_wrkl);
	inuse_wrkl = NULL;
	assert(available_wrkl != NULL);
	worker_list_dealloc(available_wrkl);
	worker_list_free(available_wrkl);
	available_wrkl = NULL;
	assert(incoming_reql != NULL);
	request_list_free(incoming_reql);
	incoming_reql = NULL;
}

void
worker_module_stats(void)
{
	int available_count, available_peak, inuse_count, inuse_peak,
	    incoming_count, incoming_peak, running_count, running_peak,
	    waiting_count, waiting_peak;

	assert(worker_module_initialized);
	PTHREAD_MUTEX_LOCK(&worker_mutex);
	available_count = available_wrkl->count;
	available_peak = available_wrkl->peak;
	available_wrkl->peak = available_wrkl->count;
	inuse_count = inuse_wrkl->count;
	inuse_peak = inuse_wrkl->peak;
	inuse_wrkl->peak = inuse_wrkl->count;
	PTHREAD_MUTEX_UNLOCK(&worker_mutex);
	NOTICE("inuse=%d/%d available=%d/%d",
	    inuse_count, inuse_peak, available_count, available_peak);

	request_list_lock(incoming_reql);
	incoming_count = incoming_reql->count;
	incoming_peak = incoming_reql->peak;
	incoming_reql->peak = incoming_reql->count;
	running_count = running_wrkl->count;
	running_peak = running_wrkl->peak;
	running_wrkl->peak = running_wrkl->count;
	waiting_count = waiting_wrkl->count;
	waiting_peak = waiting_wrkl->peak;
	waiting_wrkl->peak = waiting_wrkl->count;
	request_list_unlock(incoming_reql);
	NOTICE("incoming_requests=%d/%d running_workers=%d/%d "
	    "waiting_workers=%d/%d",
	    incoming_count, incoming_peak, running_count, running_peak,
	    waiting_count, waiting_peak);
}

int
worker_alloc(struct worker **wrkpp)
{
	struct worker *wrkp;
#ifdef USE_LINUX_AIO
	size_t len;
#endif

	assert(worker_module_initialized);
	PTHREAD_MUTEX_LOCK(&worker_mutex);
	wrkp = worker_list_first(available_wrkl);
	if (wrkp == NULL) {
		assert(available_wrkl->count == 0);
		PTHREAD_MUTEX_UNLOCK(&worker_mutex);
		wrkp = malloc(sizeof(*wrkp));
		if (wrkp == NULL) {
			ERR("malloc(%zu): %s", sizeof(*wrkp), strerror(errno));
			return (-2);
		}
		wrkp->thr = NULL;
		if (thread_alloc(&wrkp->thr, wrkp) < 0) {
			assert(wrkp->thr == NULL);
			free(wrkp);
			return (-3);
		}
		assert(wrkp->thr != NULL);
#ifdef USE_LINUX_AIO
		wrkp->spool_reql = NULL;
		if (request_list_alloc(&wrkp->spool_reql, REQL_IDX_STD) < 0) {
			assert(wrkp->spool_reql == NULL);
			thread_free(wrkp->thr);
			free(wrkp);
			return (-4);
		}
		assert(wrkp->spool_reql != NULL);
		assert(cfg_aio_queue_length > 0);
		wrkp->submit_iocb_ary_len = cfg_aio_queue_length;
		len = (size_t)wrkp->submit_iocb_ary_len * sizeof(struct iocb *);
		wrkp->submit_iocb_ary = malloc(len);
		if (wrkp->submit_iocb_ary == NULL) {
			ERR("malloc(%zu): %s", len, strerror(errno));
			request_list_free(wrkp->spool_reql);
			thread_free(wrkp->thr);
			free(wrkp);
			return (-5);
		}
#endif
		worker_init(wrkp);
		PTHREAD_MUTEX_LOCK(&worker_mutex);
	} else {
		worker_list_remove(available_wrkl, wrkp);
	}
	assert(!wrkp->flags);
	wrkp->flags |= WRK_FL_INUSE;
	worker_list_insert(inuse_wrkl, wrkp);
	PTHREAD_MUTEX_UNLOCK(&worker_mutex);
	*wrkpp = wrkp;
	return (0);
}

void
worker_init(struct worker *wrkp)
{

	wrkp->flags = 0;
	wrkp->submit_peak = 0;
}

void
worker_uninit(struct worker *wrkp)
{

	thread_uninit(wrkp->thr);
	assert(wrkp->flags == WRK_FL_INUSE);
	wrkp->submit_peak = 0;
#ifdef USE_LINUX_AIO
	request_list_uninit(wrkp->spool_reql);
#endif
}

void
worker_free(struct worker *wrkp)
{

	assert(worker_module_initialized);
	worker_uninit(wrkp);
	PTHREAD_MUTEX_LOCK(&worker_mutex);
	worker_list_remove(inuse_wrkl, wrkp);
	wrkp->flags &= ~WRK_FL_INUSE;
	assert(!wrkp->flags);
	worker_list_insert(available_wrkl, wrkp);
	PTHREAD_MUTEX_UNLOCK(&worker_mutex);
}

static
void
worker_dealloc(struct worker *wrkp)
{

	assert(!wrkp->flags);
#ifdef USE_LINUX_AIO
	free(wrkp->submit_iocb_ary);
	request_list_free(wrkp->spool_reql);
#endif
	thread_free(wrkp->thr);
	free(wrkp);
}

void
worker_thread_wakeup(struct worker *wrkp)
{

	thread_wakeup(wrkp->thr);
}

static
void
worker_thread_shutdown(struct worker *wrkp)
{

	request_list_lock(incoming_reql);
	thread_shutdown_begin(wrkp->thr);
	worker_thread_wakeup(wrkp);
	request_list_unlock(incoming_reql);
}

void
worker_reql_submit(struct request_list *reqlp)
{
	struct worker *wrkp;
	int peak;

	assert(reqlp->count >= 0);
	if (reqlp->count == 0)
		return;
	peak = 0;
	if (reqlp->count > reqlp->submit_peak) {
		reqlp->submit_peak = reqlp->count;
		peak = 1;
	}
	if (reqlp->submit_bytes > reqlp->submit_bytes_peak) {
		reqlp->submit_bytes_peak = reqlp->submit_bytes;
		peak = 1;
	}
	if (peak)
		DEBUG("request_burst_peak=%d/%lld",
		    reqlp->count, (long long)reqlp->submit_bytes);
	reqlp->submit_bytes = (int64_t)0;
	request_list_lock(incoming_reql);
	request_list_concat(incoming_reql, reqlp);
	assert(request_list_empty(reqlp));
	wrkp = worker_list_first(waiting_wrkl);
	if (wrkp == NULL) {
		request_list_unlock(incoming_reql);
		return;
	}
	worker_list_remove(waiting_wrkl, wrkp);
	worker_list_append(running_wrkl, wrkp);
	request_list_unlock(incoming_reql);
	worker_thread_wakeup(wrkp);
}

static
void *
worker_thread(void *data)
{
	struct worker *wrkp;

	wrkp = (struct worker *)data;
	assert(wrkp != NULL);
	assert(wrkp->thr != NULL);

	thread_prologue(wrkp->thr);

	worker_thread_loop(wrkp);

	thread_epilogue(NULL);
	return (NULL);
}

#ifdef USE_LINUX_AIO
static void io_submit_all(struct worker *, io_context_t, int);

static
void
worker_thread_loop(struct worker *wrkp)
{
	struct request_list *spool_reqlp;
	struct request *nreqp, *reqp;
	struct iocb **submit_iocb_aryp;
	io_context_t ioctx;
	int iocb_count, event_fd, submit_iocb_ary_len;

	spool_reqlp = wrkp->spool_reql;
	submit_iocb_aryp = wrkp->submit_iocb_ary;
	assert(submit_iocb_aryp != NULL);
	submit_iocb_ary_len = wrkp->submit_iocb_ary_len;
	assert(submit_iocb_ary_len > 0);
	ioctx = catcher_ioctx();
	event_fd = catcher_event_fd();

	/* off of any list */
	request_list_lock(incoming_reql);
	worker_list_insert(running_wrkl, wrkp);

	for (;;) {
		/* on the running_wrkl with the incoming_reql locked */

		/*
		 * PHASE 1: grab as many requests as possible off
		 *          of the the incoming_reql and add them
		 *          to the spool_reql
		 */
		nreqp = request_list_first(incoming_reql);
		for (;;) {
			reqp = nreqp;
			if (reqp == NULL)
				break;
			nreqp = request_list_next(incoming_reql, reqp);
			assert(spool_reqlp->count <= cfg_aio_queue_length);
			if (spool_reqlp->count == submit_iocb_ary_len)
				break;
			switch (reqp->type) {
			case REQ_TY_READ:
				assert(reqp->nbd.request.type == NBD_CMD_READ);
				assert(reqp->volp != NULL);
				assert(reqp->outgoing_reqlp != NULL);
				assert(reqp->mirp == NULL);
				assert(reqp->master_reqp == NULL);
				break;
			case REQ_TY_WRITE:
				assert(reqp->nbd.request.type == NBD_CMD_WRITE);
				assert(reqp->volp != NULL);
				assert(reqp->outgoing_reqlp != NULL);
				assert(reqp->mirp == NULL);
				assert(reqp->master_reqp == NULL);
				break;
			case REQ_TY_MASTER_WRITE:
				assert(reqp->nbd.request.type == NBD_CMD_WRITE);
				assert(reqp->volp != NULL);
				assert(reqp->outgoing_reqlp != NULL);
				assert(reqp->mirp == NULL);
				assert(reqp->master_reqp == reqp);
				break;
			case REQ_TY_SLAVE_WRITE:
				assert(reqp->nbd.request.type == NBD_CMD_WRITE);
				assert(reqp->volp == NULL);
				assert(reqp->outgoing_reqlp != NULL);
				assert(reqp->mirp != NULL);
				assert(reqp->master_reqp != NULL);
				assert(reqp->master_reqp != reqp);
				break;
			case REQ_TY_MASTER_COPY:
			case REQ_TY_SLAVE_PASTE:
				assert(reqp->volp != NULL);
				assert(reqp->outgoing_reqlp != NULL);
				assert(reqp->mirp != NULL);
				assert(reqp->master_reqp == NULL);
				break;
			default:
				abort();
			}
			request_list_remove(incoming_reql, reqp);
			request_list_append(spool_reqlp, reqp);
		}
		assert(spool_reqlp->count <= submit_iocb_ary_len);
		request_list_unlock(incoming_reql);

		/*
		 * PHASE 2: prepare requests for submission
		 */
		iocb_count = 0;
		nreqp = request_list_first(spool_reqlp);
		for (;;) {
			reqp = nreqp;
			if (reqp == NULL)
				break;
			nreqp = request_list_next(spool_reqlp, reqp);
			request_list_remove(spool_reqlp, reqp);
			if (reqp->flags & REQ_FL_ABORT) {
				request_done(reqp, 0xABABABAB);
				continue;
			}
			memset(&reqp->iocb, 0x00, sizeof(reqp->iocb));
			switch (reqp->type) {
			case REQ_TY_READ:
				assert(reqp->buf.buf_len ==
				    reqp->nbd.request.len);
				if (reqp->buf.buf_len == (uint32_t)0) {
					assert(reqp->buf.buf_ptr == NULL);
					request_done(reqp, 0);
					continue;
				}
				assert(reqp->buf.buf_ptr != NULL);
				reqp->iocb.aio_lio_opcode = IO_CMD_PREAD;
				break;
			case REQ_TY_WRITE:
			case REQ_TY_MASTER_WRITE:
			case REQ_TY_SLAVE_WRITE:
				assert(reqp->buf.buf_len ==
				    reqp->nbd.request.len);
				if (reqp->buf.buf_len == (uint32_t)0) {
					assert(reqp->buf.buf_ptr == NULL);
					request_done(reqp, 0);
					continue;
				}
				assert(reqp->buf.buf_ptr != NULL);
				reqp->iocb.aio_lio_opcode = IO_CMD_PWRITE;
				break;
			case REQ_TY_MASTER_COPY:
				assert(reqp->buf.buf_ptr != NULL);
				assert(reqp->buf.buf_len == 
				    reqp->nbd.request.len);
				assert(reqp->buf.buf_len > (uint32_t)0);
				reqp->iocb.aio_lio_opcode = IO_CMD_PREAD;
				break;
			case REQ_TY_SLAVE_PASTE:
				assert(reqp->buf.buf_ptr != NULL);
				assert(reqp->buf.buf_len ==
				    reqp->nbd.request.len);
				assert(reqp->buf.buf_len > (uint32_t)0);
				reqp->iocb.aio_lio_opcode = IO_CMD_PWRITE;
				break;
			default:
				abort();
			}
			reqp->iocb.data = reqp;
			reqp->iocb.aio_reqprio = 0;
			reqp->iocb.aio_fildes = reqp->fd;
			reqp->iocb.u.c.buf = reqp->buf.buf_ptr;
			reqp->iocb.u.c.nbytes =
			    (unsigned long)reqp->buf.buf_len;
			reqp->iocb.u.c.offset =
			    (long long)reqp->nbd.request.from;
			reqp->iocb.u.c.flags |= (1 << 0); /* IOCB_FLAG_RESFD */
			reqp->iocb.u.c.resfd = event_fd;
			submit_iocb_aryp[iocb_count++] = &reqp->iocb;
		}
		assert(request_list_empty(spool_reqlp));

		/*
		 * PHASE 3: submit at once as many requests as possible
		 */
		assert(iocb_count >= 0);
		if (iocb_count > 0)
			io_submit_all(wrkp, ioctx, iocb_count);

		request_list_lock(incoming_reql);
		if (!request_list_empty(incoming_reql))
			continue;
		worker_list_remove(running_wrkl, wrkp);
		worker_list_insert(waiting_wrkl, wrkp);
		thread_wait_mutex(wrkp->thr, &incoming_reql->mutex);
		if (THREAD_SHUTDOWN_PENDING(wrkp->thr)) {
			request_list_unlock(incoming_reql);
			assert(request_list_empty(spool_reqlp));
			thread_shutdown_commit(wrkp->thr);
			break;
		}
	}
}

static
void
io_submit_all(struct worker *wrkp, io_context_t ioctx, int iocb_count)
{
	struct request *reqp;
	uint64_t new_collect_count, old_collect_count;
	int i, n;

	worker_list_lock(backoff_wrkl);
	old_collect_count = collect_count;
	worker_list_unlock(backoff_wrkl);

	i = 0;
	for (;;) {
		assert(i <= iocb_count);
		if (i == iocb_count)
			break;
		n = io_submit(ioctx, (long)(iocb_count - i),
		    &wrkp->submit_iocb_ary[i]);
		if (n < 0) {
			ERR("io_submit(%d): %s", iocb_count - i, strerror(-n));
			if (n != -EAGAIN) {
				reqp = wrkp->submit_iocb_ary[i]->data;
				request_done(reqp, n);
				i++;
				continue;
			}
			worker_list_lock(backoff_wrkl);
			new_collect_count = collect_count;
			if (new_collect_count != old_collect_count) {
				worker_list_unlock(backoff_wrkl);
				old_collect_count = new_collect_count;
				continue;
			}
			worker_list_append(backoff_wrkl, wrkp);
			thread_wait_mutex(wrkp->thr, &backoff_wrkl->mutex);
			old_collect_count = collect_count;
			worker_list_unlock(backoff_wrkl);
			continue;
		}
		assert(n > 0);
		i += n;
		if (n > wrkp->submit_peak) {
			wrkp->submit_peak = n;
			DEBUG("aio_submit_peak=%d", wrkp->submit_peak);
		}
	}
	assert(i == iocb_count);
}
#else
static
void
worker_thread_loop(struct worker *wrkp)
{

	(void)wrkp;
	abort();
}
#endif

int
worker_list_alloc(struct worker_list **wrklpp, int index)
{
	struct worker_list *wrklp;
	int error;

	wrklp = malloc(sizeof(*wrklp));
	if (wrklp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*wrklp), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&wrklp->mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(wrklp);
		return (-3);
	}
	worker_list_init(wrklp, index);
	*wrklpp = wrklp;
	return (0);
}

void
worker_list_init(struct worker_list *wrklp, int index)
{
#ifndef NDEBUG
	struct worker *wrkp;
#endif

	assert(index >= 0 && index < NELEM(wrkp->all_wrkl_ary));
	TAILQ_INIT(&wrklp->head);
	wrklp->index = index;
	wrklp->count = 0;
	wrklp->peak = 0;
}

void
worker_list_uninit(struct worker_list *wrklp)
{

	assert(TAILQ_EMPTY(&wrklp->head));
	assert(wrklp->count == 0);
	wrklp->peak = 0;
}

void
worker_list_free(struct worker_list *wrklp)
{

	worker_list_uninit(wrklp);
	PTHREAD_MUTEX_DESTROY(&wrklp->mutex);
	free(wrklp);
}

static
void
worker_list_dealloc(struct worker_list *wrklp)
{
	struct worker *nwrkp, *wrkp;

	nwrkp = worker_list_first(wrklp);
	for (;;) {
		wrkp = nwrkp;
		if (wrkp == NULL)
			break;
		nwrkp = worker_list_next(wrklp, wrkp);
		worker_list_remove(wrklp, wrkp);
		worker_dealloc(wrkp);
	}
}

void
worker_list_lock(struct worker_list *wrklp)
{

	PTHREAD_MUTEX_LOCK(&wrklp->mutex);
}

void
worker_list_unlock(struct worker_list *wrklp)
{

	PTHREAD_MUTEX_UNLOCK(&wrklp->mutex);
}

int
worker_list_empty(struct worker_list *wrklp)
{

	return (TAILQ_EMPTY(&wrklp->head));
}

struct worker *
worker_list_first(struct worker_list *wrklp)
{

	return (TAILQ_FIRST(&wrklp->head));
}

struct worker *
worker_list_next(struct worker_list *wrklp, struct worker *wrkp)
{

	return (TAILQ_NEXT(wrkp, all_wrkl_ary[wrklp->index]));
}

void
worker_list_insert(struct worker_list *wrklp, struct worker *wrkp)
{

	TAILQ_INSERT_HEAD(&wrklp->head, wrkp, all_wrkl_ary[wrklp->index]);
	wrklp->count++;
	assert(wrklp->count > 0);
	if (wrklp->count > wrklp->peak)
		wrklp->peak = wrklp->count;
}

void
worker_list_append(struct worker_list *wrklp, struct worker *wrkp)
{

	TAILQ_INSERT_TAIL(&wrklp->head, wrkp, all_wrkl_ary[wrklp->index]);
	wrklp->count++;
	assert(wrklp->count > 0);
	if (wrklp->count > wrklp->peak)
		wrklp->peak = wrklp->count;
}

void
worker_list_remove(struct worker_list *wrklp, struct worker *wrkp)
{

	TAILQ_REMOVE(&wrklp->head, wrkp, all_wrkl_ary[wrklp->index]);
	wrklp->count--;
	assert(wrklp->count >= 0);
}

static
void
worker_list_shutdown(struct worker_list *wrklp)
{
	struct worker *wrkp;

	for (;;) {
		wrkp = worker_list_first(wrklp);
		if (wrkp == NULL)
			break;
		worker_list_remove(wrklp, wrkp);
		worker_thread_shutdown(wrkp);
		thread_join(wrkp->thr);
		worker_free(wrkp);
	}
}
