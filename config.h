/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

/* command line options */
#ifndef DEF_CONFIG
#define DEF_CONFIG				"/etc/bnbd/bnbd-server.conf"
#endif
extern const char *opt_config;
#ifndef DEF_DEBUG
#define DEF_DEBUG				0
#endif
extern int opt_debug;

/* configuration file options */
#ifndef DEF_SYSLOG_IDENT
#define DEF_SYSLOG_IDENT			"bnbd-server"
#endif
extern char *cfg_syslog_ident;
#ifndef DEF_SYSLOG_FACILITY
#define DEF_SYSLOG_FACILITY			LOG_DAEMON
#endif
extern int cfg_syslog_facility;
#ifndef DEF_SYSLOG_PRIORITY
#define DEF_SYSLOG_PRIORITY			LOG_INFO
#endif
extern int cfg_syslog_priority;
#ifndef DEF_PID_FILE
#define DEF_PID_FILE				"/var/run/bnbd-server.pid"
#endif
extern char *cfg_pid_file;
#ifndef DEF_WORK_DIRECTORY
#define DEF_WORK_DIRECTORY			"/var/tmp"
#endif
extern char *cfg_work_directory;
#ifndef DEF_CTRL_DIRECTORY
#define DEF_CTRL_DIRECTORY			"/var/lib/bnbd-server/ctrl"
#endif
extern char *cfg_ctrl_directory;
#ifndef DEF_DATA_DIRECTORY
#define DEF_DATA_DIRECTORY			"/var/lib/bnbd-server/data"
#endif
extern char *cfg_data_directory;
#ifndef DEF_SYNC
#define DEF_SYNC				0
#endif
extern int cfg_sync;
#ifndef DEF_FLUSH
#define DEF_FLUSH				1
#endif
extern int cfg_flush;
#ifndef DEF_TRIM
#define DEF_TRIM				0
#endif
extern int cfg_trim;
#ifndef DEF_VTY_LISTEN_IP
#define DEF_VTY_LISTEN_IP			"0.0.0.0"
#endif
extern char *cfg_vty_listen_ip;
#ifndef DEF_VTY_LISTEN_PORT
#define DEF_VTY_LISTEN_PORT			10810
#endif
extern int cfg_vty_listen_port;
#ifndef DEF_VTY_LISTEN_BACKLOG
#define DEF_VTY_LISTEN_BACKLOG			16
#endif
extern int cfg_vty_listen_backlog;
#ifndef DEF_MAX_VTY_CLIENTS
#define DEF_MAX_VTY_CLIENTS			16
#endif
extern int cfg_max_vty_clients;
#ifndef DEF_VTY_TIMEOUT
#define DEF_VTY_TIMEOUT				120
#endif
extern int cfg_vty_timeout;
#ifndef DEF_NBD_LISTEN_IP
#define DEF_NBD_LISTEN_IP			"0.0.0.0"
#endif
extern char *cfg_nbd_listen_ip;
#ifndef DEF_NBD_LISTEN_PORT
#define DEF_NBD_LISTEN_PORT			10809
#endif
extern int cfg_nbd_listen_port;
#ifndef DEF_NBD_LISTEN_BACKLOG
#define DEF_NBD_LISTEN_BACKLOG			1024
#endif
extern int cfg_nbd_listen_backlog;
#ifndef DEF_MAX_NBD_CLIENTS
#define DEF_MAX_NBD_CLIENTS			512
#endif
extern int cfg_max_nbd_clients;
#ifndef DEF_NBD_NEGOTIATE_TIMEOUT
#define DEF_NBD_NEGOTIATE_TIMEOUT		30
#endif
extern int cfg_nbd_negotiate_timeout;
#ifndef DEF_NBD_SEND_TIMEOUT
#define DEF_NBD_SEND_TIMEOUT			120
#endif
extern int cfg_nbd_send_timeout;
#ifndef DEF_NBD_KEEPALIVE_IDLE
#define DEF_NBD_KEEPALIVE_IDLE			60
#endif
extern int cfg_nbd_keepalive_idle;
#ifndef DEF_NBD_KEEPALIVE_INTERVAL
#define DEF_NBD_KEEPALIVE_INTERVAL		20
#endif
extern int cfg_nbd_keepalive_interval;
#ifndef DEF_NBD_KEEPALIVE_COUNT
#define DEF_NBD_KEEPALIVE_COUNT			6
#endif
extern int cfg_nbd_keepalive_count;
#ifndef DEF_NBD_RECEIVE_SOCKBUF_KB
#define DEF_NBD_RECEIVE_SOCKBUF_KB		0
#endif
extern int cfg_nbd_receive_sockbuf_kb;
#ifndef DEF_NBD_SEND_SOCKBUF_KB
#define DEF_NBD_SEND_SOCKBUF_KB			0
#endif
extern int cfg_nbd_send_sockbuf_kb;
#ifndef DEF_AIO_WORKERS
#define DEF_AIO_WORKERS				1
#endif
extern int cfg_aio_workers;
#ifndef DEF_AIO_QUEUE_LENGTH
#define DEF_AIO_QUEUE_LENGTH			1024
#endif
extern int cfg_aio_queue_length;
#ifndef DEF_BIO_WORKERS
#define DEF_BIO_WORKERS				16
#endif
extern int cfg_bio_workers;
#ifndef DEF_REQUEST_BUFFER_KB
#define DEF_REQUEST_BUFFER_KB			128
#endif
extern int cfg_request_buffer_kb;
#ifndef DEF_MAX_REQUESTS
#define DEF_MAX_REQUESTS			1024
#endif
extern int cfg_max_requests;
#ifndef DEF_MAX_TOKENS
#define DEF_MAX_TOKENS				0
#endif
extern int cfg_max_tokens;
#ifndef DEF_MAX_READ_REQUESTS_KB
#define DEF_MAX_READ_REQUESTS_KB		131072
#endif
extern int cfg_max_read_requests_kb;
#ifndef DEF_MAX_WRITE_REQUESTS_KB
#define DEF_MAX_WRITE_REQUESTS_KB		131072
#endif
extern int cfg_max_write_requests_kb;
#ifndef DEF_MAX_MIRROR_REQUESTS_KB
#define DEF_MAX_MIRROR_REQUESTS_KB		131072
#endif
extern int cfg_max_mirror_requests_kb;
#ifndef DEF_MIRROR_REQUEST_KB
#define DEF_MIRROR_REQUEST_KB			128
#endif
extern int cfg_mirror_request_kb;
#ifndef DEF_MIRROR_QUEUE_LENGTH
#define DEF_MIRROR_QUEUE_LENGTH			8
#endif
extern int cfg_mirror_queue_length;
#ifndef DEF_MAX_REQUEST_BURST
#define DEF_MAX_REQUEST_BURST			256
#endif
extern int cfg_max_request_burst;
#ifndef DEF_MAX_REQUEST_BURST_KB
#define DEF_MAX_REQUEST_BURST_KB		131072
#endif
extern int cfg_max_request_burst_kb;
#ifndef DEF_MAX_REPLY_BURST
#define DEF_MAX_REPLY_BURST			256
#endif
extern int cfg_max_reply_burst;
#ifndef DEF_MAX_REPLY_BURST_KB
#define DEF_MAX_REPLY_BURST_KB			0
#endif
extern int cfg_max_reply_burst_kb;
#ifndef DEF_STATS_INTERVAL
#define DEF_STATS_INTERVAL			60
#endif
extern int cfg_stats_interval;

void config_module_setup(void);
void config_module_teardown(void);
void config_load(void);
void config_dump(void);
