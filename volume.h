/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

struct volume {
	TAILQ_ENTRY(volume)	all_voll_ary[2];
#define VOL_FL_INUSE		0x00000001
	int			flags;
#define VOL_ST_UP		0
#define VOL_ST_DOWN		1
#define VOL_ST_SHUTDOWN		2
	int			state;
	dev_t			st_dev;
	ino_t			st_ino;
	int			fd;
	uint32_t		blk_size;
	uint64_t		size;
	struct session_list	*established_sesl;
	struct mirror_list	*slave_mirl;
	struct token_tree	*range_tokt;
	struct blkmap		*bm;
	char			id[64];
};
TAILQ_HEAD(volume_head, volume);

struct volume_list {
	struct volume_head	head;
	pthread_mutex_t		mutex;
#define VOLL_IDX_ALLOC		0
#define VOLL_IDX_STD		1
	int			index;
	int			count;
	int			peak;
};

extern struct volume_list *master_voll;

int volume_module_setup(void);
void volume_module_hello(void);
void volume_module_teardown(void);
void volume_module_stats(void);
int volume_alloc(struct volume **);
void volume_init(struct volume *);
void volume_uninit(struct volume *);
void volume_free(struct volume *);
const char *volume_strstate(struct volume *);
int volume_setup(struct volume **, const char *, struct stat *);
int volume_create(const char *, uint64_t, int);
int volume_delete_begin(struct volume *);
int volume_delete_commit(struct volume *);
int volume_up(struct volume *);
int volume_down_begin(struct volume *);
void volume_down_commit(struct volume *);
void volume_shutdown(struct volume *);
int volume_list_alloc(struct volume_list **, int);
void volume_list_init(struct volume_list *, int);
void volume_list_uninit(struct volume_list *);
void volume_list_free(struct volume_list *);
void volume_list_lock(struct volume_list *);
void volume_list_unlock(struct volume_list *);
int volume_list_empty(struct volume_list *);
struct volume *volume_list_first(struct volume_list *);
struct volume *volume_list_next(struct volume_list *, struct volume *);
void volume_list_insert(struct volume_list *, struct volume *);
void volume_list_append(struct volume_list *, struct volume *);
void volume_list_remove(struct volume_list *, struct volume *);
struct volume *volume_list_lookup_by_id(struct volume_list *, const char *);
struct volume *volume_list_lookup_by_inode(struct volume_list *, struct stat *);
int volume_list_load(struct volume_list *);
int volume_list_reload(struct volume_list *);
void volume_list_shutdown(struct volume_list *);
