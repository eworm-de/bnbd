/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "io.h"
#include "thread.h"
#include "token.h"
#include "session.h"
#include "blkmap.h"
#include "mirror.h"
#include "volume.h"

static int volume_module_initialized = 0;
static pthread_mutex_t volume_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct volume_list *available_voll = NULL;
static struct volume_list *inuse_voll = NULL;

struct volume_list *master_voll = NULL;

static void volume_dealloc(struct volume *);
static void volume_list_dealloc(struct volume_list *);
static int volume_opt_get(struct volume *, const char *, char *, size_t, int);
static int volume_opt_set(struct volume *, const char *, const char *);
static int volume_open(struct volume *);
static void volume_close(struct volume *);
static int volume_list_open(struct volume_list *, int);

int
volume_module_setup(void)
{

	assert(!volume_module_initialized);
	assert(available_voll == NULL);
	if (volume_list_alloc(&available_voll, VOLL_IDX_ALLOC) < 0) {
		assert(available_voll == NULL);
		goto out0;
	}
	assert(available_voll != NULL);
	assert(inuse_voll == NULL);
	if (volume_list_alloc(&inuse_voll, VOLL_IDX_ALLOC) < 0) {
		assert(inuse_voll == NULL);
		goto out1;
	}
	assert(inuse_voll != NULL);
	assert(master_voll == NULL);
	if (volume_list_alloc(&master_voll, VOLL_IDX_STD) < 0) {
		assert(master_voll == NULL);
		goto out2;
	}
	assert(master_voll != NULL);
	volume_module_initialized = 1;
	return (0);
out2:
	assert(inuse_voll != NULL);
	volume_list_free(inuse_voll);
	inuse_voll = NULL;
out1:
	assert(available_voll != NULL);
	volume_list_dealloc(available_voll);
	volume_list_free(available_voll);
	available_voll = NULL;
out0:
	return (-1);
}

void
volume_module_hello(void)
{

	assert(volume_module_initialized);
	DEBUG("sizeof(volume)=%zu", sizeof(struct volume));
	DEBUG("sizeof(volume_list)=%zu", sizeof(struct volume_list));
}

void
volume_module_teardown(void)
{

	assert(volume_module_initialized);
	assert(master_voll != NULL);
	volume_list_free(master_voll);
	master_voll = NULL;
	assert(inuse_voll != NULL);
	volume_list_free(inuse_voll);
	inuse_voll = NULL;
	assert(available_voll != NULL);
	volume_list_dealloc(available_voll);
	volume_list_free(available_voll);
	available_voll = NULL;
	volume_module_initialized = 0;
}

void
volume_module_stats(void)
{
	int available_count, available_peak, inuse_count, inuse_peak;

	assert(volume_module_initialized);
	PTHREAD_MUTEX_LOCK(&volume_mutex);
	available_count = available_voll->count;
	available_peak = available_voll->peak;
	available_voll->peak = available_voll->count;
	inuse_count = inuse_voll->count;
	inuse_peak = inuse_voll->peak;
	inuse_voll->peak = inuse_voll->count;
	PTHREAD_MUTEX_UNLOCK(&volume_mutex);
	NOTICE("inuse=%d/%d available=%d/%d",
	    inuse_count, inuse_peak, available_count, available_peak);
}

int
volume_alloc(struct volume **volpp)
{
	struct volume *volp;

	assert(volume_module_initialized);
	PTHREAD_MUTEX_LOCK(&volume_mutex);
	volp = volume_list_first(available_voll);
	if (volp == NULL) {
		assert(available_voll->count == 0);
		PTHREAD_MUTEX_UNLOCK(&volume_mutex);
		volp = malloc(sizeof(*volp));
		if (volp == NULL) {
			ERR("malloc(%zu): %s", sizeof(*volp), strerror(errno));
			return (-2);
		}
		volp->established_sesl = NULL;
		if (session_list_alloc(&volp->established_sesl, SESL_IDX_STD) <
		    0) {
			assert(volp->established_sesl == NULL);
			free(volp);
			return (-3);
		}
		assert(volp->established_sesl != NULL);
		volp->slave_mirl = NULL;
		if (mirror_list_alloc(&volp->slave_mirl, MIRL_IDX_STD) < 0) {
			assert(volp->slave_mirl == NULL);
			session_list_free(volp->established_sesl);
			free(volp);
			return (-4);
		}
		assert(volp->slave_mirl != NULL);
		volp->range_tokt = NULL;
		if (token_tree_alloc(&volp->range_tokt) < 0) {
			assert(volp->range_tokt == NULL);
			mirror_list_free(volp->slave_mirl);
			session_list_free(volp->established_sesl);
			free(volp);
			return (-5);
		}
		volume_init(volp);
		PTHREAD_MUTEX_LOCK(&volume_mutex);
	} else {
		volume_list_remove(available_voll, volp);
	}
	assert(!volp->flags);
	volp->flags |= VOL_FL_INUSE;
	volume_list_insert(inuse_voll, volp);
	PTHREAD_MUTEX_UNLOCK(&volume_mutex);
	assert(volp->state < 0);
	assert(volp->fd < 0);
	*volpp = volp;
	return (0);
}

void
volume_init(struct volume *volp)
{

	volp->flags = 0;
	volp->state = -1;
	volp->fd = -1;
	volp->bm = NULL;
	volp->id[0] = '\0';
}

void
volume_uninit(struct volume *volp)
{

	assert(volp->flags == VOL_FL_INUSE);
	assert(volp->state < 0);
	assert(volp->fd < 0);
	session_list_uninit(volp->established_sesl);
	mirror_list_uninit(volp->slave_mirl);
	token_tree_uninit(volp->range_tokt);
	assert(volp->bm == NULL);
	volp->id[0] = '\0';
}

void
volume_free(struct volume *volp)
{

	assert(volume_module_initialized);
	volume_uninit(volp);
	PTHREAD_MUTEX_LOCK(&volume_mutex);
	volume_list_remove(inuse_voll, volp);
	volp->flags &= ~VOL_FL_INUSE;
	assert(!volp->flags);
	volume_list_insert(available_voll, volp);
	PTHREAD_MUTEX_UNLOCK(&volume_mutex);
}

static
void
volume_dealloc(struct volume *volp)
{

	assert(!volp->flags);
	assert(volp->state < 0);
	assert(volp->fd < 0);
	assert(volp->bm == NULL);
	token_tree_free(volp->range_tokt);
	mirror_list_free(volp->slave_mirl);
	session_list_free(volp->established_sesl);
	free(volp);
}

const char *
volume_strstate(struct volume *volp)
{
	const char *state;

	switch (volp->state) {
	case VOL_ST_UP:
		state = "up";
		break;
	case VOL_ST_DOWN:
		state = "down";
		break;
	case VOL_ST_SHUTDOWN:
		state = "shutdown";
		break;
	default:
		state = "undefined";
		break;
	}
	return (state);
}

int
volume_setup(struct volume **volpp, const char *id, struct stat *stp)
{
	struct volume *volp;
#ifndef NDEBUG
	int len;
#endif

	volp = NULL;
	if (volume_alloc(&volp) < 0) {
		assert(volp == NULL);
		return (-1);
	}
	assert(volp != NULL);
	volp->st_dev = stp->st_dev;
	volp->st_ino = stp->st_ino;
#ifndef NDEBUG
	len =
#endif
	    snprintf(volp->id, sizeof(volp->id), "%s", id);
	assert(len > 0 && len < (int)sizeof(volp->id));
	*volpp = volp;
	return (0);
}

int
volume_create(const char *id, uint64_t size, int soft)
{
	char vctrldir[MY_MAXPATH], vdatadir[MY_MAXPATH];
	struct stat ctrldirst, datadirst;
	pthread_t self;
	pid_t pid;
	int ctrldirfd, datadirfd, datafd, flags, len, vctrldirfd, vdatadirfd;

	flags = O_RDONLY;
#ifdef __linux__
	flags |= O_DIRECTORY;
#endif
	DEBUG("OPEN %s", cfg_ctrl_directory);
	ctrldirfd = fd_open(cfg_ctrl_directory, flags, 0);
	if (ctrldirfd < 0)
		goto out0;
	memset(&ctrldirst, 0x00, sizeof(ctrldirst));
	if (fstat(ctrldirfd, &ctrldirst) < 0) {
		ERR("stat(%s): %s", cfg_ctrl_directory, strerror(errno));
		goto out1;
	}
	DEBUG("OPEN %s", cfg_data_directory);
	datadirfd = fd_open(cfg_data_directory, flags, 0);
	if (datadirfd < 0)
		goto out1;
	memset(&datadirst, 0x00, sizeof(datadirst));
	if (fstat(datadirfd, &datadirst) < 0) {
		ERR("stat(%s): %s", cfg_data_directory, strerror(errno));
		goto out2;
	}
	if (ctrldirst.st_dev == datadirst.st_dev &&
	    ctrldirst.st_ino == datadirst.st_ino) {
		ERR("ctrl_directory and data_directory must be distinct");
		goto out2;
	}

	pid = getpid();
	self = pthread_self();
	len = snprintf(vctrldir, sizeof(vctrldir), ".create-ctrl.%s.%d.%llX",
	    id, (int)pid, (long long)self);
	if (len < 0 || len >= (int)sizeof(vctrldir))
		goto out2;
	DEBUG("MKDIR %s/%s", cfg_ctrl_directory, vctrldir);
	if (mkdirat(ctrldirfd, vctrldir, 0755) < 0) {
		ERR("mkdir(%s/%s): %s",
		    cfg_ctrl_directory, vctrldir, strerror(errno));
		goto out2;
	}
	len = snprintf(vdatadir, sizeof(vdatadir), ".create-data.%s.%d.%llX",
	    id, (int)pid, (long long)self);
	if (len < 0 || len >= (int)sizeof(vdatadir))
		goto out3;
	DEBUG("MKDIR %s/%s", cfg_data_directory, vdatadir);
	if (mkdirat(datadirfd, vdatadir, 0755) < 0) {
		ERR("mkdir(%s/%s): %s",
		    cfg_data_directory, vdatadir, strerror(errno));
		goto out3;
	}

	DEBUG("OPEN %s/%s", cfg_ctrl_directory, vctrldir);
	vctrldirfd = fd_openat(ctrldirfd, vctrldir, flags, 0);
	if (vctrldirfd < 0)
		goto out4;
	DEBUG("OPEN %s/%s", cfg_data_directory, vdatadir);
	vdatadirfd = fd_openat(datadirfd, vdatadir, flags, 0);
	if (vdatadirfd < 0)
		goto out5;

	DEBUG("OPEN %s/%s/data", cfg_data_directory, vdatadir);
	datafd = fd_openat(vdatadirfd, "data", O_RDWR | O_CREAT | O_EXCL, 0640);
	if (datafd < 0)
		goto out6;

	DEBUG("TRUNCATE %s/%s/data", cfg_data_directory, vdatadir);
	if (fd_truncate(datafd, size) < 0) {
		DEBUG("CLOSE %s/%s/data", cfg_data_directory, vdatadir);
		fd_close(datafd);
		goto out7;
	}
	DEBUG("FSYNC %s/%s/data", cfg_data_directory, vdatadir);
	if (fsync(datafd) < 0)
		WARNING("fsync(%s/%s/data): %s",
		    cfg_data_directory, vdatadir, strerror(errno));
	DEBUG("CLOSE %s/%s/data", cfg_data_directory, vdatadir);
	fd_close(datafd);

	if (soft > 0) {
		DEBUG("CREATE %s/%s/blkmap", cfg_data_directory, vdatadir);
		if (blkmap_create(vdatadirfd, "blkmap", size, id) < 0)
			goto out7;
	}

	DEBUG("FSYNC %s/%s", cfg_data_directory, vdatadir);
	if (fsync(vdatadirfd) < 0)
		WARNING("fsync(%s/%s): %s",
		    cfg_data_directory, vdatadir, strerror(errno));

	DEBUG("SYMLINK %s/%s/soft --> %d",
	    cfg_ctrl_directory, vctrldir, soft > 0);
	if (symlinkat(soft > 0 ? "1" : "0", vctrldirfd, "soft") < 0) {
		ERR("symlink(%s/%s/soft, %d): %s",
		    cfg_ctrl_directory, vctrldir, soft > 0, strerror(errno));
		goto out8;
	}

	DEBUG("SYMLINK %s/%s/up --> 0", cfg_ctrl_directory, vctrldir);
	if (symlinkat("0", vctrldirfd, "up") < 0) {
		ERR("symlink(%s/%s/up, 0): %s",
		    cfg_ctrl_directory, vctrldir, strerror(errno));
		goto out9;
	}

	DEBUG("RENAME %s/%s --> %s", cfg_data_directory, vdatadir, id);
	if (renameat(datadirfd, vdatadir, datadirfd, id) < 0) {
		ERR("rename(%s/%s, %s): %s",
		    cfg_data_directory, vdatadir, id, strerror(errno));
		goto out10;
	}
#ifndef NDEBUG
	len =
#endif
	    snprintf(vdatadir, sizeof(vdatadir), "%s", id);
	assert(len > 0 && len < (int)sizeof(vdatadir));
	DEBUG("RENAME %s/%s --> %s", cfg_ctrl_directory, vctrldir, id);
	if (renameat(ctrldirfd, vctrldir, ctrldirfd, id) < 0) {
		ERR("rename(%s/%s, %s): %s",
		    cfg_ctrl_directory, vctrldir, id, strerror(errno));
		goto out10;
	}
#ifndef NDEBUG
	len =
#endif
	    snprintf(vctrldir, sizeof(vctrldir), "%s", id);
	assert(len > 0 && len < (int)sizeof(vctrldir));

	DEBUG("CLOSE %s/%s", cfg_data_directory, vdatadir);
	fd_close(vdatadirfd);
	DEBUG("FSYNC %s", cfg_data_directory);
	if (fsync(datadirfd) < 0)
		WARNING("fsync(%s): %s", cfg_data_directory, strerror(errno));
	DEBUG("CLOSE %s", cfg_data_directory);
	fd_close(datadirfd);
	DEBUG("CLOSE %s/%s", cfg_ctrl_directory, vctrldir);
	fd_close(vctrldirfd);
	DEBUG("FSYNC %s", cfg_ctrl_directory);
	if (fsync(ctrldirfd) < 0)
		WARNING("fsync(%s): %s", cfg_ctrl_directory, strerror(errno));
	DEBUG("CLOSE %s", cfg_ctrl_directory);
	fd_close(ctrldirfd);
	NOTICE("VOLUME CREATE: id=%s size=%llu soft=%d",
	    id, (unsigned long long)size, soft > 0);
	return (0);
out10:
	DEBUG("UNLINK %s/%s/up", cfg_ctrl_directory, vctrldir);
	if (unlinkat(vctrldirfd, "up", 0) < 0)
		WARNING("unlink(%s/%s/up): %s",
		    cfg_ctrl_directory, vctrldir, strerror(errno));
out9:
	DEBUG("UNLINK %s/%s/soft", cfg_ctrl_directory, vctrldir);
	if (unlinkat(vctrldirfd, "soft", 0) < 0)
		WARNING("unlink(%s/%s/soft): %s",
		    cfg_ctrl_directory, vctrldir, strerror(errno));
out8:
	if (soft > 0) {
		DEBUG("DELETE %s/%s/blkmap", cfg_data_directory, vdatadir);
		blkmap_delete(vdatadirfd, "blkmap");
	}
out7:
	DEBUG("UNLINK %s/%s/data", cfg_data_directory, vdatadir);
	if (unlinkat(vdatadirfd, "data", 0) < 0)
		WARNING("unlink(%s/%s/data): %s",
		    cfg_data_directory, vdatadir, strerror(errno));
out6:
	DEBUG("CLOSE %s/%s", cfg_data_directory, vdatadir);
	fd_close(vdatadirfd);
out5:
	DEBUG("CLOSE %s/%s", cfg_ctrl_directory, vctrldir);
	fd_close(vctrldirfd);
out4:
	DEBUG("UNLINK %s/%s", cfg_data_directory, vdatadir);
	if (unlinkat(datadirfd, vdatadir, AT_REMOVEDIR) < 0)
		WARNING("unlink(%s/%s): %s",
		    cfg_data_directory, vdatadir, strerror(errno));
out3:
	DEBUG("UNLINK %s/%s", cfg_ctrl_directory, vctrldir);
	if (unlinkat(ctrldirfd, vctrldir, AT_REMOVEDIR) < 0)
		WARNING("unlink(%s/%s): %s",
		    cfg_ctrl_directory, vctrldir, strerror(errno));
out2:
	DEBUG("CLOSE %s", cfg_data_directory);
	fd_close(datadirfd);
out1:
	DEBUG("CLOSE %s", cfg_ctrl_directory);
	fd_close(ctrldirfd);
out0:
	return (-1);
}

int
volume_delete_begin(struct volume *volp)
{
	char vctrldir[MY_MAXPATH], vdatadir[MY_MAXPATH];
	struct stat ctrldirst, datadirst;
	pthread_t self;
	pid_t pid;
	int ctrldirfd, datadirfd, flags, len;

	flags = O_RDONLY;
#ifdef __linux__
	flags |= O_DIRECTORY;
#endif
	DEBUG("OPEN %s", cfg_ctrl_directory);
	ctrldirfd = fd_open(cfg_ctrl_directory, flags, 0);
	if (ctrldirfd < 0)
		goto out0;
	memset(&ctrldirst, 0x00, sizeof(ctrldirst));
	if (fstat(ctrldirfd, &ctrldirst) < 0) {
		ERR("stat(%s): %s", cfg_ctrl_directory, strerror(errno));
		goto out1;
	}
	DEBUG("OPEN %s", cfg_data_directory);
	datadirfd = fd_open(cfg_data_directory, flags, 0);
	if (datadirfd < 0)
		goto out1;
	memset(&datadirst, 0x00, sizeof(datadirst));
	if (fstat(datadirfd, &datadirst) < 0) {
		ERR("stat(%s): %s", cfg_data_directory, strerror(errno));
		goto out2;
	}
	if (ctrldirst.st_dev == datadirst.st_dev &&
	    ctrldirst.st_ino == datadirst.st_ino) {
		ERR("ctrl_directory and data_directory must be distinct");
		goto out2;
	}

	pid = getpid();
	self = pthread_self();
	len = snprintf(vctrldir, sizeof(vctrldir), ".delete-ctrl.%s.%d.%llX",
	    volp->id, (int)pid, (long long)self);
	if (len < 0 || len >= (int)sizeof(vctrldir))
		goto out2;
	len = snprintf(vdatadir, sizeof(vdatadir), ".delete-data.%s.%d.%llX",
	    volp->id, (int)pid, (long long)self);
	if (len < 0 || len >= (int)sizeof(vdatadir))
		goto out2;
	DEBUG("RMDIR %s/%s", cfg_ctrl_directory, vctrldir);
	if (unlinkat(ctrldirfd, vctrldir, AT_REMOVEDIR) < 0 &&
	    errno != ENOENT) {
		ERR("unlink(%s/%s): %s",
		    cfg_ctrl_directory, vctrldir, strerror(errno));
		goto out2;
	}
	DEBUG("RMDIR %s/%s", cfg_data_directory, vdatadir);
	if (unlinkat(datadirfd, vdatadir, AT_REMOVEDIR) < 0 &&
	    errno != ENOENT) {
		ERR("unlink(%s/%s): %s",
		    cfg_data_directory, vdatadir, strerror(errno));
		goto out2;
	}
	DEBUG("RENAME %s/%s --> %s", cfg_ctrl_directory, volp->id, vctrldir);
	if (renameat(ctrldirfd, volp->id, ctrldirfd, vctrldir) < 0) {
		ERR("rename(%s/%s, %s): %s",
		    cfg_ctrl_directory, volp->id, vctrldir, strerror(errno));
		goto out2;
	}
	DEBUG("RENAME %s/%s --> %s", cfg_data_directory, volp->id, vdatadir);
	if (renameat(datadirfd, volp->id, datadirfd, vdatadir) < 0) {
		ERR("rename(%s/%s, %s): %s",
		    cfg_data_directory, volp->id, vdatadir, strerror(errno));
		goto out3;
	}
	DEBUG("FSYNC %s", cfg_data_directory);
	if (fsync(datadirfd) < 0)
		WARNING("fsync(%s): %s", cfg_data_directory, strerror(errno));
	DEBUG("CLOSE %s", cfg_data_directory);
	fd_close(datadirfd);
	DEBUG("FSYNC %s", cfg_ctrl_directory);
	if (fsync(ctrldirfd) < 0)
		WARNING("fsync(%s): %s", cfg_ctrl_directory, strerror(errno));
	DEBUG("CLOSE %s", cfg_ctrl_directory);
	fd_close(ctrldirfd);
	NOTICE("VOLUME DELETE 1/2: id=%s", volp->id);
	return (0);
out3:
	DEBUG("RENAME %s/%s --> %s", cfg_ctrl_directory, vctrldir, volp->id);
	if (renameat(ctrldirfd, vctrldir, ctrldirfd, volp->id) < 0)
		WARNING("rename(%s/%s, %s): %s",
		    cfg_ctrl_directory, vctrldir, volp->id, strerror(errno));
out2:
	DEBUG("CLOSE %s", cfg_data_directory);
	fd_close(datadirfd);
out1:
	DEBUG("CLOSE %s", cfg_ctrl_directory);
	fd_close(ctrldirfd);
out0:
	return (-1);
}

int
volume_delete_commit(struct volume *volp)
{
	char vctrldir[MY_MAXPATH], vdatadir[MY_MAXPATH];
	struct stat ctrldirst, datadirst;
	pthread_t self;
	pid_t pid;
	int ctrldirfd, datadirfd, flags, vctrldirfd, vdatadirfd;
#ifndef NDEBUG
	int len;
#endif

	flags = O_RDONLY;
#ifdef __linux__
	flags |= O_DIRECTORY;
#endif
	DEBUG("OPEN %s", cfg_ctrl_directory);
	ctrldirfd = fd_open(cfg_ctrl_directory, flags, 0);
	if (ctrldirfd < 0)
		goto out0;
	memset(&ctrldirst, 0x00, sizeof(ctrldirst));
	if (fstat(ctrldirfd, &ctrldirst) < 0) {
		ERR("stat(%s): %s", cfg_ctrl_directory, strerror(errno));
		goto out1;
	}
	DEBUG("OPEN %s", cfg_data_directory);
	datadirfd = fd_open(cfg_data_directory, flags, 0);
	if (datadirfd < 0)
		goto out1;
	memset(&datadirst, 0x00, sizeof(datadirst));
	if (fstat(datadirfd, &datadirst) < 0) {
		ERR("stat(%s): %s", cfg_data_directory, strerror(errno));
		goto out2;
	}
	if (ctrldirst.st_dev == datadirst.st_dev &&
	    ctrldirst.st_ino == datadirst.st_ino) {
		ERR("ctrl_directory and data_directory must be distinct");
		goto out2;
	}

	pid = getpid();
	self = pthread_self();
#ifndef NDEBUG
	len =
#endif
	    snprintf(vctrldir, sizeof(vctrldir), ".delete-ctrl.%s.%d.%llX",
		volp->id, (int)pid, (long long)self);
	assert(len > 0 && len < (int)sizeof(vctrldir));
#ifndef NDEBUG
	len =
#endif
	    snprintf(vdatadir, sizeof(vdatadir), ".delete-data.%s.%d.%llX",
		volp->id, (int)pid, (long long)self);
	assert(len > 0 && len < (int)sizeof(vdatadir));
	DEBUG("OPEN %s/%s", cfg_ctrl_directory, vctrldir);
	vctrldirfd = fd_openat(ctrldirfd, vctrldir, flags, 0);
	if (vctrldirfd < 0)
		goto out2;
	DEBUG("OPEN %s/%s", cfg_data_directory, vdatadir);
	vdatadirfd = fd_openat(datadirfd, vdatadir, flags, 0);
	if (vdatadirfd < 0)
		goto out3;

	DEBUG("DELETE %s/%s/blkmap", cfg_data_directory, vdatadir);
	if (blkmap_delete(vdatadirfd, "blkmap") < 0)
		goto out4;
	DEBUG("UNLINK %s/%s/data", cfg_data_directory, vdatadir);
	if (unlinkat(vdatadirfd, "data", 0) < 0 && errno != ENOENT) {
		ERR("unlink(%s/%s/data): %s",
		    cfg_data_directory, vdatadir, strerror(errno));
		goto out4;
	}
	DEBUG("FSYNC %s/%s", cfg_data_directory, vdatadir);
	if (fsync(vdatadirfd) < 0)
		WARNING("fsync(%s/%s): %s",
		    cfg_data_directory, vdatadir, strerror(errno));

	DEBUG("UNLINK %s/%s/up", cfg_ctrl_directory, vctrldir);
	if (unlinkat(vctrldirfd, "up", 0) < 0 && errno != ENOENT) {
		ERR("unlink(%s/%s/up): %s",
		    cfg_ctrl_directory, vctrldir, strerror(errno));
		goto out4;
	}
	DEBUG("UNLINK %s/%s/soft", cfg_ctrl_directory, vctrldir);
	if (unlinkat(vctrldirfd, "soft", 0) < 0 && errno != ENOENT) {
		ERR("unlink(%s/%s/soft): %s",
		    cfg_ctrl_directory, vctrldir, strerror(errno));
		goto out4;
	}
	DEBUG("FSYNC %s/%s", cfg_ctrl_directory, vctrldir);
	if (fsync(vctrldirfd) < 0)
		WARNING("fsync(%s/%s): %s",
		    cfg_ctrl_directory, vctrldir, strerror(errno));

	DEBUG("CLOSE %s/%s", cfg_data_directory, vdatadir);
	fd_close(vdatadirfd);
	DEBUG("CLOSE %s/%s", cfg_ctrl_directory, vctrldir);
	fd_close(vctrldirfd);

	DEBUG("RMDIR %s/%s", cfg_data_directory, vdatadir);
	if (unlinkat(datadirfd, vdatadir, AT_REMOVEDIR) < 0) {
		ERR("unlink(%s/%s): %s",
		    cfg_data_directory, vdatadir, strerror(errno));
		goto out2;
	}
	DEBUG("FSYNC %s", cfg_data_directory);
	if (fsync(datadirfd) < 0)
		WARNING("fsync(%s): %s", cfg_data_directory, strerror(errno));
	DEBUG("CLOSE %s", cfg_data_directory);
	fd_close(datadirfd);

	DEBUG("RMDIR %s/%s", cfg_ctrl_directory, vctrldir);
	if (unlinkat(ctrldirfd, vctrldir, AT_REMOVEDIR) < 0) {
		ERR("unlink(%s/%s): %s",
		    cfg_ctrl_directory, vctrldir, strerror(errno));
		goto out1;
	}
	DEBUG("FSYNC %s", cfg_ctrl_directory);
	if (fsync(ctrldirfd) < 0)
		WARNING("fsync(%s): %s", cfg_ctrl_directory, strerror(errno));
	DEBUG("CLOSE %s", cfg_ctrl_directory);
	fd_close(ctrldirfd);
	NOTICE("VOLUME DELETE 2/2: id=%s", volp->id);
	return (0);
out4:
	DEBUG("CLOSE %s/%s", cfg_data_directory, vdatadir);
	fd_close(vdatadirfd);
out3:
	DEBUG("CLOSE %s/%s", cfg_ctrl_directory, vctrldir);
	fd_close(vctrldirfd);
out2:
	DEBUG("CLOSE %s", cfg_data_directory);
	fd_close(datadirfd);
out1:
	DEBUG("CLOSE %s", cfg_ctrl_directory);
	fd_close(ctrldirfd);
out0:
	return (-1);
}

int
volume_up(struct volume *volp)
{

	switch (volp->state) {
	case VOL_ST_UP:
		return (0);
	case VOL_ST_DOWN:
		break;
	case VOL_ST_SHUTDOWN:
		return (-1);
	default:
		abort();
	}
	if (volume_opt_set(volp, "up", "1") < 0)
		return (-1);
	volp->state = VOL_ST_UP;
	NOTICE("VOLUME UP: id=%s", volp->id);
	return (1);
}

int
volume_down_begin(struct volume *volp)
{

	switch (volp->state) {
	case VOL_ST_UP:
		break;
	case VOL_ST_DOWN:
		return (0);
	case VOL_ST_SHUTDOWN:
		return (-1);
	default:
		abort();
	}
	if (volume_opt_set(volp, "up", "0") < 0)
		return (-1);
	volp->state = VOL_ST_SHUTDOWN;
	NOTICE("VOLUME SHUTDOWN: id=%s", volp->id);
	return (1);
}

void
volume_down_commit(struct volume *volp)
{

	assert(volp->state == VOL_ST_SHUTDOWN);
	volp->state = VOL_ST_DOWN;
	NOTICE("VOLUME DOWN: id=%s", volp->id);
}

void
volume_shutdown(struct volume *volp)
{

	assert(volp->state == VOL_ST_UP || volp->state == VOL_ST_DOWN);
	session_list_shutdown(volp->established_sesl);
	mirror_list_shutdown(volp->slave_mirl);
	volp->state = -1;
	volume_close(volp);
}

static
int
volume_opt_get(struct volume *volp, const char *key, char *val, size_t val_len,
    int mandatory)
{
	char vctrldir[MY_MAXPATH];
	ssize_t n;
	int flags, len, retval, vctrldirfd;

	assert(key != NULL && key[0] != '\0' && key[0] != '.');
	assert(val != NULL);
	assert(val_len > (size_t)0);
	vctrldirfd = -1;
	retval = -1;

	len = snprintf(vctrldir, sizeof(vctrldir), "%s/%s",
	    cfg_ctrl_directory, volp->id);
	if (len < 0 || len >= (int)sizeof(vctrldir))
		goto out;
	flags = O_RDONLY;
#ifdef __linux__
	flags |= O_DIRECTORY;
#endif
	DEBUG("OPEN %s", vctrldir);
	vctrldirfd = fd_open(vctrldir, flags, 0);
	if (vctrldirfd < 0)
		goto out;
	DEBUG("READLINK %s/%s", vctrldir, key);
	n = readlinkat(vctrldirfd, key, val, val_len - 1);
	if (n < (ssize_t)0) {
		if (errno != ENOENT || mandatory) {
			ERR("readlink(%s/%s): %s",
			    vctrldir, key, strerror(errno));
			goto out;
		}
		n = (ssize_t)0;
	}
	assert((size_t)n < val_len);
	val[n] = '\0';
	retval = 0;
out:
	if (vctrldirfd >= 0) {
		DEBUG("CLOSE %s/%s", cfg_ctrl_directory, volp->id);
		fd_close(vctrldirfd);
	}
	return (retval);
}

static
int
volume_opt_set(struct volume *volp, const char *key, const char *val)
{
	char vctrldir[MY_MAXPATH];
	int flags, len, retval, vctrldirfd;

	assert(key != NULL && key[0] != '\0' && key[0] != '.');
	assert(val != NULL && val[0] != '\0');
	vctrldirfd = -1;
	retval = -1;

	len = snprintf(vctrldir, sizeof(vctrldir), "%s/%s",
	    cfg_ctrl_directory, volp->id);
	if (len < 0 || len >= (int)sizeof(vctrldir))
		goto out;
	flags = O_RDONLY;
#ifdef __linux__
	flags |= O_DIRECTORY;
#endif
	DEBUG("OPEN %s", vctrldir);
	vctrldirfd = fd_open(vctrldir, flags, 0);
	if (vctrldirfd < 0)
		goto out;
	len = snprintf(vctrldir, sizeof(vctrldir), ".%s", key);
	if (len < 0 || len >= (int)sizeof(vctrldir))
		goto out;
	DEBUG("UNLINK %s/%s/%s", cfg_ctrl_directory, volp->id, vctrldir);
	if (unlinkat(vctrldirfd, vctrldir, 0) < 0 && errno != ENOENT) {
		ERR("unlink(%s/%s/%s): %s",
		    cfg_ctrl_directory, volp->id, vctrldir, strerror(errno));
		goto out;
	}
	DEBUG("SYMLINK %s/%s/%s --> %s",
	    cfg_ctrl_directory, volp->id, vctrldir, val);
	if (symlinkat(val, vctrldirfd, vctrldir) < 0) {
		ERR("symlink(%s/%s/%s, %s): %s",
		    cfg_ctrl_directory, volp->id, vctrldir, val,
		    strerror(errno));
		goto out;
	}
	DEBUG("RENAME %s/%s/%s --> %s",
	    cfg_ctrl_directory, volp->id, vctrldir, key);
	if (renameat(vctrldirfd, vctrldir, vctrldirfd, key) < 0) {
		ERR("rename(%s/%s/%s, %s): %s",
		    cfg_ctrl_directory, volp->id, vctrldir, key,
		    strerror(errno));
		goto out;
	}
	DEBUG("FSYNC %s/%s", cfg_ctrl_directory, volp->id);
	if (fsync(vctrldirfd) < 0)
		WARNING("fsync(%s/%s): %s",
		    cfg_ctrl_directory, volp->id, strerror(errno));
	retval = 0;
out:
	if (vctrldirfd >= 0) {
		DEBUG("CLOSE %s/%s", cfg_ctrl_directory, volp->id);
		fd_close(vctrldirfd);
	}
	return (retval);
}

static
int
volume_open(struct volume *volp)
{
	char path[MY_MAXPATH], buf[8];
	struct stat st;
	int flags, len, soft, state;

	assert(volp->state < 0);
	assert(volp->fd < 0);
	assert(volp->bm == NULL);

	if (volume_opt_get(volp, "up", buf, sizeof(buf), 0) < 0)
		goto fail;
	switch (atoi(buf)) {
	case 0:
		state = VOL_ST_DOWN;
		break;
	case 1:
		state = VOL_ST_UP;
		break;
	default:
		ERR("Broken flag up=%s for volume %s", buf, volp->id);
		goto fail;
	}
	if (volume_opt_get(volp, "soft", buf, sizeof(buf), 0) < 0)
		goto fail;
	switch (atoi(buf)) {
	case 0:
		soft = 0;
		break;
	case 1:
		soft = 1;
		break;
	default:
		ERR("Broken flag soft=%s for volume %s", buf, volp->id);
		goto fail;
	}
	len = snprintf(path, sizeof(path), "%s/%s/data",
	    cfg_data_directory, volp->id);
	if (len < 0 || len >= (int)sizeof(path))
		goto fail;
	flags = O_RDWR | O_DIRECT;
#ifdef __linux__
	flags |= O_NOATIME;
#endif
	if (cfg_sync == 1)
		flags |= O_SYNC;
	volp->fd = fd_open(path, flags, 0);
	if (volp->fd < 0)
		goto fail;
	memset(&st, 0x00, sizeof(st));
	if (fstat(volp->fd, &st) < 0) {
		ERR("stat(%s): %s", path, strerror(errno));
		goto fail;
	}
	volp->size = (uint64_t)st.st_size;
	if (volp->size < (uint64_t)(64 * 1024 * 1024)) {
		ERR("VOLUME MUST BE AT LEAST 64MB LARGE");
		goto fail;
	}
	volp->blk_size = (uint32_t)st.st_blksize;
	if (volp->blk_size < (uint32_t)512 || !powerof2(volp->blk_size)) {
		ERR("INVALID VOLUME BLOCK SIZE %u",
		    (unsigned int)volp->blk_size);
		goto fail;
	}
	if (soft > 0) {
		len = snprintf(path, sizeof(path), "%s/%s/blkmap",
		    cfg_data_directory, volp->id);
		if (len < 0 || len >= (int)sizeof(path))
			goto fail;
		if (blkmap_setup(&volp->bm, volp->size, volp->id) < 0) {
			assert(volp->bm == NULL);
			goto fail;
		}
		assert(volp->bm != NULL);
		if (blkmap_open(volp->bm, path) < 0)
			goto fail;
	}
	volp->state = state;
	NOTICE("VOLUME OPEN: "
	    "id=%s fd=%d size=%llu usage=%llu block=%u soft=%d state=%s",
	    volp->id,
	    volp->fd,
	    (unsigned long long)volp->size,
	    (unsigned long long)(st.st_blocks << 9),
	    (unsigned int)volp->blk_size,
	    volp->bm != NULL,
	    volume_strstate(volp));
	return (0);
fail:
	if (volp->bm != NULL) {
		blkmap_free(volp->bm);
		volp->bm = NULL;
	}
	if (volp->fd >= 0) {
		fd_close(volp->fd);
		volp->fd = -1;
	}
	return (-1);
}

static
void
volume_close(struct volume *volp)
{

	assert(volp->state < 0);
	if (volp->bm != NULL) {
		blkmap_close(volp->bm);
		blkmap_free(volp->bm);
		volp->bm = NULL;
	}
	fd_close(volp->fd);
	volp->fd = -1;
}

int
volume_list_alloc(struct volume_list **vollpp, int index)
{
	struct volume_list *vollp;
	int error;

	vollp = malloc(sizeof(*vollp));
	if (vollp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*vollp), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&vollp->mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(vollp);
		return (-3);
	}
	volume_list_init(vollp, index);
	*vollpp = vollp;
	return (0);
}

void
volume_list_init(struct volume_list *vollp, int index)
{
#ifndef NDEBUG
	struct volume *volp;
#endif

	assert(index >= 0 && index < NELEM(volp->all_voll_ary));
	TAILQ_INIT(&vollp->head);
	vollp->index = index;
	vollp->count = 0;
	vollp->peak = 0;
}

void
volume_list_uninit(struct volume_list *vollp)
{

	assert(TAILQ_EMPTY(&vollp->head));
	assert(vollp->count == 0);
	vollp->peak = 0;
}

void
volume_list_free(struct volume_list *vollp)
{

	volume_list_uninit(vollp);
	PTHREAD_MUTEX_DESTROY(&vollp->mutex);
	free(vollp);
}

static
void
volume_list_dealloc(struct volume_list *vollp)
{
	struct volume *nvolp, *volp;

	nvolp = volume_list_first(vollp);
	for (;;) {
		volp = nvolp;
		if (volp == NULL)
			break;
		nvolp = volume_list_next(vollp, volp);
		volume_list_remove(vollp, volp);
		volume_dealloc(volp);
	}
}

void
volume_list_lock(struct volume_list *vollp)
{

	PTHREAD_MUTEX_LOCK(&vollp->mutex);
}

void
volume_list_unlock(struct volume_list *vollp)
{

	PTHREAD_MUTEX_UNLOCK(&vollp->mutex);
}

int
volume_list_empty(struct volume_list *vollp)
{

	return (TAILQ_EMPTY(&vollp->head));
}

struct volume *
volume_list_first(struct volume_list *vollp)
{

	return (TAILQ_FIRST(&vollp->head));
}

struct volume *
volume_list_next(struct volume_list *vollp, struct volume *volp)
{

	return (TAILQ_NEXT(volp, all_voll_ary[vollp->index]));
}

void
volume_list_insert(struct volume_list *vollp, struct volume *volp)
{

	TAILQ_INSERT_HEAD(&vollp->head, volp, all_voll_ary[vollp->index]);
	vollp->count++;
	assert(vollp->count > 0);
	if (vollp->count > vollp->peak)
		vollp->peak = vollp->count;
}

void
volume_list_append(struct volume_list *vollp, struct volume *volp)
{

	TAILQ_INSERT_TAIL(&vollp->head, volp, all_voll_ary[vollp->index]);
	vollp->count++;
	assert(vollp->count > 0);
	if (vollp->count > vollp->peak)
		vollp->peak = vollp->count;
}

void
volume_list_remove(struct volume_list *vollp, struct volume *volp)
{

	TAILQ_REMOVE(&vollp->head, volp, all_voll_ary[vollp->index]);
	vollp->count--;
	assert(vollp->count >= 0);
}

struct volume *
volume_list_lookup_by_id(struct volume_list *vollp, const char *id)
{
	struct volume *volp;

	TAILQ_FOREACH(volp, &vollp->head, all_voll_ary[vollp->index]) {
		if (strcmp(id, volp->id))
			continue;
		return (volp);
	}
	return (NULL);
}

struct volume *
volume_list_lookup_by_inode(struct volume_list *vollp, struct stat *stp)
{
	struct volume *volp;

	TAILQ_FOREACH(volp, &vollp->head, all_voll_ary[vollp->index]) {
		if (stp->st_dev != volp->st_dev || stp->st_ino != volp->st_ino)
			continue;
		return (volp);
	}
	return (NULL);
}

static
int
volume_list_open(struct volume_list *vollp, int flags)
{
	char path[MY_MAXPATH];
	struct stat st;
	DIR *dirp;
	struct dirent *dep;
	struct volume *volp;
	int len;

	(void)flags;

	dirp = opendir(cfg_ctrl_directory);
	if (dirp == NULL) {
		ERR("opendir(%s): %s", cfg_ctrl_directory, strerror(errno));
		return (-1);
	}

	volume_list_lock(vollp);
	for (;;) {
		dep = readdir(dirp);
		if (dep == NULL)
			break;
		if (dep->d_name[0] == '.')
			continue;
		if (strlen(dep->d_name) >= sizeof(volp->id)) {
			WARNING("Volume ID %s too long!", dep->d_name);
			continue;
		}
		len = snprintf(path, sizeof(path), "%s/%s",
		    cfg_ctrl_directory, dep->d_name);
		if (len < 0 || len >= (int)sizeof(path)) {
			WARNING("Volume path %s/%s too long!",
			    cfg_ctrl_directory, dep->d_name);
			continue;
		}
		memset(&st, 0x00, sizeof(st));
		if (stat(path, &st) < 0) {
			ERR("stat(%s): %s", path, strerror(errno));
			continue;
		}
		if (!S_ISDIR(st.st_mode))
			continue;
		if (volume_list_lookup_by_id(vollp, dep->d_name) != NULL ||
		    volume_list_lookup_by_inode(vollp, &st) != NULL)
			continue;

		volp = NULL;
		if (volume_setup(&volp, dep->d_name, &st) < 0) {
			assert(volp == NULL);
			continue;
		}
		assert(volp != NULL);

		if (volume_open(volp) < 0) {
			volume_free(volp);
			continue;
		}

		volume_list_append(vollp, volp);
	}
	volume_list_unlock(vollp);

	if (closedir(dirp) < 0)
		WARNING("closedir(%s): %s",
		    cfg_ctrl_directory, strerror(errno));
	return (0);
}

int
volume_list_load(struct volume_list *vollp)
{
	int error;

	error = volume_list_open(vollp, 0);
	return (error);
}

int
volume_list_reload(struct volume_list *vollp)
{
	int error;

	error = volume_list_open(vollp, 1);
	return (error);
}

void
volume_list_shutdown(struct volume_list *vollp)
{
	struct volume *volp;

	volume_list_lock(vollp);
	for (;;) {
		volp = volume_list_first(vollp);
		if (volp == NULL)
			break;
		volume_list_remove(vollp, volp);
		volume_shutdown(volp);
		volume_free(volp);
	}
	volume_list_unlock(vollp);
}
