/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "io.h"
#include "thread.h"
#include "request.h"
#include "worker.h"
#include "blkmap.h"
#include "mirror.h"
#include "volume.h"

static int mirror_module_initialized = 0;
static pthread_mutex_t mirror_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct mirror_list *available_mirl = NULL;
static struct mirror_list *inuse_mirl = NULL;

static void mirror_dealloc(struct mirror *);
static void mirror_list_dealloc(struct mirror_list *);
static int mirror_open(struct mirror *);
static void mirror_close(struct mirror *);
static void *mirror_thread(void *);
static void mirror_thread_loop(struct mirror *);

int
mirror_module_setup(void)
{

	assert(!mirror_module_initialized);
	assert(available_mirl == NULL);
	if (mirror_list_alloc(&available_mirl, MIRL_IDX_ALLOC) < 0) {
		assert(available_mirl == NULL);
		goto out0;
	}
	assert(available_mirl != NULL);
	assert(inuse_mirl == NULL);
	if (mirror_list_alloc(&inuse_mirl, MIRL_IDX_ALLOC) < 0) {
		assert(inuse_mirl == NULL);
		goto out1;
	}
	assert(inuse_mirl != NULL);
	mirror_module_initialized = 1;
	return (0);
out1:
	assert(available_mirl != NULL);
	mirror_list_dealloc(available_mirl);
	mirror_list_free(available_mirl);
	available_mirl = NULL;
out0:
	return (-1);
}

void
mirror_module_hello(void)
{

	assert(mirror_module_initialized);
	DEBUG("sizeof(mirror)=%zu", sizeof(struct mirror));
	DEBUG("sizeof(mirror_list)=%zu", sizeof(struct mirror_list));
}

void
mirror_module_teardown(void)
{

	assert(mirror_module_initialized);
	assert(inuse_mirl != NULL);
	mirror_list_free(inuse_mirl);
	inuse_mirl = NULL;
	assert(available_mirl != NULL);
	mirror_list_dealloc(available_mirl);
	mirror_list_free(available_mirl);
	available_mirl = NULL;
	mirror_module_initialized = 0;
}

void
mirror_module_stats(void)
{
	int available_count, available_peak, inuse_count, inuse_peak;

	assert(mirror_module_initialized);
	PTHREAD_MUTEX_LOCK(&mirror_mutex);
	available_count = available_mirl->count;
	available_peak = available_mirl->peak;
	available_mirl->peak = available_mirl->count;
	inuse_count = inuse_mirl->count;
	inuse_peak = inuse_mirl->peak;
	inuse_mirl->peak = inuse_mirl->count;
	PTHREAD_MUTEX_UNLOCK(&mirror_mutex);
	NOTICE("inuse=%d/%d available=%d/%d",
	    inuse_count, inuse_peak, available_count, available_peak);
}

int
mirror_alloc(struct mirror **mirpp)
{
	struct mirror *mirp;
	int error;

	assert(mirror_module_initialized);
	PTHREAD_MUTEX_LOCK(&mirror_mutex);
	mirp = mirror_list_first(available_mirl);
	if (mirp == NULL) {
		assert(available_mirl->count == 0);
		PTHREAD_MUTEX_UNLOCK(&mirror_mutex);
		mirp = malloc(sizeof(*mirp));
		if (mirp == NULL) {
			ERR("malloc(%zu): %s", sizeof(*mirp), strerror(errno));
			return (-2);
		}
		mirp->thr = NULL;
		if (thread_alloc(&mirp->thr, mirp) < 0) {
			assert(mirp->thr == NULL);
			free(mirp);
			return (-3);
		}
		assert(mirp->thr != NULL);
		error = pthread_mutex_init(&mirp->mutex, NULL);
		if (error) {
			ERR("pthread_mutex_init(): %d", error);
			thread_free(mirp->thr);
			free(mirp);
			return (-4);
		}
		mirp->aux_reql = NULL;
		if (request_list_alloc(&mirp->aux_reql, REQL_IDX_STD) < 0) {
			assert(mirp->aux_reql == NULL);
			PTHREAD_MUTEX_DESTROY(&mirp->mutex);
			thread_free(mirp->thr);
			free(mirp);
			return (-5);
		}
		assert(mirp->aux_reql != NULL);
		mirp->return_reql = NULL;
		if (request_list_alloc(&mirp->return_reql, REQL_IDX_STD) < 0) {
			assert(mirp->return_reql == NULL);
			request_list_free(mirp->aux_reql);
			PTHREAD_MUTEX_DESTROY(&mirp->mutex);
			thread_free(mirp->thr);
			free(mirp);
			return (-6);
		}
		assert(mirp->return_reql != NULL);
		mirror_init(mirp);
		PTHREAD_MUTEX_LOCK(&mirror_mutex);
	} else {
		mirror_list_remove(available_mirl, mirp);
	}
	assert(!mirp->flags);
	mirp->flags |= MIR_FL_INUSE;
	mirror_list_insert(inuse_mirl, mirp);
	PTHREAD_MUTEX_UNLOCK(&mirror_mutex);
	assert(mirp->state < 0);
	assert(mirp->fd < 0);
	assert(!mirp->flags2);
	assert(mirp->volp == NULL);
	assert(mirp->inflight == 0);
	*mirpp = mirp;
	return (0);
}

void
mirror_init(struct mirror *mirp)
{

	mirp->flags = 0;
	mirp->state = -1;
	mirp->fd = -1;
	mirp->flags2 = 0;
	mirp->volp = NULL;
	mirp->inflight = 0;
	mirp->target[0] = '\0';
}

void
mirror_uninit(struct mirror *mirp)
{

	thread_uninit(mirp->thr);
	assert(mirp->flags == MIR_FL_INUSE);
	assert(mirp->state < 0);
	assert(mirp->fd < 0);
	assert(!mirp->flags2);
	mirp->volp = NULL;
	assert(mirp->inflight == 0);
	request_list_uninit(mirp->aux_reql);
	request_list_uninit(mirp->return_reql);
	mirp->target[0] = '\0';
}

void
mirror_free(struct mirror *mirp)
{

	assert(mirror_module_initialized);
	mirror_uninit(mirp);
	PTHREAD_MUTEX_LOCK(&mirror_mutex);
	mirror_list_remove(inuse_mirl, mirp);
	mirp->flags &= ~MIR_FL_INUSE;
	assert(!mirp->flags);
	mirror_list_insert(available_mirl, mirp);
	PTHREAD_MUTEX_UNLOCK(&mirror_mutex);
}

static
void
mirror_dealloc(struct mirror *mirp)
{

	assert(!mirp->flags);
	assert(mirp->state < 0);
	assert(mirp->fd < 0);
	assert(!mirp->flags2);
	assert(mirp->inflight == 0);
	request_list_free(mirp->return_reql);
	request_list_free(mirp->aux_reql);
	PTHREAD_MUTEX_DESTROY(&mirp->mutex);
	thread_free(mirp->thr);
	free(mirp);
}

void
mirror_lock(struct mirror *mirp)
{

	PTHREAD_MUTEX_LOCK(&mirp->mutex);
}

void
mirror_unlock(struct mirror *mirp)
{

	PTHREAD_MUTEX_UNLOCK(&mirp->mutex);
}

int
mirror_thread_create(struct mirror *mirp)
{

	return (thread_create(mirp->thr, "mirror", mirror_thread));
}

void
mirror_thread_launch(struct mirror *mirp)
{

	thread_launch(mirp->thr);
}

static
void *
mirror_thread(void *data)
{
	struct mirror *mirp;

	mirp = (struct mirror *)data;
	assert(mirp != NULL);
	assert(mirp->thr != NULL);
	assert(mirp->volp != NULL);

	thread_prologue(mirp->thr);

	NOTICE("MIRROR SYNC START: target=%s volume=%s full=%d rs=%uKB ql=%d",
	    mirp->target, mirp->volp->id, mirp->sync_full,
	    (unsigned int)mirp->sync_request_size >> 10,
	    mirp->sync_queue_length);
	mirror_thread_loop(mirp);
	NOTICE("MIRROR SYNC STOP: target=%s volume=%s",
	    mirp->target, mirp->volp->id);

	mirror_lock(mirp);
	assert(mirp->flags & MIR_FL_SYNCHRONISING);
	if (thread_shutdown_commit(mirp->thr))
		thread_detach(mirp->thr);
	mirp->flags &= ~MIR_FL_SYNCHRONISING;
	mirror_shutdown_commit(mirp);
	mirror_unlock(mirp);

	thread_epilogue(NULL);
	return (NULL);
}

static
void
mirror_thread_loop(struct mirror *mirp)
{
	struct request *nreqp, *reqp;
	struct blkmap *bmp;
	uint64_t offset, wr_count;
	uint32_t boundary;
	int cancelled, dirty, error, failed, inflight, old_progress, progress;

	assert(mirp->aux_reql != NULL);
	assert(request_list_empty(mirp->aux_reql));
	assert(mirp->return_reql != NULL);
	assert(request_list_empty(mirp->return_reql));

	bmp = mirp->volp->bm;
	if (bmp != NULL && !mirp->sync_full)
		boundary = bmp->blk_size > mirp->volp->blk_size ?
		    bmp->blk_size : mirp->volp->blk_size;
	else
		boundary = mirp->volp->blk_size;

	cancelled = 0;
	failed = 0;
	inflight = 0;
	old_progress = 0;
	offset = (uint64_t)0;
	wr_count = (uint64_t)0;
	for (;;) {
		if (!cancelled && !failed) {
			mirror_lock(mirp);
			if (THREAD_SHUTDOWN_PENDING(mirp->thr)) {
				mirror_unlock(mirp);
				cancelled = 1;
				NOTICE("Cancelled, shutting down...");
			} else if (mirp->state != MIR_ST_UP) {
				assert(mirp->state == MIR_ST_SHUTDOWN);
				mirror_unlock(mirp);
				failed = 1;
				NOTICE("Failed (active), shutting down...");
			} else {
				mirror_unlock(mirp);
			}
		}
		if (cancelled || failed) {
			if (inflight == 0)
				break;
			worker_reql_submit(mirp->aux_reql);
			goto catch;
		}
issue:
		assert(inflight >= 0 && inflight <= mirp->sync_queue_length);
		if (inflight == mirp->sync_queue_length) {
			worker_reql_submit(mirp->aux_reql);
			goto catch;
		}
		assert(offset <= mirp->volp->size);
		if (offset == mirp->volp->size) {
			if (inflight == 0)
				break;
			worker_reql_submit(mirp->aux_reql);
			goto catch;
		}
		reqp = NULL;
		if (request_alloc(mirp->thr, &reqp, 0) < 0) {
			assert(reqp == NULL);
			worker_reql_submit(mirp->aux_reql);
			if (inflight > 0)
				goto catch;
			if (request_alloc(mirp->thr, &reqp, 1) < 0) {
				assert(reqp == NULL);
				ERR("OUT OF MEMORY (req)");
				failed = 1;
				continue;
			}
		}
		assert(reqp != NULL);
		reqp->nbd.request.from = offset;
		reqp->nbd.request.len = mirp->sync_request_size;
		reqp->blk_size = mirp->volp->blk_size;
		reqp->fd = mirp->volp->fd;
		reqp->volp = mirp->volp;
		reqp->outgoing_reqlp = mirp->return_reql;
		reqp->mirp = mirp;
		if (request_rwbuf_alloc(mirp->thr, reqp, 0) < 0) {
			worker_reql_submit(mirp->aux_reql);
			if (inflight > 0) {
				request_free(reqp);
				goto catch;
			}
			if (request_rwbuf_alloc(mirp->thr, reqp, 1) < 0) {
				ERR("OUT OF MEMORY (rwbuf)");
				request_free(reqp);
				failed = 1;
				continue;
			}
		}
		assert(reqp->buf.buf_ptr != NULL);
		assert(reqp->buf.buf_len == (size_t)reqp->nbd.request.len);
		token_setup(reqp->tok, reqp->nbd.request.from,
		    reqp->nbd.request.len, boundary);
		if (token_acquire(mirp->thr, reqp->volp->range_tokt,
		    reqp->tok, 0) < 0) {
			worker_reql_submit(mirp->aux_reql);
			if (inflight > 0) {
				request_free(reqp);
				goto catch;
			}
			token_acquire(mirp->thr, reqp->volp->range_tokt,
			    reqp->tok, 1);
		}
		if (bmp != NULL && !mirp->sync_full) {
			blkmap_lock(bmp);
			dirty = blkmap_exec(bmp,
			    reqp->nbd.request.from,
			    reqp->nbd.request.len,
			    BM_OP_CHECK);
			blkmap_unlock(bmp);
			assert(dirty == 0 || dirty == 1);
			if (!dirty) {
				token_release(reqp->volp->range_tokt,
				    reqp->tok);
				request_free(reqp);
				offset += mirp->sync_request_size;
				assert(offset <= mirp->volp->size);
				wr_count += mirp->sync_request_size;
				progress = (wr_count * (uint32_t)100) /
				    mirp->volp->size;
				if (progress > old_progress) {
					old_progress = progress;
					mirror_lock(mirp);
					mirp->sync_progress = progress;
					mirror_unlock(mirp);
				}
				goto issue;
			}
		}
		reqp->type = REQ_TY_MASTER_COPY;
		request_list_append(mirp->aux_reql, reqp);
		mirp->aux_reql->submit_bytes += reqp->nbd.request.len;
		assert(mirp->aux_reql->submit_bytes > (int64_t)0);
		inflight++;
		offset += mirp->sync_request_size;
		assert(offset <= mirp->volp->size);
		goto issue;
catch:
		assert(inflight > 0);
		assert(request_list_empty(mirp->aux_reql));
		request_list_lock(mirp->return_reql);
		if (request_list_empty(mirp->return_reql)) {
			mirp->return_reql->flags &= ~REQL_FL_NOTIFIED;
			request_list_wait(mirp->return_reql);
		}
		assert(mirp->return_reql->flags & REQL_FL_NOTIFIED);
		assert(!request_list_empty(mirp->return_reql));
		for (;;) {
			reqp = request_list_first(mirp->return_reql);
			if (reqp == NULL)
				break;
			request_list_remove(mirp->return_reql, reqp);
			request_list_append(mirp->aux_reql, reqp);
			mirp->aux_reql->submit_bytes += reqp->nbd.request.len;
			assert(mirp->aux_reql->submit_bytes > (int64_t)0);
		}
		request_list_unlock(mirp->return_reql);
		error = 0;
		nreqp = request_list_first(mirp->aux_reql);
		for (;;) {
			reqp = nreqp;
			if (reqp == NULL)
				break;
			nreqp = request_list_next(mirp->aux_reql, reqp);
			switch (reqp->type) {
			case REQ_TY_MASTER_COPY:
				if (reqp->nbd.reply.error == (uint32_t)0) {
					if (cancelled || failed || error)
						break;
					reqp->fd = mirp->fd;
					reqp->type = REQ_TY_SLAVE_PASTE;
					continue;
				}
				error = 1;
				ERR("MIRROR SYNC READ ERROR: "
				    "target=%s volume=%s offset=%llu length=%u",
				    mirp->target,
				    mirp->volp->id,
				    (unsigned long long)reqp->nbd.request.from,
				    (unsigned int)reqp->nbd.request.len);
				break;
			case REQ_TY_SLAVE_PASTE:
				if (reqp->nbd.reply.error == (uint32_t)0) {
					wr_count += mirp->sync_request_size;
					progress = (wr_count * (uint32_t)100) /
					    mirp->volp->size;
					if (progress > old_progress) {
						old_progress = progress;
						mirror_lock(mirp);
						mirp->sync_progress = progress;
						mirror_unlock(mirp);
					}
					break;
				}
				error = 1;
				ERR("MIRROR SYNC WRITE ERROR: "
				    "target=%s volume=%s offset=%llu length=%u",
				    mirp->target,
				    mirp->volp->id,
				    (unsigned long long)reqp->nbd.request.from,
				    (unsigned int)reqp->nbd.request.len);
				break;
			default:
				abort();
			}
			mirp->aux_reql->submit_bytes -= reqp->nbd.request.len;
			assert(mirp->aux_reql->submit_bytes >= (int64_t)0);
			request_list_remove(mirp->aux_reql, reqp);
			inflight--;
			assert(inflight >= 0 &&
			    inflight < mirp->sync_queue_length);
			token_release(reqp->volp->range_tokt, reqp->tok);
			request_free(reqp);
		}
		if (!failed && error) {
			mirror_lock(mirp);
			if (mirp->state != MIR_ST_SHUTDOWN) {
				assert(mirp->state == MIR_ST_UP);
				mirp->flags &= ~MIR_FL_COMPLETE;
				mirp->state = MIR_ST_SHUTDOWN;
				NOTICE("MIRROR SHUTDOWN: target=%s volume=%s",
				    mirp->target, mirp->volp->id);
			}
			mirror_unlock(mirp);
			failed = 1;
		}
	}
	assert(request_list_empty(mirp->aux_reql));
	assert(request_list_empty(mirp->return_reql));
	if (!cancelled && !failed) {
		mirror_lock(mirp);
		if (mirp->state == MIR_ST_UP &&
		    !(mirp->flags & MIR_FL_COMPLETE)) {
			mirp->flags |= MIR_FL_COMPLETE;
			NOTICE("MIRROR COMPLETE: target=%s volume=%s",
			    mirp->target, mirp->volp->id);
		}
		mirror_unlock(mirp);
	}
}

const char *
mirror_strstate(struct mirror *mirp)
{
	const char *state;

	switch (mirp->state) {
	case MIR_ST_UP:
		state = "up";
		break;
	case MIR_ST_DOWN:
		state = "down";
		break;
	case MIR_ST_SHUTDOWN:
		state = "shutdown";
		break;
	default:
		state = "undefined";
		break;
	}
	return (state);
}

int
mirror_setup(struct mirror **mirpp, const char *target, struct volume *volp)
{
	struct mirror *mirp;
#ifndef NDEBUG
	int len;
#endif

	mirp = NULL;
	if (mirror_alloc(&mirp) < 0) {
		assert(mirp == NULL);
		return (-1);
	}
	assert(mirp != NULL);
	mirp->volp = volp;
#ifndef NDEBUG
	len =
#endif
	    snprintf(mirp->target, sizeof(mirp->target), "%s", target);
	assert(len > 0 && len < (int)sizeof(mirp->target));
	*mirpp = mirp;
	return (0);
}

int
mirror_create(struct volume *volp, const char *target)
{
	struct mirror *mirp;
	struct mirror_list *mirlp;

	mirp = NULL;
	if (mirror_setup(&mirp, target, volp) < 0) {
		assert(mirp == NULL);
		return (-1);
	}
	assert(mirp != NULL);

	if (mirror_open(mirp) < 0) {
		mirror_free(mirp);
		return (-2);
	}

	mirlp = volp->slave_mirl;
	mirror_list_lock(mirlp);
	assert(mirror_list_lookup_by_target(mirlp, mirp->target) == NULL);
	mirror_list_append(mirlp, mirp);
	mirror_list_unlock(mirlp);
	return (0);
}

void
mirror_delete(struct mirror *mirp)
{

	mirror_close(mirp);
	mirror_free(mirp);
}

static
int
mirror_open(struct mirror *mirp)
{
	struct stat st;
	int flags;

	assert(mirp->state < 0);
	assert(mirp->fd < 0);
	assert(!mirp->flags2);
	assert(mirp->volp != NULL);
	assert(mirp->inflight == 0);
	assert(mirp->target[0] != '\0');

	flags = O_RDWR | O_DIRECT;
#ifdef __linux__
	flags |= O_NOATIME;
#endif
	if (cfg_sync == 1)
		flags |= O_SYNC;
	mirp->fd = fd_open(mirp->target, flags, 0);
	if (mirp->fd < 0)
		goto fail;
	memset(&st, 0x00, sizeof(st));
	if (fstat(mirp->fd, &st) < 0) {
		ERR("stat(%s): %s", mirp->target, strerror(errno));
		goto fail;
	}
	if (!S_ISREG(st.st_mode) && !S_ISBLK(st.st_mode)) {
		ERR("MIRROR TARGET MUST BE A REGULAR FILE OR A BLOCK DEVICE");
		goto fail;
	}
	if (S_ISBLK(st.st_mode))
		mirp->flags2 |= MIR_FL2_ISBLK;
	if (st.st_dev == mirp->volp->st_dev)
		mirp->flags2 |= MIR_FL2_MASTER_DEV;
	if (mirp->flags2 & MIR_FL2_ISBLK) {
		mirp->size = (uint64_t)0;
		if (fd_blkdevmediasize(mirp->fd, &mirp->size) < 0) {
			assert(mirp->size == (uint64_t)0);
			ERR("MIRROR TARGET SIZE UNKNOWN");
			goto fail;
		}
	} else {
		mirp->size = (uint64_t)st.st_size;
	}
	if (mirp->size != mirp->volp->size) {
		ERR("MIRROR TARGET SIZE %llu DOES NOT MATCH VOLUME SIZE %llu",
		    (unsigned long long)mirp->size,
		    (unsigned long long)mirp->volp->size);
		goto fail;
	}
	if (mirp->flags2 & MIR_FL2_ISBLK) {
		mirp->blk_size = (uint32_t)0;
		if (fd_blkdevsectorsize(mirp->fd, &mirp->blk_size) < 0) {
			assert(mirp->blk_size == (uint32_t)0);
			ERR("MIRROR TARGET BLOCK UNKNOWN");
			goto fail;
		}
	} else {
		mirp->blk_size = (uint32_t)st.st_blksize;
	}
	if (mirp->blk_size < (uint32_t)512 ||
	    mirp->blk_size > mirp->volp->blk_size ||
	    !powerof2(mirp->blk_size)) {
		ERR("MIRROR TARGET BLOCK %u DOES NOT MATCH VOLUME BLOCK %u",
		    (unsigned int)mirp->blk_size,
		    (unsigned int)mirp->volp->blk_size);
		goto fail;
	}
	mirp->state = MIR_ST_DOWN;
	NOTICE("MIRROR CREATE: "
	    "target=%s volume=%s fd=%d size=%llu usage=%llu block=%u",
	    mirp->target,
	    mirp->volp->id,
	    mirp->fd,
	    (unsigned long long)mirp->size,
	    (unsigned long long)(st.st_blocks << 9),
	    (unsigned int)mirp->blk_size);
	return (0);
fail:
	if (mirp->fd >= 0) {
		fd_close(mirp->fd);
		mirp->fd = -1;
		mirp->flags2 = 0;
	}
	return (-1);
}

static
void
mirror_close(struct mirror *mirp)
{

	mirp->flags &= ~MIR_FL_COMPLETE;
	fd_close(mirp->fd);
	mirp->fd = -1;
	mirp->flags2 = 0;
	assert(mirp->state == MIR_ST_DOWN);
	mirp->state = -1;
}

int
mirror_up(struct mirror *mirp)
{

	switch (mirp->state) {
	case MIR_ST_UP:
		return (0);
	case MIR_ST_DOWN:
		break;
	case MIR_ST_SHUTDOWN:
		return (-1);
	default:
		abort();
	}
	assert(!(mirp->flags & MIR_FL_COMPLETE));
	mirp->state = MIR_ST_UP;
	NOTICE("MIRROR UP: target=%s volume=%s", mirp->target, mirp->volp->id);
	return (1);
}

int
mirror_shutdown_begin(struct mirror *mirp)
{
	if (mirp->state == MIR_ST_DOWN)
		return (0);
	if (mirp->state == MIR_ST_UP) {
		mirp->flags &= ~MIR_FL_COMPLETE;
		mirp->state = MIR_ST_SHUTDOWN;
		NOTICE("MIRROR SHUTDOWN: target=%s volume=%s",
		    mirp->target, mirp->volp->id);
	}
	assert(mirp->state == MIR_ST_SHUTDOWN);
	return (mirror_shutdown_commit(mirp));
}

int
mirror_shutdown_commit(struct mirror *mirp)
{

	if (mirp->state != MIR_ST_SHUTDOWN ||
	    mirp->flags & MIR_FL_SYNCHRONISING || mirp->inflight != 0)
		return (-1);
	mirp->state = MIR_ST_DOWN;
	NOTICE("MIRROR DOWN: target=%s volume=%s",
	    mirp->target, mirp->volp->id);
	return (0);
}

int
mirror_list_alloc(struct mirror_list **mirlpp, int index)
{
	struct mirror_list *mirlp;
	int error;

	mirlp = malloc(sizeof(*mirlp));
	if (mirlp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*mirlp), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&mirlp->mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(mirlp);
		return (-3);
	}
	mirror_list_init(mirlp, index);
	*mirlpp = mirlp;
	return (0);
}

void
mirror_list_init(struct mirror_list *mirlp, int index)
{
#ifndef NDEBUG
	struct mirror *mirp;
#endif

	assert(index >= 0 && index < NELEM(mirp->all_mirl_ary));
	TAILQ_INIT(&mirlp->head);
	mirlp->index = index;
	mirlp->count = 0;
	mirlp->peak = 0;
}

void
mirror_list_uninit(struct mirror_list *mirlp)
{

	assert(TAILQ_EMPTY(&mirlp->head));
	assert(mirlp->count == 0);
	mirlp->peak = 0;
}

void
mirror_list_free(struct mirror_list *mirlp)
{

	mirror_list_uninit(mirlp);
	PTHREAD_MUTEX_DESTROY(&mirlp->mutex);
	free(mirlp);
}

static
void
mirror_list_dealloc(struct mirror_list *mirlp)
{
	struct mirror *mirp, *nmirp;

	nmirp = mirror_list_first(mirlp);
	for (;;) {
		mirp = nmirp;
		if (mirp == NULL)
			break;
		nmirp = mirror_list_next(mirlp, mirp);
		mirror_list_remove(mirlp, mirp);
		mirror_dealloc(mirp);
	}
}

void
mirror_list_lock(struct mirror_list *mirlp)
{

	PTHREAD_MUTEX_LOCK(&mirlp->mutex);
}

void
mirror_list_unlock(struct mirror_list *mirlp)
{

	PTHREAD_MUTEX_UNLOCK(&mirlp->mutex);
}

int
mirror_list_empty(struct mirror_list *mirlp)
{

	return (TAILQ_EMPTY(&mirlp->head));
}

struct mirror *
mirror_list_first(struct mirror_list *mirlp)
{

	return (TAILQ_FIRST(&mirlp->head));
}

struct mirror *
mirror_list_next(struct mirror_list *mirlp, struct mirror *mirp)
{

	return (TAILQ_NEXT(mirp, all_mirl_ary[mirlp->index]));
}

void
mirror_list_insert(struct mirror_list *mirlp, struct mirror *mirp)
{

	TAILQ_INSERT_HEAD(&mirlp->head, mirp, all_mirl_ary[mirlp->index]);
	mirlp->count++;
	assert(mirlp->count > 0);
	if (mirlp->count > mirlp->peak)
		mirlp->peak = mirlp->count;
}

void
mirror_list_append(struct mirror_list *mirlp, struct mirror *mirp)
{

	TAILQ_INSERT_TAIL(&mirlp->head, mirp, all_mirl_ary[mirlp->index]);
	mirlp->count++;
	assert(mirlp->count > 0);
	if (mirlp->count > mirlp->peak)
		mirlp->peak = mirlp->count;
}

void
mirror_list_remove(struct mirror_list *mirlp, struct mirror *mirp)
{

	TAILQ_REMOVE(&mirlp->head, mirp, all_mirl_ary[mirlp->index]);
	mirlp->count--;
	assert(mirlp->count >= 0);
}

struct mirror *
mirror_list_lookup_by_target(struct mirror_list *mirlp, const char *target)
{
	struct mirror *mirp;

	TAILQ_FOREACH(mirp, &mirlp->head, all_mirl_ary[mirlp->index]) {
		if (strcmp(target, mirp->target))
			continue;
		return (mirp);
	}
	return (NULL);
}

void
mirror_list_shutdown(struct mirror_list *mirlp)
{
	struct mirror *mirp;
	int join_thread;

	for (;;) {
		mirror_list_lock(mirlp);
		mirp = mirror_list_first(mirlp);
		if (mirp == NULL) {
			mirror_list_unlock(mirlp);
			break;
		}
		mirror_list_remove(mirlp, mirp);
		mirror_list_unlock(mirlp);
		mirror_lock(mirp);
		mirror_shutdown_begin(mirp);
		if (mirp->flags & MIR_FL_SYNCHRONISING) {
			thread_shutdown_begin(mirp->thr);
			join_thread = 1;
		} else {
			join_thread = 0;
		}
		mirror_unlock(mirp);
		if (join_thread)
			thread_join(mirp->thr);
		mirror_lock(mirp);
		mirror_close(mirp);
		mirror_unlock(mirp);
		mirror_free(mirp);
	}
}
