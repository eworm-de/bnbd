/*-
 * Copyright (c) 2013-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#define BM_OP_SET		1
#define BM_OP_CHECK		2
#define BM_OP_CLEAR		3

struct blkmap {
#define BM_FL_OPEN		0x00000001
#define BM_FL_DIRTY		0x00000002
#define BM_FL_BROKEN		0x00000004
	int			flags;
	int			fd;
	pthread_mutex_t		mutex;
	void			*data;
	size_t			size;
	uint32_t		blk_power;
	uint32_t		blk_size;
	uint64_t		vol_size;
	char			vol_id[64];
};

int blkmap_alloc(struct blkmap **);
void blkmap_free(struct blkmap *);
void blkmap_lock(struct blkmap *);
void blkmap_unlock(struct blkmap *);
int blkmap_setup(struct blkmap **, uint64_t, const char *);
int blkmap_create(int, const char *, uint64_t, const char *);
int blkmap_delete(int, const char *);
int blkmap_open(struct blkmap *, const char *);
void blkmap_close(struct blkmap *);
int blkmap_exec(struct blkmap *, uint64_t, uint32_t, int);
int blkmap_sync(struct blkmap *);
