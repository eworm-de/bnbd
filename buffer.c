/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "thread.h"
#include "buffer.h"

static int buffer_module_initialized = 0;

struct buffer_pool *read_bp = NULL;
struct buffer_pool *write_bp = NULL;
struct buffer_pool *mirror_bp = NULL;

static int buffer_pool_alloc(struct buffer_pool **);
static void buffer_pool_free(struct buffer_pool *);

int
buffer_module_setup(void)
{

	assert(!buffer_module_initialized);
	assert(read_bp == NULL);
	if (buffer_pool_alloc(&read_bp) < 0)
		goto out0;
	read_bp->bp_bytes_limit = (int64_t)cfg_max_read_requests_kb << 10;
	read_bp->bp_warn_key = "max_read_requests_kb";
	read_bp->bp_warn_val = cfg_max_read_requests_kb;
	assert(write_bp == NULL);
	if (buffer_pool_alloc(&write_bp) < 0)
		goto out1;
	write_bp->bp_bytes_limit = (int64_t)cfg_max_write_requests_kb << 10;
	write_bp->bp_warn_key = "max_write_requests_kb";
	write_bp->bp_warn_val = cfg_max_write_requests_kb;
	assert(mirror_bp == NULL);
	if (buffer_pool_alloc(&mirror_bp) < 0)
		goto out2;
	mirror_bp->bp_bytes_limit = (int64_t)cfg_max_mirror_requests_kb << 10;
	mirror_bp->bp_warn_key = "max_mirror_requests_kb";
	mirror_bp->bp_warn_val = cfg_max_mirror_requests_kb;
	buffer_module_initialized = 1;
	return (0);
out2:
	assert(write_bp != NULL);
	buffer_pool_free(write_bp);
	write_bp = NULL;
out1:
	assert(read_bp != NULL);
	buffer_pool_free(read_bp);
	read_bp = NULL;
out0:
	return (-1);
}

void
buffer_module_teardown(void)
{

	assert(buffer_module_initialized);
	assert(mirror_bp != NULL);
	buffer_pool_free(mirror_bp);
	mirror_bp = NULL;
	assert(write_bp != NULL);
	buffer_pool_free(write_bp);
	write_bp = NULL;
	assert(read_bp != NULL);
	buffer_pool_free(read_bp);
	read_bp = NULL;
}

void
buffer_module_hello(void)
{

	assert(buffer_module_initialized);
	DEBUG("sizeof(buffer)=%zu", sizeof(struct buffer));
	DEBUG("sizeof(buffer_pool)=%zu", sizeof(struct buffer_pool));
}

void
buffer_module_stats(void)
{
	long long read_bytes_count, write_bytes_count, mirror_bytes_count;

	assert(buffer_module_initialized);
	PTHREAD_MUTEX_LOCK(&read_bp->bp_mutex);
	read_bytes_count = (long long)read_bp->bp_bytes_count;
	PTHREAD_MUTEX_UNLOCK(&read_bp->bp_mutex);
	PTHREAD_MUTEX_LOCK(&write_bp->bp_mutex);
	write_bytes_count = (long long)write_bp->bp_bytes_count;
	PTHREAD_MUTEX_UNLOCK(&write_bp->bp_mutex);
	PTHREAD_MUTEX_LOCK(&mirror_bp->bp_mutex);
	mirror_bytes_count = (long long)mirror_bp->bp_bytes_count;
	PTHREAD_MUTEX_UNLOCK(&mirror_bp->bp_mutex);
	NOTICE("read=%lld/%lld write=%lld/%lld mirror=%lld/%lld",
	    read_bytes_count, (long long)read_bp->bp_bytes_limit,
	    write_bytes_count, (long long)write_bp->bp_bytes_limit,
	    mirror_bytes_count, (long long)mirror_bp->bp_bytes_limit);
}

int
buffer_alloc(struct thread *thrp, struct buffer *buf,
    struct buffer_pool *pool, uint32_t alignment, uint32_t size, int waitok)
{
	time_t now;
	int error, woken;

	assert(alignment > (uint32_t)0);
	assert(size > (uint32_t)0);
	assert(buf->buf_bpp == NULL);
	assert(buf->buf_ptr == NULL);
	assert(buf->buf_idx == (uint32_t)0);
	assert(buf->buf_len == (uint32_t)0);
	if (pool->bp_bytes_limit > (int64_t)0 &&
	    (int64_t)size > pool->bp_bytes_limit)
		return (-1);
	woken = 0;
	PTHREAD_MUTEX_LOCK(&pool->bp_mutex);
	if (!thread_head_empty(&pool->bp_wait_thrh)) {
		if (!waitok) {
			PTHREAD_MUTEX_UNLOCK(&pool->bp_mutex);
			return (-1);
		}
block:
		assert(!woken);
		thrp->alloc_len = size;
		thread_head_append(&pool->bp_wait_thrh, thrp, THRL_IDX_STD);
		assert(!(thrp->flags2 & THR_FL2_NOTIFIED));
		thread_wait_mutex(thrp, &pool->bp_mutex);
		assert(thrp->flags2 & THR_FL2_NOTIFIED);
		thrp->flags2 &= ~THR_FL2_NOTIFIED;
		woken = 1;
	}
	if (!woken &&
	    pool->bp_bytes_limit > (int64_t)0 &&
	    pool->bp_bytes_count + size > pool->bp_bytes_limit) {
		if (!waitok) {
			PTHREAD_MUTEX_UNLOCK(&pool->bp_mutex);
			return (-1);
		}
		now = time(NULL);
		if (pool->bp_warn_time < now) {
			pool->bp_warn_time = now;
			NOTICE("%s=%d limit hit!",
			    pool->bp_warn_key, pool->bp_warn_val);
		}
		goto block;
	}
	if (!woken) {
		pool->bp_bytes_count += size;
		assert(pool->bp_bytes_count > (int64_t)0);
	}
	PTHREAD_MUTEX_UNLOCK(&pool->bp_mutex);
	buf->buf_bpp = pool;
	buf->buf_len = size;
	error = posix_memalign((void **)&buf->buf_ptr, (size_t)alignment,
	    (size_t)size);
	if (error) {
		assert(error > 0);
		assert(buf->buf_ptr == NULL);
		ERR("posix_memalign(alignment=%u, size=%u, %s=%d): %s",
		    (unsigned int)alignment, (unsigned int)size,
		    pool->bp_warn_key, pool->bp_warn_val, strerror(error));
		if (!waitok) {
			buffer_free(buf);
			return (-2);
		}
		PTHREAD_MUTEX_LOCK(&pool->bp_mutex);
		pool->bp_bytes_count -= size;
		assert(pool->bp_bytes_count >= (int64_t)0);
		if (pool->bp_bytes_count >= (int64_t)size) {
			woken = 0;
			goto block;
		}
		pool->bp_bytes_count += size;
		assert(pool->bp_bytes_count > (int64_t)0);
		PTHREAD_MUTEX_UNLOCK(&pool->bp_mutex);
		buffer_free(buf);
		return (-2);
	}
	assert(buf->buf_ptr != NULL);
	return (0);
}

void
buffer_free(struct buffer *buf)
{
	struct buffer_pool *pool;
	struct thread *nthrp, *thrp;
	struct thread_head wakeup_thrh;

	pool = (struct buffer_pool *)buf->buf_bpp;
	assert(pool != NULL);
	buf->buf_bpp = NULL;
	if (buf->buf_ptr != NULL) {
		assert(buf->buf_len > (uint32_t)0);
		free(buf->buf_ptr);
		buf->buf_ptr = NULL;
	}
	assert(buf->buf_len > (uint32_t)0);
	thread_head_init(&wakeup_thrh);
	PTHREAD_MUTEX_LOCK(&pool->bp_mutex);
	pool->bp_bytes_count -= buf->buf_len;
	assert(pool->bp_bytes_count >= (int64_t)0);
	nthrp = thread_head_first(&pool->bp_wait_thrh);
	for (;;) {
		thrp = nthrp;
		if (thrp == NULL)
			break;
		if (pool->bp_bytes_count + (int64_t)thrp->alloc_len >
		    pool->bp_bytes_limit)
			break;
		pool->bp_bytes_count += thrp->alloc_len;
		assert(pool->bp_bytes_count > (int64_t)0);
		nthrp = thread_head_next(thrp, THRL_IDX_STD);
		thread_head_remove(&pool->bp_wait_thrh, thrp, THRL_IDX_STD);
		thread_head_append(&wakeup_thrh, thrp, THRL_IDX_STD);
		assert(!(thrp->flags2 & THR_FL2_NOTIFIED));
		thrp->flags2 |= THR_FL2_NOTIFIED;
	}
	PTHREAD_MUTEX_UNLOCK(&pool->bp_mutex);
	nthrp = thread_head_first(&wakeup_thrh);
	for (;;) {
		thrp = nthrp;
		if (thrp == NULL)
			break;
		nthrp = thread_head_next(thrp, THRL_IDX_STD);
		thread_head_remove(&wakeup_thrh, thrp, THRL_IDX_STD);
		thread_wakeup(thrp);
	}
}

static
int
buffer_pool_alloc(struct buffer_pool **poolp)
{
	struct buffer_pool *pool;
	int error;

	pool = malloc(sizeof(*pool));
	if (pool == NULL) {
		ERR("malloc(%zu): %s", sizeof(*pool), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&pool->bp_mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(pool);
		return (-3);
	}
	thread_head_init(&pool->bp_wait_thrh);
	pool->bp_bytes_limit = (int64_t)0;
	pool->bp_bytes_count = (int64_t)0;
	pool->bp_warn_time = (time_t)0;
	pool->bp_warn_val = 0;
	pool->bp_warn_key = NULL;
	*poolp = pool;
	return (0);
}

static
void
buffer_pool_free(struct buffer_pool *pool)
{

	assert(pool->bp_bytes_count == (int64_t)0);
	thread_head_uninit(&pool->bp_wait_thrh);
	PTHREAD_MUTEX_DESTROY(&pool->bp_mutex);
	free(pool);
}
