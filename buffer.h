/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

struct buffer {
	void			*buf_bpp;
	char			*buf_ptr;
	uint32_t		buf_idx;
	uint32_t		buf_len;
};

struct buffer_pool {
	pthread_mutex_t		bp_mutex;
	struct thread_head	bp_wait_thrh;
	int64_t			bp_bytes_limit;
	int64_t			bp_bytes_count;
	time_t			bp_warn_time;
	const char		*bp_warn_key;
	int			bp_warn_val;
};

extern struct buffer_pool *read_bp;
extern struct buffer_pool *write_bp;
extern struct buffer_pool *mirror_bp;

int buffer_module_setup(void);
void buffer_module_hello(void);
void buffer_module_teardown(void);
void buffer_module_stats(void);
int buffer_alloc(struct thread *, struct buffer *, struct buffer_pool *,
    uint32_t, uint32_t, int);
void buffer_free(struct buffer *);
