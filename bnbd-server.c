/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "syslog.h"
#include "io.h"
#include "acceptor.h"

static void usage(const char *);

int
main(int argc, char **argv)
{
	char pid_file[16];
	ssize_t n;
	int ch, error, pid_fd, len;
	pid_t pid;

	while ((ch = getopt(argc, argv, "dC:h")) != -1) {
		switch (ch) {
		case 'd':
			opt_debug++;
			break;
		case 'C':
			opt_config = optarg;
			break;
		/* case 'h': */
		default:
			usage(argv[0]);
			/* NOTREACHED */
		}
	}

	config_module_setup();
	syslog_module_setup();

	config_load();

	/* set umask */
	umask(022);

	/* change working directory */
	if (chdir(cfg_work_directory)) {
		fprintf(stderr, "error: chdir(%s): %s\n",
		    cfg_work_directory, strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* daemonize */
	if (!opt_debug && daemon(1, 0)) {
		fprintf(stderr, "error: daemon(): %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	config_dump();

	/* create pid file */
	pid = getpid();
	pid_fd = fd_open(cfg_pid_file, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (pid_fd < 0)
		exit(EXIT_FAILURE);
	len = snprintf(pid_file, sizeof(pid_file), "%d\n", (int)pid);
	assert(len > 0 && len < (int)sizeof(pid_file));
	n = buf_tx(pid_fd, pid_file, (size_t)len, 1);
	if (n < (ssize_t)0)
		exit(EXIT_FAILURE);
	assert(n == (ssize_t)len);
	error = fsync(pid_fd);
	if (error < 0) {
		ERR("fsync(%s): %s", pid_file, strerror(errno));
		exit(EXIT_FAILURE);
	}
	if (fd_close(pid_fd) < 0)
		exit(EXIT_FAILURE);

	if (acceptor_module_setup() < 0)
		exit(EXIT_FAILURE);
	if (acceptor_thread_loop() < 0)
		exit(EXIT_FAILURE);
	acceptor_module_teardown();

	config_module_teardown();
	syslog_module_teardown();
	return (0);
}

static
void
usage(const char *argv0)
{

	fprintf(stderr, "Usage: %s [-dh] [-C config]\n", argv0);
	exit(EXIT_FAILURE);
}
