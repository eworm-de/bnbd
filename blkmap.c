/*-
 * Copyright (c) 2013-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include <sys/mman.h>

#include "io.h"
#include "blkmap.h"

static int blkmap_set(uint32_t *, uint32_t *, uint32_t, uint32_t);
static int blkmap_check(uint32_t *, uint32_t *, uint32_t, uint32_t);
static int blkmap_clear(uint32_t *, uint32_t *, uint32_t, uint32_t);

struct blkmap_header {
#define MAGIC			(uint32_t)0x424e4244
	uint32_t		magic;
#define VERSION			(uint32_t)1
	uint32_t		version;
#define BLK_POWER		(uint32_t)17
	uint32_t		blk_power;
#define BLK_SIZE		(uint32_t)(1 << BLK_POWER)
	uint32_t		blk_size;
	uint32_t		pad1[28];
	uint64_t		vol_size;
	uint32_t		pad2[14];
	char			vol_id[64];
	uint32_t		pad3[960];
} __attribute__((packed));

int
blkmap_alloc(struct blkmap **bmpp)
{
	struct blkmap *bmp;
	int error;

	bmp = malloc(sizeof(*bmp));
	if (bmp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*bmp), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&bmp->mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(bmp);
		return (-3);
	}
	bmp->flags = 0;
	bmp->fd = -1;
	bmp->data = NULL;
	bmp->blk_power = (uint32_t)0;
	bmp->blk_size = (uint32_t)0;
	bmp->vol_size = (uint64_t)0;
	memset(bmp->vol_id, 0x00, sizeof(bmp->vol_id));
	*bmpp = bmp;
	return (0);
}

void
blkmap_free(struct blkmap *bmp)
{

	assert(!bmp->flags);
	assert(bmp->fd < 0);
	assert(bmp->data == NULL);
	PTHREAD_MUTEX_DESTROY(&bmp->mutex);
	free(bmp);
}

void
blkmap_lock(struct blkmap *bmp)
{

	PTHREAD_MUTEX_LOCK(&bmp->mutex);
}

void
blkmap_unlock(struct blkmap *bmp)
{

	PTHREAD_MUTEX_UNLOCK(&bmp->mutex);
}

int
blkmap_setup(struct blkmap **bmpp, uint64_t vol_size, const char *vol_id)
{
	struct blkmap *bmp;
	int len;

	bmp = NULL;
	if (blkmap_alloc(&bmp) < 0) {
		assert(bmp == NULL);
		return (-1);
	}
	bmp->vol_size = vol_size;
	len = snprintf(bmp->vol_id, sizeof(bmp->vol_id), "%s", vol_id);
	if (len <= 0 || len >= (int)sizeof(bmp->vol_id)) {
		blkmap_free(bmp);
		return (-2);
	}
	*bmpp = bmp;
	return (0);
}

int
blkmap_create(int vdatadirfd, const char *name, uint64_t vol_size,
    const char *vol_id)
{
	size_t size;
	ssize_t n;
	int fd, len;
	struct blkmap_header *bmhp;
	char *data;

	fd = fd_openat(vdatadirfd, name, O_RDWR | O_CREAT | O_EXCL, 0640);
	if (fd < 0)
		return (-1);

	assert(sizeof(*bmhp) == (size_t)4096);
	size = (size_t)0;
	size += 4096;					/* blkmap_header */
	size += (vol_size >> (BLK_POWER + 3));		/* blkmap itself */
	size += (4096 - 1);
	size &= ~((size_t)4096 - 1);
	assert(size > (size_t)4096);
	data = calloc((size_t)1, size);
	if (data == NULL) {
		ERR("calloc(%zu): %s", size, strerror(errno));
		goto fail;
	}
	bmhp = (struct blkmap_header *)data;
	bmhp->magic = htonl(MAGIC);
	bmhp->version = htonl(VERSION);
	bmhp->blk_power = htonl(BLK_POWER);
	bmhp->blk_size = htonl(BLK_SIZE);
	bmhp->vol_size = htonll((uint64_t)vol_size);
	len = snprintf(bmhp->vol_id, sizeof(bmhp->vol_id), "%s", vol_id);
	if (len <= 0 || len >= (int)sizeof(bmhp->vol_id)) {
		free(data);
		goto fail;
	}
	n = buf_tx(fd, data, size, 1);
	free(data);
	if (n < (ssize_t)0)
		goto fail;
	assert(n == (ssize_t)size);
	if (fsync(fd) < 0) {
		ERR("fsync(%s): %s", name, strerror(errno));
		goto fail;
	}
	fd_close(fd);
	return (0);
fail:
	fd_close(fd);
	blkmap_delete(vdatadirfd, name);
	return (-1);
}

int
blkmap_delete(int vdatadirfd, const char *name)
{

	if (unlinkat(vdatadirfd, name, 0) < 0 && errno != ENOENT) {
		ERR("unlinkat(%d/%s): %s",
		    vdatadirfd, name, strerror(errno));
		return (-1);
	}
	return (0);
}

int
blkmap_open(struct blkmap *bmp, const char *path)
{
	struct stat st;
	struct blkmap_header *bmhp;
	int flags;

	assert(!bmp->flags);
	assert(bmp->fd < 0);
	assert(bmp->data == NULL);
	assert(bmp->vol_size > (uint64_t)0);
	assert(bmp->vol_id[0] != '\0');

	flags = O_RDWR;
#ifdef __linux__
	flags |= O_NOATIME;
#endif
	bmp->fd = fd_open(path, flags, 0);
	if (bmp->fd < 0)
		goto fail;

	assert(sizeof(*bmhp) == (size_t)4096);
	bmp->size = (size_t)0;
	bmp->size += 4096;
	bmp->size += (bmp->vol_size >> (BLK_POWER + 3));
	bmp->size += (4096 - 1);
	bmp->size &= ~((size_t)4096 - 1);
	assert(bmp->size > (size_t)4096);
	memset(&st, 0x00, sizeof(st));
	if (fstat(bmp->fd, &st) < 0) {
		ERR("stat(%s): %s", path, strerror(errno));
		goto fail;
	}
	if (st.st_size != (off_t)bmp->size) {
		ERR("INVALID BLKMAP SIZE");
		goto fail;
	}
	bmp->data = mmap(NULL, bmp->size, PROT_READ | PROT_WRITE, MAP_SHARED,
	    bmp->fd, (off_t)0);
	if (bmp->data == MAP_FAILED) {
		ERR("mmap(%s, %zu): %s", path, bmp->size, strerror(errno));
		bmp->data = NULL;
		goto fail;
	}
	bmhp = (struct blkmap_header *)bmp->data;
	if (ntohl(bmhp->magic) != MAGIC) {
		ERR("INVALID BLKMAP MAGIC=0x%X", (int)ntohl(bmhp->magic));
		goto fail;
	}
	if (ntohl(bmhp->version) != 1) {
		ERR("INVALID BLKMAP VERSION=%u",
		    (unsigned int)ntohl(bmhp->version));
		goto fail;
	}
	if (ntohl(bmhp->blk_power) != BLK_POWER) {
		ERR("INVALID BLKMAP BLOCK POWER=%u",
		    (unsigned int)ntohl(bmhp->blk_power));
		goto fail;
	}
	if (ntohl(bmhp->blk_size) != BLK_SIZE) {
		ERR("INVALID BLKMAP BLOCK SIZE=%u",
		    (unsigned int)ntohl(bmhp->blk_size));
		goto fail;
	}
	if (ntohll(bmhp->vol_size) != bmp->vol_size) {
		ERR("INVALID BLKMAP VOLUME SIZE=%llu",
		    (unsigned long long)ntohll(bmhp->vol_size));
		goto fail;
	}
	assert(sizeof(bmhp->vol_id) == sizeof(bmp->vol_id));
	if (memcmp(bmhp->vol_id, bmp->vol_id, sizeof(bmhp->vol_id))) {
		ERR("INVALID BLKMAP VOLUME ID=%.*s",
		    (int)sizeof(bmhp->vol_id), bmhp->vol_id);
		goto fail;
	}
	bmp->blk_power = ntohl(bmhp->blk_power);
	bmp->blk_size = ntohl(bmhp->blk_size);
	bmp->flags |= BM_FL_OPEN;
	return (0);
fail:
	if (bmp->data != NULL) {
		if (munmap(bmp->data, bmp->size) < 0)
			ERR("munmap(%s, %zu): %s",
			    bmp->vol_id, bmp->size, strerror(errno));
		bmp->data = NULL;
	}
	if (bmp->fd >= 0) {
		fd_close(bmp->fd);
		bmp->fd = -1;
	}
	return (-1);
}

void
blkmap_close(struct blkmap *bmp)
{

	assert(bmp->flags & BM_FL_OPEN);
	assert(bmp->fd >= 0);
	assert(bmp->data != NULL);
	if (munmap(bmp->data, bmp->size) < 0)
		ERR("munmap(%s, %zu): %s",
		    bmp->vol_id, bmp->size, strerror(errno));
	bmp->data = NULL;
	fd_close(bmp->fd);
	bmp->fd = -1;
	bmp->flags &= ~(BM_FL_DIRTY | BM_FL_BROKEN);
	bmp->flags &= ~BM_FL_OPEN;
}

int
blkmap_exec(struct blkmap *bmp, uint64_t offset, uint32_t length, int op)
{
	uint64_t from_blkno, from_dword, to_blkno, to_dword;
	uint32_t *from, *to;
	uint32_t from_bit, from_mask, to_bit, to_mask;
	int retval;

	assert(length > (uint32_t)0);

	/* Calculate the first and the last block numbers */
	switch (op) {
	case BM_OP_SET:
	case BM_OP_CHECK:
		/* Set/check any touched block */
		from_blkno = offset >> BLK_POWER;
		to_blkno = (offset + length - 1) >> BLK_POWER;
		break;
	case BM_OP_CLEAR:
		/* Clear only full blocks */
		if (length < BLK_SIZE)
			return (0);
		from_blkno = (offset + BLK_SIZE - 1) >> BLK_POWER;
		to_blkno = (offset + length - BLK_SIZE) >> BLK_POWER;
		if (from_blkno > to_blkno)
			return (0);
		break;
	default:
		abort();
	}

	/*
	 * Calculate the first bit in the first integer
	 * and the last bit in the last integer:
	 * from_bit = from_blkno % 32;
	 * to_bit = to_blkno % 32;
	 */
	from_bit = (uint32_t)from_blkno & 31;
	to_bit = (uint32_t)to_blkno & 31;

	/* Calculate the bitmask of the first and the last integers */
	from_mask = (uint32_t)0xffffffff >> from_bit;
	to_mask = (uint32_t)0xffffffff << (31 - to_bit);

	/*
	 * Calculate the first and the last 32-bit integers:
	 * from_dword = from_blkno / 32;
	 * to_dword = to_blokno / 32;
	 */
	from_dword = from_blkno >> 5;
	to_dword = to_blkno >> 5;

	/* Skip the blkmap header */
	from = (uint32_t *)((char *)bmp->data + sizeof(struct blkmap_header));
	to = from + to_dword;
	from += from_dword;

	switch (op) {
	case BM_OP_SET:
		retval = blkmap_set(from, to, from_mask, to_mask);
		assert(retval >= 0);
		if (retval)
			bmp->flags |= BM_FL_DIRTY;
		break;
	case BM_OP_CHECK:
		retval = blkmap_check(from, to, from_mask, to_mask);
		break;
	case BM_OP_CLEAR:
		retval = blkmap_clear(from, to, from_mask, to_mask);
		assert(retval >= 0);
		if (retval)
			bmp->flags |= BM_FL_DIRTY;
		break;
	default:
		abort();
	}
	return (retval);
}

static
int
blkmap_set(uint32_t *from, uint32_t *to, uint32_t from_mask, uint32_t to_mask)
{
	uint32_t dword;
	int dirty;

	dirty = 0;
	dword = ntohl(*from);
	if (from == to) {
		if ((dword & (from_mask & to_mask)) !=
		    (from_mask & to_mask)) {
			dword |= (from_mask & to_mask);
			*from = htonl(dword);
			dirty = 1;
		}
		return (dirty);
	}
	if ((dword & from_mask) != from_mask) {
		dword |= from_mask;
		*from = htonl(dword);
		dirty = 1;
	}
	from++;
	while (from < to) {
		if (*from != (uint32_t)0xffffffff) {
			*from = (uint32_t)0xffffffff;
			dirty = 1;
		}
		from++;
	}
	assert(from == to);
	dword = ntohl(*from);
	if ((dword & to_mask) != to_mask) {
		dword |= to_mask;
		*from = htonl(dword);
		dirty = 1;
	}
	return (dirty);
}

static
int
blkmap_check(uint32_t *from, uint32_t *to, uint32_t from_mask, uint32_t to_mask)
{
	uint32_t dword;

	dword = ntohl(*from);
	if (from == to) {
		if ((dword & (from_mask & to_mask)) != (uint32_t)0)
			return (1);
		return (0);
	}
	if ((dword & from_mask) != (uint32_t)0)
		return (1);
	from++;
	while (from < to) {
		if (*from != (uint32_t)0)
			return (1);
		from++;
	}
	assert(from == to);
	dword = ntohl(*from);
	if ((dword & to_mask) != (uint32_t)0)
		return (1);
	return (0);
}

static
int
blkmap_clear(uint32_t *from, uint32_t *to, uint32_t from_mask, uint32_t to_mask)
{
	uint32_t dword;
	int dirty;

	dirty = 0;
	dword = ntohl(*from);
	if (from == to) {
		if ((dword & (from_mask & to_mask)) != (uint32_t)0) {
			dword &= ~(from_mask & to_mask);
			*from = htonl(dword);
			dirty = 1;
		}
		return (dirty);
	}
	if ((dword & from_mask) != (uint32_t)0) {
		dword &= ~from_mask;
		*from = htonl(dword);
		dirty = 1;
	}
	from++;
	while (from < to) {
		if (*from != (uint32_t)0) {
			*from = (uint32_t)0;
			dirty = 1;
		}
		from++;
	}
	assert(from == to);
	dword = ntohl(*from);
	if ((dword & to_mask) != (uint32_t)0) {
		dword &= ~to_mask;
		*from = htonl(dword);
		dirty = 1;
	}
	return (dirty);
}

int
blkmap_sync(struct blkmap *bmp)
{

	if (bmp->flags & BM_FL_BROKEN)
		return (-1);
	if (bmp->flags & BM_FL_DIRTY) {
		if (msync(bmp->data, bmp->size, MS_SYNC) < 0) {
			bmp->flags |= BM_FL_BROKEN;
			ERR("msync(%s, %zu): %s",
			    bmp->vol_id, bmp->size, strerror(errno));
			return (-2);
		}
		bmp->flags &= ~BM_FL_DIRTY;
	}
	return (0);
}
