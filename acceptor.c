/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "io.h"
#include "thread.h"
#include "request.h"
#include "reader.h"
#include "writer.h"
#include "worker.h"
#include "skiver.h"
#include "session.h"
#include "catcher.h"
#include "dispatch_glue.h"
#include "mirror.h"
#include "volume.h"
#include "vty.h"
#include "periodic.h"
#include "acceptor.h"

static int acceptor_module_initialized = 0;
static volatile sig_atomic_t do_exit = (sig_atomic_t)0;

static void sigint(int);
static void sigusr1(int);
static void signals_switch(int);

static
void
sigint(int sig)
{

	(void)sig;
	do_exit = (sig_atomic_t)1;
}

static
void
sigusr1(int sig)
{

	(void)sig;
}

static
void
signals_switch(int on)
{
	sigset_t sigset;
	int how;

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGINT);
	sigaddset(&sigset, SIGTERM);
	if (on)
		how = SIG_UNBLOCK;
	else
		how = SIG_BLOCK;
	PTHREAD_SIGMASK(how, &sigset, NULL);
	DEBUG("SIGINT,SIGTERM: %d", how);
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGUSR1);
	if (on)
		how = SIG_BLOCK;
	else
		how = SIG_UNBLOCK;
	PTHREAD_SIGMASK(how, &sigset, NULL);
	DEBUG("SIGUSR1: %d", how);
}

int
acceptor_module_setup(void)
{
	struct sigaction sa;
	int error;

	assert(!acceptor_module_initialized);
	memset(&sa, 0x00, sizeof(sa));
	sa.sa_handler = SIG_IGN;
	SIGACTION(SIGPIPE, &sa, NULL);
	SIGACTION(SIGHUP, &sa, NULL);
	memset(&sa, 0x00, sizeof(sa));
	sa.sa_handler = sigint;
	SIGACTION(SIGINT, &sa, NULL);
	SIGACTION(SIGTERM, &sa, NULL);
	memset(&sa, 0x00, sizeof(sa));
	sa.sa_handler = sigusr1;
	SIGACTION(SIGUSR1, &sa, NULL);

	if (thread_module_setup() < 0)
		goto out0;
	if (buffer_module_setup() < 0)
		goto out1;
	if (token_module_setup() < 0)
		goto out2;
	if (request_module_setup() < 0)
		goto out3;

	signals_switch(0);
	error = catcher_module_setup();
	signals_switch(1);
	if (error < 0)
		goto out4;

	signals_switch(0);
	error = worker_module_setup();
	signals_switch(1);
	if (error < 0)
		goto out5;

	signals_switch(0);
	error = skiver_module_setup();
	signals_switch(1);
	if (error < 0)
		goto out6;

	if (reader_module_setup() < 0)
		goto out7;
	if (writer_module_setup() < 0)
		goto out8;
	if (dispatch_glue_setup() < 0)
		goto out9;
	if (session_module_setup() < 0)
		goto out10;
	if (mirror_module_setup() < 0)
		goto out11;
	if (volume_module_setup() < 0)
		goto out12;
	if (vty_module_setup() < 0)
		goto out13;

	signals_switch(0);
	error = periodic_module_setup();
	signals_switch(1);
	if (error < 0)
		goto out14;

	thread_module_hello();
	buffer_module_hello();
	token_module_hello();
	request_module_hello();
	catcher_module_hello();
	worker_module_hello();
	skiver_module_hello();
	reader_module_hello();
	writer_module_hello();
	session_module_hello();
	mirror_module_hello();
	volume_module_hello();
	vty_module_hello();
	periodic_module_hello();
	acceptor_module_initialized = 1;
	return (0);
out14:
	vty_module_teardown();
out13:
	volume_module_teardown();
out12:
	mirror_module_teardown();
out11:
	session_module_teardown();
out10:
	dispatch_glue_teardown();
out9:
	writer_module_teardown();
out8:
	reader_module_teardown();
out7:
	skiver_module_teardown();
out6:
	worker_module_teardown();
out5:
	catcher_module_teardown();
out4:
	request_module_teardown();
out3:
	token_module_teardown();
out2:
	buffer_module_teardown();
out1:
	thread_module_teardown();
out0:
	return (-1);
}

void
acceptor_module_teardown(void)
{

	assert(acceptor_module_initialized);
	DEBUG("Starting shutdown sequence...");
	periodic_module_teardown();
	vty_list_shutdown(active_vtyl);
	session_list_shutdown(negotiating_sesl);
	volume_list_shutdown(master_voll);
	vty_module_teardown();
	volume_module_teardown();
	mirror_module_teardown();
	session_module_teardown();
	dispatch_glue_teardown();
	writer_module_teardown();
	reader_module_teardown();
	skiver_module_teardown();
	worker_module_teardown();
	catcher_module_teardown();
	request_module_teardown();
	token_module_teardown();
	buffer_module_teardown();
	thread_module_teardown();
	acceptor_module_initialized = 0;
	DEBUG("Shutdown sequence finished!");
}

int
acceptor_thread_loop(void)
{
	char inet_addr[INET_ADDRSTRLEN];
	struct vty *vtyp;
	struct session *sesp;
	fd_set rfds;
	int inet_port, n, nfds, so, so_nbd, so_vty;

	assert(acceptor_module_initialized);
	if (volume_list_load(master_voll) < 0)
		return (-1);

	so_vty = tcp_listen(cfg_vty_listen_ip, cfg_vty_listen_port,
	    cfg_vty_listen_backlog);
	if (so_vty < 0)
		return (-1);

	if (fd_blocking(so_vty, 0) < 0) {
		fd_close(so_vty);
		return (-1);
	}

	so_nbd = tcp_listen(cfg_nbd_listen_ip, cfg_nbd_listen_port,
	    cfg_nbd_listen_backlog);
	if (so_nbd < 0) {
		fd_close(so_vty);
		return (-1);
	}

	if (fd_blocking(so_nbd, 0) < 0) {
		fd_close(so_nbd);
		fd_close(so_vty);
		return (-1);
	}

	nfds = MAX(so_vty, so_nbd) + 1;
	for (;;) {
		signals_switch(1);
		if (do_exit)
			break;
		FD_ZERO(&rfds);
		FD_SET(so_nbd, &rfds);
		FD_SET(so_vty, &rfds);
		n = select(nfds, &rfds, NULL, NULL, NULL);
		if (n < 0) {
			if (errno == EINTR) {
				WARNING("select(): %s", strerror(errno));
				continue;
			}
			ERR("select(): %s", strerror(errno));
			continue;
		}
		assert(n);

		if (FD_ISSET(so_vty, &rfds)) {
			so = tcp_accept(so_vty, inet_addr, &inet_port);
			if (so < 0)
				continue;
			NOTICE("Accepted VTY connection from %s:%d",
			    inet_addr, inet_port);

			vtyp = NULL;
			if (vty_alloc(&vtyp) < 0) {
				assert(vtyp == NULL);
				fd_close(so);
				continue;
			}
			assert(vtyp != NULL);
			vtyp->so = so;
			memcpy(vtyp->inet_addr, inet_addr, sizeof(inet_addr));
			vtyp->inet_port = inet_port;

			signals_switch(0);
			if (vty_thread_create(vtyp) < 0) {
				vty_free(vtyp);
				fd_close(so);
				continue;
			}

			vty_list_lock(active_vtyl);
			vty_list_append(active_vtyl, vtyp);
			vty_list_unlock(active_vtyl);

			vty_thread_launch(vtyp);
			continue;
		}
		
		if (FD_ISSET(so_nbd, &rfds)) {
			so = tcp_accept(so_nbd, inet_addr, &inet_port);
			if (so < 0)
				continue;
			NOTICE("Accepted NBD connection from %s:%d",
			    inet_addr, inet_port);

			sesp = NULL;
			if (session_alloc(&sesp) < 0) {
				assert(sesp == NULL);
				fd_close(so);
				continue;
			}
			assert(sesp != NULL);
			sesp->so = so;
			memcpy(sesp->inet_addr, inet_addr, sizeof(inet_addr));
			sesp->inet_port = inet_port;

			signals_switch(0);
			if (reader_thread_create(sesp->rdr) < 0) {
				session_free(sesp);
				fd_close(so);
				continue;
			}

			if (writer_thread_create(sesp->wrr) < 0) {
				reader_thread_abort(sesp->rdr);
				session_free(sesp);
				fd_close(so);
				continue;
			}

			session_list_lock(negotiating_sesl);
			session_list_append(negotiating_sesl, sesp);
			session_list_unlock(negotiating_sesl);

			sesp->wrr->sesp = sesp;
			writer_thread_launch(sesp->wrr);
			sesp->rdr->sesp = sesp;
			reader_thread_launch(sesp->rdr);
			continue;
		}
	}
	fd_close(so_nbd);
	fd_close(so_vty);
	return (0);
}
