/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

struct mirror {
	TAILQ_ENTRY(mirror)	all_mirl_ary[3];
	pthread_mutex_t		mutex;
	struct thread		*thr;
#define MIR_FL_INUSE		0x00000001
#define MIR_FL_SYNCHRONISING	0x00010000
#define MIR_FL_COMPLETE		0x00020000
	int			flags;
#define MIR_ST_UP		0
#define MIR_ST_DOWN		1
#define MIR_ST_SHUTDOWN		2
	int			state;
	int			fd;
#define MIR_FL2_ISBLK		0x10000000
#define MIR_FL2_MASTER_DEV	0x20000000
	int			flags2;
	struct volume		*volp;
	uint64_t		size;
	uint32_t		blk_size;
	int			inflight;
	struct request_list	*aux_reql;
	struct request_list	*return_reql;
	int			sync_full;
	uint32_t		sync_request_size;
	int			sync_queue_length;
	int			sync_progress;
	char			target[MY_MAXPATH];
};
TAILQ_HEAD(mirror_head, mirror);

struct mirror_list {
	struct mirror_head	head;
	pthread_mutex_t		mutex;
#define MIRL_IDX_ALLOC		0
#define MIRL_IDX_STD		1
	int			index;
	int			count;
	int			peak;
};

int mirror_module_setup(void);
void mirror_module_hello(void);
void mirror_module_teardown(void);
void mirror_module_stats(void);
int mirror_alloc(struct mirror **);
void mirror_init(struct mirror *);
void mirror_uninit(struct mirror *);
void mirror_free(struct mirror *);
void mirror_lock(struct mirror *);
void mirror_unlock(struct mirror *);
int mirror_thread_create(struct mirror *);
void mirror_thread_launch(struct mirror *);
const char *mirror_strstate(struct mirror *);
int mirror_setup(struct mirror **, const char *, struct volume *);
int mirror_create(struct volume *, const char *);
void mirror_delete(struct mirror *);
int mirror_up(struct mirror *);
int mirror_shutdown_begin(struct mirror *);
int mirror_shutdown_commit(struct mirror *);
int mirror_list_alloc(struct mirror_list **, int);
void mirror_list_init(struct mirror_list *, int);
void mirror_list_uninit(struct mirror_list *);
void mirror_list_free(struct mirror_list *);
void mirror_list_lock(struct mirror_list *);
void mirror_list_unlock(struct mirror_list *);
int mirror_list_empty(struct mirror_list *);
struct mirror *mirror_list_first(struct mirror_list *);
struct mirror *mirror_list_next(struct mirror_list *, struct mirror *);
void mirror_list_insert(struct mirror_list *, struct mirror *);
void mirror_list_append(struct mirror_list *, struct mirror *);
void mirror_list_remove(struct mirror_list *, struct mirror *);
struct mirror *mirror_list_lookup_by_target(struct mirror_list *, const char *);
void mirror_list_shutdown(struct mirror_list *);
