/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#ifdef USE_LINUX_AIO
#include <sys/eventfd.h>
#include <libaio.h>
#endif

struct worker {
	TAILQ_ENTRY(worker)	all_wrkl_ary[3];
	struct thread		*thr;
#define WRK_FL_INUSE		0x00000001
	int			flags;
	int			submit_peak;
#ifdef USE_LINUX_AIO
	struct request_list	*spool_reql;
	struct iocb		**submit_iocb_ary;
	int			submit_iocb_ary_len;
#endif
};
TAILQ_HEAD(worker_head, worker);

struct worker_list {
	struct worker_head	head;
	pthread_mutex_t		mutex;
#define WRKL_IDX_ALLOC		0
#define WRKL_IDX_STD		1
#define WRKL_IDX_AUX		2
	int			index;
	int			count;
	int			peak;
};

extern struct worker_list *backoff_wrkl;

int worker_module_setup(void);
void worker_module_hello(void);
void worker_module_teardown(void);
void worker_module_stats(void);
int worker_alloc(struct worker **);
void worker_init(struct worker *);
void worker_uninit(struct worker *);
void worker_free(struct worker *);
void worker_thread_wakeup(struct worker *);
void worker_reql_submit(struct request_list *);
int worker_list_alloc(struct worker_list **, int);
void worker_list_init(struct worker_list *, int);
void worker_list_uninit(struct worker_list *);
void worker_list_free(struct worker_list *);
void worker_list_lock(struct worker_list *);
void worker_list_unlock(struct worker_list *);
int worker_list_empty(struct worker_list *);
struct worker *worker_list_first(struct worker_list *);
struct worker *worker_list_next(struct worker_list *, struct worker *);
void worker_list_insert(struct worker_list *, struct worker *);
void worker_list_append(struct worker_list *, struct worker *);
void worker_list_remove(struct worker_list *, struct worker *);
