/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "io.h"
#include "thread.h"
#include "request.h"
#include "reader.h"
#include "writer.h"
#include "worker.h"
#include "skiver.h"
#include "session.h"
#include "catcher.h"
#include "dispatch_glue.h"
#include "volume.h"
#include "mirror.h"
#include "vty.h"
#include "periodic.h"

static int periodic_module_initialized = 0;
static struct periodic *periodic0 = NULL;

static int periodic_alloc(struct periodic **);
static void periodic_free(struct periodic *);
static void periodic_thread_shutdown(struct periodic *);
static void *periodic_thread(void *);
static void periodic_thread_loop(struct periodic *);

int
periodic_module_setup(void)
{

	assert(!periodic_module_initialized);
	assert(periodic0 == NULL);
	if (cfg_stats_interval < 1) {
		periodic_module_initialized = 1;
		return (0);
	}
	if (periodic_alloc(&periodic0) < 0) {
		assert(periodic0 == NULL);
		return (-1);
	}
	assert(periodic0 != NULL);
	if (thread_create(periodic0->thr, "periodic0", periodic_thread) < 0) {
		periodic_free(periodic0);
		periodic0 = NULL;
		return (-2);
	}
	thread_launch(periodic0->thr);
	periodic_module_initialized = 1;
	return (0);
}

void
periodic_module_hello(void)
{

	assert(periodic_module_initialized);
	DEBUG("sizeof(periodic)=%zu", sizeof(struct periodic));
}

void
periodic_module_teardown(void)
{

	assert(periodic_module_initialized);
	if (cfg_stats_interval < 1) {
		assert(periodic0 == NULL);
		periodic_module_initialized = 0;
		return;
	}
	assert(periodic0 != NULL);
	periodic_thread_shutdown(periodic0);
	periodic_free(periodic0);
	periodic0 = NULL;
	periodic_module_initialized = 0;
}

static
int
periodic_alloc(struct periodic **perpp)
{
	struct periodic *perp;

	perp = malloc(sizeof(*perp));
	if (perp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*perp), strerror(errno));
		return (-2);
	}
	perp->thr = NULL;
	if (thread_alloc(&perp->thr, perp) < 0) {
		assert(perp->thr == NULL);
		free(perp);
		return (-3);
	}
	assert(perp->thr != NULL);
	if (pipe(perp->pype) < 0) {
		ERR("pipe(): %s", strerror(errno));
		thread_free(perp->thr);
		free(perp);
		return (-4);
	}
	*perpp = perp;
	return (0);
}

static
void
periodic_free(struct periodic *perp)
{

	fd_close(perp->pype[1]);
	fd_close(perp->pype[0]);
	thread_free(perp->thr);
	free(perp);
}

static
void
periodic_thread_shutdown(struct periodic *perp)
{
	ssize_t w;
	char go[1] = { 'X' };

	thread_shutdown_begin(perp->thr);
try_write:
	w = write(perp->pype[1], go, sizeof(go));
	if (w < (ssize_t)0 && errno == EINTR)
		goto try_write;
	assert(w == (ssize_t)sizeof(go));
	thread_join(perp->thr);
	thread_shutdown_commit(perp->thr);
}

static
void *
periodic_thread(void *data)
{
	struct periodic *perp;

	perp = (struct periodic *)data;
	assert(perp != NULL);
	assert(perp->thr != NULL);

	thread_prologue(perp->thr);

	periodic_thread_loop(perp);

	thread_epilogue(NULL);
	return (NULL);
}

static
void
periodic_thread_loop(struct periodic *perp)
{
	struct timeval tv;
	fd_set rfds;
	ssize_t r;
	int cntrl_fd, n, nfds;

	cntrl_fd = perp->pype[0];

	nfds = cntrl_fd + 1;
	for (;;) {
		FD_ZERO(&rfds);
		FD_SET(cntrl_fd, &rfds);
		tv.tv_sec = (long)cfg_stats_interval;
		tv.tv_usec = (long)0;
		n = select(nfds, &rfds, NULL, NULL, &tv);
		if (n < 0) {
			if (errno == EINTR) {
				WARNING("select(): %s", strerror(errno));
				continue;
			}
			ERR("select(): %s", strerror(errno));
			continue;
		}

		if (FD_ISSET(cntrl_fd, &rfds)) {
			char go[1];
try_read_cntrl_fd:
			r = read(cntrl_fd, go, sizeof(go));
			if (r < (ssize_t)0 && errno == EINTR)
				goto try_read_cntrl_fd;
			assert(r == (ssize_t)sizeof(go));
			assert(go[0] == 'X');
			break;
		}

		thread_module_stats();
		reader_module_stats();
		writer_module_stats();
		worker_module_stats();
		skiver_module_stats();
		buffer_module_stats();
		token_module_stats();
		request_module_stats();
		session_module_stats();
		catcher_module_stats();
		mirror_module_stats();
		volume_module_stats();
		vty_module_stats();
	}
}
