/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

struct session {
	TAILQ_ENTRY(session)	all_sesl_ary[2];
#define SES_FL_INUSE		0x00000001
	int			flags;
	int			so;
	char			inet_addr[INET_ADDRSTRLEN];
	int			inet_port;
	int			pad;
	struct reader		*rdr;
	struct writer		*wrr;
	struct volume		*volp;
	struct request_list	*outgoing_reql;
	struct request_list	*flush_reql;
	int64_t			last_req_id;
};
TAILQ_HEAD(session_head, session);

struct session_list {
	struct session_head	head;
	pthread_mutex_t		mutex;
#define SESL_IDX_ALLOC		0
#define SESL_IDX_STD		1
	int			index;
	int			count;
	int			peak;
};

extern struct session_list *negotiating_sesl;

int session_module_setup(void);
void session_module_hello(void);
void session_module_teardown(void);
void session_module_stats(void);
int session_alloc(struct session **);
void session_init(struct session *);
void session_uninit(struct session *);
void session_free(struct session *);
void session_shutdown(struct session *);
int session_list_alloc(struct session_list **, int);
void session_list_init(struct session_list *, int);
void session_list_uninit(struct session_list *);
void session_list_free(struct session_list *);
void session_list_lock(struct session_list *);
void session_list_unlock(struct session_list *);
int session_list_empty(struct session_list *);
struct session *session_list_first(struct session_list *);
struct session *session_list_next(struct session_list *, struct session *);
void session_list_insert(struct session_list *, struct session *);
void session_list_append(struct session_list *, struct session *);
void session_list_remove(struct session_list *, struct session *);
void session_list_shutdown(struct session_list *);
