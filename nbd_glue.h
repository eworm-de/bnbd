/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#define NBD_REQUEST_MAGIC	((uint32_t)0x25609513)
#define NBD_REPLY_MAGIC		((uint32_t)0x67446698)

#define NBD_CMD_READ		((uint32_t)0)
#define NBD_CMD_WRITE		((uint32_t)1)
#define NBD_CMD_DISC		((uint32_t)2)
#define NBD_CMD_FLUSH		((uint32_t)3)
#define NBD_CMD_TRIM		((uint32_t)4)

#define NBD_FLAG_HAS_FLAGS	((uint32_t)(1 << 0))
#define NBD_FLAG_SEND_FLUSH	((uint32_t)(1 << 2))
#define NBD_FLAG_SEND_TRIM	((uint32_t)(1 << 5))

#define NBD_FLAG_FIXED_NEWSTYLE	((uint16_t)1)

#define NBD_OPT_EXPORT_NAME	((uint32_t)1)
#define NBD_OPT_ABORT		((uint32_t)2)
#define NBD_OPT_LIST		((uint32_t)3)

struct nbd_request {
	uint32_t		magic;
	uint32_t		type;
	uint64_t		handle;
	uint64_t		from;
	uint32_t		len;
} __attribute__((packed));

struct nbd_reply {
	uint32_t		magic;
	uint32_t		error;
	uint64_t		handle;
} __attribute__((packed));

struct nbd_data {
	struct nbd_request	request;
	int			pad;
	struct nbd_reply	reply;
};
