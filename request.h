/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#ifdef USE_LINUX_AIO
#include <sys/eventfd.h>
#include <libaio.h>
#endif

#include "buffer.h"
#include "token.h"
#include "nbd_glue.h"

struct request {
	TAILQ_ENTRY(request)	all_reql_ary[2];
#define REQ_FL_INUSE		0x00000001
#define REQ_FL_ABORT		0x00000002
	int			flags;
#define REQ_TY_NONE		0x00000000
#define REQ_TY_READ		0x00000001
#define REQ_TY_WRITE		0x00000002
#define REQ_TY_FLUSH		0x00000004
#define REQ_TY_TRIM		0x00000008
#define REQ_TY_MASTER_WRITE	0x00000010
#define REQ_TY_SLAVE_WRITE	0x00000020
#define REQ_TY_MASTER_COPY	0x00000100
#define REQ_TY_SLAVE_PASTE	0x00000200
#define REQ_TY_MASTER_TRIM	0x00001000
#define REQ_TY_SLAVE_TRIM	0x00002000
#define REQ_TY_ALL		0xffffffff
#define REQ_TY_AIO		(REQ_TY_ALL & ~(REQ_TY_BIO))
#define REQ_TY_BIO		(REQ_TY_TRIM | REQ_TY_MASTER_TRIM | \
    REQ_TY_SLAVE_TRIM)
	int			type;
	int64_t			id;
	struct nbd_data		nbd;
	struct buffer		buf;
	uint32_t		blk_size;	 /* sesp->volp->blk_size */
	int			fd;		 /* sesp->volp->fd */
	struct volume		*volp;		 /* sesp->volp */
	struct request_list	*outgoing_reqlp; /* sesp->outgoing_reql */
	struct mirror		*mirp;
	struct request		*master_reqp;
	struct token		*tok;
#ifdef USE_LINUX_AIO
	struct iocb		iocb;
#endif
};
TAILQ_HEAD(request_head, request);

struct request_list {
	struct request_head	head;
	pthread_mutex_t		mutex;
	pthread_cond_t		cond;
#define REQL_FL_SHUTDOWN	0x00000002
#define REQL_FL_NOTIFIED	0x10000000
	int			flags;
#define REQL_IDX_ALLOC		0
#define REQL_IDX_STD		1
	int			index;
	int			count;
	int			peak;
	int			submit_peak;
	int64_t			submit_bytes;
	int64_t			submit_bytes_peak;
};

int request_module_setup(void);
void request_module_hello(void);
void request_module_teardown(void);
void request_module_stats(void);
int request_alloc(struct thread *, struct request **, int);
void request_init(struct request *);
void request_uninit(struct request *);
void request_free(struct request *);
void request_done(struct request *, int);
void request_reply(struct request *);
void request_dump(struct request *);
int request_rdbuf_alloc(struct thread *, struct request *, int);
int request_wrbuf_alloc(struct thread *, struct request *, int);
int request_rwbuf_alloc(struct thread *, struct request *, int);
void request_buffer_free(struct request *);
int request_list_alloc(struct request_list **, int);
void request_list_init(struct request_list *, int);
void request_list_uninit(struct request_list *);
void request_list_free(struct request_list *);
void request_list_lock(struct request_list *);
void request_list_unlock(struct request_list *);
void request_list_wait(struct request_list *);
void request_list_wakeup(struct request_list *);
int request_list_empty(struct request_list *);
struct request *request_list_first(struct request_list *);
struct request *request_list_next(struct request_list *, struct request *);
void request_list_insert(struct request_list *, struct request *);
void request_list_append(struct request_list *, struct request *);
void request_list_remove(struct request_list *, struct request *);
void request_list_concat(struct request_list *, struct request_list *);
void request_list_abort(struct request_list *, int, int);
void request_list_dump(struct request_list *, int);
