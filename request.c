/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "thread.h"
#include "request.h"
#include "session.h"
#include "mirror.h"
#include "volume.h"

static int request_module_initialized = 0;
static pthread_mutex_t request_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct request_list *available_reql = NULL;
static struct request_list *inuse_reql = NULL;
static struct thread_list *request_alloc_wait_thrl = NULL;
static struct thread_list *request_alloc_wakeup_thrl = NULL;
static time_t max_requests_warn_time = (time_t)0;

static void request_dealloc(struct request *);
static void request_list_dealloc(struct request_list *);

int
request_module_setup(void)
{

	assert(!request_module_initialized);
	assert(available_reql == NULL);
	if (request_list_alloc(&available_reql, REQL_IDX_ALLOC) < 0) {
		assert(available_reql == NULL);
		goto out0;
	}
	assert(available_reql != NULL);
	assert(inuse_reql == NULL);
	if (request_list_alloc(&inuse_reql, REQL_IDX_ALLOC) < 0) {
		assert(inuse_reql == NULL);
		goto out1;
	}
	assert(inuse_reql != NULL);
	assert(request_alloc_wait_thrl == NULL);
	if (thread_list_alloc(&request_alloc_wait_thrl, THRL_IDX_STD) < 0) {
		assert(request_alloc_wait_thrl == NULL);
		goto out2;
	}
	assert(request_alloc_wait_thrl != NULL);
	assert(request_alloc_wakeup_thrl == NULL);
	if (thread_list_alloc(&request_alloc_wakeup_thrl, THRL_IDX_STD) < 0) {
		assert(request_alloc_wakeup_thrl == NULL);
		goto out3;
	}
	assert(request_alloc_wakeup_thrl != NULL);
	request_module_initialized = 1;
	return (0);
out3:
	assert(request_alloc_wait_thrl != NULL);
	thread_list_free(request_alloc_wait_thrl);
	request_alloc_wait_thrl = NULL;
out2:
	assert(inuse_reql != NULL);
	request_list_free(inuse_reql);
	inuse_reql = NULL;
out1:
	assert(available_reql != NULL);
	request_list_dealloc(available_reql);
	request_list_free(available_reql);
	available_reql = NULL;
out0:
	return (-1);
}

void
request_module_hello(void)
{

	assert(request_module_initialized);
	DEBUG("sizeof(nbd_data)=%zu", sizeof(struct nbd_data));
#ifdef USE_LINUX_AIO
	DEBUG("sizeof(iocb)=%zu", sizeof(struct iocb));
#endif
	DEBUG("sizeof(request)=%zu", sizeof(struct request));
	DEBUG("sizeof(request_list)=%zu", sizeof(struct request_list));
}

void
request_module_teardown(void)
{

	assert(request_alloc_wakeup_thrl != NULL);
	thread_list_free(request_alloc_wakeup_thrl);
	request_alloc_wakeup_thrl = NULL;
	assert(request_alloc_wait_thrl != NULL);
	thread_list_free(request_alloc_wait_thrl);
	request_alloc_wait_thrl = NULL;
	assert(inuse_reql != NULL);
	request_list_free(inuse_reql);
	inuse_reql = NULL;
	assert(available_reql != NULL);
	request_list_dealloc(available_reql);
	request_list_free(available_reql);
	available_reql = NULL;
	request_module_initialized = 0;
}

void
request_module_stats(void)
{
	int available_count, available_peak, inuse_count, inuse_peak;

	assert(request_module_initialized);
	PTHREAD_MUTEX_LOCK(&request_mutex);
	available_count = available_reql->count;
	available_peak = available_reql->peak;
	available_reql->peak = available_reql->count;
	inuse_count = inuse_reql->count;
	inuse_peak = inuse_reql->peak;
	inuse_reql->peak = inuse_reql->count;
	PTHREAD_MUTEX_UNLOCK(&request_mutex);
	NOTICE("inuse=%d/%d available=%d/%d limit=%d",
	    inuse_count, inuse_peak, available_count, available_peak,
	    cfg_max_requests);
}

int
request_alloc(struct thread *thrp, struct request **reqpp, int waitok)
{
	struct request *reqp;
	time_t now;

	assert(request_module_initialized);
	PTHREAD_MUTEX_LOCK(&request_mutex);
	if (!thread_list_empty(request_alloc_wait_thrl) ||
	    !thread_list_empty(request_alloc_wakeup_thrl)) {
		if (!waitok) {
			PTHREAD_MUTEX_UNLOCK(&request_mutex);
			return (-1);
		}
block:
		assert(!(thrp->flags2 & THR_FL2_NOTIFIED));
		thread_list_append(request_alloc_wait_thrl, thrp);
		thread_wait_mutex(thrp, &request_mutex);
		thread_list_remove(request_alloc_wakeup_thrl, thrp);
		assert(thrp->flags2 & THR_FL2_NOTIFIED);
		thrp->flags2 &= ~THR_FL2_NOTIFIED;
	}
	reqp = request_list_first(available_reql);
	if (reqp == NULL) {
		assert(available_reql->count == 0);
		if (cfg_max_requests > 0 &&
		    inuse_reql->count >= cfg_max_requests) {
			if (!waitok) {
				PTHREAD_MUTEX_UNLOCK(&request_mutex);
				return (-1);
			}
			now = time(NULL);
			if (max_requests_warn_time < now) {
				max_requests_warn_time = now;
				NOTICE("max_requests=%d limit hit!",
				    cfg_max_requests);
			}
			goto block;
		}
		PTHREAD_MUTEX_UNLOCK(&request_mutex);
		reqp = malloc(sizeof(*reqp));
		if (reqp == NULL) {
			ERR("malloc(%zu): %s", sizeof(*reqp), strerror(errno));
			return (-2);
		}
		reqp->tok = NULL;
		if (token_alloc(&reqp->tok) < 0) {
			assert(reqp->tok == NULL);
			free(reqp);
			return (-3);
		}
		assert(reqp->tok != NULL);
		request_init(reqp);
		PTHREAD_MUTEX_LOCK(&request_mutex);
	} else {
		request_list_remove(available_reql, reqp);
	}
	assert(!reqp->flags);
	reqp->flags |= REQ_FL_INUSE;
	request_list_insert(inuse_reql, reqp);
	PTHREAD_MUTEX_UNLOCK(&request_mutex);
	assert(reqp->type == REQ_TY_NONE);
	assert(reqp->id < (int64_t)0);
	assert(reqp->buf.buf_bpp == NULL);
	assert(reqp->buf.buf_ptr == NULL);
	assert(reqp->buf.buf_idx == (uint32_t)0);
	assert(reqp->buf.buf_len == (uint32_t)0);
	assert(reqp->blk_size == (uint32_t)0);
	assert(reqp->fd == -1);
	assert(reqp->volp == NULL);
	assert(reqp->outgoing_reqlp == NULL);
	assert(reqp->mirp == NULL);
	assert(reqp->master_reqp == NULL);
	*reqpp = reqp;
	return (0);
}

void
request_init(struct request *reqp)
{

	reqp->flags = 0;
	reqp->type = REQ_TY_NONE;
	reqp->id = (int64_t)-1;
	memset(&reqp->nbd, 0xAB, sizeof(reqp->nbd));
	reqp->buf.buf_bpp = NULL;
	reqp->buf.buf_ptr = NULL;
	reqp->buf.buf_idx = (uint32_t)0;
	reqp->buf.buf_len = (uint32_t)0;
	reqp->blk_size = (uint32_t)0;
	reqp->fd = -1;
	reqp->volp = NULL;
	reqp->outgoing_reqlp = NULL;
	reqp->mirp = NULL;
	reqp->master_reqp = NULL;
}

void
request_uninit(struct request *reqp)
{

	assert(reqp->flags == REQ_FL_INUSE);
	reqp->type = REQ_TY_NONE;
	reqp->id = (int64_t)-1;
	memset(&reqp->nbd, 0xAB, sizeof(reqp->nbd));
	if (reqp->buf.buf_ptr != NULL)
		request_buffer_free(reqp);
	assert(reqp->buf.buf_bpp == NULL);
	assert(reqp->buf.buf_ptr == NULL);
	reqp->buf.buf_idx = (uint32_t)0;
	reqp->buf.buf_len = (uint32_t)0;
	reqp->blk_size = (uint32_t)0;
	reqp->fd = -1;
	reqp->volp = NULL;
	reqp->outgoing_reqlp = NULL;
	reqp->mirp = NULL;
	reqp->master_reqp = NULL;
	token_uninit(reqp->tok);
}

void
request_free(struct request *reqp)
{
	struct thread *thrp;

	assert(request_module_initialized);
	request_uninit(reqp);
	PTHREAD_MUTEX_LOCK(&request_mutex);
	request_list_remove(inuse_reql, reqp);
	reqp->flags &= ~REQ_FL_INUSE;
	assert(!reqp->flags);
	request_list_insert(available_reql, reqp);
	thrp = thread_list_first(request_alloc_wait_thrl);
	if (thrp == NULL) {
		PTHREAD_MUTEX_UNLOCK(&request_mutex);
		return;
	}
	thread_list_remove(request_alloc_wait_thrl, thrp);
	assert(!(thrp->flags2 & THR_FL2_NOTIFIED));
	thrp->flags2 |= THR_FL2_NOTIFIED;
	thread_list_append(request_alloc_wakeup_thrl, thrp);
	PTHREAD_MUTEX_UNLOCK(&request_mutex);
	thread_wakeup(thrp);
}

static
void
request_dealloc(struct request *reqp)
{

	assert(!reqp->flags);
	assert(reqp->buf.buf_ptr == NULL);
	token_free(reqp->tok);
	free(reqp);
}

void
request_done(struct request *reqp, int error)
{
	struct request *master_reqp;
	struct mirror *mirp, *nmirp;
	struct mirror_list *slave_mirlp;
	int refs;

	if (reqp->flags & REQ_FL_ABORT)
		reqp->flags &= ~REQ_FL_ABORT;
	else
		reqp->nbd.reply.error = htonl((uint32_t)error);
	switch (reqp->type) {
	case REQ_TY_READ:
		assert(reqp->nbd.request.type == NBD_CMD_READ);
		assert(reqp->volp != NULL);
		assert(reqp->outgoing_reqlp != NULL);
		assert(reqp->mirp == NULL);
		assert(reqp->master_reqp == NULL);
		if (reqp->nbd.reply.error != (uint32_t)0 &&
		    reqp->buf.buf_ptr != NULL)
			/* free failed read reuests buffers */
			request_buffer_free(reqp);
		break;
	case REQ_TY_WRITE:
	case REQ_TY_TRIM:
		assert(reqp->nbd.request.type == NBD_CMD_WRITE ||
		    reqp->nbd.request.type == NBD_CMD_TRIM);
		assert(reqp->volp != NULL);
		assert(reqp->outgoing_reqlp != NULL);
		assert(reqp->mirp == NULL);
		assert(reqp->master_reqp == NULL);
#ifndef NDEBUG
		refs =
#endif
		    token_release(reqp->volp->range_tokt, reqp->tok);
		assert(!refs);
		if (reqp->buf.buf_ptr != NULL)
			/* free write requests buffers */
			request_buffer_free(reqp);
		break;
	case REQ_TY_MASTER_WRITE:
	case REQ_TY_MASTER_TRIM:
		assert(reqp->nbd.request.type == NBD_CMD_WRITE ||
		    reqp->nbd.request.type == NBD_CMD_TRIM);
		assert(reqp->volp != NULL);
		assert(reqp->outgoing_reqlp != NULL);
		assert(reqp->mirp == NULL);
		assert(reqp->master_reqp == reqp);
		if (reqp->nbd.reply.error != (uint32_t)0) {
			slave_mirlp = reqp->volp->slave_mirl;
			mirror_list_lock(slave_mirlp);
			nmirp = mirror_list_first(slave_mirlp);
			for (;;) {
				mirp = nmirp;
				if (mirp == NULL)
					break;
				nmirp = mirror_list_next(slave_mirlp, mirp);
				mirror_lock(mirp);
				ERR("MIRROR MASTER WRITE/TRIM ERROR: "
				    "target=%s volume=%s offset=%llu length=%u",
				    mirp->target,
				    mirp->volp->id,
				    (unsigned long long)reqp->nbd.request.from,
				    (unsigned int)reqp->nbd.request.len);
				mirror_shutdown_begin(mirp);
				mirror_unlock(mirp);
			}
			mirror_list_unlock(slave_mirlp);
		}
		refs = token_release(reqp->volp->range_tokt, reqp->tok);
		/* reqp->tok.refs >= 0 */
		if (refs)
			return;
		if (reqp->buf.buf_ptr != NULL)
			request_buffer_free(reqp);
		break;
	case REQ_TY_SLAVE_WRITE:
	case REQ_TY_SLAVE_TRIM:
		assert(reqp->nbd.request.type == NBD_CMD_WRITE ||
		    reqp->nbd.request.type == NBD_CMD_TRIM);
		assert(reqp->volp == NULL);
		assert(reqp->outgoing_reqlp != NULL);
		assert(reqp->mirp != NULL);
		assert(reqp->master_reqp != NULL);
		assert(reqp->master_reqp != reqp);
		mirror_lock(reqp->mirp);
		reqp->mirp->inflight--;
		if (reqp->nbd.reply.error != (uint32_t)0) {
			ERR("MIRROR SLAVE WRITE/TRIM ERROR: "
			    "target=%s volume=%s offset=%llu length=%u",
			    reqp->mirp->target,
			    reqp->mirp->volp->id,
			    (unsigned long long)reqp->nbd.request.from,
			    (unsigned int)reqp->nbd.request.len);
			mirror_shutdown_begin(reqp->mirp);
		} else {
			mirror_shutdown_commit(reqp->mirp);
		}
		mirror_unlock(reqp->mirp);
		master_reqp = reqp->master_reqp;
		if (reqp->buf.buf_ptr != NULL) {
			assert(reqp->buf.buf_bpp != NULL);
			reqp->buf.buf_bpp = NULL;
			reqp->buf.buf_ptr = NULL;
		}
		request_free(reqp);
		refs = token_release(master_reqp->volp->range_tokt,
		    master_reqp->tok);
		/* master_reqp->tok.refs >= 0 */
		if (refs)
			return;
		reqp = master_reqp;
		if (reqp->buf.buf_ptr != NULL)
			request_buffer_free(reqp);
		break;
	case REQ_TY_MASTER_COPY:
	case REQ_TY_SLAVE_PASTE:
		assert(reqp->volp != NULL);
		assert(reqp->outgoing_reqlp != NULL);
		assert(reqp->mirp != NULL);
		assert(reqp->outgoing_reqlp == reqp->mirp->return_reql);
		assert(reqp->master_reqp == NULL);
		break;
	default:
		abort();
	}
	request_reply(reqp);
}

void
request_reply(struct request *reqp)
{
	int wakeup;

	request_list_lock(reqp->outgoing_reqlp);
	request_list_append(reqp->outgoing_reqlp, reqp);
	if (reqp->outgoing_reqlp->flags & REQL_FL_NOTIFIED) {
		wakeup = 0;
	} else {
		reqp->outgoing_reqlp->flags |= REQL_FL_NOTIFIED;
		wakeup = 1;
	}
	request_list_unlock(reqp->outgoing_reqlp);
	if (wakeup)
		request_list_wakeup(reqp->outgoing_reqlp);
}

void
request_dump(struct request *reqp)
{

	INFO(
	    "fd=%d type=0x%x id=%lld "
	    "offset=%llu "
	    "length=%u "
	    "error=%d "
	    "(0x%X)",
	    reqp->fd, reqp->type, (long long)reqp->id,
	    (unsigned long long)reqp->nbd.request.from,
	    (unsigned int)reqp->nbd.request.len,
	    (int)ntohl(reqp->nbd.reply.error),
	    (int)ntohl(reqp->nbd.reply.error));
}

int
request_rdbuf_alloc(struct thread *thrp, struct request *reqp, int waitok)
{
	int error;

	error = buffer_alloc(thrp, &reqp->buf, read_bp, reqp->blk_size,
	    reqp->nbd.request.len, waitok);
	return (error);
}

int
request_wrbuf_alloc(struct thread *thrp, struct request *reqp, int waitok)
{
	int error;

	error = buffer_alloc(thrp, &reqp->buf, write_bp, reqp->blk_size,
	    reqp->nbd.request.len, waitok);
	return (error);
}

int
request_rwbuf_alloc(struct thread *thrp, struct request *reqp, int waitok)
{
	int error;

	error = buffer_alloc(thrp, &reqp->buf, mirror_bp, reqp->blk_size,
	    reqp->nbd.request.len, waitok);
	return (error);
}

void
request_buffer_free(struct request *reqp)
{

	buffer_free(&reqp->buf);
}

int
request_list_alloc(struct request_list **reqlpp, int index)
{
	struct request_list *reqlp;
	int error;

	reqlp = malloc(sizeof(*reqlp));
	if (reqlp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*reqlp), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&reqlp->mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(reqlp);
		return (-3);
	}
	error = pthread_cond_init(&reqlp->cond, NULL);
	if (error) {
		ERR("pthread_cond_init(): %d", error);
		PTHREAD_MUTEX_DESTROY(&reqlp->mutex);
		free(reqlp);
		return (-4);
	}
	request_list_init(reqlp, index);
	*reqlpp = reqlp;
	return (0);
}

void
request_list_init(struct request_list *reqlp, int index)
{
#ifndef NDEBUG
	struct request *reqp;
#endif

	assert(index >= 0 && index < NELEM(reqp->all_reql_ary));
	TAILQ_INIT(&reqlp->head);
	reqlp->flags = 0;
	reqlp->index = index;
	reqlp->count = 0;
	reqlp->peak = 0;
	reqlp->submit_peak = 0;
	reqlp->submit_bytes = (int64_t)0;
	reqlp->submit_bytes_peak = (int64_t)0;
}

void
request_list_uninit(struct request_list *reqlp)
{

	request_list_lock(reqlp);
	reqlp->flags = 0;
	request_list_unlock(reqlp);
	assert(TAILQ_EMPTY(&reqlp->head));
	assert(reqlp->count == 0);
	reqlp->peak = 0;
	reqlp->submit_peak = 0;
	reqlp->submit_bytes = (int64_t)0;
	reqlp->submit_bytes_peak = (int64_t)0;
}

void
request_list_free(struct request_list *reqlp)
{

	request_list_uninit(reqlp);
	PTHREAD_COND_DESTROY(&reqlp->cond);
	PTHREAD_MUTEX_DESTROY(&reqlp->mutex);
	free(reqlp);
}

static
void
request_list_dealloc(struct request_list *reqlp)
{
	struct request *nreqp, *reqp;

	nreqp = request_list_first(reqlp);
	for (;;) {
		reqp = nreqp;
		if (reqp == NULL)
			break;
		nreqp = request_list_next(reqlp, reqp);
		request_list_remove(reqlp, reqp);
		request_dealloc(reqp);
	}
}

void
request_list_lock(struct request_list *reqlp)
{

	PTHREAD_MUTEX_LOCK(&reqlp->mutex);
}

void
request_list_unlock(struct request_list *reqlp)
{

	PTHREAD_MUTEX_UNLOCK(&reqlp->mutex);
}

void
request_list_wait(struct request_list *reqlp)
{

	PTHREAD_COND_WAIT(&reqlp->cond, &reqlp->mutex);
}

void
request_list_wakeup(struct request_list *reqlp)
{

	PTHREAD_COND_SIGNAL(&reqlp->cond);
}

int
request_list_empty(struct request_list *reqlp)
{

	return (TAILQ_EMPTY(&reqlp->head));
}

struct request *
request_list_first(struct request_list *reqlp)
{

	return (TAILQ_FIRST(&reqlp->head));
}

struct request *
request_list_next(struct request_list *reqlp, struct request *reqp)
{

	return (TAILQ_NEXT(reqp, all_reql_ary[reqlp->index]));
}

void
request_list_insert(struct request_list *reqlp, struct request *reqp)
{

	TAILQ_INSERT_HEAD(&reqlp->head, reqp, all_reql_ary[reqlp->index]);
	reqlp->count++;
	assert(reqlp->count > 0);
	if (reqlp->count > reqlp->peak)
		reqlp->peak = reqlp->count;
}

void
request_list_append(struct request_list *reqlp, struct request *reqp)
{

	TAILQ_INSERT_TAIL(&reqlp->head, reqp, all_reql_ary[reqlp->index]);
	reqlp->count++;
	assert(reqlp->count > 0);
	if (reqlp->count > reqlp->peak)
		reqlp->peak = reqlp->count;
}

void
request_list_remove(struct request_list *reqlp, struct request *reqp)
{

	TAILQ_REMOVE(&reqlp->head, reqp, all_reql_ary[reqlp->index]);
	reqlp->count--;
	assert(reqlp->count >= 0);
}

void
request_list_concat(struct request_list *dreqlp, struct request_list *sreqlp)
{

	assert(dreqlp->index == sreqlp->index);
	TAILQ_CONCAT(&dreqlp->head, &sreqlp->head, all_reql_ary[sreqlp->index]);
	dreqlp->count += sreqlp->count;
	assert(dreqlp->count > 0);
	if (dreqlp->count > dreqlp->peak)
		dreqlp->peak = dreqlp->count;
	sreqlp->count = 0;
}

void
request_list_abort(struct request_list *reqlp, int types, int error)
{
	struct request *nreqp, *reqp;

	nreqp = request_list_first(reqlp);
	for (;;) {
		reqp = nreqp;
		if (reqp == NULL)
			break;
		nreqp = request_list_next(reqlp, reqp);
		if (!(reqp->type & types))
			continue;
		reqp->nbd.reply.error = htonl((uint32_t)error);
		reqp->flags |= REQ_FL_ABORT;
	}
}

void
request_list_dump(struct request_list *reqlp, int types)
{
	struct request *nreqp, *reqp;

	nreqp = request_list_first(reqlp);
	for (;;) {
		reqp = nreqp;
		if (reqp == NULL)
			break;
		nreqp = request_list_next(reqlp, reqp);
		if (!(reqp->type & types))
			continue;
		request_dump(reqp);
	}
}
