/*-
 * Copyright (c) 2012-2014 Michal Belczyk <belczyk@bsd.krakow.pl>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "common.h"
#include "config.h"

#include "thread.h"
#include "token.h"

struct token_slab {
	TAILQ_ENTRY(token_slab)	ts_tailq_entries[1];
	TAILQ_HEAD(, token)	ts_tokens;
	ssize_t			ts_avail;
	ssize_t			ts_total;
	void			*ts_buf;
};

static int token_module_initialized = 0;
static pthread_mutex_t token_mutex = PTHREAD_MUTEX_INITIALIZER;
static TAILQ_HEAD(, token_slab) slabs;
static ssize_t slabs_count;
static ssize_t tokens_avail;
static ssize_t tokens_total;

static int token_slab_alloc(struct token_slab **);
static void token_slab_free(struct token_slab *);

int
token_module_setup(void)
{

	assert(!token_module_initialized);
	TAILQ_INIT(&slabs);
	slabs_count = (ssize_t)0;
	tokens_avail = (ssize_t)0;
	tokens_total = (ssize_t)0;
	token_module_initialized = 1;
	return (0);
}

void
token_module_hello(void)
{

	assert(token_module_initialized);
	DEBUG("sizeof(token)=%zu", sizeof(struct token));
	DEBUG("sizeof(token_slab)=%zu", sizeof(struct token_slab));
	DEBUG("sizeof(token_tree)=%zu", sizeof(struct token_tree));
}

void
token_module_teardown(void)
{

	assert(token_module_initialized);
	assert(TAILQ_EMPTY(&slabs));
	assert(slabs_count == (ssize_t)0);
	assert(tokens_avail == (ssize_t)0);
	assert(tokens_total == (ssize_t)0);
	token_module_initialized = 0;
}

void
token_module_stats(void)
{
	ssize_t _avail, _inuse, _slabs;

	assert(token_module_initialized);
	PTHREAD_MUTEX_LOCK(&token_mutex);
	_inuse = tokens_total - tokens_avail;
	_avail = tokens_avail;
	_slabs = slabs_count;
	PTHREAD_MUTEX_UNLOCK(&token_mutex);
	NOTICE("inuse=%zd available=%zd slabs=%zd", _inuse, _avail, _slabs);
}

int
token_alloc(struct token **tokpp)
{
	struct token_slab *tsp;
	struct token *tokp;

	assert(token_module_initialized);
	PTHREAD_MUTEX_LOCK(&token_mutex);
	tokp = NULL;
	TAILQ_FOREACH(tsp, &slabs, ts_tailq_entries[0]) {
		if (TAILQ_EMPTY(&tsp->ts_tokens))
			continue;
		tokp = TAILQ_FIRST(&tsp->ts_tokens);
		break;
	}
	if (tokp == NULL) {
		tsp = NULL;
		if (token_slab_alloc(&tsp) < 0) {
			assert(tsp == NULL);
			PTHREAD_MUTEX_UNLOCK(&token_mutex);
			return (-1);
		}
		assert(tsp != NULL);
		tokp = TAILQ_FIRST(&tsp->ts_tokens);
		assert(tokp != NULL);
	}
	TAILQ_REMOVE(&tsp->ts_tokens, tokp, tok_tailq_entries[0]);
	tsp->ts_avail--;
	assert(tsp->ts_avail >= (ssize_t)0);
	tokens_avail--;
	assert(tokens_avail >= (ssize_t)0);
	PTHREAD_MUTEX_UNLOCK(&token_mutex);
	assert(tokp->tok_from == (uint64_t)0);
	assert(tokp->tok_to == (uint64_t)0);
	assert(tokp->tok_refs == 0);
	assert(!tokp->tok_flags);
	tokp->tok_flags |= TOK_FL_INUSE;
	assert(tokp->tok_tsp == tsp);
	assert(thread_head_empty(&tokp->tok_wait_thrh));
	*tokpp = tokp;
	return (0);
}

static
int
token_slab_alloc(struct token_slab **tspp)
{
	struct token_slab *tsp;
	struct token *tokp;
	size_t alignment = (size_t)4096, slab_size = (size_t)4096;
	int error;

	tsp = malloc(sizeof(*tsp));
	if (tsp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*tsp), strerror(errno));
		return (-1);
	}

	tsp->ts_buf = NULL;
	error = posix_memalign(&tsp->ts_buf, alignment, slab_size);
	if (error) {
		assert(error > 0);
		assert(tsp->ts_buf == NULL);
		ERR("posix_memalign(alignment=%zu, size=%zu): %s",
		    alignment, slab_size, strerror(error));
		free(tsp);
		return (-2);
	}
	assert(tsp->ts_buf != NULL);

	TAILQ_INIT(&tsp->ts_tokens);
	tokp = (struct token *)tsp->ts_buf;
	tsp->ts_avail = (ssize_t)0;
	tsp->ts_total = (ssize_t)slab_size / (ssize_t)sizeof(*tokp);
	assert(tsp->ts_total > (ssize_t)0);
	while (tsp->ts_avail < tsp->ts_total) {
		token_init(tokp);
		tokp->tok_tsp = tsp;
		TAILQ_INSERT_TAIL(&tsp->ts_tokens, tokp, tok_tailq_entries[0]);
		tsp->ts_avail++;
		assert(tsp->ts_avail > (ssize_t)0);
		tokp++;
	}
	assert(tsp->ts_avail == tsp->ts_total);
	tokens_avail += tsp->ts_avail;
	assert(tokens_avail > (ssize_t)0);
	tokens_total += tsp->ts_total;
	assert(tokens_total > (ssize_t)0);
	TAILQ_INSERT_TAIL(&slabs, tsp, ts_tailq_entries[0]);
	slabs_count++;
	assert(slabs_count > (ssize_t)0);
	*tspp = tsp;
	return (0);
}

void
token_init(struct token *tokp)
{

	tokp->tok_from = (uint64_t)0;
	tokp->tok_to = (uint64_t)0;
	tokp->tok_refs = 0;
	tokp->tok_flags = 0;
	/* tokp->tok_tsp = NULL; */
	thread_head_init(&tokp->tok_wait_thrh);
}

void
token_uninit(struct token *tokp)
{

	tokp->tok_from = (uint64_t)0;
	tokp->tok_to = (uint64_t)0;
	assert(tokp->tok_refs == 0);
	assert(tokp->tok_flags == TOK_FL_INUSE);
	thread_head_uninit(&tokp->tok_wait_thrh);
}

void
token_free(struct token *tokp)
{
	struct token_slab *tsp;

	assert(token_module_initialized);
	tsp = (struct token_slab *)tokp->tok_tsp;
	assert(tsp != NULL);
	token_uninit(tokp);
	tokp->tok_flags &= ~TOK_FL_INUSE;
	assert(!tokp->tok_flags);
	PTHREAD_MUTEX_LOCK(&token_mutex);
	tokens_avail++;
	assert(tokens_avail > (ssize_t)0);
	tsp->ts_avail++;
	assert(tsp->ts_avail > (ssize_t)0);
	TAILQ_INSERT_HEAD(&tsp->ts_tokens, tokp, tok_tailq_entries[0]);
	if (tsp->ts_avail == tsp->ts_total)
		token_slab_free(tsp);
	PTHREAD_MUTEX_UNLOCK(&token_mutex);
}

static
void
token_slab_free(struct token_slab *tsp)
{

	assert(tsp->ts_buf != NULL);
	assert(tsp->ts_avail == tsp->ts_total);
	slabs_count--;
	assert(slabs_count >= (ssize_t)0);
	TAILQ_REMOVE(&slabs, tsp, ts_tailq_entries[0]);
	tokens_avail -= tsp->ts_avail;
	assert(tokens_avail >= (ssize_t)0);
	tokens_total -= tsp->ts_total;
	assert(tokens_total >= (ssize_t)0);
	free(tsp->ts_buf);
	free(tsp);
}

void
token_setup(struct token *tokp, uint64_t offset, uint32_t length,
    uint32_t boundary)
{

	assert(length > (uint32_t)0);
	tokp->tok_from = offset & ~((uint64_t)boundary - 1);
	tokp->tok_to = ((offset + length + boundary - 1) &
	    ~((uint64_t)boundary - 1)) - 1;
	assert(tokp->tok_refs == 0);
	assert(thread_head_empty(&tokp->tok_wait_thrh));
}

static
int
token_equal(struct token *atokp, struct token *btokp)
{

	if (atokp->tok_to < btokp->tok_from)
		return (-1);
	if (atokp->tok_from > btokp->tok_to)
		return (1);
	return (0);
}
RB_GENERATE_STATIC(token_rb_head, token, tok_rb_entries[0], token_equal)

int
token_acquire(struct thread *thrp, struct token_tree *toktp, struct token *tokp,
    int waitok)
{
	struct token *ntokp;
	int refs;

	PTHREAD_MUTEX_LOCK(&toktp->tt_mutex);
	for (;;) {
		ntokp = RB_FIND(token_rb_head, &toktp->tt_tokens, tokp);
		if (ntokp == NULL) {
			assert(thrp != NULL);
			tokp->tok_refs++;
			assert(tokp->tok_refs == 1);
			refs = tokp->tok_refs;
#ifndef NDEBUG
			ntokp =
#endif
			    RB_INSERT(token_rb_head, &toktp->tt_tokens, tokp);
			assert(ntokp == NULL);
			break;
		}
		if (ntokp == tokp) {
			assert(thrp == NULL);
			tokp->tok_refs++;
			assert(tokp->tok_refs > 1);
			refs = tokp->tok_refs;
			break;
		}
		if (!waitok) {
			refs = -1;
			break;
		}
		assert(thrp != NULL);
		thread_head_append(&ntokp->tok_wait_thrh, thrp, THRL_IDX_STD);
		thread_wait_mutex(thrp, &toktp->tt_mutex);
	}
	PTHREAD_MUTEX_UNLOCK(&toktp->tt_mutex);
	return (refs);
}

int
token_release(struct token_tree *toktp, struct token *tokp)
{
#ifndef NDEBUG
	struct token *ntokp;
#endif
	struct thread *thrp;
	int refs;

	PTHREAD_MUTEX_LOCK(&toktp->tt_mutex);
#ifndef NDEBUG
	ntokp =
#endif
	    RB_FIND(token_rb_head, &toktp->tt_tokens, tokp);
	assert(ntokp != NULL);
	assert(ntokp == tokp);
	tokp->tok_refs--;
	refs = tokp->tok_refs;
	assert(tokp->tok_refs >= 0);
	if (tokp->tok_refs > 0) {
		PTHREAD_MUTEX_UNLOCK(&toktp->tt_mutex);
		return (refs);
	}
#ifndef NDEBUG
	ntokp =
#endif
	    RB_REMOVE(token_rb_head, &toktp->tt_tokens, tokp);
	assert(ntokp == tokp);
	PTHREAD_MUTEX_UNLOCK(&toktp->tt_mutex);
	for (;;) {
		thrp = thread_head_first(&tokp->tok_wait_thrh);
		if (thrp == NULL)
			break;
		thread_head_remove(&tokp->tok_wait_thrh, thrp, THRL_IDX_STD);
		thread_wakeup(thrp);
	}
	return (refs);
}

int
token_tree_alloc(struct token_tree **toktpp)
{
	struct token_tree *toktp;
	int error;

	toktp = malloc(sizeof(*toktp));
	if (toktp == NULL) {
		ERR("malloc(%zu): %s", sizeof(*toktp), strerror(errno));
		return (-2);
	}
	error = pthread_mutex_init(&toktp->tt_mutex, NULL);
	if (error) {
		ERR("pthread_mutex_init(): %d", error);
		free(toktp);
		return (-3);
	}
	token_tree_init(toktp);
	*toktpp = toktp;
	return (0);
}

void
token_tree_init(struct token_tree *toktp)
{

	RB_INIT(&toktp->tt_tokens);
}

void
token_tree_uninit(struct token_tree *toktp)
{

#ifndef NDEBUG
	assert(RB_EMPTY(&toktp->tt_tokens));
#else
	(void)toktp;
#endif
}

void
token_tree_free(struct token_tree *toktp)
{

	token_tree_uninit(toktp);
	PTHREAD_MUTEX_DESTROY(&toktp->tt_mutex);
	free(toktp);
}
